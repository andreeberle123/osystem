/*******************************************************************************

  dorothy/kernel/conio.c
 
  Copyright (C) 2005 D-OZ Team

  Console I/O functions
 
*******************************************************************************/

#include "conio.h"
#include "defines.h"
#include "io.h"

static byte lines   = 25;
static byte columns = 80;
static byte color   = (BLACK << 4) | LIGHTGRAY; 

static unsigned char *video_memory = (unsigned char *) VIDEO_MEMORY;

void clrscr (void) {
	unsigned int i;

	for (i = 0; i < lines * columns; i++) {
		video_memory [i * 2] = 0x20;		// White space
		video_memory [i * 2 + 1] = color;
	}

	outportb (0x3D4, 0x0F);					// Cursor goes to (0, 0)
	outportb (0x3D5, 0x00);
	outportb (0x3D4, 0x0E);
	outportb (0x3D5, 0x00);
}

void textcolor (byte fg) {
	color = (color & 0xF0) | (fg & 0x0F);
}

void textbackground (byte bg) {
	color = (color & 0x0F) | (bg & 0xF0);
}

void gotoxy (byte x, byte y) {
	outportb (0x3D4, 0x0F);					// Cursor goes to (x, y)
	outportb (0x3D5, x);
	outportb (0x3D4, 0x0E);
	outportb (0x3D5, y);
}

void x_y(int * x, int *y) {
	outportb (0x3D4, 0xE);
	*x = inportb (0x3D5);
	outportb (0x3D4, 0x0F);
	*y = inportb (0x3D5);
}

void putch (char c) {
	unsigned int offset,
				 i;

	outportb (0x3D4, 0xE);
	offset = inportb (0x3D5);
	offset <<= 8;
	outportb (0x3D4, 0x0F);
	offset += inportb (0x3D5);
	offset <<= 1;

	switch (c) {
		case '\n':
			offset = (offset/(2 * columns)) * (2 * columns) + (2 * columns);
			break;
		case '\r':
			offset = (offset/(2 * columns)) * (2 * columns);
			break;
		case '\t':
			offset += 8;
			break;
		case 8:	// Delete
			offset -= 2;
			video_memory [offset] = 0x0;
			break;
		default:
			video_memory [offset++] = c;
			video_memory [offset++] = color;
			break;
	}

	if (offset >= 2 * columns * lines) {	// off-screen?
		for (i = 0; i < 2 * columns * (lines - 1); i++)  // scroll
			video_memory [i] = video_memory [i + 2 * columns];
		for (i = 0; i < columns; i++) {					 // and clean the bottom row
			video_memory [ (2 * columns * (lines - 1)) + (i * 2) ] = 0x20;
			video_memory [ (2 * columns * (lines - 1)) + (i * 2) + 1] = color;
		}
		offset -= 2 * columns;
	}

	offset >>= 1;
	outportb (0x3D4, 0x0F);					// Cursor goes to (x, y)
	outportb (0x3D5, offset & 0x0FF);
	outportb (0x3D4, 0x0E);
	outportb (0x3D5, offset >> 8);
}

void syscall_color_cursor_handler(int bg, int fg,int x,int y) {
	//textcolor(fg);
	//textbackground(bg);
	dword offset;
	outportb (0x3D4, 0xE);
	offset = inportb (0x3D5);
	offset <<= 8;
	outportb (0x3D4, 0x0F);
	offset += inportb (0x3D5);
	
	//offset <<= 1;
 	//kprintf("moving: %i, %i\n",-x,y);
	offset+=x;
	//offset >>= 1;
	outportb (0x3D4, 0x0F);					// Cursor goes to (x, y)
	outportb (0x3D5, offset & 0x0FF);
	outportb (0x3D4, 0x0E);
	outportb (0x3D5, offset >> 8);
}
