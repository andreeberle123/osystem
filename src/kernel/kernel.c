/*******************************************************************************

  dorothy/kernel/kernel.c

  Copyright (C) 2005 D-OZ Team

*******************************************************************************/

#include "conio.h"
#include "stdio.h"
#include "8259a.h"
#include "panic.h"
#include "idt.h"
#include "gdt.h"
#include "timer.h"
#include "keyboard.h"
#include "kish.h"
#include "ne2k.h"
#include "floppy.h"
#include "mem.h"
#include "pages.h"
#include "ffs.h"
#include "event.h"
#include "sched.h"
#include "net/dlink/eth.h"
#include "loader.h"
#include "io.h"
#include "syscall.h"
#include "iosched.h"
#include "ide.h"
#include "i386/pci.h"
#include "pci.h"
#include "nic.h"
#include "sys/fs.h"

//#include "tfs.h"

unsigned char KERNEL_STACK[8192];
void _start(void);
void main();

unsigned char *eth_mac;

/* global vars for event.h */
volatile EVENT *_evt = NULL;
unsigned int volatile _number_of_evt = 0;
unsigned int volatile fifo_number = 0;
unsigned int volatile semaphore_number = 0;
/* global vars for event.h */

/* NIC */

/* NIC */

/* global vars for timer.h*/
unsigned long delaycount = 0;
/* global vars for timer.h*/

extern _pci_bios_entry *pci_bios_entry;

desc_table(GDT, 5)
{
	{dummy:0},
	stnd_desc(0, 0xFFFFF, (D_CODE + D_READ + D_BIG + D_BIG_LIM + D_DPL0)),
	stnd_desc(0, 0xFFFFF, (D_DATA + D_WRITE + D_BIG + D_BIG_LIM + D_DPL0)),
	stnd_desc(0, 0xFFFFF, (D_CODE + D_READ + D_BIG + D_BIG_LIM + D_DPL3)),
	stnd_desc(0, 0xFFFFF, (D_DATA + D_WRITE + D_BIG + D_BIG_LIM + D_DPL3))
};


struct
{
	unsigned short limit __attribute__ ((packed));
	union DT_entry *idt __attribute__ ((packed));
} loadgdt = { (3 * sizeof(union DT_entry) - 1), GDT };

/*unsigned int pqp = 0;
void write(unsigned int num) {
	unsigned char *video_memory = (unsigned char *) 0xB8000;
	unsigned int temp = num;
	unsigned int b  = 1;
	while ( temp >= 10 ) {
		b*=10;
		temp /= 10;
	}
	while ( b > 0 ) {
		
        	video_memory[pqp++] = (num/b) + 0x30;
		video_memory[pqp++] = 0x05;
		num = num % b;
		b /= 10;

	}
        video_memory[pqp++] = 0x20;
	video_memory[pqp++] = 0x05;
}*/

/* Startup kernel sequence (after system init). We currently read a init
	file with loading info. (namely /system/startup). The file must be on 
	FAT for proper s.o. utilization.
	Maxing, for now, the binary name of the first executable to 512 bytes.
*/

/* Put the path as a static memory array. Dynamic can cause some leaks here */
char _startup_buf_path[512];

char * startup_file = "/system/startup";

void startup_sequence() {
	
	FILE * f = fopen(startup_file,"r");
	
	/* binary name should fit in 512 bytes, if not, then the file is invalid */
	int read = fread(_startup_buf_path, 512, 1, f);
	if (read == 512) {
		kprintf("Error in start file...");
		return; /* we are going down to hlt here */
	}

	/* finish the string */
	if (_startup_buf_path[read-1] == '\n')
		_startup_buf_path[read-1] = 0;
	else
		_startup_buf_path[read] = 0;	
	executable_loader(_startup_buf_path,0,1);

}

void kmain (void) {
	//unsigned long delaycount = 0;
	asm ("pushl $2; popf");		/* Zero the flags		*/

	 asm volatile                /* Load the GDT and the kernel stack     */
	 (                           /* We assume the loader has cleared ints */
      		"lgdtl (loadgdt)     \n" /* Load our own GDT                      */
	        "movw $0x10,%%ax     \n" /* Init data registers with flat_data    */
      		"movw %%ax,%%ds      \n" // ?
      		"movw %%ax,%%es      \n"
      		"movw %%ax,%%fs      \n"
      		"movw %%ax,%%gs      \n"
      		"movw %%ax,%%ss      \n" /* ... and the stack, too                */
      		"movl $0xFFFF,%%esp       \n" /* Set the SP to the end of the stack    */
      		"ljmp $0x08,$next    \n" /* Flush prefetch queue and skip MBHeadr */
		"nop\n"
		"nop\n"
	        "next:               \n" /* Continue here                         */
      		:
      		: "r" (GDT), "p" (KERNEL_STACK+4096)
      		: "%eax"
   	);
	
	INTS (false);
	clrscr ();

	kprints ("D-OZ 0.0.1 (dorothy)\n\n");

	/* remap PIC IRQs to cover software interrupts 0x20-0x30 */
	remapPIC (0x20, 0x28);
	/* and mask the IRQs so they don't make a mess */
	maskIRQ (ALL);

	kprints ("	8259a PIC remapped.\n");

	/* load exceptions to treat faults */
	loadExceptions ();
	kprints ("	Exceptions loaded.\n");

	/* starting timer interrupt */
	loadTimer ();
	kprints ("	Timer interrupt loaded.\n");

	/* points to our IDT */
	loadIDTR ();
	kprints ("	IDT initialized.\n");

	initKeyboard ();
	kprints ("	Keyboard initialized.\n");

	/* interrupts are back! */
	INTS (true);

	/* calibrating... */

	kprints ("	Calibrating delay loop... ");
	delaycount = calibrate_delay ();
	kprints ("done.\n");
	kprintf ("		Delay count for 1 ms is %d.\n", delaycount);

	/* counting memory available */
/// this doesn't work in BOB's 386, so the memory is 2MB
//	mem_end = 1024*1024*2;
	count_memory();
	kprintf("	Memory %dKBytes\n", mem_end/1024);

	/* initialing memory queue to permit allocation */

	kprintf("	Initializing memory pages");
	InitPages();

	/* Just a mm test - kprintf("memory address %d\n", malloc(100)); */
	//testMemory();

	kprintf("	Initializing Event Queue: TIME, SEMA, FIFO...\n");

	init_event_queue();

	/* floppy */

	init_floppy ();
	set_highest_priority (6, 8);
	kprints ("	Floppy initialized.\n");

	/* NIC start */

	ne2k_init (0x300, 3);
//	eth_mac = eth_get_MAC ();
//	kprintf ("	NE2K initialized. MAC is %2X:%2X:%2X:%2X:%2X:%2X\n", eth_mac[0],
//	                                                           eth_mac[1],
//	                                                           eth_mac[2],
//	                                                           eth_mac[3],
//	                                                           eth_mac[4],
//	                                                           eth_mac[5]);

	/* TFS Load*/
//	if (init_tfs()) kprints("	TFS initialized.\n");

//	if (load_fat()) kprints("	FFS initialized.\n");

	pci_bios_init();
	pci_init();
	pci_irq_init();

	init_nic();

	//kprintf("	PCI initialized.\n");

	initializeFileSystem();
	kprintf("	Ext2 FS initialized.\n");

	init_scheduler();
	kprints ("	Scheduler initialized.\n");

//	init_io_scheduler();
//	kprints ("	I/O scheduler initialized.\n");

	init_syscall();
	kprints ("	Syscalls initialized.\n");

	init_fs_syscalls();
	kprints ("	FS Syscalls initialized.\n");

	/*init_ide();
	kprints ("	Ide activated.\n");*/

	//kprints ("\n	Please press any key to start KISH.");
	kprints ("\n	Please press any key to proceed...");
	getch ();

	clrscr ();

	//kish ();
	//load_kish();
	
	//executable_loader("pshell");
	startup_sequence();
}


