;*******************************************************************************
;
; dorothy/kernel/irqwrapper.asm
;
; Copyright (C) 2005 D-OZ Team
;
; IRQs wrapper
;
;*******************************************************************************

; Interrupts:
;
;Number		Description											Type
;0			Divide-by-zero										fault
;1			Debug exception	trap or 							fault
;2			Non-Maskable Interrupt (NMI)						trap
;3			Breakpoint (INT 3)									trap
;4			Overflow (INTO with EFlags[OF] set)					trap
;5			Bound exception (BOUND on out-of-bounds access)		trap
;6			Invalid Opcode										trap
;7			FPU not available									trap
;8*			Double Fault										abort
;9			Coprocessor Segment Overrun							abort
;10*		Invalid TSS											fault
;11*		Segment not present									fault
;12*		Stack exception										fault
;13*		General Protection									fault/trap
;14*		Page fault											fault
;15			Reserved
;16			Floating-point error								fault
;17			Alignment Check										fault
;18			Machine Check										abort
;19-31		Reserved By Intel
;32-255		Available for Software and Hardware Interrupts
;
;
; Interrupts from  0 to 18 for processor exceptions
; Interrupts from 19 to 31 reserved by Intel
; Interrupts from 32 to 47 for hardware
; Interrupts above 49 for software
;
; IRQs from 8259a PIC are mapped starting in 32:
;
; 32 - IRQ 0
; 33 - IRQ 1
; ...
; 40 - IRQ 8 	RTC (real time clock) - called 18.2 per second

pr:
DB '%'
DB 'd'
DB ' '
DB 0x00


[global intnull]
intnull:
     iret

[extern int_00]
[global int00]
int00:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_00     ; Divide by Zero #DE
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

;[extern int_01]
;[global int01]
;int01:
;     pusha
;     push ds
;     push es
;     push fs
;     push gs
;     mov eax,0x10    ; Data segment
;     mov ds,eax
;     mov es,eax
;     cld
;     call int_01    ; Debug #DB
;     pop gs
;     pop fs
;     pop es
;     pop ds
;     popa
;     iret

[extern int_02]
[global int02]
int02:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_02     ; NMI interrupt #--
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

;[extern int_03]
;[global int03]
;int03:
;     pusha
;     push ds
;     push es
;     push fs
;     push gs
;     mov eax,0x10    ; Data segment
;     mov ds,eax
;     mov es,eax
;     cld
;     call int_03    ; Breakpoint #BP
;     pop gs
;     pop fs
;     pop es
;     pop ds
;     popa
;     iret

[extern int_04]
[global int04]
int04:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_04     ; Overflow #OF
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern int_05]
[global int05]
int05:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_05     ; Bound Range Exception #BR
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern int_06]
[global int06]
int06:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_06     ; Invalid Opcode #UD
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern int_07]
[global int07]
int07:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_07     ; Device Not Available #NM
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern int_08]
[global int08]
int08:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_08     ; Double Fault #DF
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret


[extern int_09]
[global int09]
int09:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_09     ; Coprocessor Segment Overrun
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern int_10]
[global int10]
int10:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_10     ; Invalid Task State Segment (TSS) #TS
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern int_11]
[global int11]
int11:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_11     ; Segment Not Present #NP
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern int_12]
[global int12]
int12:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_12     ; Stack Segment Fault #SS
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern int_13]
[global int13]
int13:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_13     ; General Protection Fault (GPF) #GP
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret
[extern kprinth]
;[extern int_14]
[extern handle_page_fault]
[global int14]
int14:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
	 push ebp
	 mov eax,esp
	 mov esp, 0x4FFF	; set the stack to 64kb, so we can enter kernel stack space
						; NOTE this wont work if we have a kernel process
						; TODO This is error prone in multiprocess environment,
						; specially if 2 processes call a syscall or page fault at the same time
	 push eax
	 push dword [eax+48]
	 mov eax,cr2
	 push eax
     cld
     call handle_page_fault     ; Page Fault #PF
	 add esp,8
	 pop esp
     pop ebp
     pop gs
     pop fs
     pop es
     pop ds
     popa
	 add esp,4		; Patched it here. At page fault the cpu pushes an error code at the stack, we consume it here
     iret

; Exception 15 is an Intel Reserved Interrupt

[extern int_16]
[global int16]
int16:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_16     ; x87 Floating-Point Error #MF
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern int_17]
[global int17]
int17:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_17     ; Alignment Check #AC
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern int_18]
[global int18]
int18:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_18     ; Machine Check #MC
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern int_19]
[global int19]
int19:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_19     ; SIMD Floating-Point Exception #XF
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

;; Exceptions 20-31 are Intel Reserved Interrupts

; E n d   o f   E x c e p t i o n s   I n t e r r u p t s

;[extern int_calibrate]
;[global intcalibrate]
;intcalibrate:
;     pusha
;     push ds
;     push es
;     push fs
;     push gs
;     mov eax,0x10    ; Data segment
;     mov ds,eax
;     mov es,eax
;     cld
;     call int_calibrate	; Timer Interrupt Handler
;     pop gs
;     pop fs
;     pop es
;     pop ds
;     popa
;     iret

[extern _schedflag]

[extern int_timer]
[global inttimer]
inttimer:
     ;cli
	 ;force the stack into kernel space
     push eax
	 mov eax,esp
	 mov esp,0xEFFFF   ; i am placing the timer stack at 960kb region for now TODO revisit all stack movements
						; this address is temporary and a better stack layout must be done *urgently*
	 push dword [eax+0xc]
	 push dword [eax+0x8]
	 push dword [eax+0x4]
;push dword 0x04
;push dword 0x04
;push dword 0x04
	 push eax
	 
     pusha
     push ds
     push es
     push fs
     push gs

     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     push esp	; pass esp as argument
     cld
     call int_timer	 ; Timer Interrupt Handler

     add esp,4

     pop gs
     pop fs
     pop es
     pop ds
     popa
	 pop esp
     pop eax
     ;sti
	 iret
     

[extern floppy_irq]
[global floppyirq]
floppyirq:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call floppy_irq ; Floppy IRQ6 Interrupt Handler
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

;[extern int1c]
;[global int1C]
;int1C:
;     pusha
;     push ds
;     push es
;     push fs
;     push gs
;     mov eax,0x10    ; Data segment
;     mov ds,eax
;     mov es,eax
;     cld
;     call int1c		; Timer Interrupt Handler
;     pop gs
;     pop fs
;     pop es
;     pop ds
;     popa
;     iret

[extern int_kbd]
[global intkbd]
intkbd:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_kbd	; Timer Interrupt Handler
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern int_ide]
[global intide]
intide:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call int_ide	; Timer Interrupt Handler
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret


[extern int_syscall]
[global intsyscall]
intsyscall:
		; eax is clobbered!
     ;pusha
	 push ebx
	 push ecx
	 push edx
	 push esi
	 push edi
     push ds
     push es
     push fs
     push gs
	 push eax		; store eax, do not lose it
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
	 pop eax
	 push esp
	 ;push ebp
	 ;mov eax,esp
	 ;mov esp, 0xFFFF	; set the stack to 64kb, so we can enter kernel stack space
						; NOTE this wont work if we have a kernel process
	 ;push eax
	 ;mov eax,[eax+4]

     cld
     call int_syscall	; Timer Interrupt Handler
;	 pop esp
;	 pop ebp
	 add esp,4
     pop gs
     pop fs
     pop es
     pop ds
	 pop edi
	 pop esi
	 pop edx
	 pop ecx
	 pop ebx
     ;popa
     iret

;[global _reboot]
;reboot:
;     xor ax, ax
;     push ax
;     mov al, 0x40
;     mov ds, ax
;     mov ax, 0x1234
;     xor bx, bx
;     mov bh, 0x72
;     mov [bx], ax
;     pop ax
;     ;int 0x16                                      ; ler tecla teclado...
;     jmp 0xffff:0000

;ne2k interrupt wrapper
[extern ne2k_irq]
[extern ne2k_imr]
[global ne2kirq]
ne2kirq:
	cli
	pusha
	push di
	push si
	push ds
	push es
	push bp
	mov al, 0xbc
	out 0x21, al
	sti
	
	cld
	call ne2k_irq
	
	cli
	mov al, 0xb4
	out 0x21, al
	mov al, 0x63
	out 0x20, al
	sti

	cld
	call ne2k_imr
	
	pop bp
	pop es
	pop ds
	pop si
	pop di
	popa
	iret
[extern pcnet32_interrupt]
[global pcnetirq]
pcnetirq:
	cli
	pusha
	push di
	push si
	push ds
	push es
	push bp
	
	cld
	call pcnet32_interrupt
			
	pop bp
	pop es
	pop ds
	pop si
	pop di
	popa
	iret
[extern int_network]
[global int_net_internal]
int_net_internal:
	cli
	pusha
	push di
	push si
	push ds
	push es
	push bp

	cld
	call int_network 
			
	pop bp
	pop es
	pop ds
	pop si
	pop di
	popa
	sti
	iret
