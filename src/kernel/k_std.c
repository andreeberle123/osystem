
#include "k_std.h"


// Procura pela substring dentro da string
int k_strstr(char *string, char *substring)
{
    char *a, *b;

    b = substring;
    if (*b == 0) return 1; //Se a substring for vazia retorna 1

    for ( ; *string != 0; string+=1) // Percorre a string
    {
        if (*string != *b) continue; /* Compara o primeiro char da substring com os chars da string, quando encontra algum igual executa as linhas seguintes */
        a = string;
        while (1) // Loop que compara char por char da string e substring enquanto tem sucesso
        {
            if (*b == 0) return 1; // Retorna 1 quando substring termina de comparar com sucesso
            if (*a++ != *b++) break; // Sai do loop quando comparacao falha
        }
        b = substring; // b volta para o comeco da substring
    }
    return 0; // retorna 0 quando a string termina sem sucesso
}

void  k_itoa(int valor, char *string)
{
    char temp[11];    /* inteiro vai de -2147483648 a 2147483647. Assim, 11 � o
             * maior tamanho poss�vel do vetor de carateres.
                     * Esse vetor de caracteres guarda o numero ao contrario.
                     */
    char *ponteiro = temp;// ponteiro que percorre o vetor de caracteres
    char *ponteiro_s;     /* ponteiro que percorre a string passada como parametro (e guarda
             * o numero na ordem certa)
             */
    int i;      
    unsigned val;        // valor manipulado durante a interpretacao do inteiro
    int sinal = 0;    // verifica se o inteiro que entra como parametro tem sinal

    if(valor < 0) 
    {
        sinal = 1;    // ativa o sinal
        val = -valor; // transforma o valor com sinal passado como parametro em positivo
    }
    else
        val = (unsigned)valor; // recebe o valor sem sinal passado como parametro

    if (val == 0)
    {
        *ponteiro = '0';
        ponteiro++;
    }
    else
        while (val > 0) /* enquanto ainda tivermos d�gitos a pegar, ou seja, enquanto o valor da
             * divis�o for maior do que zero.
              */
        {
            i = val % 10;    // valor que vai para o vetor de sa�da
            val = val / 10;
            *ponteiro = i + '0';// armazena seu valor em ASCII
            ponteiro++;
        }

    ponteiro_s = string; // para modificarmos o vetor de entrada que ser� retornado

    if(sinal)           // se houver sinal, coloca-se o caracter "-" antes   
    {
        *ponteiro_s = '-';
        ponteiro_s++;
    }

    while(ponteiro > temp) /* enquanto o endere�o do ponteiro for maior que o endere�o da cabe�a do                   *vetor tempor�rio
              */
    {
        ponteiro--;  
        *ponteiro_s = *ponteiro; // passa o vetor temporario para a ordem correta
        ponteiro_s++;
    }
 
    *ponteiro_s = 0; // joga nulo na �ltima posi��o

}

/* retorna o base elavado a pot */
int k_pow(int base, int pot)
{
	int cont = pot;
	int ret = base;

	if (pot < 0)		// se potencia < 0, retorna -1 (de erro)
		return -1;
	else if (pot == 0)	// qualquer coisa elevado a zero = 1
		return 1;
	else
	{
		while (cont > 1)	// calculo do valor
		{
			ret *= base;
			cont--;
		}
		return ret;		// retorno
	}

}




/************************************************************************
 *
 *    int k_atoi (char *)
 *
 *    Convert ascii string to integer...
 *
 */

int k_atoi(char *inteiro)
{
  
    int n;        /* Valor que ser� retornado como inteiro */
    int sign;    /* Guarda o sinal do inteiro */

    n = 0;
    sign = 0;

    /* Efetua as verifica��es iniciais */
    for(;;inteiro++) {
        switch(*inteiro) {
        case ' ':
        case '\t':
            continue;
        case '-':
            sign++;
        case '+':
            inteiro++;
        }
        break;
    }

    /* Verifica na faixa num�rica */
    while(*inteiro >= '0' && *inteiro <= '9')
        /* Adicina o n�mero na casa decimal certa */
        n = n*10 + (*inteiro++ - '0');

    /* Retorna com o sinal certo */
    return(sign? -n: n);

}





/* copia a string de src para dest */
void k_strcpy(char *dest, char *src)
{
	while (*src != 0)	// percorre src e coloca em dest
	{
		*dest = *src;
		dest++;
		src++;
	}
}




