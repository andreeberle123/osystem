/*******************************************************************************

  dorothy/kernel/sched.c

  Copyright (C) 2005 D-OZ Team

  Scheduler

*******************************************************************************/

#include "defines.h"
#include "timer.h"
#include "stdio.h"
#include "io.h"
#include "8259a.h"
#include "process.h"
#include "mem.h"

#include "pages.h"

static volatile struct task_struct *current = NULL;


char schedflag = 0;


unsigned char exec_debug = 0;
char tmp[500];

void cswitch(int clockesp) {

	int old_esp;


	struct task_struct *next;

	//kprintf("scheduling %i\n",current);
	asm ("cli");


	if (current != NULL) {

		asm ("movl %%esp, %0" : "=r" (old_esp));

		asm ("movl %0, %%esp " : : "r" (clockesp));

		asm ("movl (%%esp), %0" : "=r" (current->gs));
		asm ("movl 4(%%esp), %0" : "=r" (current->fs));
		asm ("movl 8(%%esp), %0" : "=r" (current->es));
		asm ("movl 12(%%esp), %0" : "=r" (current->ds));

		/* AX, CX, DX, BX, original SP, BP, SI, and DI -> 32 bytes
		 * in the following order:
		 *
		 * push eax
		 * push ecx
		 * push edx
		 * push ebx
		 * push esp
		 * push ebp
		 * push esi
		 * push edi
		 * */
		asm ("movl 16(%%esp), %0" : "=r" (current->edi));
		asm ("movl 20(%%esp), %0" : "=r" (current->esi));
		asm ("movl 24(%%esp), %0" : "=r" (current->ebp));
		/* seems that popa ignore eSP after all */
		//asm ("movl 28(%%esp), %0" : "=r" (current->esp));
		asm ("movl 32(%%esp), %0" : "=r" (current->ebx));
		asm ("movl 36(%%esp), %0" : "=r" (current->edx));
		asm ("movl 40(%%esp), %0" : "=r" (current->ecx));
		asm ("movl 40(%%esp), %0" : "=r" (current->ecx));
		asm ("movl 44(%%esp), %0" : "=r" (current->eax));

		asm ("movl 48(%%esp), %0" : "=r" (current->esp));
		asm ("movl 52(%%esp), %0" : "=r" (current->eip));
		asm ("movl 56(%%esp), %0" : "=r" (current->cs));
		asm ("movl 60(%%esp), %0" : "=r" (current->flags));

		asm ("movl %0, %%esp" : : "r" (old_esp));
kprintf("eip1 is at %X\n",current->eip);
	}

	/* Switching -> changing from a current to other one
	 *
	 * put current into the process queue (ready to run)
	 * get the next ready to run process
	 * */

	next = get_next_ready_to_run();

	//kprintf("next = %i\n",next);
	/* alterei pra nao alternar com a lista quando next = null */

	if (next != NULL) {
		
		/* retira o processo a ser chaveado da lista de espera */
		remove_process(next);

		/* verifica o estado do processo atual */

		switch (current->status) {
		case RUNNING:
			current->status = READY_TO_RUN;
			add_process((struct task_struct*) current);
			break;
		case READY_TO_RUN:
			/* essa situa��o n�o deve ocorrer normalmente */
		//	kprintf("erro no scheduler");

			break;
		case WAITING:
			/* processo est� em espera, nesse momento ele n�o volta pra fila
			o que ser� feito pelo escalonador de E/S mais tarde ou alguma int */

			break;
		case FINISHED:
			/* processo terminou, apenas destr�i o n� */

			//kfree(current->stack); arrumar
			kfree((void *)current->exe);
			kfree((void *)current); // talvez passar para uma fila de mortos

			/* TODO we must destroy the page directory now as well */
			break;
		}


		current = next;
		current->status = RUNNING;

		asm ("movl %%esp, %0" : "=r" (old_esp));

		asm ("movl %0, %%esp " : : "r" (clockesp));

		asm ("movl %0, (%%esp)"   : : "r" (current->gs));
		asm ("movl %0, 4(%%esp)"  : : "r" (current->fs));
		asm ("movl %0, 8(%%esp)"  : : "r" (current->es));
		asm ("movl %0, 12(%%esp)" : : "r" (current->ds));

		asm ("movl %0, 16(%%esp)" : : "r" (current->edi));
		asm ("movl %0, 20(%%esp)" : : "r" (current->esi));
		asm ("movl %0, 24(%%esp)" : : "r" (current->ebp));
		//asm ("movl %0, 28(%%esp)" : : "r" (current->esp));
		asm ("movl %0, 32(%%esp)" : : "r" (current->ebx));
		asm ("movl %0, 36(%%esp)" : : "r" (current->edx));
		asm ("movl %0, 40(%%esp)" : : "r" (current->ecx));
		asm ("movl %0, 44(%%esp)" : : "r" (current->eax));

		asm ("movl %0, 48(%%esp)" : : "r" (current->esp-0xC));

		use_pd((unsigned long)current->pd);

		/* changed it here, now we set up the return stack right above the next process
			current stack, and let the wrapper set the esp correctly at interrupt end 
		*/
		asm ("movl %0, %%esp " : : "r" (current->esp-0xC));
		asm ("movl %0, (%%esp)" : : "r" (current->eip));
		asm ("movl %0, 4(%%esp)" : : "r" (current->cs));
		asm ("movl %0, 8(%%esp)" : : "r" (current->flags));

		asm ("movl %0, %%esp" : : "r" (old_esp));

		
		
	} /*else {
		kprintf("Next is NULL\n");
	}*/

//	schedflag = 0;
	
}

void sheduler (int esp) {
	//kprintf("scheduler... %d\n", tick++);
	//tick++;

	/* checking up the event queue */

	/* checking up the process queue */

	/*if (exec_debug) {
		kprintf("sched ");
	}*/

	cswitch(esp);

	addTrigger ((void (*) (void *))sheduler, 10,NULL);

	//asm ("sti");
	
}

void init_scheduler (void) {

	addTrigger ((void (*) (void *))sheduler, 10,NULL);
}

void set_current_process(struct task_struct * ts) {
	/* seta o process ts como o atual, utilizado para carregar
	o primeiro processo somente */

	asm("cli");
	current = ts;
	current->status = RUNNING;
	asm("sti");
}

struct task_struct * get_current_process() {
	struct task_struct * pts = (struct task_struct *)current;
	return pts;
}
