#include "stdio.h"
#include "process.h"
#include "string.h"
#include "sched.h"

float current_load = 0;

extern unsigned long delaycount;

void print_top(struct task_struct * node) {
	
	char tmp[256];
	int i;

	ksprintf(tmp," %d",node->ppid);
	kprintf(tmp);

	for(i=0;i<(6-kstrlen(tmp));i++)
		kprintf(" ");

	ksprintf(tmp,"%d",node->pid);
	kprintf(tmp);

	for(i=0;i<(5-kstrlen(tmp));i++)
		kprintf(" ");

	switch (node->status) {
	case WAITING:
		ksprintf(tmp,"WAITING");
		kprintf(tmp);
		break;
	case RUNNING:
		ksprintf(tmp,"RUNNING");
		kprintf(tmp);
		break;
	case READY_TO_RUN:
		ksprintf(tmp,"READY_TO_RUN");
		kprintf(tmp);
		break;
	case FINISHED:
		ksprintf(tmp,"FINISHED");
		kprintf(tmp);
		break;
	}

	for(i=0;i<(13-kstrlen(tmp));i++)
		kprintf(" ");

	kprintf("%s",node->name);
	

	for(i=0;i<(10-kstrlen((const char *)(node->name)));i++)
		kprintf(" ");

	ksprintf(tmp,"%d",node->start);
	kprintf(tmp);

	for(i=0;i<(8-kstrlen(tmp));i++)
		kprintf(" ");

	ksprintf(tmp,"%d",node->bin_size);
	kprintf(tmp);

	for(i=0;i<(14-kstrlen(tmp));i++)
		kprintf(" ");

	ksprintf(tmp,"%d",STACK_SIZE);
	kprintf(tmp);

	for(i=0;i<(5-kstrlen(tmp));i++)
		kprintf(" ");

	kprintf("\n");

}

void ktop() {

	struct task_struct * current = get_current_process();
	struct task_struct * node = getPlist();

	float av_load;
	float weight = 0.1;
	float ints_per_sec;

	asm("cli");

	ints_per_sec = 10 /delaycount ;

	av_load = count_process();
	av_load *= ints_per_sec;


	av_load = current_load * weight + av_load * (1 - weight);
	current_load = av_load;

	kprintf("\n Informacoes sobre os processos.\n");
	kprintf("Tempo media de carga: %d ms\n",av_load);

	kprintf(" ppid pid  status       name      inicio  memoria exec  tam. pilha\n");

	

	print_top(current);

	while (node) {
		print_top(node);
		node = node->next;
	}

	
	asm("sti");

}
