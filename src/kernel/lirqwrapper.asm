;*******************************************************************************
;
; dorothy/kernel/irqwrapper.asm
;
; Copyright (C) 2005 D-OZ Team
;
; IRQs wrapper
;
;*******************************************************************************

; Interrupts:
;
;Number		Description											Type
;0			Divide-by-zero										fault
;1			Debug exception	trap or 							fault
;2			Non-Maskable Interrupt (NMI)						trap
;3			Breakpoint (INT 3)									trap
;4			Overflow (INTO with EFlags[OF] set)					trap
;5			Bound exception (BOUND on out-of-bounds access)		trap
;6			Invalid Opcode										trap
;7			FPU not available									trap
;8*			Double Fault										abort
;9			Coprocessor Segment Overrun							abort
;10*		Invalid TSS											fault
;11*		Segment not present									fault
;12*		Stack exception										fault
;13*		General Protection									fault/trap
;14*		Page fault											fault
;15			Reserved
;16			Floating-point error								fault
;17			Alignment Check										fault
;18			Machine Check										abort
;19-31		Reserved By Intel
;32-255		Available for Software and Hardware Interrupts
;
;
; Interrupts from  0 to 18 for processor exceptions
; Interrupts from 19 to 31 reserved by Intel
; Interrupts from 32 to 47 for hardware
; Interrupts above 49 for software
;
; IRQs from 8259a PIC are mapped starting in 32:
;
; 32 - IRQ 0
; 33 - IRQ 1
; ...
; 40 - IRQ 8 	RTC (real time clock) - called 18.2 per second

pr:
DB '%'
DB 'd'
DB ' '
DB 0x00


[global _intnull]
_intnull:
     iret

[extern _int_00]
[global _int00]
_int00:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_00     ; Divide by Zero #DE
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

;[extern _int_01]
;[global _int01]
;int01:
;     pusha
;     push ds
;     push es
;     push fs
;     push gs
;     mov eax,0x10    ; Data segment
;     mov ds,eax
;     mov es,eax
;     cld
;     call int_01    ; Debug #DB
;     pop gs
;     pop fs
;     pop es
;     pop ds
;     popa
;     iret

[extern _int_02]
[global _int02]
_int02:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_02     ; NMI interrupt #--
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

;[extern _int_03]
;[global _int03]
;int03:
;     pusha
;     push ds
;     push es
;     push fs
;     push gs
;     mov eax,0x10    ; Data segment
;     mov ds,eax
;     mov es,eax
;     cld
;     call int_03    ; Breakpoint #BP
;     pop gs
;     pop fs
;     pop es
;     pop ds
;     popa
;     iret

[extern _int_04]
[global _int04]
_int04:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_04     ; Overflow #OF
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern _int_05]
[global _int05]
_int05:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_05     ; Bound Range Exception #BR
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern _int_06]
[global _int06]
_int06:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_06     ; Invalid Opcode #UD
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern _int_07]
[global _int07]
_int07:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_07     ; Device Not Available #NM
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern _int_08]
[global _int08]
_int08:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_08     ; Double Fault #DF
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret


[extern _int_09]
[global _int09]
_int09:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_09     ; Coprocessor Segment Overrun
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern _int_10]
[global _int10]
_int10:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_10     ; Invalid Task State Segment (TSS) #TS
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern _int_11]
[global _int11]
_int11:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_11     ; Segment Not Present #NP
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern _int_12]
[global _int12]
_int12:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_12     ; Stack Segment Fault #SS
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern _int_13]
[global _int13]
_int13:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_13     ; General Protection Fault (GPF) #GP
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern _int_14]
[global _int14]
_int14:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     
     call _int_14     ; Page Fault #PF
     
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

; Exception 15 is an Intel Reserved Interrupt

[extern _int_16]
[global _int16]
_int16:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_16     ; x87 Floating-Point Error #MF
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern _int_17]
[global _int17]
_int17:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_17     ; Alignment Check #AC
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern _int_18]
[global _int18]
_int18:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_18     ; Machine Check #MC
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern _int_19]
[global _int19]
_int19:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_19     ; SIMD Floating-Point Exception #XF
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

;; Exceptions 20-31 are Intel Reserved Interrupts

; E n d   o f   E x c e p t i o n s   I n t e r r u p t s

;[extern _int_calibrate]
;[global _intcalibrate]
;intcalibrate:
;     pusha
;     push ds
;     push es
;     push fs
;     push gs
;     mov eax,0x10    ; Data segment
;     mov ds,eax
;     mov es,eax
;     cld
;     call int_calibrate	; Timer Interrupt Handler
;     pop gs
;     pop fs
;     pop es
;     pop ds
;     popa
;     iret


[extern _schedflag]

[extern _int_timer]
[global _inttimer]
_inttimer:
     ;cli
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     
     push esp     
     cld
     call _int_timer	 ; Timer Interrupt Handler
     add esp,4
     pop gs
     pop fs
     pop es
     pop ds
     popa
     ;sti
     iret

[extern _floppy_irq]
[global _floppyirq]
_floppyirq:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _floppy_irq ; Floppy IRQ6 Interrupt Handler
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

;[extern _int1c]
;[global _int1C]
;int1C:
;     pusha
;     push ds
;     push es
;     push fs
;     push gs
;     mov eax,0x10    ; Data segment
;     mov ds,eax
;     mov es,eax
;     cld
;     call int1c		; Timer Interrupt Handler
;     pop gs
;     pop fs
;     pop es
;     pop ds
;     popa
;     iret

[extern _int_kbd]
[global _intkbd]
_intkbd:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_kbd	; Timer Interrupt Handler
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern _int_ide]
[global _intide]
_intide:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     cld
     call _int_ide	; Timer Interrupt Handler
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

[extern _int_syscall]
[global _intsyscall]
_intsyscall:
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10    ; Data segment
     mov ds,eax
     mov es,eax
     push esp
     cld
     call _int_syscall	; Timer Interrupt Handler
     add esp,4
     pop gs
     pop fs
     pop es
     pop ds
     popa
     iret

;[global _reboot]
;reboot:
;     xor ax, ax
;     push ax
;     mov al, 0x40
;     mov ds, ax
;     mov ax, 0x1234
;     xor bx, bx
;     mov bh, 0x72
;     mov [bx], ax
;     pop ax
;     ;int 0x16                                      ; ler tecla teclado...
;     jmp 0xffff:0000

;ne2k interrupt wrapper
[extern _ne2k_irq]
[extern _ne2k_imr]
[global _ne2kirq]
_ne2kirq:
	cli
	pusha
	push di
	push si
	push ds
	push es
	push bp
	mov al, 0xbc
	out 0x21, al
	sti
	
	cld
	call _ne2k_irq
	
	cli
	mov al, 0xb4
	out 0x21, al
	mov al, 0x63
	out 0x20, al
	sti

	cld
	call _ne2k_imr
	
	pop bp
	pop es
	pop ds
	pop si
	pop di
	popa
	iret

[extern _int_network]
[global _int_net_internal]
_int_net_internal:
     cli
     pusha
     push ds
     push es
     push fs
     push gs
     mov eax,0x10
     mov ds,eax
     mov es,eax
     cld
     call _int_network
     pop gs
     pop fs
     pop es
     pop ds
     popa
     std
     iret