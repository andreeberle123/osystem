/*******************************************************************************

  dorothy/kernel/stdlib.c
 
  Copyright (C) 2005 D-OZ Team

  Standard library functions
 
*******************************************************************************/

#include "stdlib.h"
#include "string.h"

int katoi (char *s) {
	int r;
	for (r = 0; *s != '\0'; s++)
		r = r * 10 + *s - '0';
	return r;
}
