/*******************************************************************************

  dorothy/kernel/idt.c
 
  Copyright (C) 2005 D-OZ Team

  IDT (Interrupt Descriptor Table) functions
 
*******************************************************************************/

#include "idt.h"
#include "defines.h"

idtr IDTR;
x86_interrupt IDT[256];

void loadIDTR (void) {
	idtr *IDTRptr = &IDTR;
	IDTR.limit = 256 * (sizeof (x86_interrupt) - 1);
	IDTR.base  = IDT;

	asm volatile ("LIDT (%0)"::"p" (IDTRptr));
}

void addInt (int number, void (*handler)(), dword dpl) {
	word selector = 0x08;
	word settings;
	dword offset = (dword) handler;

	/* get CS */
	asm volatile ("movw %%cs, %0" : "=g" (selector));

	switch (dpl) {
		case 0: settings = INT_0; break;
		case 1: settings = 0xAE00; break; /**** TESTE ****/
		case 2: settings = 0x8F00; break;
		case 3: settings = INT_3; break;
	}

	IDT[number].low_offset  = (offset & 0xFFFF);
	IDT[number].selector    = selector;
	IDT[number].settings    = settings;
	IDT[number].high_offset = (offset >> 16);
}
