/*******************************************************************************

  dorothy/kernel/syscall.c

  Copyright (C) 2005 D-OZ Team

  Syscalls - Implemented a few, trying to make as many gates as possible
  to create an actual user-mode environment

*******************************************************************************/

#include "defines.h"
#include "timer.h"
#include "fsfile.h"
#include "syscall.h"
#include "stdio.h"
#include "process.h"
#include "io.h"
#include "8259a.h"
#include "irqwrapper.h"
#include "idt.h"
#include "conio.h"
#include "mem.h"
#include "iosched.h"
#include "sched.h"
#include "sys/fs.h"
#include "net/transp/tcp.h"

#include "floppy.h"

void proc_end() {
//	asm("sti");

	for(;;);
}

extern unsigned int schedflag;

void syscall_do_nothing() {
	kprintf("This should be a syscall!!! But it isn't.\n");
}

void (*syscalls_table[256])(void) = {
	(void (*) (void))syscall_color_cursor_handler,/* 0x00 */ /* WARNING this is no actual Linux style syscall, I put it here for now since i dont know where to put it */
	(void (*) (void))exit_syscall_handler,		/* 0x01 */
	(void (*) (void))fork_syscall_handler,		/* 0x02 */
	(void (*) (void))read_syscall_handler,		/* 0x03 */
	(void (*) (void))write_syscall_handler,		/* 0x04 */
	(void (*) (void))open_syscall_handler,		/* 0x05 */
	syscall_do_nothing,							/* 0x06 */
	syscall_do_nothing,							/* 0x07 */
	syscall_do_nothing,							/* 0x08 */
	syscall_do_nothing,							/* 0x09 */
	syscall_do_nothing,							/* 0x0A */
	(void (*) (void))execve_syscall_handler,	/* 0x0B */
	(void (*) (void))chdir_syscall_handler,		/* 0x0C */
	syscall_do_nothing,							/* 0x0D */
	syscall_do_nothing,							/* 0x0E */
	syscall_do_nothing,							/* 0x0F */
	syscall_do_nothing,							/* 0x10 */
	syscall_do_nothing,							/* 0x11 */
	syscall_do_nothing,							/* 0x12 */
	syscall_do_nothing,							/* 0x13 */
	syscall_do_nothing,							/* 0x14 */
	syscall_do_nothing,							/* 0x15 */
	syscall_do_nothing,							/* 0x16 */
	syscall_do_nothing,							/* 0x17 */
	syscall_do_nothing,							/* 0x18 */
	syscall_do_nothing,							/* 0x19 */
	syscall_do_nothing,							/* 0x1A */
	syscall_do_nothing,							/* 0x1B */
	syscall_do_nothing,							/* 0x1C */
	syscall_do_nothing,							/* 0x1D */
	syscall_do_nothing,							/* 0x1E */
	syscall_do_nothing,							/* 0x1F */
	syscall_do_nothing,							/* 0x20 */
	(void (*) (void))access_syscall_handler,	/* 0x21 */
	syscall_do_nothing,							/* 0x22 */
	syscall_do_nothing,							/* 0x23 */
	syscall_do_nothing,							/* 0x24 */
	syscall_do_nothing,							/* 0x25 */
	syscall_do_nothing,							/* 0x26 */
	syscall_do_nothing,							/* 0x27 */
	syscall_do_nothing,							/* 0x28 */
	syscall_do_nothing,							/* 0x29 */
	syscall_do_nothing,							/* 0x2A */
	syscall_do_nothing,							/* 0x2B */
	syscall_do_nothing,							/* 0x2C */
	(void (*) (void))brk_syscall_handler,		/* 0x2D */
	syscall_do_nothing,							/* 0x2E */
	syscall_do_nothing,							/* 0x2F */
	syscall_do_nothing,							/* 0x30 */
	syscall_do_nothing,							/* 0x31 */
	syscall_do_nothing,							/* 0x32 */
	syscall_do_nothing,							/* 0x33 */
	syscall_do_nothing,							/* 0x34 */
	syscall_do_nothing,							/* 0x35 */
	syscall_do_nothing,							/* 0x36 */
	syscall_do_nothing,							/* 0x37 */
	syscall_do_nothing,							/* 0x38 */
	syscall_do_nothing,							/* 0x39 */
	syscall_do_nothing,							/* 0x3A */
	syscall_do_nothing,							/* 0x3B */
	syscall_do_nothing,							/* 0x3C */
	syscall_do_nothing,							/* 0x3D */
	syscall_do_nothing,							/* 0x3E */
	syscall_do_nothing,							/* 0x3F */
	syscall_do_nothing,							/* 0x40 */
	syscall_do_nothing,							/* 0x41 */
	syscall_do_nothing,							/* 0x42 */
	syscall_do_nothing,							/* 0x43 */
	syscall_do_nothing,							/* 0x44 */
	syscall_do_nothing,							/* 0x45 */
	syscall_do_nothing,							/* 0x46 */
	syscall_do_nothing,							/* 0x47 */
	syscall_do_nothing,							/* 0x48 */
	syscall_do_nothing,							/* 0x49 */
	syscall_do_nothing,							/* 0x4A */
	syscall_do_nothing,							/* 0x4B */
	syscall_do_nothing,							/* 0x4C */
	syscall_do_nothing,							/* 0x4D */
	syscall_do_nothing,							/* 0x4E */
	syscall_do_nothing,							/* 0x4F */
	syscall_do_nothing,							/* 0x50 */
	syscall_do_nothing,							/* 0x51 */
	syscall_do_nothing,							/* 0x52 */
	syscall_do_nothing,							/* 0x53 */
	syscall_do_nothing,							/* 0x54 */
	syscall_do_nothing,							/* 0x55 */
	syscall_do_nothing,							/* 0x56 */
	syscall_do_nothing,							/* 0x57 */
	syscall_do_nothing,							/* 0x58 */
	syscall_do_nothing,							/* 0x59 */
	syscall_do_nothing,							/* 0x5A */
	syscall_do_nothing,							/* 0x5B */
	syscall_do_nothing,							/* 0x5C */
	syscall_do_nothing,							/* 0x5D */
	syscall_do_nothing,							/* 0x5E */
	syscall_do_nothing,							/* 0x5F */
	syscall_do_nothing,							/* 0x60 */
	syscall_do_nothing,							/* 0x61 */
	syscall_do_nothing,							/* 0x62 */
	syscall_do_nothing,							/* 0x63 */
	syscall_do_nothing,							/* 0x64 */
	syscall_do_nothing,							/* 0x65 */
	syscall_do_nothing,							/* 0x66 */
	syscall_do_nothing,							/* 0x67 */
	syscall_do_nothing,							/* 0x68 */
	syscall_do_nothing,							/* 0x69 */
	syscall_do_nothing,							/* 0x6A */
	syscall_do_nothing,							/* 0x6B */
	syscall_do_nothing,							/* 0x6C */
	syscall_do_nothing,							/* 0x6D */
	syscall_do_nothing,							/* 0x6E */
	syscall_do_nothing,							/* 0x6F */
	syscall_do_nothing,							/* 0x70 */
	syscall_do_nothing,							/* 0x71 */
	syscall_do_nothing,							/* 0x72 */
	syscall_do_nothing,							/* 0x73 */
	syscall_do_nothing,							/* 0x74 */
	syscall_do_nothing,							/* 0x75 */
	syscall_do_nothing,							/* 0x76 */
	syscall_do_nothing,							/* 0x77 */
	syscall_do_nothing,							/* 0x78 */
	syscall_do_nothing,							/* 0x79 */
	syscall_do_nothing,							/* 0x7A */
	syscall_do_nothing,							/* 0x7B */
	syscall_do_nothing,							/* 0x7C */
	syscall_do_nothing,							/* 0x7D */
	syscall_do_nothing,							/* 0x7E */
	syscall_do_nothing,							/* 0x7F */
	syscall_do_nothing,							/* 0x80 */
	syscall_do_nothing,							/* 0x81 */
	syscall_do_nothing,							/* 0x82 */
	syscall_do_nothing,							/* 0x83 */
	syscall_do_nothing,							/* 0x84 */
	syscall_do_nothing,							/* 0x85 */
	syscall_do_nothing,							/* 0x86 */
	syscall_do_nothing,							/* 0x87 */
	syscall_do_nothing,							/* 0x88 */
	syscall_do_nothing,							/* 0x89 */
	syscall_do_nothing,							/* 0x8A */
	syscall_do_nothing,							/* 0x8B */
	syscall_do_nothing,							/* 0x8C */
	(void (*) (void))getdents_syscall_handler,	/* 0x8D */
	syscall_do_nothing,							/* 0x8E */
	syscall_do_nothing,							/* 0x8F */
	syscall_do_nothing,							/* 0x90 */
	syscall_do_nothing,							/* 0x91 */

};


void int_syscall() { //void) {

	/* NOTE we are at user mem space here (i.e. process virtual space */

	unsigned int eax = 0;
 	unsigned int ebx = 0;
 	unsigned int ecx = 0;
 	unsigned int edx = 0;
 	unsigned int esi = 0;
 	unsigned int edi = 0;


//	unsigned int oldesp;

	asm("movl %%eax, %0" : "=r" (eax) : );
	asm("movl %%ebx, %0" : "=r" (ebx) : );
	asm("movl %%ecx, %0" : "=r" (ecx) : );
	asm("movl %%edx, %0" : "=r" (edx) : );
	asm("movl %%edx, %0" : "=r" (esi) : );
	asm("movl %%edx, %0" : "=r" (edi) : );


	/* The params for syscal are eax, ecx and edx
	 *
	 * where:
	 *
	 * eax - syscall code
	 * ebx - we are using old linux argument calling style for now, all
	 * register may receive arguments, up to 5 (...)
	 * */

	//kprintf("\n*syscall* eax: %d %X ebx: %d %X  ecx: %d  %X  edx: %d  %X\n\n", eax, eax, ebx, ebx, ecx, ecx, edx, edx);
	//kprintf("\nSyscall %i, %i, %i\n",eax,ecx,edx);

	/* push everything here, let the function sort it out */
	asm ("pushl %0" : : "r" (edi));	
	asm ("pushl %0" : : "r" (esi));	
	asm ("pushl %0" : : "r" (edx));	
	asm ("pushl %0" : : "r" (ecx));	
	asm ("pushl %0" : : "r" (ebx));	
	(*(syscalls_table+eax))();
	/* NOTE all syscalls handling functions *must* be __cdecl to proper stack cleaning */
	/* clean the stack */
	asm("addl $0x14,%esp");
	/* eax now contains the return value */
	asm("pushl %eax");
	
	/* signal end of interupt to the master interrupt controller */
	outportb(MASTER, EOI);
	asm("popl %eax");

}

void init_syscall (void) {
	INTS (false);
	addInt (0x80, intsyscall, 0);
	unmaskIRQ(0x80);
	INTS (true);
}
