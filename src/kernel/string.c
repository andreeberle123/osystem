/*******************************************************************************

  dorothy/kernel/string.c
 
  Copyright (C) 2005 D-OZ Team
 
  String handling functions

*******************************************************************************/

#include "string.h"
#include "defines.h"

int kstrcmp (const char *a, const char *b) {
	while ((*a == *b) && (*a) && (*b)) {
		a++;
		b++;
	}
	if (*a == *b) return 0;
	if (*a > *b) return 1;
	if (*a < *b) return -1;

	return -1;
}

int kstrncmp (const char *a, const char *b, int n) {
	int i = 1;
	while ((*a == *b) && (*a) && (*b) && (i < n)) {
		a++;
		b++;
		i++;
	}
	if (*a == *b) return 0;
	if (*a > *b) return 1;
	if (*a < *b) return -1;
	return -1;
}

char *kstrchtok (char *s, const char delim) {
	/* the plain, old kstrchtok works. well,
	   for now, let it be :) */
	static char *string_buffer;
	int i = 0;
	char *ret;

	if (s != NULL) {
		string_buffer = s;
	} else if (string_buffer == NULL)
		return NULL;

	while (string_buffer[i] != '\0' && string_buffer[i] != delim)
		i++;

	ret = string_buffer;
	if (string_buffer[i] == '\0') 
		string_buffer = NULL;
	else {
		string_buffer[i] = '\0';
		string_buffer = &string_buffer[i+1];
	}

	return ret;
}

void kstrcpy (char *dest, const char *src) {
	while (*src != '\0') {
		*dest = *src;
		src++;
		dest++;
	}
	*dest = '\0';
}

void kstrncpy (char *dest, const char *src, int n) {
	int i = 0;
	while (*src != '\0' && i < n) {
		*dest = *src;
		src++;
		dest++;
		i++;
	}
	*dest = '\0';
}

int kstrlen (const char *s) {
	int i = 0;
	while (s[i] != '\0')
		i++;
	return i;
}

char *kstrcat (char *first, const char *second)
{
    char *aux;

    // Joga a primeira string na auxiliar 
    for (aux = first; *first++; );

    // Coloca a segunda string na sequencia na auxiliar 
    for (--first; (*first++ = *second++)!=0; ) ;

    // Retorna a string concatenada 
    return aux;

}
