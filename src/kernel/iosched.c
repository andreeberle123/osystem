/****************************************************************************

	I/O scheduler

	- changed it, i am trying to remove the execution of the i/o from the
	interruption and put it in a separate process that the scheduler will
	schedule eventually.

****************************************************************************/




//#include "iosched.h"

#include "timer.h"
#include "iosched.h"
#include "process.h"
#include "sched.h"
#include "mem.h"


//static volatile struct io_queue * current;
static volatile struct io_queue * iolist = NULL;

struct io_queue *  make_ionode() {

	struct io_queue * ionode = kalloc(sizeof(struct io_queue));

	ionode->exec_ready = 1;
	ionode->process = get_current_process();

	ionode->process->status = WAITING;

	//change_current_process_status(WAITING);

	ionode->next = NULL;

	return ionode;

}

void add_ionode(struct io_queue * ionode) {
	struct io_queue * temp;

	if (iolist) {
		temp = (void *)iolist;
		while (temp->next)
			temp = temp->next;
		temp->next = ionode;
	}
	else
		iolist = ionode;

}

void ioexecute_process(struct io_queue * ionode) {


	ionode->handler(ionode->block_num,ionode->buffer,ionode->size);

	/* I/O requisition is done */

	ionode->exec_ready = 0;

	/* let's put the process back in the scheduler execution queue,
	we check if it's the only process running, in this case we just
	set it to running */

	if (ionode->process != get_current_process()) {

		/* process back to ready list */
		ionode->process->status = READY_TO_RUN;
		add_process(iolist->process);

	}
	else

		iolist->process->status = RUNNING;

	/* let's end it properly by calling exit syscall */

	asm("movl $0x04, %ebx");
	asm("int $0x80");

}

void iosheduler() {

	/* are there requests in the queue? */
	//kprintf("io scheduler!\n");
	if (iolist) {

		//asm("cli");

		/* fixed arguments, perhaps make a pointer to args */

		//add_process(build_ghost_process((unsigned char *)ioexecute_process,&iolist));
		build_ghost_process((unsigned char *)ioexecute_process,&iolist);

//		ioexecute(ionode);

		//struct io_queue * ionode = iolist;

		/* remove this task from the queue */

		iolist = iolist->next;



		/* activate the iosched, let it not be
		locked while we call those slow hard disks/floppy */

		addTrigger((void (*) (void *))iosheduler,10,NULL);


		//asm("sti");

	}

	else

		addTrigger((void (*) (void *))iosheduler,10,NULL);


}

void init_io_scheduler() {

	addTrigger((void (*) (void *))iosheduler,10,NULL);

}

