/*******************************************************************************

  dorothy/kernel/io.c
 
  Copyright (C) 2005 D-OZ Team

  Basic I/O functions
 
*******************************************************************************/

#include "io.h"
#include "defines.h"

inline void outportb (dword port, byte value) {
	asm ("outb %%al, %%dx" :: "d" (port), "a" (value));
}

inline void outportw (dword port, word value) {
	asm ("outw %%ax, %%dx" :: "d" (port), "a" (value));
}

inline void outportdw (dword port, dword value) {
	asm ("outl %%eax, %%dx" :: "d" (port), "a" (value));
}

inline byte inportb (dword port) {
	byte value;
	asm ("inb %w1, %b0" : "=a" (value) : "d" (port));
	return value;
}

inline word inportw (dword port) {
	word value;
	asm ("inw %w1, %w0" : "=a" (value) : "d" (port));
	return value;
}

inline dword inportdw (dword port) {
	dword value;
	asm ("inl %w1, %0" : "=a" (value) : "d" (port));
	return value;
}
