/*******************************************************************************

  dorothy/kernel/panic.c
 
  Copyright (C) 2005 D-OZ Team

  Exceptions handling
 
*******************************************************************************/

#include "panic.h"
#include "irqwrapper.h"
#include "stdio.h"
#include "8259a.h"
#include "defines.h"
#include "conio.h"
#include "idt.h"
#include "io.h"

void loadExceptions (void) {
	int i = 0;

	/* sets all interrupts to a do-nothing handler */
	for (i = 0; i < 256; i++)
		addInt (i, intnull, 0);

	/* adds all exception interrupts */
	addInt (0, int00, 0);
//	addInt (1, int01, 0);
	addInt (2, int02, 0);
//	addInt (3, int03, 0);
	addInt (4, int04, 0);
	addInt (5, int05, 0);
	addInt (6, int06, 0);
	addInt (7, int07, 0);
//	addInt (8, int08, 0);
	addInt (9, int09, 0);
	addInt (10, int10, 0);
	addInt (11, int11, 0);
	addInt (12, int12, 0);
	addInt (13, int13, 0);
	addInt (14, int14, 0);
//	addInt (15, int15, 0);		// Intel reserved
	addInt (16, int16, 0);
	addInt (17, int17, 0);
	addInt (18, int18, 0);
	addInt (19, int19, 0);
	/* reserved exceptions are handled by nobody :P */
	addInt (20, NULL, 0);
	addInt (21, NULL, 0);
	addInt (22, NULL, 0);
	addInt (23, NULL, 0);
	addInt (24, NULL, 0);
	addInt (25, NULL, 0);
	addInt (26, NULL, 0);
	addInt (27, NULL, 0);
	addInt (28, NULL, 0);
	addInt (29, NULL, 0);
	addInt (30, NULL, 0);
	addInt (31, NULL, 0);
}

void panic (char *message, char *code, bool halt) {
	clrscr ();
	kprints ("dorothy is in panic because: \n");
	kprintf ("	%s\n", message);
	if (halt) {
		kprints ("	and now she has gone home.\n");
		asm ("cli");
		asm ("hlt");
	} else 
		kprints ("	but she may spend more time in OZ\n");
	outportb (MASTER, EOI);	// Exception handled, warn PIC
}

void int_00 (void) {
	panic ("somebody divided something by zero!", "#00", false);
}

void int_01 (void) {
	panic ("there was a debug error!", "#DB", false);
}

void int_02 (void) {
	panic ("a NMI interrupt occurred", "#--", false);
}

void int_03 (void) {
	panic ("she found a breakpoint!", "#BP", false);
}

void int_04 (void) {
	panic ("an overflow happened!", "#OF", false);
}

void int_05 (void) {
	panic ("bound range was exceeded!", "#BR", false);
}

void int_06 (void) {
	kprintf("Invalid Opcode\n");
	while(1);
	panic ("a mean program sent an invalid opcode!", "#UD", false);
}

void int_07 (void) {
	panic ("some device was not avaiable...", "#NM", false);
}

void int_08 (void) {
	panic ("there was a double fault!", "#DF", true);
}

void int_09 (void) {
	panic ("there was a coprocessor segment overrun", "#NA", true);
}

void int_10 (void) {
	panic ("she saw an invalid TSS...", "#TS", false);
}

void int_11 (void) {
	panic ("some segment was not present!", "#NP", false);
}

void int_12 (void) {
	panic ("a stack segment fault happened!", "#SS", false);
}

void int_13 (void) {
kprintf("general protection fault\n");while(1);
	panic ("she saw the General Protection Fault!", "#GP", false);
}

void int_14 (void) {
kprintf("page fault\n");while(1);
	panic ("a page fault occurred!", "#PF", false);
}

void int_16 (void) {
	panic ("a FPU floating-point error occurred!", "#MF", false);
}

void int_17 (void) {
	panic ("an alignment check occurred!", "#AC", false);
}

void int_18 (void) {
	panic ("a machine check occurred", "#MC", true);
}

void int_19 (void) {
	panic ("she saw a SIMD floating-point!", "#XF", false);
}

