/*******************************************************************************

  dorothy/kernel/process.c

  Copyright (C) 2005 D-OZ Team

  Process

*******************************************************************************/

#include "defines.h"
#include "stdio.h"
#include "process.h"
#include "mem.h"

#include "sched.h"
#include "pages.h"

#include "loader.h"

char pidpai = 0;

extern char schedflag;

static volatile unsigned int pid = 1;
//static volatile struct list *plist = NULL;
static volatile struct task_struct *plist = NULL;
int ihateyou = 0;
struct no_pstree {
	struct task_struct * ts;
	struct no_pstree * filhos[16];
	struct task_struct * prox;
};


/*
 * unsigned char *memory is the pointer to the memory region
 * where the loader has loaded the program to execute!!!
 * NOTE must be careful here, the stack must exist in process
 * virtual address and pages now.
 * */
struct task_struct* create_process(unsigned char *mem, int create_pd) {
	struct task_struct *ts = (struct task_struct *) kalloc(sizeof(struct task_struct));

	ts->pid = pid++;
	ts->status = WAITING;
	/* we will not allocate the stack here, let it be done
	   when data is loaded to process address space */
	//ts->stack = kalloc(STACK_SIZE * sizeof(char));

	/* TODO this define in pages.h is not properly used in pages.c, fix this */
	ts->ebp = (unsigned long) VIRTUAL_MEMORY_SIZE;
	ts->esp = (unsigned long) VIRTUAL_MEMORY_SIZE-7;
	ts->stack = (unsigned char*)ts->ebp-STACK_MAX_SIZE; 

	ts->exe = mem;
	ts->next = NULL;

	ts->mem_area = (memory_descriptor*)kalloc(sizeof(memory_descriptor));
	ts->mem_area->stack_base = (dword)ts->ebp;
	ts->mem_area->stack_end = (dword)ts->ebp-4096; /* we will allocate a page for stack */ 
	ts->mem_area->p_heap_start = ts->mem_area->p_heap_break = PROCESS_HEAP_START;
	
	if (create_pd) {
		/* we create the process page directory */
		page_entry_type * process_pd = safe_create_process_page_directory();
		ts->pd = (unsigned long *) process_pd;
		safe_alloc_pages((unsigned long*)process_pd, ts->esp-1, 1, 1); 
		/* NOTE patched it here. Seems that stack page fault doesnt cause a #PF,
			instead it freezes the cpu (why?), so i am allocating all the 65kb for the
			stack here */
		//kprintf("l=%X %i\n", ts->esp-STACK_MAX_SIZE,STACK_MAX_SIZE/4096);
		safe_alloc_pages((unsigned long*)process_pd, ts->esp-STACK_MAX_SIZE, STACK_MAX_SIZE/4096, 1);
	}


	/*
	if (plist == NULL) {
		plist = (struct list *) kalloc(sizeof(struct list));
		plist->proc = ts;
		plist->next = NULL;
	} else {
		struct list *nd = (struct list *) kalloc(sizeof(struct list));
		struct list *node = (struct list *) plist;
		while (node->next != NULL)
			node = node->next;

		nd->proc = ts;
		nd->next = NULL;

		node->next = nd;
	}*/

	return ts;
}

/* added this function, essentially it creates a process which starts at
entrypoint, returns to retpoint when entrypoints finishes executing and
receives arguments as a pointer, i really think this is not a good alternative
to the fork in the general case, but it's useful to create an auxialiary process,
like in the ioscheduler */

void proc_end();

void build_ghost_process(/*void * retpoint,*/ unsigned char * entrypoint, void * args) {

	struct task_struct * ts = create_process(entrypoint,1);

	void (*exec)(void*);

	exec = (void (*) (void*))entrypoint;

	ts->status = READY_TO_RUN;

	if (fork(ts))
		exec(args);

//	ts->eip = (unsigned int) entrypoint;

	/*//kmemset((unsigned char *)&(ts->gs),0,sizeof(struct registers));
	unsigned char * tmp = (unsigned char *)&(ts->gs);

	while (tmp < ( (unsigned char *)&(ts->gs) + sizeof(struct registers)))
	   *tmp++ = 0;

	struct task_struct * current = get_current_process();

	ts->cs = current->cs;
	ts->flags = 0;

	// make a sort of "push" here, push the arguments (dword) and retpoint */

	//(unsigned long *)(ts->stack + STACK_SIZE) = (unsigned long)args;
	//(unsigned long *)(ts->stack + STACK_SIZE - 4) = (unsigned long)retpoint;

	// quite unecessary, let's just default this to the proc_end func
	//*(unsigned long *)(ts->stack + STACK_SIZE - 4) = (unsigned long)proc_end;

	//ts->esp = (unsigned long) ts->stack + STACK_SIZE - 4;

	/* fine, we are ready  */

	/*ts->status = READY_TO_RUN;

	return ts;*/

}

void add_process(struct task_struct* p) {
//	struct list *node = (struct list *) plist;
	struct task_struct *node = (struct task_struct *) plist;

	if (plist == NULL) {
		//plist = (struct list *) kalloc(sizeof(struct list));
		//plist->proc = p;
		plist = p;
		plist->next = NULL;
	} else {
		while (node->next != NULL)
			node = node->next;

	//	node->next = (struct list *) kalloc(sizeof(struct list));
	//	node->next->proc = p;
		node->next = p;
		node->next->next = NULL;
	}

	return;
}

void remove_process(struct task_struct* p) {
	//struct list *node = (struct list *) plist;
	//struct list *pnode = NULL;

	struct task_struct *node = (struct task_struct *) plist;
	struct task_struct *pnode = NULL;

	//while (node != NULL && node->next != NULL && node->proc != p) {
	while (node != NULL && node->next != NULL && node != p) {
		pnode = node;
		node = node->next;
	}

	/* removing */
	//if (node != NULL && node->proc == p) {
	if (node != NULL && node == p) {
		if (pnode == NULL) {
			plist = plist->next;
		} else {
			pnode->next = node->next;
		}
		//kfree(node);
	} else {
		/* there is nothing to remove */
		return;
	}

	return;
}

struct task_struct *get_next_ready_to_run(void) {
	//struct list *node = (struct list *) plist;
	struct task_struct *node = (struct task_struct *) plist;

	//while (node != NULL && node->proc->status != READY_TO_RUN) {
	while (node != NULL && node->status != READY_TO_RUN) {
		node = node->next;
	}
	
	return node;
}

void change_process_status(unsigned int pid, enum STATUS st) {
	struct list *node = (struct list *) plist;

	while (node != NULL && node->proc->pid != pid) {
		node = node->next;
	}

	if (node != NULL)
		node->proc->status = st;
}

void change_current_process_status( enum STATUS st) {
	struct task_struct *current = get_current_process();

	current->status = st;
}


float count_process() {

	float num = 1;
	struct task_struct * node = (struct task_struct *) plist;

	while (node) {
		num++;
		node = node->next;
	}

	return num;

}

/*struct no_pstree * make_pstree() {

	struct task_struct * node = plist();
	struct task_struct * current;

	struct no_pstree * temp;
	struct no_pstree * fila_head = NULL;
	struct no_pstree * fila_tail;

	int i;

	current = get_current_process();

	temp = (struct no_pstree *)kalloc(sizeof(struct no_pstree));
	temp->ts = kalloc(sizeof(struct task_struct));
	temp->ts->pid = 0;
	temp->prox = NULL;

	fila_head = temp;
	fila_tail = temp;

	while (temp) {

		i = 0;

		if (current->ppid == temp->ts->pid) {
			temp->filhos[i] = (struct no_pstree *)kalloc(sizeof(struct no_pstree));
			fila_tail->prox = temp->filhos[i];
			fila_tail->prox->ts = current;
			fila_tail = fila_tail->prox;
			i++;
		}


		while (node) {
			while(node && node->ppid != temp->ts->pid)
				node = node->next;
			if (node) {
				temp->filhos[i] = (struct no_pstree *)kalloc(sizeof(struct no_pstree));
				fila_tail->prox = temp->filhos[i];
				fila_tail->prox->ts = node;
				fila_tail = fila_tail->prox;
				i++;
			}
		}

		temp = temp->prox;
	}


}*/

void print_pstree(int current_pid, int * depth) {

	struct task_struct * node = (void *) plist;
	struct task_struct * current = get_current_process();
	int i;

	if (current->ppid == current_pid) {

		//kprintf("|");
		for(i=0;i<(*depth);i++)
			kprintf("|");

		kprintf("-%s\n",current->name);
		*depth = *depth + 1;
		print_pstree(current->pid,depth);

	}

	while (node) {

		if (node->ppid == current_pid) {

			//kprintf("|");
			for(i=0;i<(*depth);i++)
				kprintf("|");

			kprintf("-%s\n",node->name);
			*depth = *depth + 1;
			print_pstree(node->pid,depth);

		}
		node = node->next;
	}

	*depth = *depth - 1;


}


void pstree() {

	int depth = 1;

	kprintf("\nProcess tree\n\n");

	asm("cli");

	print_pstree(0,&depth);

	kprintf("\n");

	asm("sti");


}

/* we will dump the stack at kernel memory (i.e. code area) here first, so we
	can create a new page directory for the new process and copy from kernel to
	the new virtual address */
byte kernel_buf[8192];

int fork(struct task_struct * ts) {
	kprintf("forking process\n");
	struct task_struct * current;
	char exec_parent = 1;

	unsigned int stack_base;
	unsigned int stack_top;
	unsigned int stack_esp;
	//unsigned int stack_temp;

	asm("sti");
	/* wait here to guarantee scheduler saved this process image at this point */
	/* TODO this is a bit weird, find a better way */
	schedflag = 1;
	while (schedflag == 1);

	asm("cli");
	kprintf("exec_parent=%i\n",exec_parent);
	if (exec_parent) {
		asm("movl (%%ebp),%0" : "=r"(stack_base));
		asm("movl %%ebp,%0" : "=r"(stack_top));
		asm("movl %%esp,%0" : "=r"(stack_esp));kprintf("esp=%X\n",stack_esp);

		current = get_current_process();

		exec_parent = 0;

		ts->ppid = current->pid;

		/* copy all registers, starting at gs, to the new task struct */
		kmemcpy((unsigned char *)&(ts->gs),(unsigned char *)&(current->gs),sizeof(struct registers));

		/* NOTE stack copy is no longer necessary, as it is handled in paging directory copy */
//		kmemcpy(kernel_buf,(unsigned char *)stack_top,stack_base-stack_top);
		//kprintf("copying from %X, size:%i, %X %X\n",(unsigned char *)stack_esp,VIRTUAL_MEMORY_SIZE-stack_esp,ts->stack,STACK_MAX_SIZE);
		//kmemcpy(kernel_buf,(unsigned char *)stack_esp,VIRTUAL_MEMORY_SIZE-stack_esp);

		//stack_temp = (unsigned int)(ts->stack + STACK_SIZE);

		//kmemcpy(kernel_buf,(unsigned char *)&(stack_temp),4);

		/* duplicate current process memory */
		ts->pd = (unsigned long *)safe_duplicate_process_pd((page_entry_type *)current->pd);
		
		/* move to the new process virtual space */
		//use_pd((unsigned long)ts->pd);

		/* copy the stack of the parent process, from kernel area, to the new process */
		/* TODO the stack of the current function may actually be larger than 4096 bytes 
			(very unlikely), so adjust the buffer */
		/* TODO we may want, in the future, to have a fork which actually copies the
			entire stack */
		//kprintf("copying to %X, size:%i, %X\n",(unsigned char *)(stack_esp),VIRTUAL_MEMORY_SIZE-stack_esp,ts->stack);
		//kmemcpy((unsigned char *)(stack_esp),kernel_buf,VIRTUAL_MEMORY_SIZE-stack_esp);

		//ts->ebp = (unsigned int)(ts->stack + STACK_SIZE - (stack_base-stack_top));
		//ts->esp = (unsigned int)(ts->stack + STACK_SIZE - (stack_base-stack_esp));
		//ts->ebp = stack_top;
		//ts->esp = stack_esp;

		ts->status = READY_TO_RUN;
		
		add_process(ts);

		/* all set, return to our own virtual address */
		//use_pd((unsigned long)current->pd);

		/* ok, scheduler may work again */
		asm("sti");
		kprintf("parent is ready!\n");
		while(1);
		//while (exec_complete);

		//kprintf("a");

		return ts->pid;

	}
	else {
		asm("sti");
		kprintf("child is running!\n");
		while(1);
		return 0;
	}

}

/* 
 * NOTE we can ignore the argument for now, just provided for compatibility
 */
int fork_syscall_handler(void * regs) {
	/* the process wont execute anywhere actually, we are going to fork the current
		one, and execution will catch from there. Therefore the exe field of the
		task struct can receive 0, as it will be ignored */
	
	struct task_struct * ts = create_process(0,0);
	return fork(ts);
}

/* NOTE this is linux compatible, since the first (and only ones used here)
	elements from pt_regs struct are bx, cx and dx */
int execve_syscall_handler(char * filename, char ** argv, int edx) {
	kprintf("Execve called for %s\n",filename);

	/* edx are environment variables, we arent using them right now, so ignore them */
	executable_loader(filename,argv,0);
	
	return 1;
}

void create_system_process() {

}

struct task_struct * getPlist() {
	return (struct task_struct *)plist;
}

void kill_process(struct task_struct * ts, int ret) {

}

void kill_current_process(int ret) {
	kill_process(get_current_process(),ret);
}

void exit_syscall_handler(int status) {
	kill_current_process(status);
}
