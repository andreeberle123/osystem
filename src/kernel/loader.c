/*******************************************************************************

  dorothy/kernel/loader.c

  Copyright (C) 2005 D-OZ Team

  Loader

  OBS: This code is based on GazOS Operating System

*******************************************************************************/

#include "defines.h"
#include "stdio.h"
#include "mem.h"
#include "process.h"
#include "floppy.h"
#include "ffs.h"
#include "string.h"

#include "pages.h"
#include "sched.h"

/* NOTE TODO the interrupts shouldnt be handled here */
#include "io.h"
#include "idt.h"
#include "8259a.h"

struct task_struct *load_into_buffer(char *filename)
{
	unsigned char *mem;
	unsigned int fsz;
	struct task_struct *ts;

	//getting the filesize
	fsz = 0; //file_size(filename);

	//kalloc for the file
	mem = (unsigned char *) kalloc(fsz * sizeof(unsigned char));

	//reading the file


	//creating the process
	ts = create_process(mem,1);
	add_process(ts);

	return ts;
}

struct task_struct *make_ts(unsigned char *buffer)
{
	/* refiz a fun��o para receber o buffer ao inv�s do arquivo */

	//creating the process

	return create_process(buffer,1);
}

bool is_elf(unsigned char *buffer)
{
	/* every ELF file has 4 chars in the
	 * following order:
	 * 7F
	 * 45
	 * 4C
	 * 46
	 *
	 * So we can test if the file is ELF.
	 * For this we used an integer and as
	 * this operating systems is for little
	 * endian computers we invert the number.
	 * 
	 * TODO find a better way to solve endianess problems */

	if(*(int *)buffer != 0x464c457f)
		return false;

	return true;
}

/* it returns the entry point */
unsigned int entry_point;
unsigned int ep;


void ptc() {

	asm("xor %ebx,%ebx");
	asm("mov $0x64,%ecx");
	asm("int $0x80");

}

void copy_sections(struct task_struct *ts,unsigned int * memsize)
{
	unsigned char *buf = ts->exe;
  int full_bin_size = 0;
  int num_sect, pos=0, header_size;
  int file_off, sect_fsize, sect_msize, mem_addr, vaddr, max;
  unsigned char *target;
  int i, j;

	// get entry point from ELF header
  entry_point=*(unsigned int*)(buf+0x18);
	/* number of program headers, (not sections) */
  num_sect=*(short int*)(buf+0x2c);
  header_size=*(short int*)(buf+0x2a);

  vaddr = -1;
  max = -1;

  for(i=0;i<num_sect;i++) {
	/* 0x1c contains the offset of the first program header in the file */
    pos=*(short int*)(buf+0x1c);
    pos+=i*header_size;
	// type 1 means the header has loadable data */
    if((unsigned int *)(buf+pos)!= (unsigned int *)1) {
		full_bin_size += *(unsigned int*)(buf+pos+0x10);
	    if (vaddr == -1) {
			vaddr=*(unsigned int*)(buf+pos+0x08);
			max=vaddr;
	    } else if (*(unsigned int*)(buf+pos+0x08) > max) {
			max=*(unsigned int*)(buf+pos+0x08);
	    }
    }
  }

  //kprintf("entry_point: %d entry_point-mem_addr: %d  vaddr: %d\n", entry_point, entry_point-vaddr, vaddr);
  //getch();

  //kprintf("tamanho da aloca��o: %d + pilha\n", max - vaddr); getch();

	/* Ok, we have the base vaddr, entry point, and file buffer. At this point we (are supposed to)
		are at kernel memory space. We will now switch to process memory address, by creating a
		page directory for it. Notice that file buffers, this code and the variables are still at
		kernel area, and we (hopefuly) are at a kernel-enabled code segment. The process has memory
		visibility over kernel space, since first pages are pinned down (even though no access rights).
		After switching to process mem address, we will copy the buffer to the appropriate vaddr.
	*/

	/* we now switch to process virtual space */
	use_pd((unsigned long)ts->pd);
	/* allocate pages at the vaddr position (by ELF standards vaddr is 4kb aligned) */
	//alloc_pages_non_kernel((unsigned long*)process_pd, vaddr, (full_bin_size%4096 ? full_bin_size/4096+1 : full_bin_size/4096), 1); 

	int len = max - vaddr + 14;
	
	/* NOTE I changed this as some ELF files seem to be producing non 4kb-aligned vaddr */
	int page_num = (vaddr % 4096) +len;
	page_num = (page_num%4096 ? page_num/4096+1 : page_num/4096);

	/* TODO check if kernel alloc is ok */
	safe_alloc_pages((unsigned long*)ts->pd, vaddr, page_num, 1); 
	

	/* NOTE full_bin_size is the size of loadable segments, data or text
	   TODO check this
	*/	
	*(unsigned char *)vaddr = 3;

	//target = (unsigned char *) kalloc((max - vaddr + STACK_SIZE) * sizeof(unsigned char));
	/* we are good to go now, target is a buffer located virtually at vaddr. Naturally the real
	   memory position is elsewhere, but paging will handle this for us */
	target = (unsigned char *) vaddr;
	//kprintf("%i\n",max - vaddr + 14);
	//target = (unsigned char *)ptc;
		//kprintf("%i\n",(max - vaddr + 14));
	//for (i=0; i<(max - vaddr + 4096); i++) target[i] = 0;
	/* 14? */
	for (i=0; i<(max - vaddr + 14); i++) target[i] = 0;

	ep = (unsigned int) target + entry_point - vaddr;
		//kprintf("target = %i, entry_point = %X, vaddr = %i, ep = %i\n",target,entry_point,vaddr,ep);while(1); 
	//kprintf("target: %d  ep: %d\n", target, ep); getch();
		//kprintf("ep (v) = %X\n",(ep-(unsigned int)target));

	for(i=0;i<num_sect;i++) {

		pos=*(short int*)(buf+0x1c);
		pos+=i*header_size;

		//kprintf("1\n");

		if((unsigned int *)(buf+pos)!=(unsigned int *)1) {
			//kprintf("loadable segment\n");
		file_off=*(unsigned int*)(buf+pos+0x04);
		sect_fsize=*(unsigned int*)(buf+pos+0x10);
		mem_addr=*(unsigned int*)(buf+pos+0x08);
		sect_msize=*(unsigned int*)(buf+pos+0x14);

		
		//kprintf("file_off: %d\tsect_fsize: %d\tmem_addr: %X\tsect_msize: %d\n", file_off, sect_fsize, mem_addr, sect_msize);
		//kprintf("(mem_addr-vaddr): %d\n", mem_addr-vaddr);
		//kprintf("kmemcpy(target+(mem_addr-vaddr), buf[file_off], sect_fsize) -- target: %d\n", target);
		//getch();

		for (j=0; j < sect_fsize; j++) {
			target[mem_addr-vaddr+j] = buf[file_off+j];
			//kprintf("%X=%X, ",j,buf[file_off+j]);
		}

		//kprintf("copiei na memo\n"); getch();
		/*
		target=mem_addr;

		kprintf("2 mem_addr: %d\n", mem_addr); getch();
		for(j=0;j<sect_fsize;j++) {
			*target=buf[file_off+j];
			target++;
		}
		kprintf("3 sect_msize: %d  sect_fsize: %d\n", sect_msize, sect_fsize);
		getch();
		for(j=0;j<sect_msize-sect_fsize;j++) {
			*target=0;
			target++;
		kprintf("3.1 %d  %d  %d\n", j,  sect_msize, sect_fsize);
		}
		kprintf("4\n");*/
		}
	}

	/*  Ok, code and data are loaded in memory, we need to setup the stack now. The stack, as much 
		as the heap	is expandable, so we only allocate a single page, at the end of process memory for it. 
		It can be further expanded later */
	
	*memsize = full_bin_size + STACK_SIZE;
}

/* This function has to be called to prepare a program
 * to run
 * */
void prepare_binary_to_run(char *filename)
{
	struct task_struct *ts;

	if((ts = load_into_buffer(filename)) != NULL) {
	  kprints("Error while reading from file\n");
	  return;
	}

	if(!is_elf(ts->exe)) {
	  kprints("File is not a static ELF binary\n");
	  return;
	}

//	copy_sections(ts->exe);
	ts->entry_point = ep;

	/* this task_struct is ready to run */
	ts->status = READY_TO_RUN;
}

void push_args(char * mem_area, char ** argv) {
	if (!argv) 
		return;
	char * tmp;
	int argc = 0;
	int k = 0;
	while (*argv) {
		tmp = *argv++;
		while (*tmp) {
			mem_area[k++] = *tmp++;
		}
		mem_area[k++] = 0;
		argc++;
	}
	asm("pushl %0" : : "r" (mem_area));
	asm("pushl %0" : : "r" (argc));
}

//extern char schedflag;
void executable_loader(char * file_name, char ** argv, int keep_process) {
	/* TODO guarantee we are at kernel memory address (page directory) !!!! */
	/* I suppose we should have this check at the syscall gate */

	FILE * stream = fopen(file_name,"r");

	if (stream == 0) {
		kprintf("ERROR - ");
		/* TODO handle this better */
		asm("hlt");
	}
	void (*exec)(void);

	struct task_struct *ts;
	//int i;

	unsigned char *buffer = (unsigned char *) kalloc(ffilesize(stream) * sizeof(char));

	/* reads the file */
	fread(buffer,ffilesize(stream),1,stream);
	
	if (!keep_process) {
		/* we are going to overwrite current process */
		ts = get_current_process();
		if (ts == null) {
			ts = make_ts(buffer);
		}			
	}
	else {
		ts = make_ts(buffer);
	}

	if (is_elf(ts->exe)) {

		kmemcpy((unsigned char *)ts->name,(unsigned char *)file_name,kstrlen((const char *)file_name));

		/* we copy the contents regardless, since we are going to exec */
		copy_sections(ts,&(ts->bin_size));

		/* NOTE at this point we are at process memory address, be careful */

		/* set the process entry_point, tells the scheduler where to start it */

		ts->entry_point = ep;

		/* first call to the process, tells the scheduler to load the registers */

		ts->status = READY_TO_RUN;

		/* put the process in the process list, shall be executed by the scheduler in proper time */

		exec = (void (*) (void)) ep;

		if (get_current_process() != 0) {
			if (keep_process) {
				/* if processes exist, then add the new process for execution, doing a fork-like
					method. The fork however doesnt make the new process a child for the current,
					only create a valid execution point for the scheduler */
				/* TODO we may want alternatives to process creating later, apart from fork */
				if (!fork(ts)) {
					exec();
				}
				/* the loader process returns to the kernel paging */
				/* TODO maybe not, maybe we should stick with current paging */
				//use_kernel_pd();
			}
			else {

				/* we will overwrite current process with new process */
				/* move esp to stack base */
				ts->esp =  (unsigned long) VIRTUAL_MEMORY_SIZE-7;
				//ts->eip = (unsigned long)exec;
				asm __volatile__ ("movl %0, %%esp" : : "r" (ts->esp));
				/* TODO change syscall return to the executable, for now we simply restore interrupts */ 
				/* TODO change to user mode, by changing segments, and conform to other execve stuff */
				asm("sti");
				outportb(MASTER, EOI);

				exec();
				/* this process is expected to die on a syscall EXIT, so we shouldnt return
					here. */
				kprintf("Error - Process behaving erroneously\n");
				kill_current_process(-1);
			}
		}
		else {		
			//kprintf("Process stack will be at %X.\n",ts->esp);
			//kprintf("Process bp will be at %X.\n",ts->ebp);
			asm("cli");
			/* if no process is available, then let this flux become the process */
			set_current_process(ts);
			asm __volatile__ ("movl %0, %%esp" : : "r" (ts->esp));
			/* we dont really need to set ebp here, since the code would return here
				eventually */
			//asm __volatile__ ("movl %0, %%ebp" : : "r" (ts->ebp));
			//kprintf("Setting esp to %X\n",ts->esp);
			asm("sti");
			exec();
			/* theres nowhere to go from here, halt the system */
			asm("hlt");

		}		

		/*asm("movl 4(%%ebp),%0 " :  "=r"(tmp));
		kprintf("%d ",tmp);*/

		//while(1);
		
	}
	else
		kprintf("File is not a valid ELF\n");

}

void testLoader(void) {
	int numblocks = 9;
	unsigned char *mem = (unsigned char *) kalloc(sizeof(unsigned char) * 512 * numblocks);
	void (*exec)(void);

	kprintf("	Testing loader...\n");
	kprintf("		Loading file...\n");
	read_block(500, mem, numblocks);
	kprintf("		File loaded...\n");

	if (is_elf(mem)) {
		kprintf("		The file is ELF...\n");
		//copy_sections(mem);
		exec = (void (*) (void)) ep;
		//kprintf("exec: %d ep: %d\n", exec, ep);
		//getch();

		exec();
	} else {
		kprintf("       	Error... The file cannot be loaded...\n");
	}
}
