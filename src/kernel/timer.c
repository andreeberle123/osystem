/*******************************************************************************

  dorothy/kernel/timer.c

  Copyright (C) 2005 D-OZ Team

  Timer and delay functions

*******************************************************************************/

#include "timer.h"
#include "defines.h"
#include "io.h"
#include "8259a.h"
#include "idt.h"
#include "irqwrapper.h"

#include "sched.h"


//struct trigger {
//	void (*handler)();
//	dword timeout;
//	void *args;
//};

//struct trigger trigger[MAXTRIGGERS];
byte ntriggers = 0;

/*
void addTrigger (void (*handler)(void *), dword timeout, void *args) {
	int i;

	if (ntriggers >= MAXTRIGGERS) return;

	for (i = 0; trigger[i].handler != NULL; i++)
		;
	trigger[i].handler = handler;
	trigger[i].timeout = timeout;
	if (args)
		trigger[i].args = args;
	else
		trigger[i].args = NULL;

	ntriggers++;
}
*/
// c�digo enxuto !!!
/* substitu� a addtrigger pela antiga, tem triggers saindo da fila no
	meio, aleatoriamente e a outra s� considera o ultimo indice pra adicionar
	mais triggers, gaps no meio do vetor tao fazendo o vetor
	parecer lotado em pouco tempo */

/*void addTrigger (void (*handler)(void *), dword timeout, void *args) {
	if (ntriggers >= MAXTRIGGERS) return;

	trigger[ntriggers].handler = handler;
	trigger[ntriggers].timeout = timeout;
	if (args)
		trigger[ntriggers].args = args;
	else
		trigger[ntriggers].args = NULL;
	ntriggers++;
}*/

int addTrigger (void (*handler)(void *), dword timeout, void *args) {
	int i;
	//kprintf("%i, %i\n",ntriggers,handler);
	if (ntriggers >= MAXTRIGGERS) return -1;

	for (i = 0; trigger[i].handler != NULL; i++)
		;

	trigger[i].handler = handler;
	trigger[i].timeout = timeout;
	if (args)
		trigger[i].args = args;
	else
		trigger[i].args = NULL;

	ntriggers++;
	return i;
}

/*
void removeTrigger (word n) {
	int i;
	for (i = 0; trigger[i].handler != NULL; i++)
	; i--;
	trigger[n].handler = trigger[i].handler;
	trigger[n].timeout = trigger[i].timeout;
	trigger[n].args = trigger[i].args;
	ntriggers--;
}
*/

// c�digo enxuto !!
void removeTrigger (word n) {
	/* this must be atomic to work properly */
	asm("cli");
	// s� por seguran�a
	if (n >= MAXTRIGGERS)
		return;
	// n�o pode colocar o --ntrigger na linha abaixo e dps usar o ntrigger cmo �ndice para n�o precisar decrement�-lo no final n� ?
/*	trigger[n].handler = trigger[ntriggers - 1].handler;
	trigger[n].timeout = trigger[ntriggers - 1].timeout;
	trigger[n].args = trigger[ntriggers - 1].args;*/
	
	/* NOTE we dont shift anymore, simply clear the trigger at position */
	/* TODO this is very unsafe, we may actually be clearing another trigger
		altogether, since the intended trigger may have been cleared already,
		after firing */
	trigger[n].handler = 0;
	trigger[n].timeout = 0;
	ntriggers--;
	asm("sti");
}

static unsigned long delay_count = 1;
unsigned long ticks = 1;


void _delay (unsigned long loops) {
	unsigned long i;
	for (i = 0; i < loops; i++)
		;
}

/*void int_timer (int esp) {
	int i;
	void (*handler)() = NULL;
	//void *args = NULL;

	ticks++;
	 handles the timeouts *
	for (i = 0; i < MAXTRIGGERS; i++)
		if (trigger[i].handler != NULL) {
			trigger[i].timeout--;
			if (trigger[i].timeout == 0) {
				handler = trigger[i].handler;
				trigger[i].handler = NULL;
				if (trigger[i].args) {
					//args = trigger[i].args;
					if (handler==sheduler) {
						asm ("pushl %0" : : "r" (esp));  passar o esp como arg *
						handler();
						asm ("addl $4,%esp"); /limpar os args *
					}  pus isso aqui, ficou meio gambiarra *
					handler(trigger[i].args);
					//handler(args);
				} else {
					handler();
				}
				ntriggers--;
			}
		}
	outportb (MASTER, EOI);
}*/


void int_timer (int esp) {
	/* i am ignoring the triggers.arg here */
	int i;
	void (*handler)() = NULL;
	//void *args = NULL;

	ticks++;

	/* handles the timeouts */
	for (i = 0; i < MAXTRIGGERS; i++)
		if (trigger[i].handler != NULL) {
			trigger[i].timeout--;
			if (trigger[i].timeout == 0) {
				handler = trigger[i].handler;
				//kprintf("timer event! %i %i\n",handler,trigger);
				trigger[i].handler = NULL;
				if (handler==sheduler) {
					asm ("pushl %0" : : "r" (esp)); /* passar o esp como arg */
					//int ret;
					handler();
					asm ("addl $4,%esp"); /* limpar os args */
					//asm ("movl %%eax,%0" : "=r"(ret)); /* get return */
					/* inform the irq wrapper that we must return from current stack ! */
					//return ret;
				}
				else
					handler(trigger[i].args);

				/*if (trigger[i].args) {
					//args = trigger[i].args;
					handler(trigger[i].args);
					//handler(args);
				} else {
					handler();
				}*/
				ntriggers--;
			}
		}
	outportb (MASTER, EOI);
}

void loadTimer (void) {
	unsigned long temp = 1193180 / 100;
	int i;

	for (i = 0; i < MAXTRIGGERS; i++) {
		trigger[i].handler = NULL;
		trigger[i].args = NULL;
	}

	init_pit (100, 0);
	outportb (0x43, 0x36);
	outportb (0x40, (byte) temp);
	outportb (0x40, (byte) (temp >> 8));
	addInt (0x20, inttimer, 0);
	unmaskIRQ (0);
}

unsigned long calibrate_delay (void) {
	unsigned int prevtick;
	unsigned int i;
	unsigned int calib_bit;
//?	unsigned int temp = 1193180 / 18.2;

	/* Stage 1: first calibration */
	do {
		delay_count <<= 1;

		prevtick = ticks;
		while (prevtick==ticks)
			;

		prevtick = ticks;
		_delay (delay_count);
	} while (prevtick == ticks);

	delay_count >>= 1;

	/* Stage 2: fine calibration */
	calib_bit = delay_count;

	for (i = 0; i < PRECISION; i++) {
		calib_bit >>= 1;
		if (calib_bit == 0)
			break;

		delay_count |= calib_bit;

		prevtick = ticks;
		while (prevtick == ticks)
			;

		prevtick = ticks;
		_delay (delay_count);

		if (prevtick != ticks)
			delay_count &= ~calib_bit;
	}

	/* Stage 3: final adjustment */
	delay_count /= MILISEC;

	return delay_count;
}

void delay (unsigned long mili) {
	_delay (mili * delay_count);
}

