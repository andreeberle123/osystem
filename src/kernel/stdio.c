/*******************************************************************************

  dorothy/kernel/stdio.c

  Copyright (C) 2005 D-OZ Team

  Standard I/O functions

*******************************************************************************/

#include "stdio.h"
#include "conio.h"
#include "stdarg.h"
#include "keyboard.h"
#include "string.h"
#include "mem.h"

void kprints (char *s) {
	for (; *s != '\0'; s++)
		putch (*s);
}

void kprintl (unsigned long i, unsigned int digits) {
	unsigned char str [30];
	int j = 0,
		k;

	do {
		str [j++] = (i % 10) + '0';
		i /= 10;
	} while (i);

	for (k = j; k < digits; k++)
		putch (' ');

	for (j--; j >= 0; j--) 	// number is backwars, print flipped
		putch (str[j]);
}

void kprinth (unsigned long i, unsigned int digits) {
	unsigned char str [10];
	unsigned char d;
	int j = 0,
		k;

	do {
		d = i % 16;
		str [j++] = (d < 10 ? d + '0' : d - 10 + 'A');
		i >>= 4;
	} while (i);

	for (k = j; k < digits; k++)
		putch ('0');

	for (j--; j >= 0; j--)
		putch (str [j]);
}

void kvprintf (char *format, va_list arglist) {
	//va_list arglist;
	int i = 0,
		status = 1,
		digits;

	//va_start (arglist, format);
	while (format[i] != '\0') {
		switch (status) {
			case 1:
				if (format[i] == '%') {
					digits = 0;
					status = 2;
				} else
					putch (format[i]);
				break;
			case 2:
				if (format[i] >= '0' && format[i] <= '9') {
					digits = digits * 10 + format[i] - '0';
					break;
				}
			case 3:
				if (format[i] == 'd' || format[i] == 'u' || format[i] == 'i')
					kprintl (va_arg (arglist, int), digits);
				else if (format[i] == 's')
					kprints (va_arg (arglist, char *));
				else if (format[i] == 'c')
					putch (va_arg (arglist, char));
				else if (format[i] == 'X')
					kprinth (va_arg (arglist, int), digits);
				else if (format[i] == '%')
					putch ('%');
				status = 1;
				break;
		}
		i++;
	}
	//va_end (arglist);
}

void kprintf (char *format, ...) {
	va_list arglist;
	
	va_start (arglist, format);
	kvprintf(format,arglist);
	va_end (arglist);
}

void gets (char *s, unsigned int max) {
	unsigned char c = 0;
	unsigned char i = 0;
	for (;;) {
		c = getch ();
		if (c == '\n')
			break;
		else if (c == '\t')	// ignores tab
			continue;
		else if (i < max)
			s[i] = c;
		if (c == '\b' && i > 0) {
			putch ('\b');
			i--;
		} else if (c != '\b' && i < max) {
			putch (c);
			i++;
		}
	}
	s[i] = '\0';
	putch ('\n');
}

unsigned int exp_p(unsigned int base, int expoente) {

	int tmp = base;

	if (!expoente)
		return 1;

	while (--expoente > 0)
		tmp = tmp * base;

	return tmp;
}

char * kitoa(char * buf, int num) {

	int base = 1;
	int tmp = num;
	int count = 0;

	if (!num) {
		buf[0]=0x30;
		buf[1]=0;
		return buf;
	}

	while (tmp) {
		count++;
		tmp = tmp / 10;
	}

	base = exp_p(10,count-1);
	count = 0;

	while (num) {
		buf[count] = (num / base) + 0x30;
		num = num % base;
		base = base / 10;
		count++;
	}
	buf[count] = 0;
	return buf;

}

void ksprintf(char * ptr, const char * str, ... ) {

	char * temp = (char *)str;

	char tmp_int[20];
	va_list arglist;

	va_start (arglist, str);

	while (*temp != 0 ) {

		switch (*temp) {
		case '%':
			temp++;
			switch (*temp) {
			case 'd':
				kitoa(tmp_int,va_arg (arglist, int));
				kmemcpy((unsigned char *)ptr,(unsigned char *)tmp_int,kstrlen(tmp_int));
				ptr+= kstrlen(tmp_int);
				break;
			}
			break;
		case '\\':
			temp++;
			switch(*temp) {
			case 'n':
				*ptr = 0x0A;
				ptr++;
				break;
			case 't':
				*ptr = 0x09;
				ptr++;
				break;
			}
			break;
		default:
			*ptr = *temp;
			ptr++;
			break;
		}
		temp++;
	}
	*ptr = 0;

}
