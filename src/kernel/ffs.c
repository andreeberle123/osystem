#include "defines.h"
#include "stdio.h"
#include "floppy.h"
#include "string.h"
#include "ffs.h"
#include "mem.h"

byte ffs_fatbuffer[512];
byte ffs_bitmap[512];

char write_fat(void);

struct read_arg {
	void * buffer;
	int block_num;
	int size;
} read_block_args;

unsigned short bitmapNextFree() {

	byte * tmpBitmap = ffs_bitmap;
	char k = 1;
	int idx = 0;
	int kidx = 1;

	while (*tmpBitmap == 255) { tmpBitmap++; idx++; }

	if (*tmpBitmap) {

		while ((*tmpBitmap) & k)  { k = k << 1; kidx++; }

		return 302 + kidx + (idx*8);

	}
	else
		return 302 + (8*idx);


}

unsigned short make_new_block() {

	unsigned short freeSector = bitmapNextFree();

	ffs_bitmap[(freeSector-302)/8] = (ffs_bitmap[(freeSector-302)/8] ^ (1 << (((freeSector-302)%8)-1) ));

	return freeSector;

}

bool create_file(char *name, unsigned short pointer,
		unsigned short size) {

	INODE * tempnode = (INODE *)ffs_fatbuffer;
	int bitmap = 1;

	if (kstrlen(name)>=12)
		return 0;

	while (tempnode->pointer!=0) tempnode++;

	kmemcpy((unsigned char *)tempnode->name,(unsigned char *)name,kstrlen(name));
	tempnode->pointer=pointer;
	tempnode->size=size;

	tempnode++;
	tempnode->pointer=0;

	ffs_bitmap[(pointer-302)/8] = (ffs_bitmap[(pointer-302)/8] ^ (bitmap << (((pointer-302)%8)-1) ));

	write_fat();

	return true;
}

void refresh_fat(FILE * stream) {

	INODE * tempnode = (INODE *)ffs_fatbuffer;

	while ((tempnode->pointer) && (kstrcmp(tempnode->name,stream->name)) ) tempnode++;

	if (!(tempnode->pointer))
		return;

	tempnode->size = stream->size;

	write_fat();


}

void clearBitmap(unsigned short pointer) {

	char bit = 1;
	/* change the bit in a sector given by pointer */

	ffs_bitmap[(pointer-300)/8] = ffs_bitmap[(pointer-300)/8] ^ (bit << (pointer-300) % 8);
}

void clearBitmapSects(unsigned short pointer) {

	/* changes the bit in every sector from pointser, although the
	pointer itself wont be changed, clearBitmap may be used for that */


	byte sectorBuffer[512];


	read_block(pointer, sectorBuffer, 1);
	kmemcpy((unsigned char *)&pointer, sectorBuffer+510,sizeof(short));

	while (pointer) {
		clearBitmap(pointer);
		read_block(pointer, sectorBuffer, 1);
		kmemcpy((unsigned char *)&pointer, sectorBuffer+510,sizeof(short));
	}


}

bool unlink(char *name) {

	INODE * tempnode = (INODE *)ffs_fatbuffer;
	INODE * temp;

	while (tempnode++->pointer!=0)
		if (kstrcmp(tempnode->name,name)==0)
			temp = tempnode;

	clearBitmap(temp->pointer);

	if (temp->size > 510)
		clearBitmapSects(temp->pointer);

	tempnode--;

	kmemcpy((unsigned char *)temp,(unsigned char *)tempnode,sizeof(INODE));

	tempnode->pointer=0;

	return true;
}


FILE * fopen(const char *path, const char *mode) {

	FILE * tempFile;
	char i;
	INODE * tempnode;
	//char tempBlock[512];

	unsigned short freeSector;

	tempnode = (INODE *) ffs_fatbuffer;

	while (tempnode->pointer!=0) {
		if (kstrcmp(tempnode->name,path)==0) {

			tempFile = (FILE *) kalloc(sizeof(FILE));
			kmemcpy((unsigned char *)tempFile->name,(unsigned char * )path,kstrlen(path)+1); // sem diretorios
			tempFile->first_block = tempnode->pointer;

			switch (*mode) {
			case 'r':
				tempFile->size = tempnode->size;
				tempFile->fulloffset = 0;
				tempFile->mode = 1;
				break;
			case 'w':
				tempnode->size = 0;
				clearBitmapSects(tempnode->pointer);
				tempFile->size = 0;
				tempFile->fulloffset = 0;
				tempFile->mode = 2;
				break;
			case 'a':
				tempFile->size = tempnode->size;
				tempFile->fulloffset = tempFile->size;
				tempFile->mode = 4;
				break;
			default:
				return 0;
			}
			for(i=0;i<2;i++) {
				mode++;
				switch (*mode) {
				case 'b':
					tempFile->mode = tempFile->mode | 8;
					break;
				case '+':
					if ((tempFile->mode & 1) && (!(tempFile->mode & 4)))
						tempFile->mode = tempFile->mode | 2;
					else
						tempFile->mode = tempFile->mode | 1;
					break;
				case 't':
					break;
				default:
					return tempFile;
				}
			}

			return tempFile;
		}
		tempnode++;
	}
	switch (*mode) {
	case 'w':
		freeSector = bitmapNextFree();

		if (create_file((char *)path,freeSector,0)) {

			tempFile = (FILE *) kalloc(sizeof(FILE));
			kmemcpy((unsigned char *)tempFile->name,(unsigned char * )path,kstrlen(path)+1); // sem diretorios
			tempFile->first_block = freeSector;
			tempFile->mode = 2;
			tempFile->size=0;
			tempFile->fulloffset=0;
			return tempFile;
		}
		break;
	case 'a':
		freeSector = bitmapNextFree();

		if (create_file((char *)path,freeSector,0)) {

			tempFile = (FILE *) kalloc(sizeof(FILE));
			kmemcpy((unsigned char *)tempFile->name,(unsigned char * )path,kstrlen(path)+1); // sem diretorios
			tempFile->first_block = freeSector;
			tempFile->mode = 4;
			tempFile->size=0;
			tempFile->fulloffset=0;
			return tempFile;

		}
		break;

	}


	return NULL;
}

int fclose(FILE *stream) {

	//kfree(stream);

	return 0;

}

int min(int a, int b) {

	if (b > a)
		return a;
	else
		return b;
}

void read_block_call() {
	asm("movl %0,%%ecx" : : "r"(&read_block_args));
	asm("movl $05,%ebx");
	asm("int $0x80");
}


/*	fread uses syscalls, exists on user mode, TODO: remove it from kernel (duh) */

int fread(void *ptr, int size, int nmemb, FILE *stream) {

	int block;
	unsigned char temp_block[512];
	int cur_offset;
	int total_read;

	if (!(stream->mode & 1)) /* open without  read-mode */
		return -1;

	if (feof(stream)) /* reached eof ? */
		return 0;

	block = stream->first_block;

	cur_offset = stream->fulloffset;

	while (cur_offset > 510) {

		read_block_args.buffer = temp_block;
		read_block_args.block_num = block;
		read_block_args.size = 1;
		read_block_call();
		//read_block(block,temp_block,1);

		cur_offset-=510;
		kmemcpy((unsigned char *)&block,temp_block+510,2);

		if (block==0)
			return -2; /* stream error */

	}

	total_read = size * nmemb;

	read_block_args.buffer = temp_block;
	read_block_args.block_num = block;
	read_block_args.size = 1;
	read_block_call();

	//read_block(block,temp_block,1);

	kmemcpy((unsigned char *)ptr,temp_block+cur_offset,min(total_read-cur_offset,510-cur_offset));
	ptr = (char *)ptr + min(total_read-cur_offset,510-cur_offset);



	total_read-=(510-cur_offset);


	while (total_read > 0 ) {

		kmemcpy((unsigned char *)&block,temp_block+510,2);

		if (block==0) {
			/* reached eof */
			stream->fulloffset = stream->size;
			return 0;
		}

		read_block_args.buffer = temp_block;
		read_block_args.block_num = block;
		read_block_args.size = 1;
		read_block_call();

		//read_block(block,temp_block,1);

		kmemcpy((unsigned char *) ptr,temp_block,min(total_read,510));
		total_read-=510;
		ptr = (char *)ptr + min(total_read,510);

	}

	stream->fulloffset = min(stream->fulloffset + size * nmemb,stream->size);

	return size * nmemb;

}

int fwrite(const void *ptr, int size, int nmemb, FILE *stream) {
	int block;
	unsigned char temp_block[512];
	int cur_offset;
	int total_wr;
	unsigned short new_block;

	if ((!(stream->mode & 2))&&(!(stream->mode & 4))) /* open with neither write-mode nor append */
		return -1;

	if (stream->mode & 4)
		stream->fulloffset = stream->size;

	block = stream->first_block;

	cur_offset = stream->fulloffset;

	while (cur_offset > 510) {

		read_block(block,temp_block,1);
		cur_offset-=510;
		kmemcpy((unsigned char *)&block,temp_block+510,2);

		if (block==0)
			return -2; /* stream error */

	}

	total_wr = size * nmemb;

	read_block(block,temp_block,1);

	kmemcpy(temp_block+cur_offset,(unsigned char *)ptr,min(total_wr,510-cur_offset));

	write_block(block,temp_block,1);

	ptr = (char *)ptr + min(total_wr,510-cur_offset);

	total_wr-=(510-cur_offset);

	while (total_wr > 0 ) {

		kmemcpy((unsigned char *)&new_block,temp_block+510,2);

		if (!new_block) {
			new_block = make_new_block();

			kmemcpy(temp_block+510,(unsigned char *)&new_block,2);

			write_block(block,temp_block,1);

			temp_block[510]=0;
			temp_block[511]=0;
		}
		else
			read_block(new_block,temp_block,1);

		kmemcpy(temp_block,(unsigned char *)ptr,min(total_wr,510));

		write_block(new_block,temp_block,1);

		ptr = (char *)ptr + min(total_wr,510);

		total_wr-=510;

	}

	stream->fulloffset += size * nmemb;
	stream->size += size * nmemb;

	refresh_fat(stream);
	return size * nmemb;

}

int ffilesize(FILE *stream) {

	if (stream)
		return stream->size;
	else
		return 0;
}

int is_valid_file(char * name) {

	INODE * tempnode = (INODE *)ffs_fatbuffer;

	while (tempnode->pointer!=0) {
		if (kstrcmp(tempnode->name,name)==0)
			return 1;
		tempnode++;
	}
	return 0;


}

int feof(FILE *stream) {

	if (!stream)
		return -1;

	if (stream->fulloffset >= stream->size)
		return stream->fulloffset;
	else
		return 0;

}

// whence
// 	SEEK_SET
// 	SEEK_CUR
// 	SEEK_END
int fseek(FILE *stream, unsigned short offset, int whence) {

	switch (whence) {
		case 0:
			stream->fulloffset = offset;
			break;
		case 1:
			stream->fulloffset += offset;
			break;
		case 2:
			stream->fulloffset = stream->size - offset;
			break;
	}

	return 0;
}

char load_fat(void) {


	if ((read_block(FFS_FAT, ffs_fatbuffer, 1)) && (read_block(FFS_BITMAP, ffs_bitmap, 1)))
		return 1;
	return 0;

}

char write_fat(void) {

	write_block(FFS_FAT,ffs_fatbuffer,1);
	write_block(FFS_BITMAP,ffs_bitmap,1);
	return 1;

}

void ls(void) {
	INODE *pt = (INODE *) ffs_fatbuffer;

	while (pt->pointer != 0) {
		kprintf("%s\t\t%d\n", pt->name, pt->size);
		pt++;

	}
}

int getEntry(char *name) {
	INODE *pt = (INODE *) ffs_fatbuffer;
	int position = 0;

	while (pt->pointer != 0) {

		if (kstrcmp(pt->name, name) == 0) {
			return position;
		}

		pt++;
		position++;
	}

	return -1;
}

void cat(int position) {


	byte buffer[512];

	unsigned short *paux;
	unsigned short saux;
	unsigned short i;

	INODE *p = (INODE *) ffs_fatbuffer;

	if (position == -1) {
		kprintf("\nFile does not exist...\n");
		return;
	}

	p+=position;

	kprintf("\nname: %s\n", p->name);
	kprintf("pointer: %d\n", p->pointer);
	kprintf("size: %d\n", p->size);

	paux = &p->pointer;
	saux = p->size;

	do {
		read_block(*paux, buffer, 1);
		for (i = 0; i < 510 && i < saux; i++)
			kprintf("%c", buffer[i]);
		saux -= i;

		//buffer[510]  menos sig
		//buffer[511]  mais sig
		paux = (unsigned short *) &buffer[510];

	} while (*paux != 0);

}

void ffstry(char * debugout, int size) {

	FILE * fc = fopen("debug.out","ab");

	fwrite(debugout,size,1,fc);



}


