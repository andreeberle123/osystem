#include "idt.h"
#include "irqwrapper.h"
#include "io.h"
#include "8259a.h"
#include "stdio.h"

void intide();

void init_ide() {
	addInt (0x2e, intide, 0);
	unmaskIRQ (14);
}

void int_ide() {
	kprintf("ide active");
}
