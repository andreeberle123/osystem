;*******************************************************************************
;
; dorothy/boot/boot.asm
;
; Copyright (C) 2005 D-OZ Team
;
; This bootloader is based on the one made by Xsism (v1.5, 18/02/2003)
; This version is to be used for hard disk booting
;
;*******************************************************************************

;-------------------------------------------------------------------------------
; Memory layout:
;	0x00000-00x03FF	IVT (Interrupt Vector Table)
;	0x00400-0x004FF	BDA (BIOS Data Area)
;	0x00500-0x00CFF	GDB (Global Descriptor Table)
;	0x00D00-0x01CFF	IDT (Interrupt Descriptor Table)
;	0x01D00-0x01F00	System stack

;	0x01F00 - 0xF0000 Memory data structures

;	0xF0000-0xFFFFF	System code/data

;	0xA0000-0xB7FFF	VGA buffer
;	0xB8000-0xB8F9F	Text buffer
;-------------------------------------------------------------------------------
; Floppy setup:
;	(C/H/S) 80C, 2H/C, 18S/H, 512b/S 2880S
;
;	00-01 			Boot sector code
;	01-33			System code/data
;	33-96			System application code/data
;-------------------------------------------------------------------------------

[bits 16]
[org 0x7c00]

jmp boot						; jumps straight to boot! XD

;-------------------------------------------------------------------------------
;                                 DATA
;-------------------------------------------------------------------------------
;                                 GDT
GDTR:
	GDTsize DW (GDT_END - GDT - 1)
	GDTbase DD 0x00000500

GDT:

NULLSEL		EQU $-GDT			; we need a null descriptor (64 bit)
	DD 0x00000000
	DD 0x00000000
CODESEL		EQU $-GDT			; 4 GB flat code at 0x00000 with max 0xFFFFF limit
	DW 0xFFFF					; Limit(2):0xFFFF
	DW 0x0000					; Base(3)
	DB 0x00						; Base(2)
	DB 0x9A						; Type: present, ring0, code, exec/read/accessed (10011010)
	DB 0xCF						; Limit(1):0xF | Flags:4Kb inc, 32bit (11001111)
	DB 0x00						; Base(1)
DATASEL		EQU $-GDT			; 4 GB flat data at 0x00000 with max 0xFFFFF limit
	DW 0xFFFF                   ; Limit(2):0xFFFF
	DW 0x0000                   ; Base(3)
	DB 0x00                     ; Base(2)
	DB 0x92                     ; Type: present, ring0, data/stack, read/write (10010010)
	DB 0xCF                     ; Limit(1):0xF | Flags:4Kb inc, 32bit (11001111)
	DB 0x00                     ; Base(1)
	
GDT_END:
;-------------------------------------------------------------------------------
;                               VARIABLES
	drive DB 0
	cylinders DW 0
	heads DB 0
	sectors DB 0
	to_read DB 0
	sec_start DB 0
	track DW 0
	head DB 0
	boot_sectors DB 0x7A
;-------------------------------------------------------------------------------
;                               FUNCTIONS

disk_read_loop:
	mov ah, 2					; read disk
	mov al, [to_read]			; total sectors to read
	mov ch, [track]				; track
	int 0x13
	jc disk_read_loop

	;mov ah, 0
	sub [boot_sectors],al
	jz disk_read_end
	js disk_read_end
	
	shl ax,9
	add bx, ax

	mov cl, 1					; sector, (from first one now)
	inc dh

	cmp dh,[heads]
	jle check_to_read
	mov dh,0
	inc word [track]

check_to_read:
	mov al,[sectors]
	cmp al,[boot_sectors]
	jl disk_read_it
	mov al,[boot_sectors]

disk_read_it:
	mov [to_read],al
	jmp disk_read_loop

disk_read_end:
	ret

; wkc: 'wait keyboard to clear'
wkc:
	xor al, al
	in al, 0x64					; get keyboard status
	test al, 0x02				; bit 1 clear?
	jnz wkc						; no? try again...
	ret

; wkf: 'wait keyboard to full'
wkf:
	xor al, al					
	in al, 0x64					; get keyboard status
	test al, 0x01				; bit 0 set?
	jz wkf						; no? try again...
	ret

; halt: 'halt on error'
halt:
	mov byte [gs:0], al
	mov byte [gs:1], 0x04
	cli
	ret
;-------------------------------------------------------------------------------
;-------------------------------------------------------------------------------
;                            MAIN CODE
boot:

	mov [drive], dl				; save boot drive number (0x00 is floppy, 0x80 is HD)

	mov ax, cs					; setup data segment
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov ax, 0x1D0				; stack is at 0x1D00
	mov ss, ax					; align stack also
	mov sp, 0x200				; 512 byte stack

	mov ax, 0xB800				; setup video segment
	mov gs, ax

	jmp init					; just in case we get a buggy BIOS

init:
	; Enables A20
	cli

	call wkc
	mov al, 0xD1				; we want to write to the output port
	out 0x64, al
	call wkc
	mov al, 0xDF				; settings to A20 gate
	out 0x60, al				
	call wkc

	mov cx, 0x10
kbdwait:
	xor ax, ax					; do something
	out 0xE0, ax				; nonsense
	loop kbdwait				; to waste time

	; Checks if A20 is enabled
	mov al, 0xD0
	out 0x64, al				; we want to read the output port
	call wkf
	in al, 0x60
	test al, 0x02				; is A20 on?
	jnz a20_on					; if it is, continue...
	mov al, 'A'
	call halt					; else, halt. :P

a20_on:
	sti

	; Loads kernel from disk to 0x100000 | loads 64 KB max
read:
	xor ax, ax					; disk reset
	mov dl, [drive]				; the boot drive
	int 0x13
	jc read

	; get maximum heads/cylinders/sectors
	mov ah, 0x08
	int 0x13
	jc read

	mov [heads], dh
	mov ax, cx
	and cl, 0x3f
	mov [sectors], cl
	; other info is not quite relevant here
	sub cl,1
	mov [to_read], cl
	;mov [sec_start], 2

	mov ax, 0xFFFF
	mov es, ax					; data buffer for file
	mov bx, 0x10				; start of segment
	mov cl, 2					; sector
	mov dh, 0					; head
	mov dl, [drive]				; drive
	
	call disk_read_loop

 
;******* This is not a patch, it's a band-aid  ************
; we loaded 127 sectors, that is, (65.024 bytes) 63.5 kB (I don't know if
; it is really needed, but, as we start from 0x10 I think
; the segment loses 0x10 bytes and we need to do as follows:
; - load the other 16 sectors from track 3, head 1
; - load another 18 sectors from track 4, head 0
; - later (see part 2), copy the code at the end of the
;   already loaded kernel code

; we read 127 more sectors here	
	
	mov al,[sectors]
	sub al,[to_read]
	jnz set_cl
	mov al,[sectors]
	mov cl,1
	inc dh
	cmp dh,[heads]
	jle continue_reading
	mov dh,0
	inc word [track]
	jmp continue_reading

set_cl:
	mov cl,[to_read]
	inc cl 			; increase sector start here, since sectors start at 1 (0 would be much easier...)

continue_reading:
	mov [to_read],al

	mov byte [boot_sectors],0x7F
	mov ax, 0x1000
	mov es, ax					; data buffer for file
	mov bx, 0x0000				; start of segment
	mov dl, [drive]	
	call disk_read_loop

; 0xAE00 -> rep
; 0xAE00 bytes = 44.544 bytes = 43.5 Kbytes
; total 124 Kbytes

;******* End of band-aid, part 1  ************

	;mov dx, 0x3F2				; stops the motor
	;mov al, 0x0C				; no more spinning!
	;out dx, al

	; Puts GDT in it's place
	xor ax, ax
	mov ds, ax
	mov es, ax
	mov si, GDT					; move from [DS:SI]
	mov di, [GDTbase]			;      to   [ES:DI]
	mov cx, [GDTsize]			; with size
	cld
	rep movsb

	cli
	; Enters protected mode
	mov eax, cr0
	or al, 1
	mov cr0, eax

	lgdt[GDTR]

	jmp CODESEL:FLUSH			; set cs to CODESEL
	

[bits 32]
FLUSH:

	; Refreshes segment registers
	mov eax, DATASEL
	mov ds, eax
	mov es, eax
	mov fs, eax
	mov gs, eax
	mov ss, eax
	mov esp, 0xFFFF

;** band-aid, part 2 - copies stuff in the end of the kernel  **
	xor ecx, ecx
	;mov cx, 0x4400              ; 16+18 sectors
;	mov cx, 0xAE00              ;  16+(4*18) sectors = 88 sectors
	mov cx, 0xFE00              ;  16+(6*18) sectors = 124 sectors
	mov esi, 0x10000            ; where it was
	mov edi, 0x10F400           ; where it needs to be
	rep movsb
;******* end of band-aid - thanks, Johnson & Johnson  ************


	jmp CODESEL:0x100000		; jumps to wrapper

	hlt

TIMES 510 - ($ - $$) DB 0		; fills the bootsector with zeros

SIGNATURE DW 0xAA55				; magic number
