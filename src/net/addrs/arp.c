/*******************************************************************************

  dorothy/net/arp.c

  Copyright (C) 2005 D-OZ Team

  ARP (Address Resolution Protocol)

  OBS: The weird problem has been solved. Seems like *I* have a little problem
  with parenthesis XD, but now it's gone. Next step is to implement the ARP
  table and go on to the *IP*

  OBS: The ARP hash table will be initially implemented without collision
  handling. If we have collisions, we'll replace the existing entry.
  Also, the ARP table is 'forever'. If a host changes it's address, it
  won't be reach anymore. A 'reset' function must be made, maybe activated
  via a timeout.

  OBS: ARP is working! Now we can use a network! ARP is working also between
  the 48ETH_ADDR_SIZE and BOCHS. That means that network test can be done when IP gets
  implemented.

  BUG: ARP stops working when I comment the kprint stuff in the network
  driver. How is that related? Maybe that delays the operation?

*******************************************************************************/

#include "net/addrs/arp.h"
#include "net/dlink/eth.h"
#include "mem.h"
#include "defines.h"
#include "stdio.h"
#include "timer.h"
#include "list.h"
#include "string.h"
#include "jhash.h"
#include "8259a.h"

LIST_HEAD(arp_addresses);
LIST_HEAD(arp_eth_hashes);

byte eth_broadcast[ETH_ADDR_SIZE] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

/* builds an ARP message */
arp_packet *arp_build_message  (word hardware, word protocol,
								byte hwaddrlen, byte ptaddrlen,
								byte *srchwaddr, byte *srcptaddr,
								byte *dsthwaddr, byte *dstptaddr,
								word opcode) {

	arp_packet *p = (arp_packet *) kalloc (sizeof (struct arp_header)
	                                       + (2 * hwaddrlen + 2 * ptaddrlen) * sizeof (byte));

	byte *content = (byte *) p + sizeof (struct arp_header);

	p->header.hardware = htons(hardware);
	p->header.protocol = htons(protocol);
	p->header.hwaddrlen = hwaddrlen;
	p->header.ptaddrlen = ptaddrlen;
	p->header.opcode = htons(opcode);

	kmemcpy (content, srchwaddr, hwaddrlen);
	kmemcpy (content + hwaddrlen, srcptaddr, ptaddrlen);
	kmemcpy (content + hwaddrlen + ptaddrlen, dsthwaddr, hwaddrlen);
	kmemcpy (content + 2 * hwaddrlen + ptaddrlen, dstptaddr, ptaddrlen);

	return p;
}

/* sends an ARP message via Ethernet */
void arp_send_eth_message (arp_packet *p) {
	/* TODO put the frame building routine in the eth.c send function,
	  not in every network layer send function */
	ethernet_frame *frame = (ethernet_frame *) kalloc (sizeof (ethernet_frame)
				+ sizeof (struct arp_header)
				+ 12
				+ 2 * p->header.ptaddrlen);
	byte *content = (byte *) p + sizeof (struct arp_header);

	kmemcpy (frame->header.src_address, content, ETH_ADDR_SIZE);
	kmemcpy (frame->header.dst_address, content + p->header.hwaddrlen + p->header.ptaddrlen, ETH_ADDR_SIZE);
	kmemcpy ((byte *) frame + sizeof (ethernet_frame), (byte *) p, sizeof (struct arp_header)
	  									                          + 12
											                      + 2 * p->header.ptaddrlen);
	frame->header.type = htons(ARP_TYPE);

	eth_send ((byte *) frame, sizeof (ethernet_frame)
	                         + sizeof (struct arp_header)
							 + 2 * ETH_ADDR_SIZE
	                         + 2 * p->header.ptaddrlen);

	kfree (frame);
}

/* handle arp packets */
void arp_handle_packet (arp_packet *packet) {
	struct list_head *l;
	struct arp_address *a;
	byte *content = (byte *) packet + sizeof (struct arp_header);
	
	if (ntohs (packet->header.opcode) == ARP_REQUEST) {
		//kprintf("ARP REQUEST\n");
		list_for_each (l, &arp_addresses) {
			a = list_entry (l, struct arp_address, head);
			/* it is one of my addresses: reply! */
			if ((a->protocol == ntohs (packet->header.protocol)) &&
			    (kstrncmp ((const char *)a->address,(const char *) content + 2 * ETH_ADDR_SIZE + a->ptaddrlen, a->ptaddrlen) == 0)) {
				//kprintf("Local ARP address found\n");
	        	    arp_packet *packet = arp_build_message (ARP_ETHERNET, a->protocol, ETH_ADDR_SIZE, a->ptaddrlen,
					   						               eth_get_MAC(), a->address,
														   content, content + ETH_ADDR_SIZE,
										                   ARP_REPLY);
				    arp_send_eth_message (packet);
				    kfree (packet);
				}
		}
	} else if (ntohs (packet->header.opcode) == ARP_REPLY) {
		//kprintf("ARP REPLY\n");
		struct list_head *l;
		struct arp_hash *h;
		struct arp_entry *e;

		dword code;

		/* store ARP reply in a HASH table */
		list_for_each (l, &arp_eth_hashes) {
			h = list_entry (l, struct arp_hash, head);
			if (h->protocol == ntohs(packet->header.protocol))
				break;
		}
		if ((h->protocol != ntohs(packet->header.protocol)) ||
				(h->ptaddrlen != packet->header.ptaddrlen)) return; /* sanity check */
		/* gets the hash address */
		code = jhash (content + ETH_ADDR_SIZE, h->ptaddrlen, 0) % h->maxsize;
		e = &h->hash[code];
		/* stores the information */
		kmemcpy (e->hwaddr, content, ETH_ADDR_SIZE);
		kmemcpy (e->ptaddr, content + ETH_ADDR_SIZE, h->ptaddrlen);
	//	kprintf("Info stored\n");
	}
}

/* register a protocol address so Dorothy can reply to ARP requests */
void arp_register_address (word protocol, byte ptaddrlen, byte *address) {
	struct arp_address *a = (struct arp_address *) kalloc (sizeof (struct arp_address));

	a->protocol = protocol;
	a->ptaddrlen = ptaddrlen;
	a->address = kalloc (sizeof (byte) * ptaddrlen);
	kmemcpy (a->address, address, ptaddrlen);

	list_add ((struct list_head *) a, &arp_addresses);
}

/* creates a protocol hash table for arp */
void arp_create_eth_lookup (word protocol, byte ptaddrlen, byte *broadcast, byte maxsize) {
	struct arp_hash *h = (struct arp_hash *) kalloc (sizeof (struct arp_hash));
	int i;

	h->protocol = protocol;
	h->ptaddrlen = ptaddrlen;
	h->maxsize = maxsize;
	h->broadcast = (byte *) kalloc (ptaddrlen * sizeof (byte));
	kmemcpy (h->broadcast, broadcast, ptaddrlen);
	h->hash = (struct arp_entry *) kalloc (maxsize * sizeof (struct arp_entry));

	for (i = 0; i < maxsize; i++) {
		h->hash[i].hwaddr = kalloc (ETH_ADDR_SIZE);
		h->hash[i].ptaddr = kalloc (ptaddrlen);
	}

	list_add ((struct list_head *) h, &arp_eth_hashes);
}

/* look up the ethernet address for a protocol */
bool arp_lookup_eth_address (word protocol, byte ptaddrlen, byte *ptaddr, byte *hwaddr) {
	struct list_head *l;
	struct arp_address *a;
	struct arp_hash *h;
	struct arp_entry *e;
	dword code;


	byte tries = 2;//ARP_TRIES;

	/* we need to get our address for this protocol just
	 * in case we need to send an ARP request */
	list_for_each (l, &arp_addresses) {
		a = list_entry (l, struct arp_address, head);
		/* it is one of my addresses: reply! */
		if (a->protocol == protocol)
			break;
	}

	if (a->protocol != protocol) return false; /* sanity check */

	do {
		kprintf("ARP: Searching for address in hash\n");
		/* if the address is in HASH, returns it */
		/* first, finds the desired protocol */
		list_for_each (l, &arp_eth_hashes) {
			h = list_entry (l, struct arp_hash, head);
			if (h->protocol == protocol)
				break;
		}
		if ((h->protocol != protocol) || (h->ptaddrlen != ptaddrlen)) break; /* sanity check */
		
		/* it's broadcast! */
		if (kstrncmp ((const char *)ptaddr, (const char *)h->broadcast, ptaddrlen) == 0) {
			kmemcpy (hwaddr, eth_broadcast, ETH_ADDR_SIZE);
			return true;
		}
		/* gets the hash address */
		code = jhash (ptaddr, h->ptaddrlen, 0) % h->maxsize;
		e = &h->hash[code];
		// acho que o kprint abaixo ajuda no delay e faz funcionar huahshasa
		//kprintf ("%d.%d.%d.%d\n", e->ptaddr[0], e->ptaddr[1], e->ptaddr[2], e->ptaddr[3]);
		if (kstrncmp ((const char *)ptaddr, (const char *)e->ptaddr, h->ptaddrlen) == 0) { /* it is the real one: copy and return */
			kmemcpy (hwaddr, e->hwaddr, ETH_ADDR_SIZE);
			return true;
		}

		if (--tries <= 0) break;          /* limited number of tries */
		/* else, sends an ARP request for that address */

	    arp_packet *packet = arp_build_message (ARP_ETHERNET, h->protocol, ETH_ADDR_SIZE, h->ptaddrlen, eth_get_MAC(), a->address, eth_broadcast, ptaddr, ARP_REQUEST);

		arp_send_eth_message (packet);

		kfree (packet);

		kprintf("ARP: address wait started\n");
		/* TODO find a better way to wait for this, with something event oriented */
		/* wait a little bit: 200 ms is a good option?  */
		delay (200);
		/* loop ended, look in HASH again */
		kprintf("ARP: address wait finished\n");

	} while (true);
	return false;

}

/* just to test ARP */
/*
void arp_test (void) {
	byte pts[4] = {192, 168, 1, 56};
	byte ptd[4] = {192, 168, 1, 57};
	byte brd[4] = {192, 168, 1, 255};
	byte hwd[ETH_ADDR_SIZE];
	static bool first = true;
//	struct list_head *l;
//	struct arp_address *a;
//	int i;
//
//	arp_packet *packet = arp_build_message (ARP_ETHERNET, ARP, ETH_ADDR_SIZE, 4,
//											eth_get_MAC(), pts, hwd, ptd,
//										    ARP_REQUEST);
//	for (i = 0; i < 28; i++)
//		kprintf ("%X ", ((byte *)packet)[i]);
//	kprintf ("\n");
//
//	arp_send_eth_message (packet);
//	kfree (packet);
	if (first) {
		arp_register_address (ARP, 4, pts);
    		arp_create_eth_lookup (ARP, 4, brd, 10);
		first = false;
	}
    if (arp_lookup_eth_address (ARP, 4, ptd, hwd)) {
		kprints ("arp lookup worked. MAC is\n");
		kprintf ("%X:%X:%X:%X:%X:%X\n", hwd[0], hwd[1], hwd[2], hwd[3], hwd[4], hwd[5]);
	} else {
		kprints ("arp lookup failed.\n");
	}
//	arp_register_address (ARP, 4, ptd);
//
//	list_for_each(l, &arp_addresses) {
//		a = list_entry (l, struct arp_address, head);
//		for (i = 0; i < a->ptaddrlen; i++)
//			kprintf ("%d ", a->address[i]);
//		kprints ("\n");
//	}
}*/
