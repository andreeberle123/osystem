/****************************************************

	dorothy/net/transp/udp.c

	Copyright (C) 2006 D-OZ Team

	UDP - User Datagram Protocol

	TODO
	Fazer uma lista de sess�es (sessionbox) com as id�s, mudar udp_send
	O udp_get_message tem q retornar o tamanho tamb�m  pro tratamento pelo kish??
*****************************************************/

#include "net/transp/udp.h"
#include "net/nets/ip.h"
#include "mem.h"
#include "net/utils/idlist.h"
#include "stdio.h"
#include "keyboard.h"

word udp_checksum_do (byte *p, dword len) {
	int nleft = len;
	word *w = (word *)p;
	dword sum = 0;
	word answer = 0;
	
	while (nleft > 1) {
		sum += *w++;
		nleft -= 2;
	}

	if (nleft == 1) {
		*(byte *) (&answer) = *(byte *)w;
		sum += answer;
	}

	sum = (sum >> 16) + (sum & 0xffff);
	sum += (sum >> 16);
	answer = ~sum;

	return answer;
}

bool udp_checksum_check (udp_packet *p) {
	dword sum_in_packet = p->header.checksum;
	p->header.checksum = 0;
	dword sum_check = udp_checksum_do ((byte *)p, p->header.size);
	if (sum_in_packet != sum_check) return false;
	return true;
}

udp_packet *udp_build_packet (word sport, word dport, dword size, byte *data) {
	/* mounting packet with the values passed */
	udp_packet *p = (udp_packet *) kalloc (sizeof (struct udp_header) + size * sizeof (byte));
	// Content points to the end of udp_header, the begining of data
	byte *content = (byte *) p + sizeof (struct udp_header);
	p->header.sport = sport;
	p->header.dport = dport;
	p->header.size = size + sizeof (struct udp_header);
	p->header.checksum = 0;
	kmemcpy (content, data, size);
	p->header.checksum = udp_checksum_do ((byte *)p, p->header.size);
	return p;
}


bool udp_send (word id, byte *data, dword size) {
    struct idlist_entry *idlist_cell = idlist_check_id (id);
    if (idlist_cell != NULL) {  // if this is a registered id
        udp_packet *p = udp_build_packet (idlist_cell->sport, idlist_cell->dport, size, data);
        byte *content = (byte *) p; /* is it right? */
        /* ip_get_IP should return the ip variable addr */
        ip_send (UDP_TYPE, ip_get_IP(), idlist_cell->dst_ip, content, size);
        return true;    // OK
    }
    return false;   // Algo deu errado       
}

void udp_handle_packet (byte *ip, udp_packet *p) {
    struct idlist_entry *idlist_cell;
    int size;
    kprintf("\nudp_handle_packet  SP: %d  DP: %d\n", p->header.sport, p->header.dport);
    idlist_cell = idlist_add_id (p->header.dport, ip, p->header.sport);
    //kprintf("\nidlist_add_id ( %d, %d.%d.%d.%d, %d)\n",p->header.dport, ip[0], ip[1], ip[2], ip[3], p->header.sport);
    byte *data = (byte *) p + sizeof (struct udp_header);
    // Agora o tamanho � �referente aos dados do pacote
    size = p->header.size - sizeof (struct udp_header);
    if (udp_checksum_check (p)) {
 	struct idlist_message *msg = idlist_build_message (data, size);
    	idlist_add_message (idlist_cell->id, msg);
    }
    kfree (p);
    return;
}

//  PROVIS�RIO !!!, dps vai retornar byte e o size 
struct idlist_message *udp_get_message (word parameter_port) {
	 kprintf("\nvai chamar get_id_from_port\n");
	 getch();
    word id = idlist_get_id_from_port (parameter_port);
	 kprintf("\nget_id_from_port retornou %d\n", id);
	 getch();
    struct idlist_message *msg = idlist_get_message (id);
	 kprintf ("\n retornando msg para kish\n");
	 getch();
    return msg;
}

// Adds a new ID to idlist
word udp_register (word sport, byte *dst_ip, word dport) {
    word id = net_hash (sport, dst_ip, dport);    
	idlist_add_id (sport, dst_ip, dport);
	return id;
}
