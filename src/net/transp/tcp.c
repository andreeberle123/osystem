/********************************************************
 * dorothy/net/transp/tcp.c
 *
 * Copyright (C) 2006 D-OZ Team
 *
 * TCP - Transmission Control Protocol
 *
 * TODO: Descomentar
 *       arrumar seqnums +-
 *       CORRIGIR ACKNUM !!!!
 *       corrigir os pedidos de reenvio
 *
 * *****************************************************/

#include "net/nets/ip.h"
#include "mem.h"
#include "net/utils/tcp_idlist.h"
#include "net/transp/tcp.h"
#include "timer.h"
#include "stdio.h"
#include "keyboard.h"
#include "string.h"

static unsigned long delay_count = 1;

// Adds a new ID to idlist, case of existing ID is treated by tcp_idlist_add_id
word tcp_register (word sport, byte *dst_ip, word dport) {
	word id = tcp_net_hash (sport, dst_ip, dport);
	tcp_idlist_add_id (sport, dst_ip, dport);
	return id;
}

word tcp_listen (word sport) {
	word id = tcp_net_hash (sport, ip_get_IP(), 0); // gambi, s� pra testar
	tcp_idlist_add_id (sport, ip_get_IP(), 0);
	return id;
}
// usada para enviar msg com flags e sem conte�do, sem seqnum++
bool tcp_send (word id, byte flags, byte *data, dword size) {
	struct tcp_idlist_entry *idlist_cell = tcp_idlist_check_id(id);
	if (idlist_cell) {
		kprintf ("\nmontando idlist_cell->seqnum = %d", idlist_cell->seqnum);
		tcp_packet *p = tcp_build_packet (idlist_cell->sport, idlist_cell->dport, flags, idlist_cell->seqnum, idlist_cell->last_ack, data, size);
		// ADICIONAR MSG NO VETOR DE ENVIADAS, seria feito fora ? para podermos mandar msgs de controle
		kprintf("\nmandando seqnum(%d) - acknum(%d)", p->header.seqnum, p->header.acknum);
		ip_send (TCP_TYPE, ip_get_IP(), idlist_cell->dst_ip, (byte *)p, size);
		return true;
	}
	return false;
}
// usada para enviar msg com conte�do, com seqnum++, se n�o conseguir inserir no vetor de enviadas retorna erro e reenvia a msg travada que est� na posi��o seqnum atual % 5
bool tcp_send_message (word id, byte flags, byte *data, dword size) {
	struct tcp_idlist_entry *idlist_cell = tcp_idlist_check_id(id);
	//int seqnum;
	if (idlist_cell) {
		kprintf ("\nmontando idlist_cell->seqnum = %d", idlist_cell->seqnum);
		tcp_packet *p = tcp_build_packet (idlist_cell->sport, idlist_cell->dport, flags, idlist_cell->seqnum, idlist_cell->last_ack, data, size);
		// ADICIONAR MSG NO VETOR DE ENVIADAS, seria feito fora ? para podermos mandar msgs de controle, n�o, agora temos uma fun��o s� pra mandar msg de controle
		struct tcp_idlist_message *msg = tcp_idlist_build_message ((byte *) p,size + sizeof (struct tcp_header));
		if (!tcp_insert_sent (id, msg, idlist_cell->seqnum)) {
			// A posi��o da mensagem que deve ser reenviada � o seqnum da mensagem atual % 5, pois � exatamente nesta posi��o do vetor de enviadas que n�o est� conseguindo inserir pois tem uma mensagem l� =)
/*			int pos = idlist_cell->seqnum % 5;
			tcp_packet *p2 = (tcp_packet *) idlist_cell->enviadas[pos] + sizeof (struct tcp_idlist_message_header);
			ip_send (TCP_TYPE, ip_get_IP(), idlist_cell->dst_ip, (byte *) p2, idlist_cell->enviadas[pos]->header.size); // reenvia o pacote
			//resetTimer(id,p->header.acknum);	// reseta o timer pra reenvio, DESCOMENTAR !!!
			idlist_cell->enviadas[pos]->header.ack_counter = 0; // zera counter de acks*/
			kprintf("\nPau pra botar no vetor de enviadas");
			return false;
		}
		kprintf("\nmandando seqnum(%d) - acknum(%d)", p->header.seqnum, p->header.acknum);
		ip_send (TCP_TYPE, ip_get_IP(), idlist_cell->dst_ip, (byte *)p, size);
		//kprintf("\nMANDOU!");
		// montando a lista de argumentos para a fun��o a ser agendada
		int seqnum;
		struct tcp_timeout_args *args =(struct tcp_timeout_args *) kalloc (sizeof (struct tcp_timeout_args));
		args->id = id;
		seqnum = idlist_cell->seqnum;
		args->seqnum = seqnum;
		args->timeout = 3000;
		args->repeat = 3;
		addTrigger (tcp_timeout, 3000*delay_count, (void *) args);
		// add to enviadas
		idlist_cell->seqnum++;
		return true;
	}
	return false;
}

tcp_packet *tcp_build_packet (word sport, word dport, byte flags, dword seqnum, dword acknum, byte *data, dword size) {
	unsigned int aux;
	tcp_packet *p = (tcp_packet *) kalloc (sizeof(struct tcp_header) + size*sizeof(byte));
	p->header.sport = sport;
	p->header.dport = dport;
	p->header.seqnum = seqnum;
	p->header.acknum = acknum;
	p->header.flags = flags;
	p->header.reserved = 0;
 	aux = (unsigned int) sizeof(struct tcp_header);
	p->header.offset = (word) aux;
	p->header.checksum = 0;
	p->header.urg = 0;
	p->header.window = 0; // acoxambramento, dps vai particionar os dados de acordo com o tamanho da window recebida e receber os dados de acordo com a window enviada
	// Add the data before the checksum is calculed
	if (data != NULL) {
		byte *content = (byte *) p + sizeof (struct tcp_header);
		kmemcpy (content, data, size);
	}
	p->header.checksum = tcp_do_cksum ((byte *) p, (dword) (sizeof (struct tcp_header) + (byte*) size)); // VERIFICAR SE EST� CERTO O SEGUNDO PARAMETRO
	return p;
}


// Conex�o TCP
bool tcp_connect (word id) {
	dword seqnum;
	struct tcp_idlist_entry *idlist_cell;
	idlist_cell = tcp_idlist_check_id (id);

	if (idlist_cell == NULL)
		return false;
	tcp_send (id, FLG_SYN, NULL, 0);
	kprints ("\nSYN SENT");
	struct tcp_timeout_args *args =(struct tcp_timeout_args *) kalloc (sizeof (struct tcp_timeout_args));
			// achar o pos !!!
	args->id = id;
	seqnum = idlist_cell->seqnum;
	// n�o queria funfar o kmemcpy o_O
	//kmemcpy ((byte *)args->seqnum, (byte *)idlist_cell->seqnum, 4);
	args->seqnum = seqnum;
	args->timeout = 3000;
	args->repeat = 3;
	addTrigger (tcp_timeout, 3000*delay_count, (void *) args);
	idlist_cell->seqnum++;
	idlist_cell->status = CON_SYNSENT;
	kprintf("TCP: ID %d connected succesfully.\n");
	//retorna mesmo com possibilidade de timeout?
	return true;
}

/*bool tcp_close (word id) {
	kprints ("\n Entrou no tcp_close");
	getch();
	struct tcp_idlist_entry *idlist_cell;
	idlist_cell = tcp_idlist_check_id (id);
	if (idlist_cell == NULL) {
		kprints ("\nID nao encontrado");
		getch();
		return false;
	}
	// Modificar trecho abaixo pois a msg que mandamos � a mesma, oq difere � apenas para qual estado vai dependendo de qual estado est�
	if (idlist_cell->status == CON_ESTABLISHED) {
		kprints ("\nstatus == CON_ESTABLISHED, mandando msg de fin");
		getch();
		tcp_send (id, FLG_FIN | FLG_ACK, NULL, 0);
		//struct tcp_timeout_args *args =(struct tcp_timeout_args *) kalloc (sizeof (struct tcp_timeout_args));
		//args->id = id;
		//kmemcpy ((byte *)args->seqnum, (byte *)idlist_cell->seqnum, 4);
		//args->timeout = 3000;
		//args->repeat = 3;
		//addTrigger (tcp_timeout, 3000*delay_count, (void *) args);

		idlist_cell->status = CON_FINWAIT1;
	} else if (idlist_cell->status == CON_CLOSEWAIT) {
		kprints ("\nstatus == CON_CLOSEWAIT, mandando msg de fin");
		tcp_send (id, FLG_FIN | FLG_ACK, NULL, 0);
		//struct tcp_timeout_args *args =(struct tcp_timeout_args *) kalloc (sizeof (struct tcp_timeout_args));
		//args->id = id;
		//kmemcpy ((byte *)args->seqnum, (byte *)idlist_cell->seqnum, 4);
		//args->timeout = 3000;
		//args->repeat = 3;
		//addTrigger (tcp_timeout, 3000*delay_count, (void *) args);

		idlist_cell->status = CON_LASTACK;
	}
	return true;
}*/
/* how the sequence number will be calculated? */

bool tcp_close (word id) {
	kprints ("\n Entrou no tcp_close");
	getch();
	struct tcp_idlist_entry *idlist_cell;
	idlist_cell = tcp_idlist_check_id (id);
	if (idlist_cell == NULL) {
		kprints ("\nID nao encontrado");
		getch();
		return false;

	// Modificar trecho abaixo pois a msg que mandamos � a mesma, oq difere � apenas para qual estado vai dependendo de qual estado est�
	}
	if (idlist_cell->status == CON_ESTABLISHED) {
		kprints ("\nstatus == CON_ESTABLISHED, mandando msg de fin");
		getch();
		tcp_send (id, FLG_FIN, NULL, 0);
		//struct tcp_timeout_args *args =(struct tcp_timeout_args *) kalloc (sizeof (struct tcp_timeout_args));
		//args->id = id;
		//kmemcpy ((byte *)args->seqnum, (byte *)idlist_cell->seqnum, 4);
		//args->timeout = 3000;
		//args->repeat = 3;
		//addTrigger (tcp_timeout, 3000*delay_count, (void *) args);

		idlist_cell->status = CON_FINWAIT;
	/*} else if (idlist_cell->status == CON_CLOSING) {
		kprints ("\nstatus == CON_CLOSING, mandando msg de fin|ack");
		tcp_send (id, FLG_FIN | FLG_ACK, NULL, 0);
		//struct tcp_timeout_args *args =(struct tcp_timeout_args *) kalloc (sizeof (struct tcp_timeout_args));
		//args->id = id;
		//kmemcpy ((byte *)args->seqnum, (byte *)idlist_cell->seqnum, 4);
		//args->timeout = 3000;
		//args->repeat = 3;
		//addTrigger (tcp_timeout, 3000*delay_count, (void *) args);

		idlist_cell->status = CON_LASTACK;
	}*/
		return true;
	} else {
		return false;
	}
}

void tcp_handle_packet (byte *dst_ip, tcp_packet *p, dword data_size) {
    struct tcp_idlist_entry *idlist_cell;
	word id;
	// p->header.sport � a porta que est� esperando resposta de quem enviou a mensagem, portanto temos registrado a porta dele como nossa dport
	// p->header.dport � a porta que est� aberta no host, portanto � ela q est� associada com o ID
	id = tcp_idlist_get_id_from_port (p->header.dport);
	idlist_cell = tcp_idlist_check_id (id); // can be null, check it for security reasons

	// a conex�o n�o est� registrada, ID n�o est� na lista
	if (idlist_cell == NULL) {
		// faz nada
	} else {
		// recebe SYN e est� esperando conex�o
		if ((p->header.flags == FLG_SYN) && idlist_cell->status == CON_LISTEN) {
			idlist_cell->last_ack = p->header.seqnum + 1;
			// Iniciando corretamente os campos
			if (idlist_cell->dport == 0) {
				kmemcpy ((byte *)idlist_cell->dst_ip, (byte *)dst_ip,4);
				idlist_cell->dport = p->header.sport;
			}
			// Final da inicializa��o correta dos campos
// 			kprints ("\nRECEBI SYN");
// 			kprintf ("\nPACOTE: seqnum(%d) - acknum(%d)", p->header.seqnum, p->header.acknum);
// 			kprintf ("\nMEU: seqnum(%d) - lastack(%d)", idlist_cell->seqnum, idlist_cell->last_ack);
			idlist_cell->seqnum = 0; // era pra receber rand
			tcp_send (id, (FLG_SYN | FLG_ACK), NULL, 0);
			kprintf("\nSYN/ACK SENT");
			idlist_cell->seqnum++;
			idlist_cell->status = CON_SYNRECVD;
			kprintf("\nchegou no final do if");
		} else if ((p->header.flags == (FLG_SYN | FLG_ACK)) && (idlist_cell->status == CON_SYNSENT)) {
			// retorna ack
			idlist_cell->last_ack = p->header.seqnum + 1;
			// TODO: CANCELAR O PEDIDO DE RESEND
// 			kprints ("\nRECEBI SYN/ACK");
// 			kprintf ("\nPACOTE: seqnum(%d) - acknum(%d)", p->header.seqnum, p->header.acknum);
// 			kprintf ("\nMEU: seqnum(%d) - lastack(%d)", idlist_cell->seqnum, idlist_cell->last_ack);
			tcp_send (id, FLG_ACK, NULL, 0);
			kprints ("\nACK SENT");
			idlist_cell->status = CON_ESTABLISHED;
			removeTimer(id, p->header.acknum-1);
		} else if ((p->header.flags == FLG_SYN) && (idlist_cell->status == CON_SYNSENT)) {
			// send SYN+ACK
			idlist_cell->last_ack = p->header.seqnum + 1;
			idlist_cell->seqnum = 0; // era pra receber rand
			tcp_send (id, (FLG_ACK | FLG_SYN), NULL, 0);
			idlist_cell->seqnum++;
			idlist_cell->status = CON_SYNRECVD;
		} else if ((p->header.flags == FLG_ACK) && (idlist_cell->status == CON_SYNRECVD)) {
			kprints ("\nRECEBI ACK");
// 			kprintf ("\nPACOTE: seqnum(%d) - acknum(%d)", p->header.seqnum, p->header.acknum);
// 			kprintf ("\nMEU: seqnum(%d) - lastack(%d)", idlist_cell->seqnum, idlist_cell->last_ack);
			idlist_cell->status = CON_ESTABLISHED;
			kprintf ("\nseqnum %d - lastack %d", idlist_cell->seqnum, idlist_cell->last_ack);
		// A partir daqui come�a o tratamento de finaliza��o da conex�o
		} else if ((p->header.flags == FLG_FIN) && (idlist_cell->status == CON_ESTABLISHED)) {
			tcp_send (id, (FLG_FIN | FLG_ACK), NULL, 0);
			idlist_cell->status = CON_CLOSING;
		} else if ((p->header.flags == (FLG_FIN | FLG_ACK)) && (idlist_cell->status == CON_FINWAIT)) {
			tcp_send (id, FLG_ACK, NULL, 0);
			idlist_cell->status = CON_CLOSED;
		} else if ((p->header.flags == FLG_ACK) && (idlist_cell->status == CON_CLOSING)) {
			idlist_cell->status = CON_CLOSED;
		/*} else if ((p->header.flags == (FLG_FIN | FLG_ACK)) && (idlist_cell->status == CON_ESTABLISHED)) {
			// retorna ACK
			idlist_cell->last_ack = p->header.seqnum + 1;
			kprints ("\nrecebido fin, retornando ack");
			tcp_send(id, FLG_ACK, NULL, 0);

			idlist_cell->status = CON_CLOSEWAIT;
			// now we wait for application to end its transmission and call close
		} else if ((p->header.flags == (FLG_FIN | FLG_ACK)) && (idlist_cell->status == CON_FINWAIT2)) {
			// remote side ended the transmission
			// we should ack and enter CLOSE WAIT state
			idlist_cell->last_ack = p->header.seqnum + 1;
			tcp_send(id, FLG_ACK, NULL, 0);

			idlist_cell->status = CON_TIMEDWAIT;
			// TODO: addTrigger em uma fun��o que em tanto tempo passe o estado para closed... colocar um wrapper que pegue as closed e desaloque
		} else if ((p->header.flags == FLG_ACK) && (idlist_cell->status == CON_FINWAIT1)) {
			// go to FIN WAIT-2 state and wait until FIN received
			idlist_cell->status = CON_FINWAIT2;
		} else if ((p->header.flags == (FLG_FIN | FLG_ACK)) && (idlist_cell->status == CON_FINWAIT1)) {
			idlist_cell->last_ack = p->header.seqnum + 1;
			tcp_send(id, FLG_ACK, NULL, 0);

			idlist_cell->status = CON_CLOSING;
		} else if ((p->header.flags == FLG_ACK) && (idlist_cell->status == CON_LASTACK)) {
			idlist_cell->status = CON_CLOSED;
			kprints("\nCLOSED");
		} else if ((p->header.flags == FLG_ACK) && (idlist_cell->status == CON_CLOSING)) {
			// era pra entrar em timewait e fechar dps de 2 mls
			idlist_cell->status = CON_CLOSED;
			kprints("\nCLOSED");
		} else if ((p->header.flags == FLG_FIN) && (idlist_cell->status == CON_FINWAIT2)) {
			idlist_cell->last_ack = p->header.seqnum + 1;
			tcp_send(id, FLG_ACK, NULL, 0);
			idlist_cell->status = CON_CLOSED;
			kprints("\nCLOSED");*/
		} else if ((p->header.flags == FLG_SYN) && (idlist_cell->status == CON_ESTABLISHED)) {
			kprints ("\nAlready Connected o_O");


	// TODO da certo isso?
	// A partir de agora, tratamos as mensagens mandadas sem ser para estabelecer/encerrar conex�es

		} else if ((p->header.flags == FLG_ACK) && (idlist_cell->status == CON_ESTABLISHED)) {
			kprintf ("\nRECEBI ACK (acknum: %d)", p->header.acknum);
// 			kprintf ("\nPACOTE: seqnum(%d) - acknum(%d)", p->header.seqnum, p->header.acknum);
// 			kprintf ("\nMEU: seqnum(%d) - lastack(%d)", idlist_cell->seqnum, idlist_cell->last_ack);
			// se estiver conectado, receber um ack e a mensagem estiver na lista, chamar tcp_check_ack para remove-la da lista
			//if ((pos = tcp_idlist_check_numseq(id, p->header.acknum - 1)) != WINDOW_SIZE ) { // se a msg ainda tiver na lista, remove-la
			//	tcp_check_ack(id, p->header.acknum);
			//	se a msg foi removida
			byte sent_pos;
			byte pos = sent_pos = p->header.acknum % 5;
			if (sent_pos == 0)
				sent_pos = WINDOW_SIZE-1;
			else
				sent_pos--;
			if (tcp_remove_sent (id, sent_pos)) {
			//	kprintf("\nremoveTimer (%d, %d)",id, p->header.acknum-1);
				removeTimer (id, p->header.acknum - 1);
				kprintf("\nremoveu msg %d",p->header.acknum - 1);
			//	se a msg j� tinha sido removida, devemos incrementar o numero de acks recebidos para reenviar a pr�xima msg
			} else {
				// a msg ja foi acked, incrementar o counter de acks recebidos
				// incrementa counter do pacote referente ao ack recebebido (NAO AO ACK-1!!! AO ACK RECEBIDO), com seqnum = n�mero do ack, ou seja, se veio ack 2 significa que ele j� recebeu o 1 e quer o 2 !!!
				if (idlist_cell->enviadas[pos]->header.ack_counter == 2) { // transformar esse 2 em um define, pra ficar mais f�cil de controlar os par�metros
					tcp_packet *p2 = (tcp_packet *) idlist_cell->enviadas[pos] + sizeof (struct tcp_idlist_message_header);
					ip_send (TCP_TYPE, ip_get_IP(), idlist_cell->dst_ip, (byte *) p2, idlist_cell->enviadas[pos]->header.size); // reenvia o pacote
					resetTimer(id,p->header.acknum);	// reseta o timer pra reenvio, DESCOMENTAR !!!
					idlist_cell->enviadas[pos]->header.ack_counter = 0; // zera counter de acks

				} else {
					idlist_cell->enviadas[p->header.acknum % 5]->header.ack_counter++;
				}
				/* PODE DELETAR !!! ABAIXO
				pos = tcp_idlist_check_numseq(id, p->header.acknum);
				if (pos == WINDOW_SIZE)
					return; // XXX ISSO NUNCA DEVE ACONTECER!!! =P
				if (idlist_cell->enviadas[pos]->header.ack_counter < 3) {
					//resetTimer(id, p->header.acknum-1);
					idlist_cell->enviadas[pos]->header.ack_counter += 1;
				} else { // 3 acks recebidos, reenviar pacote
					//removeTimer(id, p->header.acknum-1);  // remove o timer
					tcp_packet *p2 = (tcp_packet *) idlist_cell->enviadas[pos] + sizeof(struct tcp_idlist_message_header);
					ip_send(TCP_TYPE, ip_get_IP(), idlist_cell->dst_ip, (byte *)p2, idlist_cell->enviadas[pos]->header.size); // reenvia o pacote
					//resetTimer(id,p->header.acknum-1);
					idlist_cell->enviadas[pos]->header.ack_counter = 0; // zera counter de acks

					struct tcp_timeout_args *args =(struct tcp_timeout_args *) kalloc (sizeof (struct tcp_timeout_args));
					args->id = id;
					kmemcpy ((byte *)args->seqnum, (byte *)p->header.seqnum, 4);
					args->timeout = 3000;
					args->repeat = 3;
					//addTrigger (tcp_timeout, 3000*delay_count, (void *) args); // seta outro timer

				}
				*/
			}
		// Mensagem sem flags
		} else if ((idlist_cell->status == CON_ESTABLISHED) && (p->header.flags == NULL)) {
			// imprimir o last ack e o seqnum, pra ver se s�o iguais
// 			kprintf ("\nPACOTE: seqnum(%d) - acknum(%d)", p->header.seqnum, p->header.acknum);
// 			kprintf ("\nMEU: seqnum(%d) - lastack(%d)", idlist_cell->seqnum, idlist_cell->last_ack);
			//kprintf ("\nack %d = %d seqnum",idlist_cell->last_ack, p->header.seqnum);
			// lastack � o seqnum que se espera
			if (idlist_cell->last_ack == p->header.seqnum)
				kprints("\nrecebi msg (inorder) vou mandar ack");
			else
				kprints("\nrecebi msg (outoforder) vou mandar ack");
			struct tcp_idlist_message *msg = tcp_idlist_build_message ((byte *) p + sizeof (struct tcp_header), data_size);
			word i;
			byte *content = (byte *) msg + sizeof (struct tcp_idlist_message);
			kprintf ("\n MSG DO PACOTE TRANSFORMADO, tamanho %d", data_size);
			for (i = 0; i < msg->header.size; i++)
				kprintf ("%c", content[i]);
			kprintf("\n");

			tcp_insert_rec (id, msg, p->header.seqnum); // this function sets my last ack automatically
			tcp_send(id, FLG_ACK, NULL, 0); // ACK the package
		} else {
			kprintf("\nchegou msg fora do esperado !");
		}
	}
}

byte *tcp_get_message (word parameter_port) {
	int i;
	word id = tcp_idlist_get_id_from_port (parameter_port);
	if (id) {
		app_msg *msg = tcp_idlist_get_app_msg (id);
		if (!msg) {
			kprintf ("\nEmpty Mailbox\n");
			return NULL;
		} else {
			byte *content = (byte *) msg + sizeof (struct app_msg_header);
			kprintf ("\n");
			for (i = 0; i < msg->header.size; i++)
				kprintf ("%c", content[i]);
			kprintf ("\n");
			byte *data = (byte *) kalloc (msg->header.size);
			kmemcpy (content, data, msg->header.size);
			kfree (msg);
			return data;
		}
	} else
		return NULL;
}

//Adds triggers to re-send messagens without ACK

//void tcp_timeout(word id, dword seqnum, dword timeout, byte repeat) {
void tcp_timeout (void *args) {
	kprintf("\nTIMEOUT");
	struct tcp_timeout_args *args1;
	args1 = (struct tcp_timeout_args *) args;
	//Get the right idlist_entry
	struct tcp_idlist_entry *idlist_cell = tcp_idlist_check_id(args1->id);
	int i;
	if (idlist_cell == NULL)
		return;
	//Look for the right seqnum
	for (i = 0; i < WINDOW_SIZE; i++) {

		if (idlist_cell->enviadas[i] == NULL)
			continue;
		tcp_packet *p2 = (tcp_packet *) idlist_cell->enviadas[i] + sizeof(struct tcp_idlist_message_header);
		kprintf ("\n%d = %d",p2->header.seqnum, args1->seqnum);
		if (p2->header.seqnum == args1->seqnum) {
			ip_send(TCP_TYPE, ip_get_IP(), idlist_cell->dst_ip, (byte *)p2, idlist_cell->enviadas[i]->header.size);
			//idlist_cell->enviadas[i]->header.repeat--;
			//if (idlist_cell->enviadas[i]->header.repeat > 0) {
				//int timeout_ticks = args1->timeout * delay_count;
			resetTimer(args1->id, args1->seqnum);
				// Aloca mem�ria pro args2 e preenche seus campos
				//struct tcp_timeout_args *args2 =(struct tcp_timeout_args *) kalloc (sizeof (struct tcp_timeout_args));
				// id -> word(2bytes) | repeat -> byte (1byte)
				// ponteiro tem 4 bytes o_O
				//args2->id = args1->id;
				//args2->seqnum = args1->seqnum;
				//args2->timeout = args1->timeout;
				//args2->repeat = args1->repeat;
				//kmemcpy ((byte *)args2, (byte *)args1, 11);
				//addTrigger (tcp_timeout, timeout_ticks, (void *) args2);
			}
			break;
	}
}

// pos � o ack recebido - 1
bool tcp_remove_sent (word id, byte pos) {
	struct tcp_idlist_entry *idlist_cell = tcp_idlist_check_id(id);

	if (idlist_cell->enviadas[pos] != NULL) {
		kfree (idlist_cell->enviadas[pos]);
		idlist_cell->enviadas[pos] = NULL;
		return true;
	} else {
		return false;
	}


}

// inserir pacote inteiro, na pos = numseq%WINDOW_SIZE;
bool tcp_insert_sent (word id, struct tcp_idlist_message *msg, dword numseq) {
	struct tcp_idlist_entry *idlist_cell = tcp_idlist_check_id(id);
	if (idlist_cell) {
		byte pos;

		pos = numseq % 5;
		if (idlist_cell->enviadas[pos] == NULL) {
			idlist_cell->enviadas[pos] = msg;
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

//recebe o id da conex�o; uma struct idlist_message com os dados; e o numseq da mensagem recebida

void tcp_insert_rec (word id, struct tcp_idlist_message *msg, dword numseq) {
	bool check = true;
	struct tcp_idlist_entry *idlist_cell = tcp_idlist_check_id(id);
	byte pos;

	pos = numseq % 5;
	kprintf ("\nPOS = %d",pos);
	if ((numseq - idlist_cell->last_ack < 5) && (numseq - idlist_cell->last_ack >= 0)) {
		if (idlist_cell->recebidas[pos] == NULL) {
			kprintf ("\nidlist_cell->recebidas[%d] == NULL",pos);
			idlist_cell->recebidas[pos] = msg;
// 			word i;
// 			byte *content = (byte *) idlist_cell->recebidas[pos] + sizeof (struct tcp_idlist_message_header);
// 			kprintf ("\n TCP_INSERT_REC message, tamanho %d\n", idlist_cell->recebidas[pos]->header.size);
// 			for (i = 0; i < idlist_cell->recebidas[pos]->header.size; i++)
// 				kprintf ("%c", content[i]);
// 			kprintf("\n");
		}
		if (numseq == idlist_cell->last_ack) {
			tcp_idlist_dispose_received(id, pos);
			idlist_cell->last_ack++;
			pos++;
			do {
				if (pos > 4)
					pos = 0;
				if (idlist_cell->recebidas[pos] != NULL) {
					tcp_idlist_dispose_received (id, pos);
					idlist_cell->last_ack++;
					pos++;
				} else {
					check = false;
				}
			} while (check);
		}
	}
}

//manda o id da conex�o e o campo acknum da mensagem recebida!!!
/* PODE DELETAR !!!
void tcp_check_ack (word id, dword acknum) { // TODO fazer essa merda tirar o timer do pacote ACKNOWLEDGED
	bool check = true;
	struct tcp_idlist_entry *idlist_cell = tcp_idlist_check_id(id);
		tcp_packet *p;
		short pos;
		pos = idlist_cell->seqnum % 5;
		acknum--;
	if ((idlist_cell->seqnum - acknum < 5) && (idlist_cell->seqnum - acknum >= 0)) {
		if (idlist_cell->enviadas[pos] != NULL) {
			tcp_idlist_dispose_sent(id, pos);
		}
		pos--;
		do {
			if (pos == -1)
				pos = 4;
			if (idlist_cell->enviadas[pos] != NULL) {
				p = (tcp_packet*) (idlist_cell->enviadas[pos] + sizeof (struct tcp_header)); // CONFERIR !!!
				if (p->header.seqnum == --acknum) {
					tcp_idlist_dispose_sent (id, pos);
				} else {
					check = false;
				}
			} else {
				check = false;
			}
			pos--;
		} while (check);
	}
}
*/

inline int findTimer(word id, dword seqnum) {
	int i;
	struct tcp_timeout_args *args_findTimer;

	// vai jogando o trigger[i] no struct e fazendo o teste, se for igual break
//	for (i = 0; (((word) trigger[i].arg1 == id) && ((word) trigger[i].arg2 == seqnum) && (trigger[i].handler != NULL)); i++)
//		;
	//TODO: T� CERTO ABAIXO ???

	// fazer um remove_timer no timer.c que pegue o �ltimo elemento da fila de processos a serem executados, copie-o no lugar doq vc quer remover e decremente o ntriggers
	for (i = 0; i < MAXTRIGGERS; i++) {
		if (trigger[i].args) {
			args_findTimer = (struct tcp_timeout_args *) trigger[i].args;
			kprintf("\nFIND TIMER: %d = %d ? | %d = %d ?\n",id, args_findTimer->id, seqnum, args_findTimer->seqnum);
			if ((args_findTimer->id == id) && (args_findTimer->seqnum == seqnum)) {
				return i;
			}
		}
	}
	return -1;
}

void resetTimer(word id, dword seqnum) {
	int i;

	i = findTimer (id, seqnum);
	struct tcp_timeout_args *args = trigger[i].args;

	trigger[i].timeout = delay_count * args->timeout;
	args->repeat = 3;
	//trigger[i].timeout = delay_count * (unsigned long)trigger[i].arg3;
	//trigger[i].arg4 = (void *) 3;
}

void removeTimer(word id, dword seqnum) {
	int i;
	kprints("\nvai chamar findTimer");
	//getch();
	i = findTimer(id, seqnum);
	if (i == -1)
		kprintf ("\nfindTimer could not find");
	else
	removeTrigger (i);
}

/* the checksum field must be zero.
 *
 * if packet size isn't multiple of 16 padding bytes will be used to fix it, but they will not be transmitted */
word tcp_do_cksum(byte *p, dword len) {
	dword nleft = len;
	word *w = (word *)p;
	dword sum = 0;
	word answer = 0;

	while (nleft > 1) {
		sum += *w++;
		nleft -= 2;
	}

	if (nleft == 1) {
		*(byte *) (&answer) = *(byte *)w;
		sum += answer;
	}

	sum = (sum >> 16) + (sum & 0xffff);
	sum += (sum >> 16);
	answer = ~sum;

	return answer;
}

bool tcp_test (word id) {
	struct tcp_idlist_entry *idlist_cell = tcp_idlist_check_id(id);
	if (idlist_cell) {
		tcp_packet *p = tcp_build_packet (idlist_cell->sport, idlist_cell->dport, NULL, 2, idlist_cell->last_ack, (byte *)"oi3", kstrlen("oi3"));
		ip_send (TCP_TYPE, ip_get_IP(), idlist_cell->dst_ip, (byte *)p, kstrlen("oi3"));
		idlist_cell->seqnum++;

		p = tcp_build_packet (idlist_cell->sport, idlist_cell->dport, NULL, 1, idlist_cell->last_ack, (byte *)"oi2", kstrlen("oi2"));
		ip_send (TCP_TYPE, ip_get_IP(), idlist_cell->dst_ip, (byte *)p, kstrlen("oi2"));
		idlist_cell->seqnum++;
		return true;
	//	tcp_packet *tcp_build_packet (word sport, word dport, byte flags, dword seqnum, dword acknum, byte *data, dword size)
	}
	return false;
}
