#include "mem.h"
#include "net/utils/idlist.h"
#include "stdio.h"
#include "keyboard.h"  

struct idlist_entry *idlist_alfa;
struct idlist_entry *idlist_omega;

inline void idlist_start () {
    idlist_alfa = NULL;
    idlist_omega = NULL;
}

struct idlist_entry *idlist_add_id (word sport, byte *dst_ip, word dport) {
    // Calculating id        
    word id = net_hash (dport, dst_ip, sport);
    struct idlist_entry *idlist_cell;
	 idlist_cell = idlist_check_id (id);
	 if (!idlist_cell) {
	   // Building idlist_entry cell
    	idlist_cell = (struct idlist_entry*) kalloc (sizeof (struct idlist_entry));
    	idlist_cell->id = id;
    	idlist_cell->dport = dport;
    	idlist_cell->sport = sport;
    	kprintf ("\nid_list_add_id: %d - dport %d - sport %d", id, dport, sport);
    	kmemcpy (idlist_cell->dst_ip, dst_ip, 4);
    	idlist_cell->next = NULL;
    	idlist_cell->msg = NULL;     
    	// Adding idlist_entry cell to the list
    	//  SE N�O EXISTIR UM ID IGUAL NA LISTA !!!, FAZER IF
    	if (!idlist_alfa) { // If the list is empty
    	    kprintf("\nidlist vazio\n");
	 		 idlist_alfa = idlist_cell;
          idlist_omega = idlist_cell;
     	} else {
	  	    kprintf("\nidlist n vazio\n");	
          idlist_omega->next = idlist_cell;
          idlist_omega = idlist_cell;    
     	}
    }
    return idlist_cell;
}

void idlist_print (void){
    struct idlist_entry *cell_seeker; 
   
    if (!idlist_alfa)
        kprints ("\nLista vazia\n");
    else {
        for (cell_seeker = idlist_alfa; cell_seeker; cell_seeker = cell_seeker->next)
            kprintf ("\nID: %d - dport %d - sport %d\n",cell_seeker->id,cell_seeker->dport,cell_seeker->sport);
    }         
}
struct idlist_entry *idlist_check_id (word parameter_id) {
	 kprintf("\ndentro de idlist_check_id\n");
    struct idlist_entry *cell_seeker;
    if (idlist_alfa != NULL) {  // If there's any element on the list 
        for (cell_seeker = idlist_alfa; cell_seeker; cell_seeker = cell_seeker->next) {
		  		kprintf("testando cell_seeker->id (%d) = parameter_id(%d)",cell_seeker->id, parameter_id);
				getch();
            if (cell_seeker->id == parameter_id)   return cell_seeker;    // If found the requested id return it's position
        }
    }
    return NULL; // If there's no element on the list or the requested id was not found, return NULL
}


struct idlist_message *idlist_build_message (byte *parameter_data, dword parameter_size) {
    // Alocating memory to the message
    struct idlist_message *msg = (struct idlist_message *) kalloc (sizeof (struct idlist_message_header) + parameter_size + sizeof (dword));
    // Setting the message
    msg->header.size = parameter_size;
    msg->header.next = NULL;
    msg->header.prev = NULL;
    byte *data = (byte *) msg + sizeof (struct idlist_message_header);            
    kmemcpy (data, parameter_data, parameter_size);
    // Returns the message address
    return msg;
}
// COM PROBLEMA !!!
bool idlist_add_message (word parameter_id, struct idlist_message *parameter_msg) {
      // se id existir, adiciona no final idlist_check_id (retorna o endere�o do idlist_cell)
      // se id n existir, cria e adiciona idlist_add_id (retorna o endere�o do idlist_cell)
    struct idlist_entry *idlist_cell = idlist_check_id (parameter_id);
	 kprintf("\nadd_message ID: %d SP: %d DP: %d\n", idlist_cell->id, idlist_cell->sport, idlist_cell->dport);
    if (idlist_cell != NULL) {  // If the requested id is already registered
        if (idlist_cell->msg != NULL) { // If there're messages on the mailbox
		  		struct idlist_message *msg_seeker;
		  		kprintf ("\nmailbox n vazia, inserindo\n");
            for (msg_seeker = idlist_cell->msg; msg_seeker->header.next != NULL; msg_seeker = msg_seeker->header.next);
            // se existirem mensagens a nova_msg->prev aponta para a antiga �ltima
            parameter_msg->header.prev = msg_seeker;
				msg_seeker->header.next = parameter_msg;
				kprintf("\n\nadd_message: parameter_msg->header.prev = %d \nmsg_seeker = %d \nmsg_seeker->header.next = %d\nparameter_msg = %d", parameter_msg->header.prev, msg_seeker, msg_seeker->header.next, parameter_msg);
				getch();
        } else {    // If there aren't messages on the mailbox
            // se n�o existirem mensagens a nova_msg->prev aponta para ela mesma   
				kprintf("mailbox vazia, inserindo");
            parameter_msg->header.prev = parameter_msg;
            idlist_cell->msg = parameter_msg;
kprintf("\n\nparameter_msg->header.prev = %d\nparameter_msg = %d\nidlist_cell->msg = %d\nparameter_msg = %d",parameter_msg->header.prev, parameter_msg, idlist_cell->msg, parameter_msg);	
	getch();
        }
        return true;    // Message added
    }
    return false;   // Message not added, id probaly not registered
}
            
struct idlist_message *idlist_get_message (word parameter_id) {
    // Let's check if the id is registered
	 kprintf("\nestou em get_message vai chamar idlist_check_id (%d)\n",parameter_id);
	 getch();
    struct idlist_entry *idlist_cell = idlist_check_id (parameter_id);
    kprintf("idlist_cell = %d", idlist_cell);
	getch();
	if (idlist_cell != NULL) {  // If the id is registered
		kprintf("\nidlist_get_message ID: %d  MSG:%d\n", idlist_cell->id, idlist_cell->msg);
		getch();
		if (idlist_cell->msg != NULL){
			struct idlist_message *msg;
			struct idlist_message *msg2;
        	msg = idlist_cell->msg; // Message address is saved
        	idlist_cell->msg = msg->header.next;
			msg2 = idlist_cell->msg;
        	if(msg2 != NULL) {
				msg2->header.prev = msg2;
			}
        	kprints ("Peguei msg aki\n");
		  	getch();
        	return msg;
		  }
    }
    return NULL;          
}

word idlist_get_id_from_port (word parameter_port) {
    kprintf ("porta: %d\n", parameter_port);
    struct idlist_entry *cell_seeker;
	 kprintf("idlist_alfa: %d", idlist_alfa);
	 getch();
    if (idlist_alfa != 0) {  // If there's any element on the list 
        for (cell_seeker = idlist_alfa; cell_seeker; cell_seeker = cell_seeker->next) {		  		
				kprintf("\nverificando %d\n", cell_seeker->sport);
				getch();
            if (cell_seeker->sport == parameter_port) {
					kprintf("Retornou %d", cell_seeker->id);
					return cell_seeker->id;    // If found the requested id return it's position
				}
        }
    } else {
	 	kprintf ("\nmailbox vazia ou id n encontrado\n");
	 	getch();
    	return 0; // If there's no element on the list or the requested id was not found, return NULL		
	 }
	 return 0;
}

word net_hash (word sport, byte *ip, word dport) {
    word id = sport + dport;
    int i;
    for (i = 0; i < 4; i++) {
        id += (word) i * ip[i];
    }
   return id;
} 
                                

