#include "mem.h"
#include "net/utils/tcp_idlist.h"
#include "net/transp/udp.h"
#include "stdio.h"
#include "keyboard.h"  
#include "net/transp/tcp.h"

// Criamos os ponteiros para as listas de conex�es do TCP
// alfa - in�cio da lista
// omega - final da lista
struct tcp_idlist_entry *tcp_idlist_alfa;
struct tcp_idlist_entry *tcp_idlist_omega;

// Inicia as listas de conex�es do TCP 
inline void tcp_idlist_start () {
	tcp_idlist_alfa = NULL;
	tcp_idlist_omega = NULL;
}

// Adiciona uma nova conex�o � lista de conex�es
struct tcp_idlist_entry *tcp_idlist_add_id (word sport, byte *dst_ip, word dport) {
	int i=0; // contador
	struct tcp_idlist_entry *idlist_cell; // nova conex�o
	word id = tcp_net_hash (dport, dst_ip, sport); // calcula o id da conex�o
	idlist_cell = tcp_idlist_check_id (id); // verifica a conex�o j� existe
	if (!idlist_cell) {
		// Montando a estrutura da nova conex�o
		idlist_cell = (struct tcp_idlist_entry*) kalloc (sizeof (struct idlist_entry));
		idlist_cell->id = id;
		idlist_cell->dport = dport;
		idlist_cell->sport = sport;
		idlist_cell->seqnum = 0; // depois fazer ele pegar um n�mero randomico ?
		idlist_cell->last_ack = 0; // recebe rand(), pois determina qual ser� o n�mero de sequencia do outro lado
		idlist_cell->status = CON_LISTEN; // j� criamos a entrada na idlist com o status listen pois s� vamos inserir algu�m na lista se for pra ficar ouvindo
		idlist_cell->app_msg_alfa = NULL;
		idlist_cell->app_msg_omega = NULL;
		//kprintf ("\nid_list_add_id: %d - dport %d - sport %d", id, dport, sport);
		kmemcpy (idlist_cell->dst_ip, dst_ip, 4);
		idlist_cell->next = NULL;
		idlist_cell->prev = NULL;
		// inicializando o buffer de mensagens da conex�o
		for(i = 0; i < WINDOW_SIZE; i++) {
			idlist_cell->enviadas[i] = NULL;
			idlist_cell->recebidas[i] = NULL;
		}
		// Adicionando a nova conex�o � lista
		// J� foi verificado se o id j� est� na lista
		if (!tcp_idlist_alfa) { // If the list is empty
			//kprintf("\nidlist vazio\n");
			tcp_idlist_alfa = idlist_cell;
			tcp_idlist_omega = idlist_cell;
			idlist_cell->prev = tcp_idlist_alfa;
		} else {
			//kprintf("\nidlist n vazio\n");
			idlist_cell->prev = tcp_idlist_omega;
			tcp_idlist_omega->next = idlist_cell;
			tcp_idlist_omega = idlist_cell;
		}
	}
	return idlist_cell;
}

// Imprime a lista de conex�es
void tcp_idlist_print (void){
	struct tcp_idlist_entry *cell_seeker;
	
	if (!tcp_idlist_alfa){
		kprints ("\nLista vazia");
	}
	else {
		for (cell_seeker = tcp_idlist_alfa; cell_seeker; cell_seeker = cell_seeker->next)
		kprintf ("\nID: %d - dport %d - sport %d : %d",cell_seeker->id,cell_seeker->dport,cell_seeker->sport, cell_seeker->status);
	}
	kprintf("\n");
}

// Verifica se a conex�o j� existe, caso exista retorna o endere�o dela na lista, caso contr�rio retorna NULL
struct tcp_idlist_entry *tcp_idlist_check_id (word parameter_id) {
	//kprintf("\ndentro de tcp_idlist_check_id\n");
	struct tcp_idlist_entry *cell_seeker;
	if (tcp_idlist_alfa != NULL) {  // If there's any element on the list
		for (cell_seeker = tcp_idlist_alfa; cell_seeker; cell_seeker = cell_seeker->next) {
			//kprintf("testando cell_seeker->id (%d) = parameter_id(%d)",cell_seeker->id, parameter_id);
			if (cell_seeker->id == parameter_id)
				return cell_seeker;    // If found the requested id return it's position
		}
	}
	return NULL; // If there's no element on the list or the requested id was not found, return NULL
}

// no caso de mensagens a serem inseridas no tcp_idlist->recebidas o parameter_data s�o os dados, no caso da mensagem ser inserida no tcp_idlist_enviadas o parameter_data � o tcp_packet
struct tcp_idlist_message *tcp_idlist_build_message (byte *parameter_data, dword parameter_size) {
	// Alocating memory to the message

	kprintf ("\ntcp_idlist_build_message: parameter_size = %d",parameter_size);
	struct tcp_idlist_message *message = (struct tcp_idlist_message *) kalloc (sizeof (struct tcp_idlist_message_header) + (parameter_size*sizeof(byte))); // CONFERIR SE A ALOCA��O EST� CORRETA !!!
	// Setting the message
	message->header.size = parameter_size;
	message->header.timeout = 30;
	message->header.repeat = 3;
	// passamos o endere�o de nossos dados para o msg->p
	byte *data = (byte *) message + sizeof (struct tcp_idlist_message_header);
	kmemcpy (data, parameter_data, parameter_size);
	// Returns the message address
	return message;
}
/*
// Adiciona uma nova mensagem, se enviadas = 1 adiciona no vetor de enviadas, se igual a zero adiciona no vetor de recebidas
bool tcp_idlist_add_message (byte enviadas, word id, struct tcp_idlist_message *msg, byte posicao) {
	// se id existir, adiciona na posi��o e retorna true ou false
	// o caso de id inexistente n�o ocorre no tcp
	struct tcp_idlist_entry *idlist_cell = tcp_idlist_check_id (id);
	//kprintf("\nadd_message ID: %d SP: %d DP: %d\n", idlist_cell->id, idlist_cell->sport, idlist_cell->dport);
	if (idlist_cell != NULL) {  // Only for security reasons
		if (enviadas) {
			if (idlist_cell->enviadas[posicao] != NULL) { // If there is a message where we want to put this
				return false; // Message not added, another one were in it's place
			} else {
				idlist_cell->enviadas[posicao] = msg; // Message added
				return true;
			}
		} else {
			if (idlist_cell->recebidas[posicao] != NULL) { // If there is a message where we want to put this
				return false; // Message not added, another one were in it's place
			} else {
				idlist_cell->recebidas[posicao] = msg; // Message added
				return true;
			}
		}

	} else {
		return false;
	}
}
*/
// d� dispose na msg e salva no buffer da aplica��o, e vai mudar o nome para dispose.. que � chamada qdo uma mensagem � acked na sequencia correta
/*struct tcp_idlist_message *idlist_get_message (word parameter_id) {
    // Let's check if the id is registered
	//kprintf("\nestou em get_message vai chamar tcp_idlist_check_id (%d)\n",parameter_id);
	getch();
    struct tcp_idlist_entry *idlist_cell = tcp_idlist_check_id (parameter_id);
    //kprintf("idlist_cell = %d", idlist_cell);
	getch();
	if (idlist_cell != NULL) {  // If the id is registered
		//kprintf("\nidlist_get_message ID: %d  MSG:%d\n", idlist_cell->id, idlist_cell->msg);
		  getch();
		  if (idlist_cell->msg != NULL){
				struct tcp_idlist_message *msg;
				struct tcp_idlist_message *msg2;
        			msg = idlist_cell->msg; // Message address is saved
        			idlist_cell->msg = msg->header.next;
				msg2 = idlist_cell->msg;
        			if(msg2 != NULL) {
					msg2->header.prev = msg2;
				}
        		//kprints ("Peguei msg aki\n");
				getch();
        			return msg;
		  }
	}
	return NULL;
}
*/

// Retorna o ID da conex�o que est� registrada em determinada porta
word tcp_idlist_get_id_from_port (word parameter_port) {
	//kprintf ("porta: %d\n", parameter_port);
	struct tcp_idlist_entry *cell_seeker;
	//kprintf("tcp_idlist_alfa: %d", tcp_idlist_alfa);
	if (tcp_idlist_alfa != 0) {  // If there's any element on the list 
		for (cell_seeker = tcp_idlist_alfa; cell_seeker; cell_seeker = cell_seeker->next) {
			//kprintf("\nverificando %d\n", cell_seeker->sport);
			if (cell_seeker->sport == parameter_port) {
				//kprintf("Retornou %d", cell_seeker->id);
				return cell_seeker->id;    // If found the requested id return it's position
			}
		}
	} else {
		//kprintf ("\nmailbox vazia ou id n encontrado\n");
		return 0; // If there's no element on the list or the requested id was not found, return NULL
	}
	return 0;
}

// Remove determinada conex�o da lista de conex�es
bool tcp_idlist_remove_id (word id){
	int i;
	struct tcp_idlist_entry *idlist_cell = tcp_idlist_check_id (id);
	if (idlist_cell != NULL) {  // If this ID is in the list
		struct tcp_idlist_entry *cell_seeker = idlist_cell->prev;
		cell_seeker->next = idlist_cell->next;
		for (i = 0; i < WINDOW_SIZE; i++) { // para garantir que vamos desalocar todas as msgs q porventura possam estar sendo apontadas
			kfree (idlist_cell->recebidas[i]);
			kfree (idlist_cell->enviadas[i]);
		}
		kfree(idlist_cell); // desalocando a estrutura do tcp_idlist_entry
		return true;
	} else {
		return false;
	}
}

// Desaloca determinada msg (recebida) de determinada conex�o
void tcp_idlist_dispose_received (word id, byte pos) {	
	struct tcp_idlist_entry *idlist_cell = tcp_idlist_check_id (id);
	if (idlist_cell) {
		byte *content = (byte *) idlist_cell->recebidas[pos] + sizeof (struct tcp_idlist_message_header);

// 		word i;
// 		kprintf ("\nDISPOSE_RECEIVED, tamanho: %d\n", idlist_cell->recebidas[pos]->header.size);
// 		for (i = 0; i < idlist_cell->recebidas[pos]->header.size; i++)
// 			kprintf ("%c", data[i]);
// 		kprintf ("\n");
/*

			word i;
			
			kprintf ("\n TCP_INSERT_REC message, tamanho %d\n", idlist_cell->recebidas[pos]->header.size);
			for (i = 0; i < idlist_cell->recebidas[pos]->header.size; i++)
				kprintf ("%c", content[i]);
			kprintf("\n");*/


		app_msg *msg = tcp_idlist_build_app_msg (content, idlist_cell->recebidas[pos]->header.size);
		//tcp_idlist_add_app_msg (msg, idlist_cell->app_msg_alfa, idlist_cell->app_msg_omega);
		tcp_idlist_add_app_msg (id, msg);
		kfree (idlist_cell->recebidas[pos]);
		idlist_cell->recebidas[pos] = NULL;
		return;
	}
	return;
}

// Desaloca determinada msg (enviada) de determinada conex�o
void tcp_idlist_dispose_sent (word id, byte pos) {
	struct tcp_idlist_entry *idlist_cell = tcp_idlist_check_id (id);
	kfree (idlist_cell->enviadas[pos]);
	idlist_cell->enviadas[pos] = NULL;
	return;
}

// Monta a mensagem para o buffer da aplica��o
app_msg *tcp_idlist_build_app_msg (byte *parameter_data, dword parameter_size) {
	// Alocating memory to the message
	app_msg *msg = (app_msg *) kalloc (sizeof (struct app_msg_header) + (parameter_size * sizeof (byte)));
	// Setting the message
	msg->header.size = parameter_size;
	msg->header.next = NULL;
	msg->header.prev = NULL;
	byte *data = (byte *) msg + sizeof (struct tcp_idlist_message_header);
	kmemcpy (data, parameter_data, parameter_size);
	// Returns the message address
	return msg;
}

// Adiciona uma mensagem no buffer da aplica��o
// supposed to work
//bool tcp_idlist_add_app_msg (app_msg *msg, app_msg *app_msg_alfa, app_msg *app_msg_omega) {
bool tcp_idlist_add_app_msg (word id, app_msg *msg) {
	//se id existir, adiciona no final tcp_idlist_check_id (retorna o endere�o do idlist_cell)
	// se id n existir, cria e adiciona tcp_idlist_add_id (retorna o endere�o do idlist_cell)
	struct tcp_idlist_entry *idlist_cell = tcp_idlist_check_id (id);

	if (idlist_cell) {
		if (idlist_cell->app_msg_alfa != NULL) { // If there're messages on the mailbox
			idlist_cell->app_msg_omega->header.next = msg;
			msg->header.prev = idlist_cell->app_msg_omega;
			idlist_cell->app_msg_omega = msg;
			msg->header.next = idlist_cell->app_msg_omega;
			return true;
		} else { // se n�o tem msgs na mailbox
		idlist_cell->app_msg_alfa = msg;
		idlist_cell->app_msg_omega = msg;
		msg->header.prev = idlist_cell->app_msg_alfa;
		return true;
		}
	}
	return false;
}


app_msg *tcp_idlist_get_app_msg (word id) {
	// Let's check if the id is registered
	//kprintf("\nestou em get_message vai chamar tcp_idlist_check_id (%d)\n",id);
	struct tcp_idlist_entry *idlist_cell = tcp_idlist_check_id (id);
	//kprintf("idlist_cell = %d", idlist_cell);
	if (idlist_cell != NULL) {  // If the id is registered
		//kprintf("\nidlist_get_message ID: %d  MSG:%d\n", idlist_cell->id, idlist_cell->app_msg_alfa);
		if (idlist_cell->app_msg_alfa != NULL) { // se existem msgs
			kprintf ("\napp_msg_alfa != NULL");
			app_msg *msg;
// 			app_msg *msg2;
			msg = idlist_cell->app_msg_alfa; // Message address is saved
			if (idlist_cell->app_msg_alfa == idlist_cell->app_msg_omega) {
				idlist_cell->app_msg_alfa = idlist_cell->app_msg_omega = NULL;
			} else {
				idlist_cell->app_msg_alfa = msg->header.next;
			}/*
			msg2 = idlist_cell->app_msg_alfa;
			if(msg2 != NULL) 
				msg2->header.prev = idlist_cell->app_msg_alfa;*/
		
			return msg;
		} else {
			return NULL;
		}
	}
	return NULL;
}

/*
byte tcp_idlist_check_numseq (word id, dword numseq) {
	int i = 0;
	struct tcp_idlist_entry *idlist_cell = tcp_idlist_check_id (id);
	tcp_packet *p;
	// Conferir se o ID existe
	if (idlist_cell) {
		for (i=0; i<5; i++) {
			if (idlist_cell->enviadas[i] != NULL) {
				p = (tcp_packet*) (idlist_cell->enviadas[i] + (sizeof (struct tcp_idlist_message_header))); // CONFERIR !!!
				if (p->header.seqnum == numseq) {
					return i; // retorna a posi��o caso encontre
				}
			}
		}
	}
	return WINDOW_SIZE; // caso n�o encontre
}
*/
// Calcula o ID para determinada conex�o. Gambi, s� pra ter um ID mesmo
word tcp_net_hash (word sport, byte *ip, word dport) {
	word id = sport + dport;
	int i;
	
	for (i = 0; i < 4; i++) {
		id += (word) i * ip[i];
	}
	return id;
}
