/*******************************************************************************

  dorothy/net/nets/ip.c

  Copyright (C) 2005 D-OZ Team

  (IP) Dorothy Network Protocol

  OBS: get_message blows it all when the list is empty. why? because it was
  missing a list_empty verification :)

  OBS: IP is very prone to mailbox attacks. We need to improve it by limiting
  mailboxes size.

*******************************************************************************/

#include "net/nets/ip.h"
#include "net/addrs/arp.h"
#include "net/dlink/eth.h"
#include "mem.h"
#include "stdio.h"
#include "string.h"
#include "net/transp/udp.h"
#include "net/transp/tcp.h"
#include "timer.h"
#include "stdio.h"
#include "8259a.h"
#include "io.h"

byte _IP[4] = {0, 0, 0, 0};
/* how could broadcast be done? */
byte _IP_bcast[4] = {255, 255, 255, 255};

void ip_send (byte protocol, byte *src_ip, byte *dst_ip, byte *data, dword size) {
	// Conserta o tamanho do pacote udp
	if (protocol == UDP_TYPE) {
		size += sizeof (struct udp_header);
	} else if (protocol == TCP_TYPE) {
		size += sizeof (struct tcp_header);
	}
//		kprintf("\nRespondendo p/ %d.%d.%d.%d\n",dst_ip[0], dst_ip[1], dst_ip[2], dst_ip[3]);
    	ip_packet *p = ip_build_message(protocol, src_ip, dst_ip, data, size);
    	//ip_send_eth_message (p);
		//TODO: TEM Q CONSTRUIR A ESTRUTURA PARA RECEBER ARGS NO IPSENDETHMSG
		addTrigger(ip_send_eth_message, 20, (void *) p);
		//kprints("\nPassou do addTrigger do ip");
     	kfree (p);
}

/* builds an ip message where data is the protocol above ip */
ip_packet *ip_build_message (byte protocol, byte *src_ip, byte *dst_ip, byte *data, dword size) {
	ip_packet *p = (ip_packet *) kalloc (sizeof (struct ip_header) + size * sizeof (byte));
	/* content points to header's end */
   	byte *content = (byte *) p + sizeof (struct ip_header);
	/* loading packet */
   	p->header.protocol = protocol;
	kmemcpy (p->header.src_ip, src_ip, 4);
	kmemcpy (p->header.dst_ip, dst_ip, 4);
	p->header.size  = size;
	kmemcpy (content, data, size);

	return p;
}

/* sends an ip message via ethernet */
//void ip_send_eth_message (ip_packet *p) {
void ip_send_eth_message (void *p_void) {
	kprintf("Sending ip eth message\n");
//while(1);
	byte hwdaddr[6];
	ethernet_frame *frame;
	byte *content;
	// ser� q vai dar certo ?
	ip_packet *p = (ip_packet *) p_void;
	// habilita interrup��es
	//INTS(true);
	INTS(true);
	// fala pro pic permitir novas interrup��es... (limpa o pic para permitir...)
	outportb (MASTER, EOI);
	/* gets destination MAC address */
	if (!arp_lookup_eth_address (ARP_IP, 4, p->header.dst_ip, hwdaddr)) {
		kprints ("\nip: timeout. address not found.");
		return;
	}
	kprintf("ARP found the target IP, target MAC 0x%X, 0x%X, 0x%X, 0x%X, 0x%X, 0x%X!\n",hwdaddr[0],hwdaddr[1],hwdaddr[2],hwdaddr[3],hwdaddr[4],hwdaddr[5]);
	/* assemble frame */
	frame = (ethernet_frame *) kalloc (sizeof (ethernet_frame)
			                           + sizeof (struct ip_header)
						                + p->header.size);
	content = (byte *) p + sizeof (struct ip_header);
	kmemcpy (frame->header.src_address, eth_get_MAC(), 6);
	kmemcpy (frame->header.dst_address, hwdaddr, 6);
	kmemcpy ((byte *) frame + sizeof (ethernet_frame), (byte *) p, sizeof (struct ip_header) + p->header.size);
	frame->header.type = htons (IP_TYPE);

	/* sends it */
	eth_send ((byte *) frame, sizeof (ethernet_frame)
			                 + sizeof (struct ip_header)
							 + p->header.size);
	kfree (frame);

}

/* handles ip packets and stores them into mailboxes */
void ip_handle_packet (ip_packet *p) {
	/* is the message broadcasted? */
	/* is the message really for us? */
	dword data_size = 0;
	if ((kstrncmp ((const char *)p->header.dst_ip,(const char *) _IP_bcast, 4) != 0) && (kstrncmp ((const char *)p->header.dst_ip,(const char *) _IP, 4) != 0))
		return;
	switch ((word) p->header.protocol) {
        	case UDP_TYPE:
		// Verificar se a chamada abaixo est� correta
		// O src_ip � o ip de quem mandou a msg para n�s
			udp_handle_packet (p->header.src_ip, (udp_packet *)((byte *) p + sizeof(struct ip_header)));
			break;
		case TCP_TYPE:
			// tive q adicionar o tamanho, sen�o n dava pra pegar
			data_size = p->header.size - sizeof (struct tcp_header);
        		tcp_handle_packet (p->header.src_ip, (tcp_packet *)((byte *) p + sizeof(struct ip_header)), data_size);
			break;
	}
	return;
}

/* registers an IP address */
void ip_register_IP (byte *address) {
	kmemcpy (_IP, address, 4);
	arp_register_address (ARP_IP, 4, _IP);
	arp_create_eth_lookup (ARP_IP, 4, _IP_bcast, 10);
}

/* gets the IP address */
byte *ip_get_IP () {
	return _IP;
}
