/*******************************************************************************

  dorothy/net/eth.c
 
  Copyright (C) 2005 D-OZ Team

  I changed this code a bit, to use a nic structure to perform
  network operations.
  
*******************************************************************************/

#include "mem.h"
#include "net/dlink/eth.h"
#include "net/addrs/arp.h"
#include "net/nets/ip.h"
#include "defines.h"
#include "stdio.h"
#include "idt.h"
#include "8259a.h"

#include "irqwrapper.h"


/*void (*_eth_send)(void *packet, word length) = NULL;
byte _MAC[6] = {0, 0, 0, 0, 0, 0};

void eth_register_send (void (*function)(void *packet, word length)) {
	_eth_send = function;
}

void eth_register_MAC (byte *address) {
	kmemcpy (_MAC, address, 6);
}*/

nic * _nic = NULL;

void eth_register_nic(nic * nic) {
	_nic = nic;
	INTS(false);
	int irq;
	if (nic->pci_irq)
		irq = nic->pci_irq;
	
	/* TODO currently all PCI IRQs are mapped to the network irq, change that */

	//addInt (irq, nic->interrupt, 0);
	addInt (irq, int_net_internal, 0);
	kprintf("Unmasking %X\n", irq);

/* NOTE something is not quite right in unmaskIRQ, the current
	implementation prevents unmasking of any irq but ALL, so
	unmask all for now */
	unmaskIRQ (0xFF);
	
	nic->receive = eth_handle_frame;

	INTS(true);
}

void eth_send (void *packet, word length) {
	/* seems that 'packet' is actually an ethernet frame, I am not
	   sure if the proper place to build the frame is in the upper
	   layer */
	if (_nic->send) {
		kprintf("Sending frame with size %d\n",length); 
		/* Ethernet eth_handle_framehas a minimum frame size */
		if (length < ETH_MIN_SIZE) {
			void *minpacket = kalloc (ETH_MIN_SIZE);
			kmemcpy (minpacket, packet, length);
			//_eth_send (minpacket, ETH_MIN_SIZE);
			_nic->send(minpacket, ETH_MIN_SIZE);
			kfree (minpacket);
		} else 
			_nic->send(packet, length);
			//_eth_send (packet, length);
	}
}

void eth_handle_frame (void *frame) {
	/* this is handled in irq, all subsequent functions must be
		sync-safe */
	struct ethernet_header *header = (struct ethernet_header *)(frame);
	kprintf("Ethernet frame receive, header type is 0x%X\n",ntohs(header->type));
	/* we need to swap the bytes in type - where? */
	switch (ntohs(header->type)) {
		case ARP_TYPE:
			kprints ("ETH_HANDLE_FRAME: passando para arp_handle_packet\n");
			arp_handle_packet (frame + sizeof (struct ethernet_header));
			break;
		case IP_TYPE:
			kprints ("IP_HANDLE_FRAME: passando para ip_handle_packet\n");
			ip_handle_packet (frame + sizeof (struct ethernet_header));
			break;
		default:
			kprintf ("eth: unknown packet type: %d.\n", ntohs(header->type));
	}
}

byte *eth_get_MAC (void) {
	//return _MAC;
	return _nic->MAC;
}

