#include "heap.h"
#include "stdio.h"
#include "defines.h"
#include "mem.h"

void swap_heap(unsigned long * p1,unsigned long * p2, void * temp, dword size) {
	kmemcpy((byte*)temp,(byte*)p2,size);
	kmemcpy((byte*)p2,(byte*)p1,size);
	kmemcpy((byte*)p1,(byte*)temp,size);	
}

/*
	Checks and replaces the heap entry with its parents, to ensure
	that the heap properties are maintained 
*/
void * heap_up(unsigned long * heap_root, unsigned long * curr,void * temp, unsigned int size) {
	if (curr == heap_root) 
		return curr;
	unsigned long key = *curr;
	unsigned long delta = (unsigned long)((byte*)curr-(byte*)heap_root);
	unsigned long *parent_pos = heap_root+((((delta/size)+1)/2)-1); /* both 2v+1 and 2v+2 array heap stuff can be reversed in this formulae */
	/* get parent key */
	unsigned long key_parent = *parent_pos; 
	/* check if swap is needed and move on */
	if (key > key_parent) {
		swap_heap(curr,parent_pos,temp,size);
		curr = heap_up(heap_root,parent_pos,temp,size);
		return (void*)parent_pos;
	}
	return (void*)curr;
	
}

void *heap_down(unsigned long * heap_root, unsigned long * curr,void * temp, unsigned int size,
				dword h_size) {
	unsigned long delta = (unsigned long)((byte*)curr-(byte*)heap_root)/size;
	unsigned long psize = size/sizeof(unsigned long *);
//kprintf("delta = %i %i %i\n",delta,size,h_size);
	unsigned long * pos = curr;
	if (2*delta + 2 < h_size) {
		dword lchild = *(heap_root+(2*delta + 1));
		dword rchild = *(heap_root+(2*delta + 2));

		if (lchild > rchild && lchild > *curr) {
			pos = (heap_root+(2*delta + 1));
			swap_heap(curr,pos,temp,size);
			pos = heap_down(heap_root,heap_root+(2*delta + 1),temp,size,h_size);
		}
		else if (rchild > lchild && rchild > *curr) {
			pos = (heap_root+(2*delta + 2));
			swap_heap(curr,pos,temp,size);
			pos = heap_down(heap_root,heap_root+(2*delta + 2),temp,size,h_size);
		}
	}
	else if (2*delta + 1 < h_size) {
		dword lchild = *(heap_root+(2*delta + psize));
		if (lchild > *curr) {
			pos = (heap_root+(2*delta + psize));
			swap_heap(curr,pos,temp,size);
			pos = heap_down(heap_root,heap_root+(2*delta + psize),temp,size,h_size);
		}
	}
	return pos;
}

unsigned long * insert_heap(unsigned long * heap_root, void * data, unsigned int size, 
				unsigned long * current, void * temp) {
	unsigned long * curr = (unsigned long *)((byte*)heap_root+(*current*size));
	kmemcpy((byte*)curr,(unsigned char *)data,size);
	/* check if we need to move the key up (recursively) */
	heap_up(heap_root,curr,temp,size);

	*current = *current + 1;
	/* TODO return address where entry is now (even after swaps) */
	return 0;
}

void remove_heap(unsigned long * heap_root, unsigned long * h_data ,void * temp, unsigned int size,
				unsigned long * h_size) {
	kmemcpy((byte*)h_data,(byte*)(heap_root+(*h_size-1)),size);
	unsigned long * new_heap_pos = heap_up(heap_root,h_data,temp,size);
//kprintf("%i %i\n",new_heap_pos,h_data);
	if (new_heap_pos == h_data)
		heap_down(heap_root,h_data,temp,size,*h_size);
	*h_size = *h_size - 1;
}