#include "avl.h" 
#include "stdio.h"
#include "defines.h"
#include "mem.h"

#define INSERT_NODE_AVL(ptr,node) ptr = (avl_tree_node *)node; \
								((avl_tree_node *)node)->left = 0; \
								((avl_tree_node *)node)->right = 0;


unsigned int count_depth(avl_tree_node * node, unsigned int * factor) {
	if (!node)
		return 0;
	unsigned int ldepth = count_depth(node->left,factor);
	unsigned int rdepth = count_depth(node->right,factor);
	*factor = ldepth - rdepth;
	return ldepth > rdepth ? ldepth+1 : rdepth + 1;
}

#define NODE_INDEX(node) *(unsigned long *)(node+1)
#define MAX(x,y) (x > y ? x : y)

#define CHECK_ROOT(root,node,target) \
		if (root == node)	\
			root = target;

#define double_rotate_l(parent,root,node,target) \
		node->left->right = target->left; \
		avl_tree_node * temp = node->left; \
		node->left = target->right; \
		target->right = node; \
		target->left = temp; \
		if (parent && parent->right == node) parent->right = target; \
		if (parent && parent->left == node) parent->left = target; \
		CHECK_ROOT(root,node,target)

#define double_rotate_r(parent,root,node,target) \
		node->right->left = target->right; \
		avl_tree_node * temp = node->right; \
		node->right = target->left; \
		target->left = node; \
		target->right = temp; \
		if (parent && parent->right == node) parent->right = target; \
		if (parent && parent->left == node) parent->left = target; \
		CHECK_ROOT(root,node,target)

#define single_rotate_l(parent,root,node,target) \
		node->left = target->right; \
		target->right = node;	\
		if (parent && parent->right == node) parent->right = target; \
		if (parent && parent->left == node) parent->left = target; \
		CHECK_ROOT(root,node,target)

#define single_rotate_r(parent,root,node,target) \
		node->right = target->left; \
		target->left = node;\
		if (parent && parent->right == node) parent->right = target; \
		if (parent && parent->left == node) parent->left = target; \
		CHECK_ROOT(root,node,target)

void avl_bsearch_and_insert(avl_tree_node ** root, avl_tree_node * current_node, avl_tree_node * target_node, 
							unsigned int * rotated, unsigned int * branch_depth, int * branch_factor, avl_tree_node * parent) {
	unsigned int side = (NODE_INDEX(target_node) <= NODE_INDEX(current_node));
	avl_tree_node ** targ = side ? &current_node->left : &current_node->right;

	if (*targ) {
		avl_bsearch_and_insert(root,*targ,target_node,rotated,branch_depth,branch_factor,current_node);
	}
	else {
		INSERT_NODE_AVL(*targ,target_node);
		*branch_depth = 1;
		//return;
	}
	/* if any rebalancing occurred, we dont need to check anything else, just move on */
	if (*rotated)
		return;

	unsigned int lfactor=0,rfactor=0;
	unsigned int ldepth = current_node->left ? (side ? *branch_depth : count_depth(current_node->left,&lfactor)) : 0;
	unsigned int rdepth = current_node->right ? (side ? count_depth(current_node->right,&rfactor) : *branch_depth) : 0;
	if (side)
		lfactor = *branch_factor;
	else
		rfactor = *branch_factor;

	int dif = ldepth - rdepth;

	avl_tree_node * tmp;
	if (dif < -1) {
		if (rfactor < 0) {
			tmp = current_node->left;
			single_rotate_r(parent,*root,current_node,tmp);
		}
		else {			
			tmp = current_node->right->left;
			double_rotate_r(parent,*root,current_node,tmp);
		}
		*rotated = 1;
	}
	else if (dif > 1) {
		if (lfactor < 0) {
			tmp = current_node->right;
			single_rotate_l(parent,*root,current_node,tmp);
		}
		else {
			tmp = current_node->left->right;
			double_rotate_l(parent,*root,current_node,tmp);
		}
		*rotated = 1;
	}
	*branch_depth = MAX(ldepth,rdepth)+1;
	*branch_factor = dif;
}

void insert_avl_tree(avl_tree_node ** root, avl_tree_node * data) {
	if (*root == 0) {
		INSERT_NODE_AVL(*root,data);
		return;
	}
	unsigned int tmp=0, tmp2=0, tmp3=0;
	avl_bsearch_and_insert(root, *root, data, &tmp,&tmp2,(int*)&tmp3,0);
}

void *remove_avl_tree(avl_tree_node ** root, unsigned long index) {
	avl_tree_node * temp = *root;
	avl_tree_node * parent = 0;
	dword side = 0;
	while(1) {
		dword p_idx = (dword)NODE_INDEX(temp);
		if (p_idx == index) {
			avl_tree_node * temp2 = temp->left;
			avl_tree_node * temp2p = temp;
			if (temp2) {
				while (temp2->right) {
					temp2p = temp2;
					temp2 = temp2->right;				
				}
			}
			else {
				if (temp->right)
					temp2= temp->right;
			}
			if (!parent) {
				*root = temp2;
			}
			else {
				if (side) 
					parent->left = temp2;
				else
					parent->right = temp2;
			}
			if (temp2) {
				temp2->left = (temp2 == temp->left ? 0 : temp->left);
				temp2->right = (temp2 == temp->right ? 0 : temp->right);
			}
			if (temp2p) {
				temp2p->right = 0;
			}
			return temp;
		}
		parent = temp;
		if (p_idx > index) {
			side = 1;
			temp = temp->left;
		}
		else {
			side = 0;
			temp = temp->right;
		}
	}

	/* TODO rebalance! */
}

/*
	This function searchs the avl tree, in a binary search. However we never
	return empty here, if the data is not found, the last searched node is
	returned.
*/
void * avl_tree_bsearch_non_empty(avl_tree_node * root, unsigned long data) {
	avl_tree_node * temp = root;
	while(1) {
		/* NOTE if we dont find the result we return the last node searched.
			This is not conventional, but this function works this way */
		if (!temp->left && !temp->right)
			return temp;
		dword idx = (dword)NODE_INDEX(temp);
		if (idx == data)
			return temp;
		if (idx > data)
			temp = temp->left;
		else
			temp=temp->right;
	}
}