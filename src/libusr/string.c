int strlen(char * str) {
	char * t = str;
	while (*str++);
	//printf("%i %i\n",t,str);
	return str-t-1;
}

int strcmp(char * str1, char * str2) {
	while (*str1 && *str2) {
		if (*str1 != *str2) {
			return *str1-*str2;
		}
		str1++;
		str2++;
	}
	return 0;
}