#include "stdarg.h"
#include "syscall.h"

/* Function for syscall calling in user mode.
	NOTE this is slower, use the macros directly instead to achieve better performance
*/
unsigned int __syscall(int sys_num, dword size, ...) {
	unsigned int ret;
	va_list arglist;
	va_start (arglist, size);

	switch(size) {
		case 0:
			__syscall0(sys_num,ret);
			break;
		case 1:
			__syscall1(sys_num,va_arg (arglist, int),ret);
			break;
		case 2:
			__syscall2(sys_num,va_arg (arglist, int),va_arg (arglist, int),ret);
			break;
		case 3:
			__syscall3(sys_num,va_arg (arglist, int),va_arg (arglist, int),va_arg (arglist, int),ret);
			break;
		case 4:
			__syscall4(sys_num,va_arg (arglist, int),va_arg (arglist, int),va_arg (arglist, int),va_arg (arglist, int),ret);
			break;
	}
	//asm volatile ("movl %%eax,%0" :"=r" (ret));
	va_end (arglist);
	
	return ret;

}

unsigned int __syscallA(int sys_num, dword size, ...) {
	asm volatile (
		"movl 0x8(%ebp),%eax	\n"
		"movl 0xB(%ebp),%ecx	\n"
		"cmpl %ecx,0	\n"
		"je case_0		\n"
		"cmpl %ecx,1	\n"
		"je case_1	\n"
		"cmpl %ecx,2	\n"
		"je case_2	\n" 
		"cmpl %ecx,3	\n"
		"je case_3	\n"
		"cmpl %ecx,4	\n"
		"je case_4	\n"
	
	);

	asm volatile("case_0:");
	__syscallA0(sys_num);
	asm volatile("jmp end");
	asm volatile("case_1:");
	__syscallA1(sys_num);
	asm volatile("jmp end");
	asm volatile("case_2:");
	__syscallA2(sys_num);
	asm volatile("jmp end");
	asm volatile("case_3:");
	__syscallA3(sys_num);
	asm volatile("jmp end");
	asm volatile("case_4:");
	__syscallA4(sys_num);
	asm volatile("jmp end");

	asm volatile("end:");

}