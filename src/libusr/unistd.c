#include "syscall.h"
#include "usr/unistd.h"

long chdir(const char *path) {
	return __syscall(SYSCALL_CHDIR,1,path);
}

long getdents(dword fd, linux_dirent *buf, dword size) {
	return __syscall(SYSCALL_GETDENTS,3,fd,buf,size);
}

int fork() {
	return __syscall(SYSCALL_FORK,0);
}

int execve(char * filename, char ** argv, char ** envp) {
	return __syscall(SYSCALL_EXECVE,3,filename,argv,envp);
}

int access(char * filename, int mode) {
	return __syscall(SYSCALL_ACCESS,2,filename,mode);
}