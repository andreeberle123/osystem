
	asm("movl 0x10(%esp),%ebx"); \
	asm("movl 0x14(%esp),%ecx"); \
	asm("movl 0x18(%esp),%edx"); \
	asm ("int $0x80");

#define __syscallA4(sys_num)	\
	asm("movl 0x10(%esp),%ebx"); \
	asm("movl 0x14(%esp),%ecx"); \
	asm("movl 0x18(%esp),%edx"); \
	asm("movl 0x1B(%esp),%edi"); \
	asm ("int $0x80");

[global __asm_syscall]
__asm_syscall:
	mov eax,0x8[esp]
	mov ecx,0xB[esp]
	movl 0x8(%esp),%eax
	movl 0xB(%esp),%ecx

	cmp 0,ecx
	je case_0
	cmpl %ecx,1
	je case_1
	cmpl %ecx,2
	je case_2" );
	cmpl %ecx,3
	je case_3
	cmpl %ecx,4
	je case_4

case_0:
	int 0x80
case_1:
	mov ebx,0x10[%esp]
	int 0x80
case_2:
	movl ebx,0x10[%esp]
	movl ecx,0x14[%esp]
	int 0x80

case_3:
	__syscallA3(sys_num);
case_4:
	__syscallA4(sys_num);
	ret
