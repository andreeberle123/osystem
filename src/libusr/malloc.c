#include "defines.h"
#include "syscall.h"

/* BUF_SIZE is 4byte multiplied, allocating 16kb ! */
#define BUF_SIZE 4096
#define MAX_MEM = 4294967296

#define MIN_BLOCK 16

typedef struct __malloc_mem_entry {
	int size;
	struct __malloc_mem_entry * next;
	struct __malloc_mem_entry * prv;
} _malloc_mem_entry;

byte * _malloc_current_buffer = 0;

dword * _malloc_mem_base;
dword * _malloc_current_break = 0;
_malloc_mem_entry * _malloc_free_mem_head = 0;

int brk(void * p_new_addr) {
	dword * n_addr = (dword *)__syscall(SYSCALL_BRK,1,p_new_addr);
	_malloc_current_break = n_addr;
	return p_new_addr == n_addr ? 0 : -1;
}

dword sbrk(dword increment) {
	if (!_malloc_current_break) {
		_malloc_current_break = (dword *)__syscall(SYSCALL_BRK,1,0);
		_malloc_mem_base = _malloc_current_break;
	}
	//printf("Malloc current break is : %i\n",_malloc_current_break);
	dword * old_break = _malloc_current_break;
	if (increment) {
		/* allocate sizeof(int) * increment memory */
		if (brk(_malloc_current_break+increment) ) {
			return -1;
		}
	}
	//printf("Old break was at : %i\n",old_break);
	return (dword)old_break;
}

void __malloc_mem_init() {
	if (sbrk(BUF_SIZE)<0) {
		/* ERROR */
		return;
	}
	_malloc_current_buffer = (byte *)_malloc_mem_base;
	_malloc_free_mem_head = (_malloc_mem_entry *)_malloc_current_buffer;
	//printf("New list head at: %i\n",_malloc_free_mem_head);
	_malloc_free_mem_head->next = 0;
	_malloc_free_mem_head->prv = 0;
	_malloc_free_mem_head->size = -(0x7FFFFFFE); /* set to maximum */
	//printf("Mem init finished \n");
}

#define _MALLOC_REPLACE_NODE(head,node,n_node,p_node) \
		if (node->next) \
			node->next->prv = p_node; \
		if (node->prv) \
			node->prv->next =n_node; \
		else \
			head = n_node; \
		p_node->next = node->next

void * malloc(dword size) {
	if (!_malloc_current_buffer) {
		__malloc_mem_init();
	}
	int total_alloc = (size + 2*sizeof(dword)); /* 8 extra bytes for bookkeeping sake */
	//printf("Want to alloc: %i\n",total_alloc);
	_malloc_mem_entry * tmp = _malloc_free_mem_head;
	//printf("tmp=%i\n",tmp);
	//printf("Moving %i, %i, %i\n",total_alloc,-(tmp->size),total_alloc > -(tmp->size));	
	while (total_alloc > -(tmp->size) && tmp) 
		tmp = tmp->next;
	if (!tmp)
		return 0;
	//printf("Moving %i, %i\n",(byte*)tmp + size,_malloc_current_break);	
	if (((byte*)tmp + size) > (byte*)_malloc_current_break) {
		dword to_break = (dword)((byte*)tmp + size + sizeof(_malloc_mem_entry) + 2*sizeof(dword));
		/* break enough to have a BUF_SIZE sized buf available */
		brk((dword*)(to_break + to_break%BUF_SIZE));
	}

	if ((-(tmp->size) - size) < MIN_BLOCK) {
		_MALLOC_REPLACE_NODE(_malloc_free_mem_head,tmp,tmp->next,tmp->prv);
		total_alloc = tmp->size;
	}
	else {
		_malloc_mem_entry * new_entry = ((_malloc_mem_entry*)((byte*)tmp + total_alloc));
		new_entry->size = tmp->size + total_alloc;
		//printf("New entry size is %i\n",-new_entry->size);
		new_entry->next = tmp->next;
		new_entry->prv = tmp->prv;
		_MALLOC_REPLACE_NODE(_malloc_free_mem_head,tmp,new_entry,new_entry);
	}
	tmp->size = total_alloc;
	/* here we set the final 4 bytes of the allocated area with the size of allocation.
		Since we allocate the area with size + 8 bytes, only move size ahead and add
		4 bytes to proper setting */
	*(dword*)((byte *)tmp + total_alloc-sizeof(dword)) = total_alloc;
	
	//printf("Malloc finished. Tmp is %i. Returning: %i.\n",tmp,(dword *)tmp + 1);
	//printf("Bookkeeping: %i and %i.\n",*(dword*)((byte*)tmp + total_alloc-sizeof(dword)),*tmp);
	//printf("Move %i.\n",(byte*)tmp + total_alloc-sizeof(dword));
	//printf("List at %i\n",_malloc_free_mem_head);
	return (dword *)tmp + 1;
	
}

void free(void * f_addr_v) {
	//printf("Going to free: %i\n",f_addr_v);
	/* back 4 bytes, to get bookkeeping info */
	dword * f_addr = (dword *) f_addr_v-1;
	int b_size = *(f_addr);
	int p_size = (f_addr-1) > _malloc_mem_base ? *(f_addr - 1) : 0;
	//printf("Sizes: %i and %i %i.\n",b_size,p_size,f_addr-1);
	_malloc_mem_entry * n_entry = (_malloc_mem_entry*)((byte*)f_addr + b_size); 
	_malloc_mem_entry * f_entry = (_malloc_mem_entry*)(f_addr); 
	//printf("Entries: %i and %i, %i.\n",n_entry,f_entry,n_entry->size);
	if ((dword*)f_addr - 1 > _malloc_mem_base && p_size < 0) {
		f_entry = (_malloc_mem_entry*)((byte*)f_addr + p_size);
		f_entry->size = f_entry->size - b_size;
		*(dword*)((byte*)f_entry-f_entry->size-sizeof(dword)) = f_entry->size;
	}
	else {
		*(dword*)((byte*)f_entry+f_entry->size-sizeof(dword)) = -f_entry->size;
		f_entry->size = -f_entry->size;
	}
	//printf("Entries2: %i and %i, %i.\n",n_entry,f_entry,-f_entry->size);
	//printf("n_entry size: %i, %i, %i.\n",n_entry->size,n_entry->size <0,-n_entry->size);
	if (n_entry->size<0) {
		_MALLOC_REPLACE_NODE(_malloc_free_mem_head,n_entry,f_entry,f_entry);
		f_entry->size += n_entry->size;
		//printf("f_entry size: %i.\n",-f_entry->size);	
		if (f_entry->next)
			*(dword*)((byte*)f_entry-f_entry->size-sizeof(dword)) = f_entry->size;
	}
	else {
		f_entry->next = _malloc_free_mem_head;
		if (_malloc_free_mem_head) 
			_malloc_free_mem_head->prv = f_entry;
	}

	_malloc_free_mem_head = f_entry;
	f_entry->prv = 0;

	//printf("Data: %i, %i and %i.\n",f_entry,f_entry->next,-f_entry->size);
	/* TODO reduce break if necessary */
	
}