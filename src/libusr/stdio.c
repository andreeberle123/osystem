/*******************************************************************************

  dorothy/kernel/stdio.c

  Copyright (C) 2005 D-OZ Team

  Standard I/O functions

	This is a stub. These functions serve as a (very) reduced libc for OZ
	executing.

*******************************************************************************/

#include "stdio.h"
#include "conio.h"
#include "stdarg.h"
#include "keyboard.h"
#include "string.h"
#include "mem.h"
#include "syscall.h" 

int sprintl (char * buf, unsigned long i, unsigned int digits) {
	unsigned char str [30];
	int j = 0,
		k;
	int b_p = 0;

	do {
		str [j++] = (i % 10) + '0';
		i /= 10;
	} while (i);

	for (k = j; k < digits; k++)
		buf[b_p++] = ' ';

	for (j--; j >= 0; j--) 	// number is backwars, print flipped
		buf[b_p++] = str[j];

	return b_p;
}

int sprinth (char * buf, unsigned long i, unsigned int digits) {
	unsigned char str [10];
	unsigned char d;
	int j = 0,
		k;
	int b_p = 0;
	do {
		d = i % 16;
		str [j++] = (d < 10 ? d + '0' : d - 10 + 'A');
		i >>= 4;
	} while (i);

	for (k = j; k < digits; k++)
		buf[b_p++] = '0';

	for (j--; j >= 0; j--)
		buf[b_p++] = str [j];
	return b_p;
}

int sprints (char * buf, char *s) {
	int b_p = 0;
	for (; *s != '\0'; s++)
		buf[b_p++] =  *s;
	return b_p;
}


int vsprintf(char * buf, const char *format, va_list arglist) {
	int b_p = 0;
	int i = 0, status = 1, digits;

	while (format[i] != '\0') {
		switch (status) {
			case 1:
				if (format[i] == '%') {
					digits = 0;
					status = 2;
				} else
					buf[b_p++] = (format[i]);
				break;
			case 2:
				if (format[i] >= '0' && format[i] <= '9') {
					digits = digits * 10 + format[i] - '0';
					break;
				}
			case 3:
				if (format[i] == 'd' || format[i] == 'u' || format[i] == 'i')
					b_p += sprintl (buf+b_p,va_arg (arglist, int), digits);
				else if (format[i] == 's')
					b_p += sprints (buf+b_p, va_arg (arglist, char *));
				else if (format[i] == 'c')
					buf[b_p++] = (va_arg (arglist, char));
				else if (format[i] == 'X')
					buf[b_p++] = sprinth (buf+b_p,va_arg (arglist, int), digits);
				else if (format[i] == '%')
					buf[b_p++] = '%';
				status = 1;
				break;
		}
		i++;
	}
	buf[b_p++] = 0;
	return b_p;

}

int sprintf(char * buf, char *format,...) {
	va_list arglist;
	
	va_start (arglist, format);
	int ret = vsprintf(buf,format,arglist);
	va_end (arglist);
	return ret;
}

#define stdin 0
#define stdout 1
#define stderr 2

int vprintf (const char *format, va_list arglist) {
	char buf[256];
	int len = vsprintf(buf,format,arglist);
	//__syscall3(SYSCALL_WRITE,stdout,buf,len);
	return __syscall(SYSCALL_WRITE,3,stdout,buf,len);
}

int printf (const char *format, ...) {
	va_list arglist;
	va_start (arglist, format);
	vprintf(format,arglist);
	va_end (arglist);
	return 0;
}

unsigned char getch() {
	unsigned char c;
	int ret = __syscall(SYSCALL_READ,3,stdin,&c,1);
	if (ret) {
		return c;
	}
	else {
		return 0;
	}
}

void gets (char *s, unsigned int max) {
	int ret = __syscall(SYSCALL_READ,3,stdin,s,max);
}

/*void textcolor(int fg, int bg) {
	__syscall(0,4,fg,bg,0,0);
}*/

void movexy(int x, int y) {
	__syscall(0,4,0,0,x,y);
}
