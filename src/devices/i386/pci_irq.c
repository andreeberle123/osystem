#include "i386/pci.h"
#include "pci.h" 

#include "stdio.h" 
#include "io.h" 
#include "piix3.h"

_pci_irq_routing_table * pci_irq_table = NULL;
word irq_table_entries_size;
_pci_irq_routing_table * pci_irq_table_end;
//byte _pci_irq_mapping[4];

/* For now all pci pins are mapped to the same irq, for simplicity sake */
byte _pci_irq;

/*
	Wrapper for driver for the local PCI Interrupt Router device, if supported
*/
pci_irq_driver * driver = NULL;

/*
 *  Code for querying and setting of IRQ routes on various interrupt routers.
 *  Got this code from linux kernel, not sure about its validity
 */

void eisa_set_level_irq(unsigned int irq) {
	unsigned char mask = 1 << (irq & 7);
	unsigned int port = 0x4d0 + (irq >> 3);
	unsigned char val;
//	static word eisa_irq_mask;

	if (irq >= 16)
		return;

	val = inportb(port);

	if (!(val & mask)) {
		outportb(port, val | mask);
	}
}

static int check_route_table(_pci_irq_routing_table * table) {
	char sum = 0;;
	char *c = (char*)table;
	while (c < ((char*)table)+table->table_size) {
		sum += *c;
		c++;
	}

	if (sum == 0)
		return 0;
	return -1;
	
}

void register_irq_driver(pci_irq_driver * drv) {
	driver = drv;
}

int scan_irq_routing_table() {
	byte *bios_lookup;
	for (bios_lookup = PCI_BIOS_LOWER_BOUND 
		; bios_lookup < PCI_BIOS_UPPER_BOUND 
		; bios_lookup++) {
		dword *sign = (dword *)bios_lookup;
		if (*sign == PCI_IRQ_TABLE_MAGIC_NUMBER && !check_route_table((_pci_irq_routing_table *)sign)) {
			pci_irq_table = (_pci_irq_routing_table *)sign;
			/* some c-magic to get the table final address */
			pci_irq_table_end = (_pci_irq_routing_table*) ((byte*)pci_irq_table) + pci_irq_table->table_size;
			IRQ_ENTRY_NUM(irq_table_entries_size,pci_irq_table);

			return PCI_SUCCESS;
		}
	}
	// set pci_irq_table as null and use it to determine whether a pci irq table is present
	pci_irq_table = NULL;
	return PCI_IRQ_TABLE_NOT_FOUND;
}

/* In protected mode all 16 hardware irq are moved to 32-47 */
#define HW_IRQ_REMAP 0x20

/**
 * @param pin pin must be 0-3, with 0 being INTA, 1 being INTB and so on, or PIN_INTX from defines
 */
byte pci_get_irq(int devfn, byte pin) {
	/* check if pin is valid */
	if (pin > PIN_INTD)
		return NULL;

	/*_irq_table_entry *entry = pci_irq_table->entries;
	while (entry < (_irq_table_entry*)pci_irq_table_end) {
		if (entry->dev == (devfn & PCI_DEVICE_MASK)) {
			//return *(ENTRY_PIN(entry,pin));
			break;
		}
		
		entry++;
	}*/

	// NOTE no point checking right now, we are mapping all PIRQs to the same IRQ
	// so just return it
	return PIRQ_IRQ_N + HW_IRQ_REMAP;
	
}

/*
  Finds a router driver for the routing table. Only Intel PIIX3 is supported right now.
  TODO add support for other drivers

	NOTE This is not ok. This function will check for the intel/piix3 devfn, but just as
	a mean to guarantee PIRQ routing is supported. Therefore all further irq devices will
	assume as mapped. Actual mapping will only happen when the device is properly mapped
	by pci.c and the driver triggered.
 */
int find_router() {
	dword val;
	pci_bios_read_configuration_dword(pci_irq_table->interrupt_devfn,pci_irq_table->interrupt_bus,PCI_VENDOR_ID,&val);
	if ((val&0xFFFF) != PCI_INTEL_VENDOR) {
		kprintf("	Unsupported PIR found!%X\n",(val&0xFFFF));
		return -1;
	}
	if (((val >> 16) & 0xFFFF) != PCI_INTEL_DEVICE_PIIX3) {
		kprintf("	Unsupported PIR found!%X\n",(val&0xFFFF));
		return -1;
	}

	_pci_irq = PIRQ_IRQ_N;

	return 0;
}



void pci_irq_init() {
	scan_irq_routing_table();
	if (!pci_irq_table) {
		kprintf("	PCI IRQ Routing table not found!\n");
		return;
	}
	// look for router and check if it's intel PIIX3, the only PIR supported for now
	if (find_router()) {
		return;
	}
	driver->map(PIRQ_IRQ_N);
	//piix3_irq_map(PIRQ_IRQ);
	//dword val;
	//pci_bios_read_configuration_byte(pci_irq_table->interrupt_devfn,pci_irq_table->interrupt_bus,0x61,&val);
	//kprintf("val = %X\n",val);
}

