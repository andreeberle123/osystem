/*******************************************************************************

  dorothy/devices/pci_bios.c
 
  Copyright (C) 2009 D-OZ Team

  PCI BIOS Functions

  Functions for searching and accessing PCI BIOS32 structure, in 32-bits
  protected mode. Certain parts of code are inspired in Linux Kernel PCI BIOS
  handling functions.
 
*******************************************************************************/



#include "i386/pci.h"
#include "pci.h"
#include "stdio.h"

_pci_bios_entry *pci_bios_entry;


/**
 *
 */
int check_bios(byte *bios_lookup) {
	pci_bios_entry = (_pci_bios_entry *) bios_lookup;

	/* specification defines the length must be 0x1 */
	if (pci_bios_entry->length != 0x1) 
		return PCI_BIOS_NOT_FOUND;
	
	char *checksum; // signed byte -s8
	char sum = 0;
	for (checksum = (char*)pci_bios_entry;
		checksum < (char*)bios_lookup+16 ; // 16 bytes for the strucutre
		checksum++) {
		sum += *checksum;
	}
	if (sum == 0)
		return PCI_BIOS_SUCCESSFULY_FOUND;
	else
		return PCI_BIOS_NOT_FOUND;
}

/**
 * Find the bios entry point structure for 32-bits protected mode.
 * According to PCI specification, this structure is guaranteed to
 * be found between 0x0E0000 and 0x0FFFF0 address, and thus this
 * range is searched by this function. It sets the global entry
 * structure and returns success if it is found.
 */
int scan_bios32() {
	byte *bios_lookup;
	for (bios_lookup = PCI_BIOS_LOWER_BOUND 
		; bios_lookup < PCI_BIOS_UPPER_BOUND 
		; bios_lookup++) {
		dword *sign = (dword *)bios_lookup;
		if (*sign == PCI_BIOS_MAGIC_NUMBER && !check_bios(bios_lookup))
			return PCI_BIOS_SUCCESSFULY_FOUND;
	}
	// set pci_bios_entry as null and use it to determine whether a pci bios is present
	pci_bios_entry = NULL;
	return PCI_BIOS_NOT_FOUND;
}

pci_bios_service pci_bios;


unsigned char pci_bios_service_present = 0;

unsigned int pci_last_bus;

struct _indirect_pci_bios_service_entry {
	dword entry_addr;
	word segment;
} indirect_pci_bios_service_entry = {0, 0x08};

/**
 *
 */
char probe_pci_interface() {
	if (!pci_bios_service_present) 
		return -1;
	indirect_pci_bios_service_entry.entry_addr = pci_bios.address+pci_bios.entry_offset;
	
	unsigned int signature,a_data,version_info;//,last_bus;
	// store flags
	asm volatile ("pushf");
	asm volatile ("lcall *(%%edi); cld	\n\t"
			/* BIOS will set the carry flag if the service is not present.
			  This is not enough to validate the service, though, as the ah must
			  be 0x00 AND edx carry the pci signature. Here we follow
			  linux kernel implementation and sets the ah according to the
			  CF (0x00 if not set). This check is probably redundant, as 
			  the true validation is in the edx signature, but increases
			  accuracy */
			"jc not_present		\n\t"
			"xor %%ah, %%ah		\n\t"
			"not_present:"
			: "=d" (signature),
			  "=a" (a_data),
			  "=b" (version_info),
			  "=c" (pci_last_bus)
			: "a" (PCI_BIOS_PRESENT_FN),
			  "D" (&indirect_pci_bios_service_entry)
			: "memory"
	);
	asm volatile ("popf");
	//kprintf("	probe %X %X %X\n",a_data,signature,pci_last_bus);
	/* Check the validity of the BIOS, ah must be 0x00, CF must NOT be set
	   and edx must carry the signature string "PCI ". This check is loose
	   here however, as we are checking if either ah is 0h or the CF is
	   not set, instead of checking both. This again follows Linux kernel
	   implementation. TODO increase strictness in this check.
	*/
	if (!(a_data >> 8 & 0xFF) && signature == PCI_BIOS_SIGNATURE) {
		/* PCI BIOS version number uses the weird BCD notation, therefore the hex value should
		   be read as decimal
		*/
		kprintf("	PCI BIOS function set interface version is %d.%X \n",(version_info >> 8 & 0xFF),(version_info & 0xFF));
		/* last bus is in last 8 bits of ecx (cl) */
		pci_last_bus = pci_last_bus & 0xFF;
		return 0;
	}
	else {
		kprintf("	Warning: PCI BIOS function set interface is not present!\n");
		return -1;
	}
}

/**
 * Function used to find a device when the device and vendor IDs are known
 */
int find_pci_device(unsigned int device_id,unsigned int vendor_id, pci_device * dev) {
	if (!pci_bios_service_present) 
		return -1;
	indirect_pci_bios_service_entry.entry_addr = pci_bios.address+pci_bios.entry_offset;
	
	unsigned int device_info, ret_code;
	// store flags
	asm volatile ("pushf");
	asm volatile ("lcall *(%%edi); cld	\n\t"
			/* BIOS will set the carry flag if successful */
			"jc fail		\n\t"
			"xor %%ah, %%ah		\n\t"
			"fail:"
			: "=a" (ret_code),
			  "=b" (device_info)
			: "a" (PCI_FIND_DEVICE_FN),
			  "c" (device_id),
			  "d" (vendor_id),
			/*
			  Use 0 as index for now. To find all devices with the
		          same ID, the index must be iterated until bad_device
			  is found. TODO iterate
			*/
			  "S" (0),
			  "D" (&indirect_pci_bios_service_entry)
			: "memory"
	);
	asm volatile ("popf");
	if (ret_code != PCI_SUCCESS) {
		return ret_code;
	}
	dev->bus = device_info >> 8 & 0xFF;
	dev->device_number = device_info & 0xF8;
	dev->function_number = device_info & 0x07;
	dev->devfn = dev->function_number + dev->device_number;
	return PCI_SUCCESS;
}

struct _indirect_pci_bios_entry {
	dword entry_addr;
	word segment;
} indirect_pci_bios_entry;

/**
 * Uses PCI BIOS32 entry point to determine whether PCI BIOS service is
 * present.
 */
void search_pci_bios() {
	if (pci_bios_entry == NULL) // no known entry?
		return;
	indirect_pci_bios_entry.entry_addr = (dword) pci_bios_entry->entry_point;
	indirect_pci_bios_entry.segment = 0x08;

	//kprintf("jumping to %d, %d\n",pci_bios_entry->entry_point,search_pci_bios);
	
	// prevent interruptions
	asm volatile ("cli");

	// store registers to avoid weird stuff
	// TODO change this to clobber list
	/*asm volatile ("pushf            \n\t"
		      "pushl %eax	\n\t"
		      "pushl %ebx	\n\t"
		      "pushl %ecx	\n\t"
                      "pushl %edx	\n\t"
		      "pushl %edi	\n\t"
	);*/

	/* we perform a far call (lcall) to the BIOS32 service directory entry
	   point. Using default PCI BIOS service id we can probe whether it
	   is active, and obtain its address base
	*/
	//asm ("movl %0, %%eax"     : : "r" (PCI_BIOS_SERVICE_ID));
	//asm ("xorl %ebx, %ebx");
	//asm ("movl %0, %edx"      : : "r" (pci_bios_entry->entry_point));
	//asm ("lcall %edi");
	
	int eax = 0;
	
	//asm ("movl %0, %%eax" : : "r" (PCI_BIOS_SERVICE_ID));
	//asm ("movl %0, %%eax" : : "r" (PCI_BIOS_PRESENT_FN));
	asm volatile ("xorl %%ebx, %%ebx	\n\t"
	//asm ("movl %0, %%ebp" : : "r" (&indirect_pci_bios_entry));
	     "lcall *(%%edi)	\n\t"
	     "cld\n\t"
	//asm ("lcall $0x08:$0x4" : : "m" (pci_bios_entry->entry_point));
		: "=b" (pci_bios.address),
 		  "=c" (pci_bios.length),
 		  "=d" (pci_bios.entry_offset),
		  "=a" (eax)
		: "a" (PCI_BIOS_SERVICE_ID),
		  "D" (&indirect_pci_bios_entry)
		  
		
		
	);
	//asm ("movb %%al, %0" : "=r" (al) : );	
	/*asm volatile ("popl %edi	\n\t"
		      "popl %edx	\n\t"
		      "popl %ecx	\n\t"
                      "popl %ebx	\n\t"
		      "popl %eax	\n\t"
		      "popf            \n\t"
	);*/

	asm volatile ("sti");
	switch (eax & 0xFF) {
		case 0x00:
			pci_bios_service_present = 1;
			// check if the bios interface is present
			if (!probe_pci_interface())
				pci_bios_service_present = 0;
			break;
		case 0x80:
			//we dont have a pci bios
			// TODO mark pci bios as not present
			kprintf("	Warning: PCI BIOS not present!\n");
			break;
		default:
			kprintf("	Error: Weird stuff happening at PCI BIOS retrieval.\n");
	}
	//kprintf("	%X %X %X %X\n",eax,pci_bios.address,pci_bios.entry_offset,pci_bios.length);

	
}


int pci_bios_write_configuration_byte(unsigned int devfn, unsigned int bus, unsigned int reg
			, unsigned int * val) {
	unsigned int ret_code;
	asm volatile (
			"lcall *(%%esi); cld	\n\t"
			/* BIOS will set the carry flag if successful */
			"jc wb_fail		\n\t"
			"xor %%ah, %%ah		\n\t"
			"wb_fail:"
			: "=a" (ret_code)
			: "a" (PCI_BIOS_WRITE_CONFIG_B_FN),
			  "b" ((bus << 8 & 0xFF00) + (devfn & 0xFF)),
			  "c" (*val),
			  "D" (reg),
			  "S" (&indirect_pci_bios_service_entry)
			: "memory"
	);
	if (ret_code & 0xFF00) {
		return ret_code;
	}
	//*val = ret_val;
	return PCI_SUCCESS;
	
}

int pci_bios_write_configuration_word(unsigned int devfn, unsigned int bus, unsigned int reg
			, unsigned int * val) {
	unsigned int ret_code, ret_val;
	asm volatile ("lcall *(%%esi); cld	\n\t"
			/* BIOS will set the carry flag if successful */
			"jc ww_fail		\n\t"
			"xor %%ah, %%ah		\n\t"
			"ww_fail:"
			: "=a" (ret_code)
			: "a" (PCI_BIOS_WRITE_CONFIG_W_FN),
			  "b" ((bus << 8 & 0xFF00) + (devfn & 0xFF)),
			  "c" (*val),
			  "D" (reg),
			  "S" (&indirect_pci_bios_service_entry)
			: "memory"
	);
	if (ret_code) {
		return ret_code;
	}
	//*val = ret_val;
	return PCI_SUCCESS;
	
}

int pci_bios_write_configuration_dword(unsigned int devfn, unsigned int bus, unsigned int reg
			, unsigned int * val) {
	unsigned int ret_code, ret_val;
	asm volatile ("lcall *(%%esi); cld	\n\t"
			/* BIOS will set the carry flag if successful */
			"jc wdw_fail		\n\t"
			"xor %%ah, %%ah		\n\t"
			"wdw_fail:"
			: "=a" (ret_code)
			: "a" (PCI_BIOS_WRITE_CONFIG_DW_FN),
			  "b" ((bus << 8 & 0xFF00) + (devfn & 0xFF)),
			  "c" (*val),
			  "D" (reg),
			  "S" (&indirect_pci_bios_service_entry)
			: "memory"
	);
	if (ret_code) {
		return ret_code;
	}
	//*val = ret_val;
	return PCI_SUCCESS;
	
}

int pci_bios_read_configuration_byte(unsigned int devfn, unsigned int bus, unsigned int reg
			, unsigned int * val) {
	unsigned int ret_code, ret_val;
	asm volatile ("lcall *(%%esi); cld	\n\t"
			/* BIOS will set the carry flag if successful */
			"jc rb_fail		\n\t"
			"xor %%ah, %%ah		\n\t"
			"rb_fail:"
			: "=a" (ret_code),
			  "=c" (ret_val)
			: "a" (PCI_BIOS_READ_CONFIG_B_FN),
			  "b" ((bus << 8 & 0xFF00) + (devfn & 0xFF)),
			  "D" (reg),
			  "S" (&indirect_pci_bios_service_entry)
			: "memory"
	);
	if (ret_code & 0xFF00) {
		return ret_code;
	}
	*val = ret_val;
	return PCI_SUCCESS;
	
}


int pci_bios_read_configuration_word(unsigned int devfn, unsigned int bus, unsigned int reg
			, unsigned int * val) {
	unsigned int ret_code, ret_val;
	asm volatile ("lcall *(%%esi); cld	\n\t"
			/* BIOS will set the carry flag if successful */
			"jc rw_fail		\n\t"
			"xor %%ah, %%ah		\n\t"
			"rw_fail:"
			: "=a" (ret_code),
			  "=c" (ret_val)
			: "a" (PCI_BIOS_READ_CONFIG_W_FN),
			  "b" ((bus << 8 & 0xFF00) + (devfn & 0xFF)),
			  "D" (reg),
			  "S" (&indirect_pci_bios_service_entry)
			: "memory"
	);
	if (ret_code) {
		return ret_code;
	}
	*val = ret_val;
	return PCI_SUCCESS;
	
}

int pci_bios_read_configuration_dword(unsigned int devfn, unsigned int bus, unsigned int reg
			, unsigned int * val) {
	unsigned int ret_code, ret_val;
	asm volatile ("lcall *(%%esi); cld	\n\t"
			/* BIOS will set the carry flag if successful */
			"jc rdw_fail		\n\t"
			"xor %%ah, %%ah		\n\t"
			"rdw_fail:"
			: "=a" (ret_code),
			  "=c" (ret_val)
			: "a" (PCI_BIOS_READ_CONFIG_DW_FN),
			  "b" ((bus << 8 & 0xFF00) + (devfn & 0xFF)),
			  "D" (reg),
			  "S" (&indirect_pci_bios_service_entry)
			: "memory"
	);
	if (ret_code & 0xFF00) {
		kprintf("ret = %X\n",ret_code);
		return ret_code;
	}
	*val = ret_val;
	return PCI_SUCCESS;
	
}


IRQ_routing_options_buffer routing_table = { 10, 0 };

static int read_routing_table() {
	int ret_code;
	asm volatile ("lcall *(%%esi); cld	\n\t"
			/* BIOS will set the carry flag if successful */
			"jc route_fail		\n\t"
			"xor %%ah, %%ah		\n\t"
			"route_fail:"
			: "=a" (ret_code)
			: "a" (GET_IRQ_ROUTING_OPTIONS_FN),
			  "b" (0),
			  "D" (&routing_table),
			  "S" (&indirect_pci_bios_service_entry)
			: "memory"
	);
	return ret_code;
}


void pci_read_routing_table() {

	int ret_code = read_routing_table();
	//kprintf("PCI route = %X\n",(ret_code>>8));
	if (ret_code != PCI_BUFFER_TOO_SMALL) {
		return;
	}
	
}

void set_hw_interrupt(byte pin, byte irq, dword devfn, dword bus) {
	int ret_code;
	asm volatile ("lcall *(%%esi); cld	\n\t"
			/* BIOS will set the carry flag if successful */
			"jc hw_irq_fail		\n\t"
			"xor %%ah, %%ah		\n\t"
			"hw_irq_fail:"
			: "=a" (ret_code)
			: "a" (SET_PCI_HW_INT_FN),
			  "b" ((bus << 8) & devfn),
			  "c" ((irq << 8) & pin),
			  "S" (&indirect_pci_bios_service_entry)
			: "memory"
	);
	if (ret_code & 0xFF00)
		kprintf(" ERROR %X\n",ret_code);	
	kprintf(" SET CODE %X\n",ret_code);
}





void pci_bios_init() {
	if (!scan_bios32())
		kprintf("	PCI BIOS32 Service Directory entry point found at 0x%X.\n",pci_bios_entry);
	else
		kprintf("	Warning: PCI BIOS32 Service Directory entry point not found!\n");

	scan_irq_routing_table();

	//dword *p = 0x200000;
	//kmemcpy(p,pci_bios_entry->entry_point,0x20000);
	//pci_bios_entry->entry_point = p;

	search_pci_bios();
	
	/*unsigned int t;
	if (!pci_bios_read_configuration_dword(0,0,0,&t))
	kprintf("read = %d\n",t);
	if (!pci_bios_read_configuration_dword(0,0,2,&t))
	kprintf("read = %d\n",t);*/
}
