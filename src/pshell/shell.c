/*
 * NOTE the pshell is now meant to be run as a standalone program
 */
#include "conio.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"

#include "pshell.h"
#include "fcntl.h"

#include "usr/unistd.h"
#include "usr/io.h"
#include "usr/malloc.h"
#include "usr/string.h"
#include "usr/stdio.h"

#include "sys/fs.h"

// check entry point ?

// the goal is to have this running as an independent program
// so the memory data will be unique to each instance

/* TODO build a header for these prototypes */
unsigned char getch();
//void textcolor(int fg, int bg);
void movexy(int x, int y);

char * path;

const char * bin_directory = "/bin";

typedef struct _line_entry {
	char * data;
	char * current;
	int dirty;
	struct _line_entry * next;
	struct _line_entry * prev;
} line_entry;

line_entry * line_entries_head;

void memcpy(void * a, void *b, int size) {
	//printf("memcpy %i %i %i\n",a,b,size);
	byte * t = (byte*)a+size;
	while((byte*)a<t)
		*(byte*)a++ = *(byte*)b++;
}

/* TODO set up config files for /bin dir and so on */
void init() {
	line_entries_head = 0;
	path = "/";
}

line_entry * insert_entry(char * data) {
	line_entry * tmp = malloc(sizeof(line_entry));
// 	printf("Malloc returned %i\n",tmp);
	tmp->data = data;
	//tmp->current[0] = 0;
	tmp->prev = 0;
	tmp->next = 0;
	if (line_entries_head) {
		tmp->next = line_entries_head;
		line_entries_head->prev = tmp;
	}
	line_entries_head = tmp;
	return tmp;
}

void shift_str(char *buf,int pos,int size,int delta) {
	int i;
	for(i=size+delta-1;i > pos+delta;i--)
		buf[i]=buf[i-delta];
}

char * change_path(char * path) {
	char prk[256];
	int i;
	char * tmp = prk;
	for(i=0 ; path[i] ; i++) {
		if (path[i] == '/') {
			path[i] = 0;
			chdir(tmp);
			tmp = prk+i;
		}
		else {
			prk[i] = path[i];
		}
	}
	return path+(tmp-prk);
	
}

typedef struct _autoc_values {
	char name[256];
	struct _autoc_values * next;
} autoc_values;

int auto_cmp_mode = 0;

int file_accessible(char * file) {
	return access(file,X_OK);
}

int auto_complete(char * str, char * curr) {
	int k = 0, lastpiece = 0;
	int i;
	char * input;
	char tmp_c = *curr;
	*curr = 0;
	if (*str != '/')
		chdir(path);
	else
		str++;
	input = change_path(str);
	autoc_values * vh = 0;
	autoc_values * vt = 0;

	linux_dirent dir_entries[64];
	int fd = open(".",O_DIRECTORY);
	int pread = 0;
	int l_size = 0;
	do {
		pread = getdents(fd, dir_entries, 64);
		for(i = 0; i < pread ; i++) {
			//printf("de %s, %s\n",dir_entries[i].d_name,input);
			if (!strcmp(dir_entries[i].d_name,input)) {
				autoc_values * val = malloc(sizeof(autoc_values));
				memcpy(val->name,dir_entries[i].d_name,strlen(dir_entries[i].d_name)+1);
				//printf("t2=%s, %i\n",val->name,strlen(dir_entries[i].d_name));
				if (vh)
					vt->next = val;
				else
					vh = val;
				vt = val;
				l_size++;
			}
		}
	} while(pread && pread >= 64);
	
	if (l_size == 1) {
		int len = strlen(vh->name);
		int wlen = len-(int)(curr-input);
		*curr = tmp_c;
		if (tmp_c) {
			/* the cursor is in the middle of the string, shift it */
			shift_str(curr-1,0,strlen(curr)+1,wlen);
		}
		memcpy(input,vh->name,len);
		free(vh);
		return wlen;
	}
	else if (l_size > 1 && !auto_cmp_mode) {
		auto_cmp_mode = 1;
	}
	else if (l_size > 1 && auto_cmp_mode) {
		/* TODO print all */
		return 0;
	}

	return 0;
}


void remove_entry(line_entry * e) {
	line_entry * tmp = line_entries_head;
	line_entry * tmp2 = 0;
	while (tmp != e) {
		tmp2 = tmp;
		tmp = tmp->next;
	}
	if (tmp2)
		tmp2->next = tmp->next;
	else
		line_entries_head = tmp->next;
	free(e->data);
	free(e);
}

void move_first(line_entry * entry) {
	if (line_entries_head != entry) {
		if (entry->next)
			entry->next->prev = entry->prev;
		if (entry->prev)
			entry->prev->next = entry->next;
		line_entries_head->prev = entry;
		entry->next = line_entries_head;
		line_entries_head = entry;
		entry->prev = 0;
	}
}

#define get_input_char(c)\
	c = getch(); \
	if (c == 0 || c == 0xE0){ \
		/*expecting more chars */ \
		c = getch(); \
	}

int get_input() {
	unsigned char c;
	int cursor = 0;
	int max = 0;
	char * buf = (char*)malloc(256*sizeof(char));
	char *t_buf = buf;
	*buf = 0;
	get_input_char(c);
	int i,delta;
	if (c=='\n') {
		printf("\n");
		free(buf);
		return 1;
	}

	line_entry * entry = insert_entry(buf);
	line_entry * write_entry = entry;
	//printf("%i \n",c);
	/* TODO replace this crap with a better function, preferably something with peek */
	while(1) {
		if (!entry) {
			buf = (char*)malloc(256*sizeof(char));
			entry = insert_entry(buf);			
		}
		switch(c) {
			case '\b':
				if (cursor > 0) {
					cursor--;
					max--;
					printf("%c",'\b');
					t_buf--;
					if (cursor == max)
						*t_buf = 0;
					if (!cursor) {
						remove_entry(entry);
						entry = 0;
					}
				}
				break;
			case '\t':
				delta = auto_complete(buf,t_buf);
				if (delta > 0) {
					cursor += delta;
					max += delta;
					printf("%s",t_buf);
					if (max > cursor)
						movexy(cursor-max,0);
					t_buf += delta;
				}
				else if (delta < 0) {

				}
				break;		
			case ARROW_LEFT:
				if (cursor) {
					movexy(-1,0);
					t_buf--;
					cursor--;
				}
				break;
			case ARROW_RIGHT:
				if (cursor < max) {
					movexy(1,0);
					t_buf++;
					cursor++;
				}
				break;
			case ARROW_UP:
				//printf("Arrow up pressed, %i\n",entry->next);
				/* Key up, move the entries list backward and print in the screen */
				if (entry->next) {
					buf = entry->next->data;
					entry = entry->next;
					char pline[256], * p_char = pline, * p_end = pline+max;
					while (p_char < p_end) { 
						*p_char++ = '\b';
					}
					*p_char = 0;
					printf(pline);
					printf(buf);
					cursor = max = strlen(buf);
					t_buf = buf+cursor;
				}
				break;
			case ARROW_DOWN:
				//printf("Arrow down pressed, %i\n",entry->prev);
				/* Key down, move the entries list backward and print in the screen */
				if (entry->prev) {
					entry = entry->prev;
					buf = entry->data;
					char pline[256], * p_char = pline, * p_end = pline+max;
					while (p_char < p_end) { 
						*p_char++ = '\b';
					}
					*p_char = 0;
					printf(pline);
					printf(buf);
					cursor = max = strlen(buf);
					t_buf = buf+cursor;
				}
				break;
			default:	
				if (cursor < max) {
					shift_str(buf,cursor,max++,1);
				}
				if (c == '\n') {
					printf("\n");
					/* entry was removed through backspace */
					if (!entry) {
						return 1;
					}
					*t_buf = 0;
					/* the written entry is empty (i.e. was writing, cleared and pressed up/down */
					if (strlen(write_entry->data) == 0)
						remove_entry(write_entry);
					/* current entry was blanked */
					if (entry != write_entry && cursor == 0) {
						remove_entry(entry);
					}
					else {
						/* last input data goes to list head */
						move_first(entry);
					}
					return 0;
				}
				*t_buf++ = c;
				cursor++;
				if (cursor > max) max = cursor;
				/* print the string, so we can print shifted strings */
				printf("%s",t_buf-1);
				/* TODO all this cursor moving is quite inefficient. I suppose a better 
				interface should be used, instead of syscalls */
				if (cursor < max)
					movexy(cursor-max,0);
		}
		get_input_char(c);
	}
}

char * tokenize_spaces(char * str, int * offset) {
	if (!*str)
		return 0;
	str = str + *offset;

	while (*str && (*str == ' ' || *str == '\t'))str++;
	char * strt = str;
	while (*str && (*str != ' ' && *str != '\t'))str++;
	if (strt == str)
		return 0; /* no string after spaces */
	*str = 0;
	*offset = str - strt;	
	return strt;
}

void execute_file(char * cmd, int * offset) {
	char * argv[256];
	char * c_cmd = tokenize_spaces(cmd,offset), * c;
	if (!file_accessible(c_cmd)) {
		printf("Invalid command!\n\n");
		return;
	}
	int k = 0;
	while (c = tokenize_spaces(cmd,offset))
		argv[k++] = c;

	argv[k] = 0;
	
	if (!fork()) {
		printf("fork done, execing %s\n",c_cmd);
		execve(c_cmd,argv,0);
		//exit
	}
}

void parse_execution(char * cmd, int * offset) {
	execute_file(cmd,offset);
}

int parse_system_command(char * cmd, int * offset) {
// 	char * pst = cmd+(*offset);
// 	/* consume spaces and tabs */
// 	while (*pst && (*pst == ' ' || *pst == '\t'))pst++;
// 	while (*pst) {
// 		/* TODO check here later if these characters are good enough for files */
// 		if (*pst < '!' || (*pst > ':' && *pst < '?') || *pst > '~') { 
// 			return 1;
// 		}
// 		pst++;
// 	}	
// 	*pst = 0;
// 	execute_system_command(cmd,pst+1);
// 	*offset = pst-cmd;
// 	return 0;
	char full_path[256];
	sprintf(full_path,"/bin/%s",cmd);
	execute_file(full_path,offset);
	return 0;
}

void parse(char * cmd) {
	int offset = 0;
	if (cmd[offset] == '.' || cmd[offset] == '/') {
		parse_execution(cmd,&offset);
	}
	else {
		if (parse_system_command(cmd,&offset)) {
			//printf
		}
	}

}

void run() {
	
	//clrscr ();

	printf ("\n\tWelcome to PShell v0.1\n\n");

	for (;;) {
		char * cmd;
		printf("pshell~%s# ",path);
		int data = get_input();
		switch(data) {
			case 0:
				cmd = line_entries_head->data;
				parse(cmd);
				break;
			case 1:
				break;
			
		}
		//insert_entry(line);
	}
	
}





struct pdata {
int a;
int b;
};

int main() {
	init();
	run();	
}