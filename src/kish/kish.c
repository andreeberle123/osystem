/*******************************************************************************

  dorothy/kish/kish.c

  Copyright (C) 2005 D-OZ Team

  KISH is the Kernel Integrated SHell

*******************************************************************************/


#include "conio.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "ne2k.h"
#include "timer.h"
#include "floppy.h"
#include "mem.h"
#include "defines.h"
#include "net/dlink/eth.h"
#include "net/addrs/arp.h"
#include "net/nets/ip.h"
#include "net/transp/udp.h"
#include "net/transp/tcp.h"
#include "loader.h"
#include "net/utils/idlist.h"
#include "net/utils/tcp_idlist.h"
#include "ffs.h"
//#include "ddfs.h"
#include "sched.h"
#include "process.h"
#include "nic.h"

#include "group.h"
#include "k_std.h"

#define fetch_cmd(s,c) s = kstrchtok (NULL, c); 		\
			if (s == NULL) { 			\
				kprintf("Bad Syntax\n"); 	\
				continue;			\
			}

//#include "ddfs.h"
extern int ihateyou;
void kish () {
	char line[61];
	char *cmd;
	char *path = "/";
	word id;

	clrscr ();
	kprints ("                    ___________________   ___________________\n");
	kprints ("                .-/|  78   ~~**~~      \\ /      ~~**~~   79  |\\-.\n");
	kprints ("                ||||                    :                    ||||\n");
	kprints ("                ||||   Dorothy asked    :   The Scarecrow    ||||\n");
	kprints ("                ||||   the Scarecrow    :   answered \"Some   ||||\n");
	kprints ("                ||||   \"How can you     :   people without   ||||\n");
	kprints ("                ||||   talk if you      :   brains do an     ||||\n");
	kprints ("                ||||   haven't got      :   awful lot        ||||\n");
	kprints ("                ||||   a brain?\"        :   of talking.\"     ||||\n");
	kprints ("                ||||   She looked at    :   She replied,     ||||\n");
	kprints ("                ||||   him puzzled.     :   \"That's true.\"   ||||\n");
	kprints ("                ||||                    :                    ||||\n");
	kprints ("                ||||  The Wizard Of Oz  :                    ||||\n");
	kprints ("                ||||___________________ : ___________________||||\n");
	kprints ("                ||/====================\\:/====================\\||\n");
	kprints ("                `---------------------~___~--------------------''\n\n");

	kprints ("You are logged into the D-OZ 0.0.1 (dorothy) Distributed System\n");
	kprints ("Type your wishes in this shell or 'help' if you need some help\n\n");

	FILE *ftemp;

	for (;;) {
		kprintf ("dorothy@d-oz - %s # ", path);
		gets (line, 60);
		//kprintf("line: %s\n", line);

		cmd = kstrchtok (line, ' ');
		word id;
		/*if (kstrcmp (cmd, "help") == 0) {
			kprints ("\nThis is the D-OZ 0.0.1 (dorothy) Distributed System\n");
			kprints ("I am the Witch of the North, and these are the things\n");
			kprints ("you can do before going to the City of Emeralds:\n\n");

			kprints ("ethtest   -	puts ethernet frames in the network\n");
			kprints ("arptest   -	puts ARP frame in the network\n");
			kprints ("ipset     -	sets machine's IP\n");
			kprints ("ipget     -	gets machine's IP\n");
			kprints ("udpreg    -	registers an UDP channel communication\n");
			kprints ("udplist   -	lists all registered UDP channel\n");
			kprints ("udpsend   -	sends an UDP message\n");
			kprints ("udprcv    -	receives an UDP message\n");
			kprints ("tcpreg    -	registers a TCP channel\n");
			kprints ("tcplisten -	register a TCP server channel\n");
			kprints ("tcpcon    -	connects a registered channel\n");
			kprints ("tcplist   -	lists all registered TCP channel\n");
			kprints ("tcpsend   -	sends a TCP message\n");
			kprints ("fdctest   -	reads and writes on the disk\n");
			kprints ("loader    -	loader test\n");
			kprints ("==================================================\n");
			kprints ("fdformat  -	formats a disk with TFS\n");
			kprints ("ls        -	list files\n");
			kprints ("chdir     -	changes the current directory\n");
			kprints ("mkdir     -	creates a directory\n");
			kprints ("rmdir     -	removes a directory\n");
			kprints ("==================================================\n");
			kprints ("ddfsstart -	starts DDFS (actually not supported)\n");
			kprints ("lsd       -	list files in DDFS (actually not supported\n");
			kprints ("cdd       -	change DDFS directory (actually not supported)\n");
			kprints ("==================================================\n");
			kprints ("halt      -	halts D-OZ\n");
			putch ('\n');
		} else if (kstrcmp (cmd, "halt") == 0) {
			kprints ("\nDorothy has gone home, your may power off your system.");
			asm ("cli");
			asm ("hlt");*/
		//} else if (kstrcmp (cmd, "ethtest") == 0) {
		//	byte frame[70];
		//	struct ethernet_header *header = (struct ethernet_header *) frame;

		//	kmemcpy (header->src_address, eth_get_MAC (), 6);
		//	header->dst_address[0] = 0xFF;
		//	header->dst_address[1] = 0xFF;
		//	header->dst_address[2] = 0xFF;
		//	header->dst_address[3] = 0xFF;
		//	header->dst_address[4] = 0xFF;
		//	header->dst_address[5] = 0xFF;
		//	header->type = htons(54);
		//	kmemcpy (frame + sizeof (struct ethernet_header), "Some people without brains do an awful lot of talking.", 54);
		//	eth_send (frame, 68);
		//	delay (100);
		//} else if (kstrcmp (cmd, "loader") == 0) {
		//	testLoader();
		//} else if (kstrcmp (cmd, "arptest") == 0) {
			//arp_test ();
		if (kstrcmp (cmd, "ipset") == 0) {
			char *s;
			byte address[4] = {0, 0, 0, 0};

			s = kstrchtok (NULL, '.');
			address[0] = katoi (s);
			s = kstrchtok (NULL, '.');
			address[1] = katoi (s);
			s = kstrchtok (NULL, '.');
			address[2] = katoi (s);
			s = kstrchtok (NULL, '\0');
			address[3] = katoi (s);
			kprintf ("IP: registering address %d.%d.%d.%d\n", address[0], address[1], address[2], address[3]);
			ip_register_IP (address);
			kprintf ("Dorothy is proud of her new IP\n");
		} else if (kstrcmp (cmd, "ipget") == 0) {
			byte *ip = kalloc (4 * sizeof (byte));
			ip = ip_get_IP ();
			kprintf ("IP: %d.%d.%d.%d\n", ip[0], ip[1], ip[2], ip[3]);
		} else if (kstrcmp(cmd,"rcv") == 0) {
			byte address[4] = {1, 1, 1, 2};
			ip_register_IP(address);
			tcp_listen (2);
		} else if (kstrcmp (cmd, "connect") == 0) {
			byte address[4] = {1, 1, 1, 1};
			byte address2[4] = {1, 1, 1, 2};
			
			ip_register_IP(address);
			id = tcp_register (3, address2, 2);
			tcp_connect (id);
		} else if (kstrcmp (cmd, "sndm") == 0) {
			byte* message = kstrchtok (NULL, '\0');
			if (message != NULL) {

				tcp_send_message(id,NULL,message,kstrlen(message));
			}
		} else if (kstrcmp(cmd,"rcvm") == 0) {
			byte *portch = kstrchtok (NULL, '\0');
			word port;
			if (portch == NULL)
				port = 2;
			else
				port = katoi (portch);

			kprintf("looking for message on %d\n",port);
			byte* message = tcp_get_message(port);
			kprintf("%s\n",message);

		/*} else if (kstrcmp (cmd, "udpreg") == 0) {
			char *s;
			byte address[4] = {0, 0, 0, 0};
			word sport, dport, id;
			s = kstrchtok (NULL, '.');
			address[0] = katoi (s);
			s = kstrchtok (NULL, '.');
			address[1] = katoi (s);
			s = kstrchtok (NULL, '.');
			address[2] = katoi (s);
			s = kstrchtok (NULL, ' ');
			address[3] = katoi (s);
			s = kstrchtok (NULL, ' ');
			dport = katoi (s);
			s = kstrchtok (NULL, ' ');
			sport = katoi (s);
			s = kstrchtok (NULL, '\0');

			id = udp_register (sport, address, dport);
			kprintf ("\nUDP Channel Registered under ID %d\n", id);
		} else if (kstrcmp (cmd, "udplist") == 0) {
			idlist_print();
		// sintaxe: ipsend ip_destino porta_destino porta_origem mensagem
		} else if (kstrcmp (cmd, "udpsend") == 0) {
			char *s;
			word id = 0;
			s = kstrchtok (NULL, ' ');
			id = katoi (s);
			s = kstrchtok (NULL, '\0');
			if(udp_send (id, s, kstrlen (s)))   kprints("Pacote enviado\n");
			else kprints("Erro ao enviar pacote\n");

		} else if (kstrcmp (cmd, "udprcv") == 0) {
			char *s;
			word port;

			s = kstrchtok (NULL, '\0');
			port = katoi (s);
			struct idlist_message *msg = udp_get_message (port);
			if (msg) {  // Se retornou algo
				byte *content = (byte *) msg + sizeof (struct idlist_message_header);
				int i;
				for (i = 0; i < msg->header.size; i++)
					kprintf ("%c", content[i]);
				putch ('\n');
				kfree (msg);
			} else
				kprints ("Empty Mailbox\n");*/
		} else if (kstrcmp (cmd, "tcpreg") == 0) {
			char *s;
			byte address[4] = {0, 0, 0, 0};
			word sport, dport, id;
			fetch_cmd(s,'.');
			address[0] = katoi (s);
			fetch_cmd(s,'.');
			address[1] = katoi (s);
			fetch_cmd(s,'.');
			address[2] = katoi (s);
			fetch_cmd(s,' ');
			address[3] = katoi (s);
			fetch_cmd(s,' ');
			dport = katoi (s);
			fetch_cmd(s,' ');
			sport = katoi (s);
			s = kstrchtok (NULL, '\0');

			id = tcp_register (sport, address, dport);
			kprintf ("\nTCP Channel Registered under ID %d\n", id);
		} else if (kstrcmp (cmd, "tcplisten") == 0) {
			word port = katoi (kstrchtok (NULL, '\0'));
			tcp_listen (port);
		} else if (kstrcmp (cmd, "tcplist") == 0) {
			tcp_idlist_print ();
		} else if (kstrcmp (cmd, "tcpsend") == 0) {
			char *s;
			id = 0;
			s = kstrchtok (NULL, ' ');
			id = katoi (s);
			s = kstrchtok (NULL, '\0');
			if (tcp_send_message (id, 0,(byte*) s, kstrlen (s)))
				kprints ("Pacote enviado\n");
			else
				kprints ("Erro ao enviar pacote\n");
		} else if (kstrcmp (cmd, "tcptest") == 0) {
			id = katoi (kstrchtok (NULL, '\0'));
			if (tcp_test (id))
				kprintf ("\n Testando...\n");
			else
				kprintf ("\n Teste Falhou\n");
		} else if (kstrcmp (cmd, "tcpcon") == 0) {
			id = katoi (kstrchtok (NULL, '\0'));
			if (tcp_connect (id))
				kprintf ("\n Conectando...\n");
			else
				kprintf ("\nConexao falhou\n");
		} else if (kstrcmp (cmd, "tcpclose") == 0) {
			id = katoi (kstrchtok (NULL, '\0'));
			if (tcp_close (id))
				kprints ("Desconectado\n");
			else
				kprints ("Desconexao falhou\n");
		} else if (kstrcmp (cmd, "intme") == 0) {
			ihateyou = 1;	
		} else if (kstrcmp (cmd, "tcpget") == 0) {
			char *s;
			word port;

			s = kstrchtok (NULL, '\0');
			port = katoi (s);
			tcp_get_message (port);
// 			word id = tcp_idlist_get_id_from_port (port);
// 			if (id) {
// 				app_msg *msg = tcp_idlist_get_app_msg (id);
// 				if (msg) {
// 					byte *content = (byte *) msg + sizeof (struct app_msg_header);
// 					int i;
// 					for (i = 0; i < msg->header.size; i++)
// 						kprintf ("%c", content[i]);
// 					putch ('\n');
// 					kfree (msg);
// 				} else
// 					kprints ("Empty Mailbox\n");
// 			}
		}/* else if (kstrcmp (cmd, "") != 0) {
			kprintf("dorothy doesn't know what to do with %s.\n", cmd);
		}
		 else if (kstrcmp (cmd, "ls") == 0) {
			//char *list;
			//list = tfs_ls(pwdtfs());
			kprints("\n\n Dorothy System\n Listing Files FFS - Floppy\n\n");
			///kprintf("%s\n\n", list);
			ls();
		}*/ else if (kstrcmp (cmd, "") == 0) {

		} else if (kstrcmp (cmd, "pstree") == 0) {
			pstree();
		}
		
		/* comandos do ext2 fs - teste - grupo 16 SO II
		   autores: ?
		 */
		else if (kstrcmp (cmd, "cat") == 0) {
			char *filename;

			filename = kstrchtok (NULL, ' ');
			if (filename != NULL)
			{
				int entry = getEntry(filename);
				cat2(entry);
			}
			else
				kprintf("USAGE - cat nomearquivo\n");
		} else if (kstrcmp (cmd, "lsa") == 0) {
			ls_lsa();
		}else if (kstrcmp (cmd, "ls") == 0) {
			ls();
		} else if (kstrcmp (cmd, "ld") == 0) {
			testLoader();
		} else if (kstrcmp (cmd, "printbm") == 0) { //printbitmap
			printBitMap();
		} else if (kstrcmp (cmd, "chdir") == 0) {
			char *dirToGo;
			dirToGo = kstrchtok (NULL, ' ');
			if (dirToGo!=NULL)
			{
				if (!chdir(dirToGo))
					kprintf("Diretorio nao existente\n");		
			}
			else
				kprintf("USAGE - chdir diretorio\n");
		}else if (kstrcmp (cmd, "cd") == 0) {
			char *dirToGo;
			dirToGo = kstrchtok (NULL, ' ');
			if (dirToGo!=NULL)
			{
				if (!toPath(dirToGo))
					kprintf("Diretorio nao existente\n");	
			}
			else
				kprintf("USAGE - cd caminho\n");
	
		}else if (kstrcmp (cmd, "mkdir") == 0) {
			char *dirToMake;
			dirToMake = kstrchtok (NULL, ' ');
			if (dirToMake!=NULL)
			{
				if (!mkdir(dirToMake))
					kprintf("Nao foi possivel criar diretorio!\n");	
			}
			else
				kprintf("USAGE - mkdir nomediretorio\n");
				
		}else if (kstrcmp (cmd, "rm") == 0) {	
			char *toDelete;
			toDelete = kstrchtok (NULL, ' ');
			if (toDelete!=NULL)
			{
				if (!unlink(toDelete))
					kprintf("Nao foi possivel deletar arquivo\n");	
			}
			else
				kprintf("USAGE - rm nomearquivo\n");
				
		}else if (kstrcmp (cmd, "rmdir") == 0) {
			char *toDelete;
			toDelete = kstrchtok (NULL, ' ');
			if (toDelete!=NULL)
			{
				if (!rmdir(toDelete))
					kprintf("Estah tentando apagar diretorio ou diretorio nao estah vazio ou voce nao eh dono do diretorio!\n");	
			}
			else
				kprintf("USAGE - rmdir nomediretorio\n");
		}else if (kstrcmp (cmd, "pwd") == 0) {
			pwd();	
		}else if (kstrcmp (cmd, "ln") == 0) {
			char *arquivo;
			char *nome;
			arquivo = kstrchtok (NULL, ' ');
			nome = kstrchtok (NULL, ' ');
			if (arquivo!=NULL && nome!=NULL)
			{
				if (!hlink(arquivo, nome))
					kprintf("Nao foi possivel criar hardlink\n");
			}
			else
				kprintf("USAGE - ln caminho nomelink\n");
		}else if (kstrcmp (cmd, "lns") == 0) {
			char *arquivo;
			char *nome;
			arquivo = kstrchtok (NULL, ' ');
			nome = kstrchtok (NULL, ' ');
			if (arquivo!=NULL && nome!=NULL)
			{
				if (!slink(arquivo, nome))
					kprintf("Nao foi possivel criar softlink\n");
			}
			else
				kprintf("USAGE - lns caminho nomelink\n");
		}else if (kstrcmp (cmd, "strlen") == 0) {
			kprintf("%d\n", kstrlen((char *) current_path));
		}else if (kstrcmp (cmd, "su") == 0) {
			toLog();
		}else if (kstrcmp (cmd, "whoami") == 0) {
			whoami();
		}else if (kstrcmp (cmd, "adduser") == 0) {
			char *nome;
			char *grupo;
			char *senha;
			nome = kstrchtok (NULL, ' ');
			grupo = kstrchtok (NULL, ' ');
			senha = kstrchtok (NULL, ' ');
			
			if (nome != NULL && grupo != NULL && senha != NULL)
				create_user(nome,senha,grupo);
			else
				kprintf("USAGE: adduser nome grupo senha\n");
		}else if (kstrcmp (cmd, "addgroup") == 0) {
			char *nome;
			nome = kstrchtok (NULL, ' ');
			if (nome != NULL)
				create_group(nome);
			else
				kprintf("USAGE: addgroup nome\n");
				
		}else if (kstrcmp (cmd, "printusers") == 0) {
			print_users();
		}else if (kstrcmp (cmd, "printgroups") == 0) {
			print_groups();
		}else if (kstrcmp (cmd, "touch") == 0) {
			char *nome;
			nome = kstrchtok (NULL, ' ');
			if (nome!=NULL)
			{	FILE *f = fopen(nome,"w+");
				fclose(f);
			}
			else
				kprintf("USAGE: touch filename\n");			
		}else if (kstrcmp (cmd, "write") == 0) {
			char *nome;
			char *string;
			nome = kstrchtok (NULL, ' ');
			string = kstrchtok (NULL, '-');
			if (nome!=NULL && string !=NULL)
			{	FILE *f = fopen(nome,"r+");
				fseek(f,0,SEEK_END);
				fwrite(string,1,kstrlen(string),f);
				fclose(f);
			}
			else
				kprintf("USAGE: write nomearquivo dados (terminando com -)\n");			
		}else if (kstrcmp (cmd, "seek") == 0) {
			char *nome;
			char *qte;
			char *whence;
			nome = kstrchtok (NULL, ' ');
			qte = kstrchtok (NULL, ' ');
			whence = kstrchtok (NULL, ' ');
			if (nome!=NULL && qte!=NULL && whence!=NULL)
			{	
				int nqte = k_atoi(qte);
				if (kstrcmp(whence,"set")==0)
					fseek(ftemp,nqte,SEEK_SET);
				else if (kstrcmp(whence,"cur")==0)
					fseek(ftemp,nqte,SEEK_CUR);
				else if (kstrcmp(whence,"end")==0)
					fseek(ftemp,nqte,SEEK_END);
			}
			else
				kprintf("USAGE: seek nomearquivo qte whence(set,cur,end)\n");			
		}else if (kstrcmp (cmd, "open") == 0) {
			char *nome;
			char *mode;
			nome = kstrchtok (NULL, ' ');
			mode = kstrchtok (NULL, ' ');
			if (nome!=NULL && mode!=NULL)
			{	
				if (kstrcmp(mode,"w")==0)
				{
					ftemp = fopen(nome,"w+");
				}else if (kstrcmp(mode,"r")==0)
				{
					ftemp = fopen(nome,"r+");
				}else if (kstrcmp(mode,"a")==0)
				{
					ftemp = fopen(nome,"a+");
				}
			}
			else
				kprintf("USAGE: open nomearquivo modo(w,r,a)\n");			
		}else if (kstrcmp (cmd, "read") == 0) {
			char *nome;
			char buffer[500];
			int i=0;
			for (i=0;i<500;i++)
				buffer[i] = 0;
			char *qte;
			nome = kstrchtok (NULL, ' ');
			qte = kstrchtok (NULL, ' ');
			if (nome!=NULL && qte !=NULL)
			{	
				int nqte = k_atoi(qte);
				fread(buffer,nqte,1,ftemp);
				kprintf("\n%s\n",buffer);
			}
			else
				kprintf("USAGE: read nomearquivo qte(<500)\n");	
		}else if (kstrcmp (cmd, "chmod") == 0) {
			char *nome;
			char *any;
			char *group;
			char *owner;
			nome = kstrchtok (NULL, ' ');
			owner = kstrchtok (NULL, ' ');
			group = kstrchtok (NULL, ' ');
			any = kstrchtok (NULL, ' ');
			if (nome!=NULL && any !=NULL && group !=NULL && owner !=NULL)
			{	
				chmod(nome,owner[0]-'0', group[0]-'0', any[0]-'0');
			}
			else
				kprintf("USAGE: chmod nomearquivo any group owner (soh um numero para cada de 0 a 7)\n");	
		}else if (kstrcmp (cmd, "printmod") == 0) {
			char *nome;
			nome = kstrchtok (NULL, ' ');
			if (nome!=NULL)
			{	
				print_permition(nome);
			}
			else
				kprintf("USAGE: printmod nomearquivo\n");	
		}else if (kstrcmp (cmd, "chown") == 0) {
			char *nome, *newowner;
			nome = kstrchtok (NULL, ' ');
			newowner = kstrchtok (NULL, ' ');
			if (nome!=NULL && newowner!=NULL)
			{	
				if (!chown(nome,newowner))
					kprintf("Nao foi possivel mudar dono do arquivo\n");
			}
			else
				kprintf("USAGE: chown nomearquivo novodono\n");	
		}else if (kstrcmp (cmd, "chgroup") == 0) {
			char *nome, *newgroup;
			nome = kstrchtok (NULL, ' ');
			newgroup = kstrchtok (NULL, ' ');
			if (nome!=NULL && newgroup!=NULL)
			{	
				if (!chgroup(nome,newgroup))
					kprintf("Nao foi possivel mudar o grupo do arquivo\n");
			}

			/* fim dos ext2 fs */
		}else if (kstrcmp (cmd, "ladapters") == 0) {
			list_adapters();

			/* fim dos ext2 fs */
		}else if (kstrcmp (cmd, "testint") == 0) {
			asm volatile ("int $0x2b");

			/* fim dos ext2 fs */
		}else {
			if (getEntry(cmd) > 0)
				executable_loader(cmd,0,0); /* TODO verify if cmd is a file, before verifying if it is an ELF */
			else
				kprintf("Command not recognized\n");
		}

	}
}
		/*} else if (kstrcmp (cmd, "fdformat") == 0) {
			if (!fdformat_tfs())
				kprints ("could not format TFS.\n");
		} else if (kstrcmp (cmd, "mkdir") == 0) {
			char *nome = kstrchtok (NULL, ' ');
			if (!nome)
				kprints ("usage: mkdir <dir name>\n");
			else
				tfsmkdir (nome);
		} else if (kstrcmp (cmd, "rmdir") == 0) {
			char *nome = kstrchtok (NULL, ' ');
			if (!nome)
				kprints ("usage: rmdir <dir name>\n");
			else
				tfsrmdir (nome);
		} else if (kstrcmp (cmd, "chdir") == 0 || kstrcmp (cmd, "cd") == 0) {
			char *nome = kstrchtok (NULL, ' ');

			if (!nome)
				kprints ("usage: chdir <dir name> OR cd <dir name>\n");
			else {
				tfschdir(nome);
				path = pwdtfs();
			}
		} else if (kstrcmp (cmd, "ls") == 0) {
			char *list;
			list = tfs_ls(pwdtfs());
			kprints("\n\n Dorothy System\n Listing Files TFS - Floppy\n\n");
			kprintf("%s\n\n", list);
		} else if (kstrcmp (cmd, "writedisk") == 0) {
			if (write_tfat()) kprints("Fat written to disk!\n");
		} else if (kstrcmp (cmd, "pwd") == 0) {
			char *path;
			path = pwdtfs();
			kprintf("\nPath: %s\n", path);
	    } else if (kstrcmp (cmd, "lsd") == 0) {
			ddfs_ls(pwdtfs());
		} else if (kstrcmp (cmd, "cdd") == 0) {
			char *nome = kstrchtok (NULL, ' ');
			if (!nome)
				kprints ("usage: cdd <dir name> OR cd <dir name>\n");
			else {
				// not an absolute path
				if (ddfs_chdir(nome))
					path = pwdtfs();
				else
					kprintf ("cannot chdir to %s. directory not found.\n", nome);
			}
		} else if (kstrcmp (cmd, "rdd") == 0) {
			char *nome = kstrchtok (NULL, ' ');
			if (!nome)
				kprints ("usage: rdd <dir name>\n");
			else
				if (!ddfs_rmdir (nome))
					kprintf ("cannot remove %s. directory not empty.\n", nome);
		} else if (kstrcmp (cmd, "mdd") == 0) {
			char *nome = kstrchtok (NULL, ' ');
			if (!nome)
				kprints ("usage: mdd <dir name>\n");
			else
				if (!ddfs_mkdir (nome))
					kprintf ("cannot create %s. directory not empty.\n", nome);
		} else if (kstrcmp (cmd, "ddfsstart") == 0) {
			ddfs_stub_start ();
		} else if (kstrcmp (cmd, "jopen") == 0) {
			char buffer[512];
			kprintf("vou abrir arquivo\n");
			FILE *fp = tfs_fopen("bozo", 2);
			kprintf("irei ler bozo que estah no setor %d\n", fp->secnumber);
			tfs_read(fp, buffer, 1);
			kprintf("%s\n", buffer);
			//tfs_close(fp);
		} else if (kstrcmp (cmd, "") != 0) {
			kprintf("dorothy doesn't know what to do with %s.\n", cmd);
		}
	}
}*/

void load_kish() {

	struct task_struct *ts;

	//asm("cli");

//	ts = create_process((unsigned char *) kish);

	//ts = make_ts((unsigned char *)kish);

	ts->entry_point = (unsigned int )kish;

	ts->status = READY_TO_RUN;
	ts->ppid = 0;
	kmemcpy((unsigned char *)ts->name,(unsigned char *)"kish",4);

	//add_process(ts);

	/*	como nao existe processo pai, faremos o processo atual
		ter o contexto atual, n�o vai pra lista */

	set_current_process(ts);

	kish();

}
