/*
* TODO remove code repetition
*/
#include "defines.h"
#include "stdio.h"
#include "floppy.h"
#include "string.h"
#include "ffs.h"
#include "group.h"
#include "mem.h"
#include "keyboard.h"
#include "conio.h"

#include "dirent.h"

unsigned char* current_path;	// stores current path
FILE * fslink;			// stores softlinks paths

/*
*	Function to open the initial file, fileslink
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void createFileSLink()
{
	
	if(getEntry(".fileslink") != -1)
	{
		fslink = fopen(".fileslink","r+");


	}else{
		fslink = fopen(".fileslink","w+");
	}
	
	// in this moment file is probably alread created in /

}


/*
*	Performs a changeDir with relative or absolut directory
*	structured. The functions moves the FAT until finding
*	the input path
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
int toPath(const char *path)
{
	char *dir;		// temp for directory
	int ret = 0;		// return value
		
	// if the path is absolute, it starts with /
	// so reads FAT_ROOT initially (directory /)
	// to start moving
	if ( path[0] == '/')
	{
		read_block(FFS_FAT_ROOT,ffs_fatdir,FAT_NUM_SECTORS); // loads root at current
		path++;
		current_path[0] = '/'; // returns current path to /, changeDir will change it
		current_path[1] = 0;
		ret = 1;
	}
	
	if ( path[0] != 0)
	{
		// use tokenizer to retrieve path
		dir = kstrchtok((char *)path, '/');
		do
		{
			ret += chdir(dir);	// move as needed
		}while ((dir = kstrchtok(NULL, '/')) != NULL);
	}

	return ret;		// if move was performed returns something >0
}

int flush_directory_entries(struct _linux_dirent * buf, unsigned int size, FILE * stream) {
	//kprintf("flush %i\n",stream->size);
	if (stream->offset >= stream->size)
		return 0; /* we reached the end of directory */

	INODE * buf2 = (INODE*)kalloc(size*sizeof(struct _linux_dirent));
	
	int pread = fread(buf2, size, sizeof(struct _linux_dirent), stream);
	//kprintf("Fread in flush returned %i.\n",pread);
	pread /= sizeof(INODE);
	INODE * pt = buf2, *pt2 = buf2+pread;
	while(pt->pointer && pt < pt2) {
		buf->d_ino = 0; /* TODO INODES lack id currently, fix this */
		buf->d_off = (unsigned long)(buf+1);
		buf->d_reclen = sizeof(struct _linux_dirent); /*TODO the struct length is fixed for now */
		kmemcpy((byte*)buf->d_name,(byte*)pt->name,MAX_FILENAME);
		/* TODO add support to other d_type types */
		if (pt->flag & 0x200)
			buf->d_type = DT_DIR;
		else 
			buf->d_type = DT_REG;
		buf++;
		pt++;
	}
	return ((dword)pt-(dword)buf2)/sizeof(INODE);
}


/*
*	Function to change directory, from the current dir (no hierarchies)
*	
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool chdir(const char *dir)
{
	INODE *pt = (INODE *) ffs_fatdir;	// pointer to current FAT
	unsigned char * aux_path = (unsigned char *)kalloc(TAM_MAX_PATH);

	int cont=0;
	    
	//check if directory exists and go to it
    	while (pt->pointer != 0 && kstrcmp(pt->name, dir) && (cont < FAT_LAST_ELEMENT)) 
    	{    
        	cont++;
        	pt++;
    	}
	
	// Tests if INODE was found
	if (cont > FAT_LAST_ELEMENT - 1 || pt->pointer==0)
	{
		return false;
	}    
	
	// Checks if is directory or softlink
	if ( (!isDirectory(pt)) && (!isSoftLink(pt)) )
	{
	    return false;
	}
	// if softlink
	else if (isSoftLink(pt))
	{
		// if softlink, uses an auxiliary function to move to directory
		return auxslink(pt);
		
	}
	else if (isDirectory(pt)) // regular directory, loads from the fat
	{
		if (kstrcmp(current_user.name,"root")!=0)	// if its root dont check persmissions
		{ 
		  if (!canExecuteInode(pt))	// check directory permissions for current user
			return false;
		}
		
		// if neither . nor .., loads directory and fixes current_path
		if( (!(kstrcmp(dir,".") == 0)) && (!(kstrcmp(dir,"..") == 0)) )
		{
			read_block(pt->pointer,ffs_fatdir,FAT_NUM_SECTORS);	// loads fat entry
			kstrcat((char *) current_path, (char *) dir);	// cast
			kstrcat((char *) current_path,(char *) "/");
		}
		// if its .. handle it differently, moves from current_path,
		// remove last directory from it and move to current_path
		else if(kstrcmp(dir,"..") == 0)
		{
			if(kstrcmp((char *) current_path, (char *) "/")==0) return true; 
						
			unsigned char* currP = current_path;
			
			do{
				currP++;
			}while(*currP!=0);
			
			currP--;

			do{
				currP--;
			}while(*currP!='/');
			
			currP++;
			*currP = 0;
		    	kstrcpy((char *) aux_path, (char *) current_path);
			toPath((char *) aux_path);
		}
		
	}
	kfree(aux_path);
	return true;
}

/*
*	Function for handling chdir for soft links
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
int auxslink(INODE *pt)
{

	
	int ret = 0;
	unsigned char *path = (unsigned char *)kalloc(TAM_MAX_PATH);
	unsigned char *aux_path = (unsigned char *)kalloc(TAM_MAX_PATH);
	unsigned char *aux_path1 = (unsigned char *)kalloc(TAM_MAX_PATH);	
	unsigned char *aux_path2 = (unsigned char *)kalloc(TAM_MAX_PATH);
	// position itself in the file, and reads
	fseek(fslink,pt->pslink,SEEK_SET);
	fread(path,pt->size,1,fslink);
	
	//OK
	// link path is in var path
    	
	kstrcpy((char *) aux_path,(char *) current_path);
	kstrcpy((char *) aux_path1,(char *) current_path);	
	kstrcpy((char *) aux_path2,(char *) path);
	kstrcat((char *) aux_path1,(char *) pt->name);	
	
	// move to it if possible
	if ( (ret = toPath((char *) path)) != 0)
	{
		if (kstrcmp((char *) current_path,(char *) aux_path2)==0)
		{
			kstrcpy((char *) current_path,(char *) aux_path1);
			int tam = kstrlen((char *) current_path);
			current_path[tam] = '/';
			current_path[tam+1] = 0;
		}
		else{
			toPath((char *) aux_path);
		}
		
	}
	
	kfree(path);
	kfree(aux_path);
	kfree(aux_path1);
	kfree(aux_path2);
	return ret;
}

/*
*	Creates a softlink to reference path
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool slink(const char *path, const char *name)
{

	unsigned char *aux_path = (unsigned char *)kalloc(TAM_MAX_PATH);
	unsigned char *path_ori = (unsigned char *)kalloc(TAM_MAX_PATH);
	
	int cont = 0;
	INODE *pt = (INODE *) ffs_fatdir;
	
	// check if ffs_fatdir end has been reached
	while (pt->pointer != 0 && cont < FAT_LAST_ELEMENT) // moves until last fat occurrence is found
	{	
		if (kstrcmp((char *) pt->name,(char *) name)==0) // already exists
			return false;
		cont++;
		pt++;
	}
	if (cont > FAT_LAST_ELEMENT - 1)
	{
		return false;
	}
	
	kstrcpy((char *) path_ori,(char *) current_path);
	
	
	if (path[0] != '/')	// check if it is absolute path or relative
	{
		kstrcpy((char *) aux_path,(char *) current_path);
		kstrcat((char *) aux_path,(char *)path);
	}
	else
		kstrcpy((char *) aux_path,(char *) path);
	
		
	INODE tempInode;
	kstrcpy((char *) tempInode.name,(char *) name);
	tempInode.pointer = fslink->first_block;
	tempInode.size = kstrlen((char *) aux_path)+1;

	kstrcpy((char *) tempInode.datac,(char *) fslink->datac);

	tempInode.flag = pt->flag | 1024;	// sets softlink bit
	
	tempInode.pslink = fslink->size;
		
	toPath("/");	
	fseek(fslink,0,SEEK_END);
	fwrite((char *)aux_path,kstrlen((char *) aux_path)+1,1,fslink);
	toPath((char *) path_ori);
	
	pt = (INODE *) ffs_fatdir;
	pt+=cont;
	kstrcpy((char *) pt->name,(char *) tempInode.name);
	pt->pointer = tempInode.pointer;
	pt->flag = tempInode.flag;
	pt->size = tempInode.size;
	pt->pslink = tempInode.pslink;

	kstrcpy((char *) pt->datac,(char *) tempInode.datac);
	pt->idgroup = current_user.idgroup;
	pt->idowner = current_user.iduser;
	
	pt++;
	pt->pointer = 0;
	
	// writes current fat
	pt = (INODE *) ffs_fatdir;		// gets value of .
	
	write_block(pt->pointer,ffs_fatdir,FAT_NUM_SECTORS);
	
	kfree(aux_path);
	kfree(path_ori);
	
	return true;
}

/**
*	Cria um hardlink de name 'name' que referencia 'path'
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool hlink(const char *path, const char *name)
{
	unsigned char* aux_path;
	unsigned char* aux_path2;
	
	INODE *inodeaux;
	int valPointer, valSize;
	unsigned short valFlag;
	
	int cont = 0;
	INODE *pt = (INODE *) ffs_fatdir;
	
	// checks if ffs_fatdir end has been reached
	while (pt->pointer != 0 && cont < FAT_LAST_ELEMENT) // moves until last fat occurrence
	{	
		if (kstrcmp(pt->name,name)==0) // hardlink already exists
			return false;
		cont++;
		pt++;
	}
	if (cont > FAT_LAST_ELEMENT - 1)
	{
		return false;
	}
	
	aux_path = (unsigned char *)kalloc(TAM_MAX_PATH);
    	aux_path2 = (unsigned char *)kalloc(TAM_MAX_PATH);
	
	kstrcpy((char *) aux_path,(char *) current_path);
	kstrcpy((char *) aux_path2,(char *) path);
	
	toPath((char *) path);
	
	// perform corrections in the path, according to the directory or
	// regular file, and retrieve correct values for pointer and size
	if(kstrcmp((char *) aux_path2,(char *) current_path)!=0) 
	{
		int tam = kstrlen((char *) aux_path2);
		aux_path2[tam] = '/';
		if(kstrcmp((char *) aux_path2,(char *) current_path)==0)  // checks if path has a tailing /
		{
			inodeaux = (INODE *) ffs_fatdir;
			valPointer = inodeaux->pointer; // retrieves value of .
			valFlag = inodeaux->flag;
			valSize = 0;
		}
		else	// its a regular file
		{
			// removes the /
			aux_path2[tam] = 0;
			unsigned char tempName[50];	// stores file name
			int i = kstrlen((char *) aux_path2) - 1; // i in the last char of aux_path2
			int j = 0;
			while (aux_path2[i] != '/') 
				i--;
			
			i++;
			while (aux_path2[i] != 0) // retrieves file name
			{
				tempName[j] = aux_path2[i];
				i++;
				j++;
			}
			
			tempName[j] = 0;
				
			// we search for the file in the current file
			int position = getEntry((char *) tempName);
			inodeaux = (INODE *) ffs_fatdir;
			
			if (position != -1)	// if file is available for creating hardlink, creates it, otherwise returns
				inodeaux+=position;
			else
				return false;
				
			valPointer = inodeaux->pointer;
			valFlag = inodeaux->flag;
			valSize = inodeaux->size;
		}
	}
	else // we received a directory, so we are already on it, retrieves value of .
	{
	    inodeaux = (INODE *) ffs_fatdir;
		valPointer = inodeaux->pointer; // retrieves the value of .
		valFlag = inodeaux->flag;
		valSize = 0;
	}
	
	// return to the directory we were on
	toPath((char *) aux_path);
	
	pt = (INODE *) ffs_fatdir;
	pt+= cont;
	
	// ----------------------------------------
	// create a new entry in the fat we were, which is the current fat
		
	kstrcpy((char *) pt->name, (char *) name);
	pt->pointer = valPointer;
	pt->size = valSize;
	pt->flag = valFlag;

	getDate(pt->datac);

	pt->idowner = current_user.iduser;
	pt->idgroup = current_user.idgroup;

	pt++;
	pt->pointer = 0;
	
	// writes current fat
	pt = (INODE *) ffs_fatdir;		// retrieves value of .
	
	write_block(pt->pointer,ffs_fatdir,FAT_NUM_SECTORS);

	kfree(aux_path);
	kfree(aux_path2);

	return true;
}

/*
*	Remvoes a directory (empty) from the current directory, named 'dir'
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool rmdir(const char *dir)
{
    byte buffer[BLOCK_SIZE]; // only need one block from the directory, if we are going to remove
    INODE *pt = (INODE *) ffs_fatdir;
    INODE *pt2;
    int numBits = FAT_NUM_SECTORS/SECTORS_TO_READ;
    int cont = 0;
    
    // checks if directory exists
    while (pt->pointer != 0 && kstrcmp(pt->name, dir) && (cont < FAT_LAST_ELEMENT)) 
    {    
        cont++;
        pt++;
    }

    // tests if INODE was found
    if (cont > FAT_LAST_ELEMENT - 1 || pt->pointer == 0)
    {
        return false;
    }    

    // checks if user is root, if not tests his/her permissions
    if (kstrcmp((char *) current_user.name,(char *) "root")!=0)	
    {
    	if (pt->idowner != current_user.iduser) // if the current user doesnt own the directory, he cant remove it
    		return false;
    }
    
    if (isDirectory(pt))
    {
	// loaded a block from FAT for the directory
	read_block(pt->pointer,buffer, SECTORS_TO_READ);
	pt2 = (INODE *) buffer;
	pt2+=2;
	// check if the directory is empty
	if (pt2->pointer != 0)
		return false;
	
	// changes bitmap
	unsigned int bitInicial;		
	unsigned int tempfb = FIRST_BLOCK;		// uh
	unsigned int sectToRead = SECTORS_TO_READ;	// change to alterbit
		
	bitInicial = (pt->pointer - tempfb)/sectToRead;
	alterBits(bitInicial,numBits);
	
	// TODO a similar code exists in fsfile.c, check these repetitions
	// remove INODE, replacing by the last entry in the FAT 
	// finds the last INODE
	pt2 = pt;
	while (pt2->pointer != 0)
	{    
		cont++;
		pt2++;        
	}
	pt2--;
	
	// overwrites the unlinked one by the last
	pt->pslink = pt2->pslink;  	// file last access date
	kstrcpy((char *) pt->datac,(char *) pt2->datac);
	pt->flag = pt2->flag;  
	pt->idgroup = pt2->idgroup; // stores the file group ID
	pt->idowner = pt2->idowner;  // stores the file user ID
	
	kstrncpy((char *) pt->name, (char *) pt2->name, 38);
	pt->pointer = pt2->pointer;
	pt->size = pt2->size;
	
	// removes the last one
	pt2->pointer = 0;
	
	pt = (INODE *) ffs_fatdir;		// retrieves value of .
	write_block(pt->pointer,ffs_fatdir,FAT_NUM_SECTORS);
	
	write_block(FFS_BITMAP,ffs_bitmap,BITMAP_NUM_SECTORS); //rewrites bitmap
	
	return true;
    }
    else // if its not a directory, its either a softlink or regular file, dont remove
    {
		return false;
    }

}

/*
*	This function creates a directory with name 'name' in the
*	current directory.
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool mkdir(char *name)
{
	// To create a directory, we must create a call for the current fat (?)
	write_block(FFS_BITMAP,ffs_bitmap,BITMAP_NUM_SECTORS);
	INODE *pt = (INODE *) ffs_fatdir;

	int cont=0;
	int i;
	int valBitmap;
	int numBits = FAT_NUM_SECTORS/SECTORS_TO_READ;

	int numSetorDiretorioAtual = pt->pointer;
	
	// checks if ffs_fatdir end has been reached
	while (pt->pointer != 0 && cont < FAT_LAST_ELEMENT) // moves until last fat occurrence
	{	
		if (kstrcmp((char *) pt->name,(char *) name)==0) // name already exists
			return false;
		cont++;
		pt++;
	}
	if (cont > FAT_LAST_ELEMENT - 1)
	{
		return false;
	}
	
	// after it, create a new fat
	// look for a free block for the fat
	valBitmap = searchFat(numBits);
	alterBits(valBitmap,numBits);
	
	// rewrite the bitmap
	write_block(FFS_BITMAP,ffs_bitmap,BITMAP_NUM_SECTORS);

	valBitmap = valBitmap*SECTORS_TO_READ;  // for BLOCK_SIZE bytes blocks

	// in valBitMap we have the value of the number of the starting block of
	// the new directory

	// now put the relevant information this entry of the curent fat
	kstrcpy((char *) pt->name,(char *) name);
	pt->pointer = valBitmap + FIRST_BLOCK;
	pt->flag = pt->flag | 512;	// sets bit 9 (directory)
	pt->idgroup = current_user.idgroup; // stores the directory group ID
	pt->idowner = current_user.iduser;  // stores the directory user ID
	pt->flag = pt->flag | 367;		// 101101111 (rwxr-xr-x)
	/* NOTE patched it here, directory entries now carry the size
		of their fat block entry */
	pt->size = BLOCK_SIZE; /* An entire block for the directory*/

	getDate(pt->datac);
	
	// this buffer is only to build a piece of the new directory
	// BLOCK_SIZE bytes of info is enough to put .,.. and fat end
	char buffer[BLOCK_SIZE];

	// copies BLOCK_SIZE bytes from the current directory to the buffer
	for (i=0;i<BLOCK_SIZE;i++)
		buffer[i] = 0;
	
	INODE *ptbuffer = (INODE *) buffer;
	kstrcpy((char *) ptbuffer->name,(char *) ".");
	ptbuffer->pointer = valBitmap + FIRST_BLOCK;
	ptbuffer->flag = ptbuffer->flag | 512;
	ptbuffer->idgroup = current_user.idgroup; // stores directory group ID
	ptbuffer->idowner = current_user.iduser;  // stores directory user ID
	ptbuffer->flag = ptbuffer->flag | 367;		// 101101111 (rwxr-xr-x)
	ptbuffer->size = BLOCK_SIZE; /* An entire block for the directory*/
	getDate(ptbuffer->datac);

	ptbuffer++;
	kstrcpy((char *) ptbuffer->name,(char *) "..");
	ptbuffer->pointer = numSetorDiretorioAtual;
	ptbuffer->flag = ptbuffer->flag | 512;
	ptbuffer->idgroup = current_user.idgroup; // stores directory group ID
	ptbuffer->idowner = current_user.iduser;  // stores directory user ID
	ptbuffer->flag = ptbuffer->flag | 367;		// 101101111 (rwxr-xr-x)
	ptbuffer->size = BLOCK_SIZE; /* An entire block for the directory*/
	getDate(ptbuffer->datac);

	ptbuffer++;
	ptbuffer->pointer = 0;

	// writes in the file system a block with the new directory info
	write_block(valBitmap + FIRST_BLOCK, (byte *) buffer,SECTORS_TO_READ);

	pt = (INODE *) ffs_fatdir;		// retrieves the value of .
	write_block(pt->pointer,ffs_fatdir,FAT_NUM_SECTORS);	// rewrites the altered current fat
	
	return true;	
}

/*
*	Creates the / directory, with . and ..
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void createRoot()
{
	// . and .. points to the same dir
	INODE *pt = (INODE *) ffs_fatdir;
	kstrcpy((char *) pt->name, (char *) ".");
	pt->pointer = FFS_FAT_ROOT;
	pt->flag = pt->flag | 512;	// sets directory flags
	pt->idgroup = 1; // stores directory group ID
	pt->idowner = 1;  // stores directory user ID
	pt->flag = pt->flag | 367;		// 101101111 (rwxr-xr-x)
	pt->size = BLOCK_SIZE;
	getDate(pt->datac);
	
	pt++;
	kstrcpy((char *) pt->name, (char *) "..");
	pt->flag = pt->flag | 512;	// sets directory flags
	pt->idgroup = 1; // stores directory group ID
	pt->idowner = 1;  // stores directory user ID
	pt->flag = pt->flag | 367;		// 101101111 (rwxr-xr-x)
	pt->pointer = FFS_FAT_ROOT;
	pt->size = BLOCK_SIZE;
	getDate(pt->datac);

	pt = (INODE *) ffs_fatdir;		// retrieves value of .
	write_block(pt->pointer,ffs_fatdir,FAT_NUM_SECTORS);	// rewrites current altered fat
	
}

