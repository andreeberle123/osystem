/*
* TODO add header
* TODO too many repeated code in the file (copy+paste), try to merge it
*/

#include "defines.h"
#include "stdio.h"
#include "floppy.h"
#include "string.h"
#include "ffs.h"
#include "group.h"
#include "mem.h"
#include "keyboard.h"
#include "conio.h"

/*
*	Function to create a file. Argument name is the file name
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool create_file(const char *name) 
{
	INODE *pt = (INODE *) ffs_fatdir;
	int cont=0;
	int bl;
	int i;
	
	// check if fatdir end has been reached
	while (pt->pointer != 0 && cont < FAT_LAST_ELEMENT) // moves up to fat last occurrence
	{	
		if (kstrcmp(pt->name,name)==0) // file already exists
			return false;
		cont++;
		pt++;
	}
	if (cont > FAT_LAST_ELEMENT - 1)
	{
		return false;
	}
	// put necessary info in this fat entry
	kstrcpy((char *) pt->name, (char *) name);
	
	// search for a free block
	bl = firstEmptyBlock();		
	
	// check if media blocks have been reached
	if (bl > LAST_FILESYSTEM_BLOCK)
		return 0;
		
	alterBit(bl);
	bl = bl*SECTORS_TO_READ;  // used to BLOCK_SIZE sized blocks
	
	
	char buffer[BLOCK_SIZE];	// temporary block for blank write
	for (i=0;i<BLOCK_SIZE;i++)	// zeroes the block
		buffer[i] = 0;
	
	// put the blank block in the file system
	write_block(bl + FIRST_BLOCK, (byte *) buffer,SECTORS_TO_READ);

	// sets INODE pointer to the correct block
	pt->pointer = bl+FIRST_BLOCK;
	pt->size = 0;		// tamanho inicial do arquivo � zero

	
	pt->flag = 0;  
	pt->flag = pt->flag | 2048; 	// set file as regular file

	getDate(pt->datac);

	pt->idgroup = current_user.idgroup; // store the file group ID
	pt->idowner = current_user.iduser;  // store the ID of the user who created the file

	// last fat entry is allocated, to set fat end point, using pointer = 0
	pt++;
	pt->pointer = 0;
	pt->size = 0;

	// writes altered bitmap (block usage bit has been set)
	write_block(FFS_BITMAP,ffs_bitmap,BITMAP_NUM_SECTORS);

	// rewrites alterations performed in current FAT (or current dir)
	pt = (INODE *) ffs_fatdir;				// para pegar o valor de .
	write_block(pt->pointer,ffs_fatdir,FAT_NUM_SECTORS);	// rewrites the current altered FAT

	chmod((char*)name,6,4,4);// sets file permission as default

	return true;
}


/*
* NOTE absolute path not supported yet
* TODO improve this to accept both relative and absolute paths
*	Function to delete a file, in the current dir, with name supplied
*	by argument.
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool unlink(char *name) {
    
    byte buffer[BLOCK_SIZE]; 			// read buffer
    INODE *pt = (INODE *) ffs_fatdir;		// current dir pointer
    INODE *pt2 = (INODE *) ffs_fatdir;		// current dir tempo pointer
    int cont = 0;				// fat entries counter
    
    
    cont = getEntry(name);
    
	if (cont == -1)		// no fat entry with such name available
		return 0;
		
	pt+=cont;
   
	// checks if the current user is root, if not checks the permissions
	if (kstrcmp(current_user.name,"root")!=0)
	{	if (pt->idowner != current_user.iduser)		// checks file owner, if its the current user, proceed
			return false;
	}
	    
    
    if (!isDirectory(pt))
    {    
	
	// find all file blocks and zero the bits (in bitmap)
	// Achar blocos do arquivo e zerar o bit (no mapa de bits) deles
	unsigned int *bl = &pt->pointer;
	unsigned int baux = *bl;
	unsigned int tempfb = FIRST_BLOCK;		// weird
	unsigned int sectToRead = SECTORS_TO_READ;	// change bit
	
	
	do {
		
		alterBit((baux - tempfb)/sectToRead);
		read_block(*bl, buffer, SECTORS_TO_READ);
		bl = (unsigned int *) &buffer[BLOCK_SIZE-POINTER_SIZE];
		baux = *bl;
	} while (*bl != 0);
	
	// remove INODE, replacing by FAT last
	// finds last INODE
	pt2 = pt;
	while (pt2->pointer != 0)
		pt2++;        
	pt2--;
	
	// overwrites the unlinked INODE with the last

	pt->pslink = pt2->pslink;  	// File last access date
	kstrcpy((char *) pt->datac, (char *) pt2->datac);
	pt->flag = pt2->flag;  
	pt->idgroup = pt2->idgroup; // stores file group ID
	pt->idowner = pt2->idowner;  // stores file owner user ID
	kstrcpy((char *) pt->name, (char *)  pt2->name);
	pt->pointer = pt2->pointer;
	pt->size = pt2->size;
	
	// remove last
	pt2->pointer = 0;
	
	pt = (INODE *) ffs_fatdir;
	write_block(pt->pointer,ffs_fatdir,FAT_NUM_SECTORS);
	
	// rewrites altered bitmap
	write_block(FFS_BITMAP,ffs_bitmap,BITMAP_NUM_SECTORS);
	
	// everything is ok, return true
	return true;
    }
    else
	return false;		// if its a directory, exits
    
}

/*
	Added this. Linux syscalls handling uses a file descriptor
	to a directory to enact file listing.
*/
FILE * open_directory_fd(char * path) {

	int position = getEntry((char *) path);
	INODE *pt = (INODE *) ffs_fatdir;

	if (position != -1)
		pt+=position;

	if (position != -1 && !isDirectory(pt))
		return NULL;

	FILE * fp = (FILE *) kalloc(sizeof(FILE));
	fp->mode = 0; /*RD_ONLY*/
	fp->open_close = 1; /* open */
	fp->offset = 0;
	cpInodeToFile(pt,fp);
	return fp;
}

/*
*	Function to open a file. Path is the full path for the file.
*	Mode is ANSI-C standard (r, r+, w, w+, a, a+)
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
/* BUG full path doesnt seem to work, the file is created with path as name */
FILE* fopen(const char *path, const char *mode) 
{
	// get all files from FAT
	int position = getEntry((char *) path);
	INODE *pt = (INODE *) ffs_fatdir;

	if (position != -1)
		pt+=position;

	if (position != -1 && isDirectory(pt))
		return NULL;
	else if (position != -1 && isSoftLink(pt))
	{
		// must open the correct file, if a softlink is provided
		// by handling it here, we avoid further changes in fwrite,
		// fread and fseek.
		// when done, returns to previous directory.
			
		// auxiliar variables, to store paths
		unsigned char *path2 = (unsigned char *)kalloc(TAM_MAX_PATH);
		unsigned char *aux_path = (unsigned char *)kalloc(TAM_MAX_PATH);
		unsigned char *aux_path2 = (unsigned char *)kalloc(TAM_MAX_PATH);
		
		// go to the correct position in the linked file and reads.
		fseek(fslink,pt->pslink,SEEK_SET);
		fread(path2,pt->size,1,fslink);
		
		// we have the link path in 'path', and current one in aux_path
    	kstrcpy((char *) aux_path2, (char *) current_path); // guarda o path original
		kstrcpy((char *) aux_path, (char *) path2); // guarda o que tem em path para poder fazer toPath
		
		if (toPath((char *) path2))
		{
		
			// we reached some other directory
			
			// assume that the symbolic link is correct
			unsigned char nameTemp[50];	// stores file name
			int i = kstrlen((char *) aux_path) - 1; // i in the last char of aux_path2
			int j = 0;
			while (aux_path[i] != '/') 
				i--;
			
			i++;
			while (aux_path[i] != 0) // retrieves file name
			{
				nameTemp[j] = aux_path[i];
				i++;
				j++;
			}
			
			nameTemp[j] = 0;	// fix string
					
			FILE *fret = (FILE *) kalloc(sizeof(FILE));	// file creation
			fret = fopen((char *) nameTemp,mode); 			// non-recursive call
			fret->flag = fret->flag | 1024;			// sets softlink flag
			kstrcpy((char *) fret->namelink, (char *) path);	// copies the namelink
			
			// now we return to the previous directory
			toPath((char *) aux_path2);
			return fret;
		}
	}
	else  {// regular file

		// read-only mode
		if( kstrcmp(mode,"r") == 0 || kstrcmp(mode,"r+") == 0 )
		{
		
			// if the file doesnt exist, ignore it
			if(position == -1)
			{
				return null;
			}

			//open the file in the memory
			FILE *fp = (FILE *) kalloc(sizeof(FILE));
			
			kstrcpy((char *) fp->name, (char *)  pt->name);
			
			// if 'r+' is set, opens it as write mode also
			if( kstrcmp(mode,"r+") == 0 )
				fp->mode = 2;
			else
				fp->mode = 0;
			
			// sets the pointer to file starting position
			fp->offset = 0;
			fp->flag = pt->flag;
			fp->size = pt->size;
			fp->first_block = pt->pointer;
			fp->open_close = 1;
			cpInodeToFile(pt,fp);
				return fp;
	
	
		}
		
		// write mode
		if( kstrcmp(mode,"w") == 0 || kstrcmp(mode,"w+") == 0 )
		{
			
			
			// if file doesnt exist, create it
			if(position == -1)
			{
				if(create_file(path)){
					int position = getEntry((char *) path);
					pt+=position;
				}
				else
				{
					return null;
				}
			}
			
			
			//open file in memory
			FILE *fp = (FILE *) kalloc(sizeof(FILE));
	
			kstrcpy((char *) fp->name, (char *)  pt->name);
			
			// if 'w+' is set, set read mode also
			if( kstrcmp(mode,"w+") == 0 )
				fp->mode = 2;
			else
				fp->mode = 1;
			
			// sets the pointer to file starting pos, truncating it
			fp->offset = 0;
			fp->size = 0;
			fp->first_block = pt->pointer;	
			fp->flag = pt->flag;
			
			
			fp->open_close = 1;
			cpInodeToFile(pt,fp);
			
				return fp;
			
		}
	
	
		
		// append mode
		if( kstrcmp(mode,"a") == 0 || kstrcmp(mode,"a+") == 0 )
		{
			FILE *fp = (FILE *) kalloc(sizeof(FILE));
			
			// if file doesnt exist, create it
			if(position == -1)
			{
				if(create_file(path)){
					int position = getEntry((char *) path);
					pt+=position;
				}
				else
				{
					return null;
				}
			}
			
			// opens the file in memory
			kstrcpy((char *) fp->name, (char *)  pt->name);
			
			// if 'a+' is provided, set it for read mode also
			if( kstrcmp(mode,"a+") == 0 )
				fp->mode = 2;
			else
				fp->mode = 1;
			// File start
			fp->offset = pt->size;
			fp->size = pt->size;
			fp->first_block = pt->pointer;	
			fp->open_close = 1;
			fp->flag = pt->flag;
			cpInodeToFile(pt,fp);
				return fp;
			
		}
	}
		
	// if this point is reached no file has been opened, return null
	return NULL;
		
}

/*
*	Function to close a file, identified by its descriptor.
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
int fclose(FILE *stream) {
	
    //returns 1 if ok, 0 if some problem occurred

	if (stream != 0)
	{
       	kfree(stream); //frees the pointer memory
		return 1;
	}
	else
		return 0;
    
}

int __read(void * ptr, unsigned int nmemb, unsigned int size, unsigned int * offset, unsigned int * pb1, unsigned int ssize) {
	//unsigned int *pb1;		// pointer to file read position
	unsigned int pbOld;		// backup pointer
	unsigned char buffer[BLOCK_SIZE];	// block read temp
	unsigned short off;			// offset
	short blocksToRead;			// number of blocks until offset
	unsigned short bytesOnBlock;		// block bytes moved until offset
	unsigned int bytesToRead, 		// bytes to read
			bytesAlreadyRead,		// bytes already read
			bytes_available;		// bytes remaining in file
	unsigned char * ptemp;			// temp pointer

	
	ptemp = (unsigned char *)ptr;
	
	// move until stream->offset
	off = *offset;
	blocksToRead = off / (BLOCK_SIZE-POINTER_SIZE);
	bytesOnBlock = off % (BLOCK_SIZE-POINTER_SIZE);

	// moves until the block which has offset
	while (blocksToRead >= 0) 
	{	

		pbOld = *pb1;
		read_block(*pb1,buffer,SECTORS_TO_READ);	// read first block
		pb1 = (unsigned int *)&buffer[(BLOCK_SIZE-POINTER_SIZE)];
		
		blocksToRead--;
	}

	// in this moment
	// we must read from buffer[bytesOnBlock] 
	// bytesToRead bytes

	bytes_available = ssize - *offset;
	bytesToRead = size*nmemb > bytes_available ? bytes_available : size*nmemb;
	bytesAlreadyRead = 0;
	
	while ( (bytesAlreadyRead < bytesToRead) )
	{
	
		// tests if last block end has been reached
		if ( (*pb1 == 0) && (bytesOnBlock == BLOCK_SIZE - POINTER_SIZE))	
			return bytesAlreadyRead;

		ptemp[bytesAlreadyRead] = buffer[bytesOnBlock];
		bytesAlreadyRead++;
		bytesOnBlock++;
		*offset = *offset + 1;
		
		// if block end reached, reads next
		if (bytesOnBlock == BLOCK_SIZE - POINTER_SIZE)
		{
			read_block(*pb1,buffer,SECTORS_TO_READ);	// reads first block
			pb1 = (unsigned int *)&buffer[(BLOCK_SIZE-POINTER_SIZE)];
			bytesOnBlock=0;			
		}		
	}

	return bytesAlreadyRead;		// return the number of bytes read
}

/*
*	Function to read data from a file
* TODO standardize
*	@param ptr pointer to the buffer where read data will be placed
*	@param size size 
*	@param nmemb n�mero de elementos (total lido ser� size*nmemb bytes
*	@param stream arquivo de entrada para a leitura
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
int fread(void *ptr, int size, int nmemb, FILE *stream) {
	//kprintf("On fread\n");
	//if it can be read, proceed
	if (stream==NULL || stream->size == 0 || stream->first_block == 0)
		return 0;

	if(canRead(stream))
	{
		// if the file descriptor is closed or is write-only mode, returns
		if (stream->open_close == 0 || stream->mode == 1)
			return 0;

		return __read(ptr,nmemb, size ,&stream->offset,&stream->first_block,stream->size);
	}
	else
		return 0;//couldnt read, returns 0
}

/*
*	Function to write data to a file
*	@param ptr data buffer pointer
*	@param size size 
*	@param nmemb block write size (total written will be nmemb*size)
*	@param stream file descriptor
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
int fwrite(void *ptr, int size, int nmemb, FILE *stream) {

//tests if can write
if(canWrite(stream))
{

	unsigned int *pb1;
	unsigned int pbOld;
	unsigned char buffer[BLOCK_SIZE];
	unsigned short off;
	short blocksToRead;
	unsigned short bytesOnBlock;
	int bytesToWrite;
	int i;
	int j;
	unsigned int proxBlock;
	
	unsigned char *path = (unsigned char *)kalloc(TAM_MAX_PATH);
	unsigned char *aux_path = (unsigned char *)kalloc(TAM_MAX_PATH);
	unsigned char *aux_path2 = (unsigned char *)kalloc(TAM_MAX_PATH);
	
	
	
	// checks whether the descriptor is null, write mode is allowed, and whether the file is open
	if ( (stream == 0) || (stream->mode == 0) || (stream->open_close == 0) )
	{
		return 0;		// returns error
	}
	else
	{
		// special handling for softlink files 
		// TODO the code for soft link retrieval seems to repeat a few times
		// TODO merge it
		if(isSoftLinkFile(stream)){
			
			// retrieves soft link from FAT
			int position = getEntry(stream->namelink);
			INODE *pt = (INODE *) ffs_fatdir;
			
			if (position != -1)
				pt+=position;
			else{
				return 0;
			}
				
			// reads the link file
			fseek(fslink,pt->pslink,SEEK_SET);
			fread(path,pt->size,1,fslink);
			
			// we have the link path in 'path', current in aux_path
			kstrcpy((char *) aux_path2, (char *) current_path); // guarda o path original
			kstrcpy((char *) aux_path, (char *) path); // guarda o que tem em path para poder fazer toPath

			// moves until soft link referenced file path
			if(!toPath((char *) path)) {
				return 0;
			}
		}
		
		// proceeds with writing
		
		pb1 = &stream->first_block;
		
		
		// move until stream->offset
		off = stream->offset;
		blocksToRead = off / (BLOCK_SIZE-POINTER_SIZE);
		bytesOnBlock = off % (BLOCK_SIZE-POINTER_SIZE);
		
		
		// move until the block which has the offsets
		while (blocksToRead >= 0) 
		{	
			
			pbOld = *pb1;
			read_block(*pb1,buffer,SECTORS_TO_READ);	// reads first block
			pb1 = (unsigned int *)&buffer[(BLOCK_SIZE-POINTER_SIZE)];
			
			blocksToRead--;
		}
		
		// write bytesToWrite from buffer[bytesOnBlock]
		bytesToWrite = size*nmemb;
	
		for (i=0; i < bytesToWrite; i++)
		{
			buffer[bytesOnBlock] = *((unsigned char *)ptr); // associate values
			
			ptr++;
			bytesOnBlock++;
			if (bytesOnBlock == (BLOCK_SIZE-POINTER_SIZE)) // check if block end has been reached
			{
	
				if (*pb1 == 0) // checks if allocation of a new block is necessary
				{
					
					// check if there is free available space to store the block
					if (firstEmptyBlock() < DISK_SIZE - FIRST_BLOCK)
					{
	
					
						int next;		// retrieve first empty block from bitmap
						next = firstEmptyBlock();
						
						alterBit(next);		// changes it to used state
						proxBlock = next*SECTORS_TO_READ + FIRST_BLOCK;
	
						*pb1 = proxBlock;	// points to next block
						

						//unsigned short *aux;
						//aux = buffer;
						//aux+=510;
						unsigned int *aux = (unsigned int *)&buffer[(BLOCK_SIZE-POINTER_SIZE)];
						*aux = proxBlock;
						// sets the buffer to point to next allocated block
						kprintf("Writing at block %i\n",pbOld);
						write_block(pbOld,buffer,SECTORS_TO_READ);	// writes the old block
						
						pbOld = *pb1;
						read_block(*pb1,buffer,SECTORS_TO_READ);	// reads next block
						
						for (j=0; j<BLOCK_SIZE; j++)
							buffer[j] = 0;			// zeroes all data from the just allocated block
									
						pb1 = (unsigned int *)&buffer[(BLOCK_SIZE-POINTER_SIZE)];	// next block
						
						
					}
					else
					{
						return i; // disk is full, returns false and discard any written data
					}
					
				}
				else		// no new allocation necessary, just fetch next block
				{
				
						// writes old block
					write_block(pbOld,buffer,SECTORS_TO_READ);
					
					// gets next block
					pbOld = *pb1;
					read_block(*pb1,buffer,SECTORS_TO_READ);	// reads next block
					pb1 = (unsigned int *)&buffer[(BLOCK_SIZE-POINTER_SIZE)];
				}
				
				bytesOnBlock = 0;
			}
		
		}
		
		write_block(pbOld,buffer,SECTORS_TO_READ);		// writes final block
		
		// fixes new file parameters, adjusts new size in file and in 
		// FAT entry
		if ( stream->offset + (size*nmemb) > stream->size)
		{
			stream->size = stream->offset + (size*nmemb);
			int position = getEntry(stream->name);
			
			// shouldnt be -1, but check anyway (sanity check)
			if (position > -1)
			{
				INODE *p = (INODE *) ffs_fatdir;
				p+=position;
				p->size = stream->size;
				p = (INODE *) ffs_fatdir;		// retrieves value of
				write_block(p->pointer,ffs_fatdir,FAT_NUM_SECTORS);	// rewrites altered FAT entry
				
			}
			else
				return 0;
		}

		// writes altered bitmap (block usage bit has been set)
		write_block(FFS_BITMAP,ffs_bitmap,BITMAP_NUM_SECTORS);
			
		// adjust file current pointer
		stream->offset += size*nmemb;
		
		// fixes path, if softlink handling was necessary
		if(isSoftLinkFile(stream)){
			toPath((char *) aux_path2);
		}
		return bytesToWrite;
	}
}
else
	return 0;//couldnt write
}

/*
*	Function to return file size
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
unsigned int ffilesize(FILE *stream) {
	return stream->size;
}

/*
*	Function to tell whether file is at its final position or not
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool feof(FILE *stream) {
	return stream->offset == stream->size;
}

/*
*	Auxiliar function, used by fseek, which implements the moving
*	of the pointer. Moves both the trivial case and the costly, where
*	strean is larger than the offset (?)
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
int aux_fseek(FILE *stream, long offset)
{
	unsigned int pbOld;			// auxiliar pointer
	unsigned char buffer[BLOCK_SIZE];	// auxiliar reading block
	unsigned int off;			// used to calculate offset and movement
	short blocksToRead;			// number of blocks to read in the movement
	unsigned short bytesOnBlock;		// bytes in the last moved block
	int bytesToWrite;			// number of bytes to write
	int i;					// counters
	int j;
	unsigned int proxBlock;			// stores next block (bitmap read)
	unsigned int *pb1;			// movement pointer
	
	// auxiliar variables for paths (used in softlink handling)

	unsigned char *aux_path2 = (unsigned char *)kalloc(TAM_MAX_PATH);
	

	// if changing the offset is enough, ok
	if (offset + stream->offset < stream->size)
	{
		stream->offset = offset;
		return 1;
	}
	else	// if end of file is reached, special handling is required
	{
	
		if(isSoftLinkFile(stream)){
			
			unsigned char *path = (unsigned char *)kalloc(TAM_MAX_PATH);
			unsigned char *aux_path = (unsigned char *)kalloc(TAM_MAX_PATH);
			// retrieves soft link from FAT
			int position = getEntry(stream->namelink);
			INODE *pt = (INODE *) ffs_fatdir;
			
			if (position != -1)
				pt+=position;
			else{
				return 0;
			}
				
			// reads soft link reference
			fseek(fslink,pt->pslink,SEEK_SET);
			fread(path,pt->size,1,fslink);
			
		
			//OK
			// soft link path handling
			kstrcpy((char *) aux_path2, (char *) current_path); // guarda o path original
			kstrcpy((char *) aux_path, (char *) path); // guarda o que tem em path para poder fazer toPath
			
			if(!toPath((char *) path)) {
				return 0;
			}
			
			kfree(path);
			kfree(aux_path);
			
			
		}

		// similar fwrite logic, used for seeking
		pb1 = &stream->first_block;
		off = stream->size;
		// move until file end is reached
		blocksToRead = off / (BLOCK_SIZE-POINTER_SIZE);
		bytesOnBlock = off % (BLOCK_SIZE-POINTER_SIZE);
		while (blocksToRead >= 0) 
		{	
			
			pbOld = *pb1;
			read_block(*pb1,buffer,SECTORS_TO_READ);	// l� o primeiro bloco	
			pb1 = (unsigned int *)&buffer[(BLOCK_SIZE-POINTER_SIZE)];
			blocksToRead--;
		}		

		// make a proper calculation of the number of bytes to write - (seek?)
		bytesToWrite = stream->offset + offset - stream->size;

		bytesOnBlock++;

		for (i=0; i < bytesToWrite; i++)
		{
			buffer[bytesOnBlock] = 0; // ?
			bytesOnBlock++;
			if (bytesOnBlock == (BLOCK_SIZE-POINTER_SIZE))
			{
				if (*pb1 == 0) // must allocate a new block
				{
					if (firstEmptyBlock() < DISK_SIZE - FIRST_BLOCK)
					{

						// allocate new block and adjusts bitmap
					
						int next;
						next = firstEmptyBlock();
						alterBit(next);
						proxBlock = next*SECTORS_TO_READ + FIRST_BLOCK;
						*pb1 = proxBlock;
						unsigned int *aux;
							aux = (unsigned int *) buffer;
						aux+=(BLOCK_SIZE-POINTER_SIZE);
						*aux = proxBlock;
						write_block(pbOld,buffer,SECTORS_TO_READ);
						pbOld = *pb1;
						read_block(*pb1,buffer,SECTORS_TO_READ);	// reads next block
						for (j=0; j<BLOCK_SIZE; j++)
							buffer[j] = 0;
						pb1 = (unsigned int *)&buffer[(BLOCK_SIZE-POINTER_SIZE)];
					}
					else
					{
						return 0; // disk is full, write is incomplete (i am not sure, i think no writing is done here, only seeking)
					}
					
				}
				else		// no new block allocation is necessary
				{	write_block(pbOld,buffer,SECTORS_TO_READ);
					pbOld = *pb1;
					read_block(*pb1,buffer,SECTORS_TO_READ);	// reads next block
					pb1 = (unsigned int *)&buffer[(BLOCK_SIZE-POINTER_SIZE)];
				}
				bytesOnBlock = 0;
			}
		}
		write_block(pbOld,buffer,SECTORS_TO_READ);

		// adjusts file attributes
		stream->size = stream->size + bytesToWrite;
		int position = getEntry(stream->name);
		// shouldnt be -1
		if (position > -1)
		{
			INODE *p = (INODE *) ffs_fatdir;
			p+=position;
			p->size = stream->size;
			p = (INODE *) ffs_fatdir;		// retrieves value of 
			write_block(p->pointer,ffs_fatdir,FAT_NUM_SECTORS);	// rewrites altered FAT entry
		}
		else
			return 0;
			
			
		stream->offset = stream->size;
		
		if(isSoftLinkFile(stream)){
			toPath((char *) aux_path2);
		}
		
		kfree(aux_path2);
	
	}
	return 1;

}

/*
*	Function to move the file descriptor pointer from 'whence' point.
*	The function move 'offset' bytes.
*	whence is ANSI-C like and assumes:
*	whence = { SEEK_SET (file start), SEEK_CUR (current pos), SEEK_END (file end, backward) }
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
int fseek(FILE *stream, long offset, int whence) 
{
		int ret;		// return value

		// handles each whence case as a simple repositioning, and a
		// call of a general function aux_fseek
		switch(whence) {
			case SEEK_SET:			
					stream->offset = 0;	
					ret = aux_fseek(stream,offset);
					break;
			case SEEK_CUR:
					ret = aux_fseek(stream,offset);
					break;
			case SEEK_END:
					stream->offset = stream->size;
					ret = aux_fseek(stream,offset);
					break;
		}

	// returns whether the operation was succesful or not
	return ret;
}


