/*
*	NOTE path seems to be unique for the entire ffs api,
*	I suppose that if multiple shell instances were to exists,
*	some change would be necessary for it.
*/

#include "defines.h"
#include "stdio.h"
#include "floppy.h"
#include "string.h"
#include "ffs.h"
#include "group.h"
#include "mem.h"
#include "keyboard.h"
#include "conio.h"
#include "io.h"

byte *ffs_root_fatdir;

byte *ffs_fatdir;	// stores the fat of the current dir
USER current_user;	// stores current user

/*
*	Function to retrieve system date
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void getDate(unsigned char date[5])
{

	unsigned char a = 0;
	unsigned char temp;
	unsigned char minute = 0;
	unsigned char hour = 0;
	unsigned char day = 0;
	unsigned char month = 0;
	unsigned char year = 0;

	
	outportb(0x0070, 0x02);		// retrieve minutes
	a = inportb(0x0071);
	temp = a;
	a = a & 240; // zero the 4 least significative bits
	a >>= 4;	 // shift left ones
	minute += a*10;
	a = temp;
	a = a & 15; // zero the left ones
	minute += a;

	outportb(0x0070, 0x04);		// retrieves hour
	a = inportb(0x0071);
	temp = a;
	a = a & 240; // zero the 4 least significative bits
	a >>= 4;	 // shift left ones
	hour += a*10;
	a = temp;
	a = a & 15; // zero the left ones
	hour += a;

	outportb(0x0070, 0x07);		// retrieves the day
	a = inportb(0x0071);
	temp = a;
	a = a & 240; // zero the 4 least significative bits
	a >>= 4;	 // shift left ones
	day += a*10;
	a = temp;
	a = a & 15; // zero the left ones
	day += a;

	outportb(0x0070, 0x08);		// retrieves the month
	a = inportb(0x0071);
	temp = a;
	a = a & 240; // zero the 4 least significative bits
	a >>= 4;	 // shift left ones
	month += a*10;
	a = temp;
	a = a & 15; // zero the left ones
	month += a;

	outportb(0x0070, 0x09);		// retrieves the year
	a = inportb(0x0071);
	temp = a;
	a = a & 240; // zero the 4 least significative bits
	a >>= 4;	 // shift left ones
	year += a*10;
	a = temp;
	a = a & 15; // zero the left ones
	year += a;

	date[0] = hour;
	date[1] = minute;
	date[2] = day;
	date[3] = month;
	date[4] = year;

}

/*
*	Function to print a given date
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void printDate(unsigned char date[5])
{
	if (date[4] >= 10)
		kprintf("%d:%d %d/%d/20%d",date[0],date[1],date[2],date[3],date[4]);
	else
		kprintf("%d:%d %d/%d/200%d",date[0],date[1],date[2],date[3],date[4]);
}

/*
*  	Prints current directory
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void pwd(void)
{
	kprintf("%s\n", current_path);
}

/*
*  	Initializes the file system
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void initializeFileSystem()
{
	allocFatAndBitmap();	// call memory allocation for fat and bitmap
	allocPath();		// ccall memory allocation for system current path
	load_fat();		// function to load fat in the memory
	
	
	allocUserAndGroup();
	
	
	loadUser();		// load user and group blocks
	loadGroup();
	

	// checks if file system has been already used (if / has . and ..)
	if (!fileSystemAlreadyInUse())
	{	
		//kprintf("	Using brand new fs\n");
		// creates root dir
		createRootUser("asd"); // asd?
		createRoot();
		
		kprintf("	Setting up filesystem\n");	
	}
}

/*
*  	Perform system login script
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void toLog()
{
	char user[255];
	char password[255];
	do
	{
	kprintf("SYSTEM LOGIN\n");
	kprintf("Username: ");
	gets(user, 255);
	kprintf("Password: ");
	gets(password, 255);
	if (login(user,password)==false)
		kprintf("Invalid username and/or password - try again\n");
	else
		break;
		
	}while (1);
}



/*
*  	Allocates memory for path
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void allocPath()
{
	current_path = (unsigned char *)kalloc(TAM_MAX_PATH);
	// initializes path to /
	current_path[0] = '/';
	current_path[1] = 0;
	
}

/*
*  	Checks if p is a directory
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool isDirectory(INODE *p)
{
	return (p->flag & 512)?true:false;
}

/*
*  	Checks if p is a softlink (directory?)
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool isSoftLink(INODE *p)
{
	return (p->flag & 1024)?true:false;
}

/*
*  	Checks if p is a softlink
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool isSoftLinkFile(FILE *p)
{
	return (p->flag & 1024)?true:false;
}

/*
*  	Checks if p is a regular file
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool isOrdinaryFile(INODE *p)
{
	return (p->flag & 2048)?true:false;
}

/*
*  	Checks if file system has already been used
*	(checks if / has . and ..)
*	This function must be run only in system initialization
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
int fileSystemAlreadyInUse()
{
	// assume that current directory is /
	INODE *pt = (INODE *) ffs_fatdir;
	if (kstrcmp(pt->name,".")==0)	// file system already exists
		return 1;
	else
		return 0;
}

/*
*	Allocate memory to 'ffs_fatdir' and 'ffs_bitmap'
*	Zeroes all content after allocation
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void allocFatAndBitmap()
{
	int i;
	
	// allocating
	ffs_fatdir = (byte *)kalloc(NUMBER_OF_FILES*INODE_SIZE);
	ffs_bitmap = (byte *)kalloc(BITMAP_NUM_SECTORS*SECTOR_SIZE);
	ffs_root_fatdir = (byte *)kalloc(NUMBER_OF_FILES*INODE_SIZE);
	
	// zeroing
	for (i=0; i < NUMBER_OF_FILES*INODE_SIZE; i++)
		ffs_fatdir[i] = 0;
		
	for (i=0; i < BITMAP_NUM_SECTORS*SECTOR_SIZE; i++)
		ffs_bitmap[i] = 0;
	
}

/*
*	Loads fat and file system bitmap
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void load_fat(void) {

	read_block(FFS_BITMAP,ffs_bitmap,BITMAP_NUM_SECTORS);	// loads bitmap
	read_block(FFS_FAT_ROOT,ffs_fatdir,FAT_NUM_SECTORS);	// loads /	
	kmemcpy(ffs_root_fatdir,ffs_fatdir,NUMBER_OF_FILES*INODE_SIZE); // store root for quick remap (...)
}

/*
*	Lists directory content
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void ls(void) {
// TODO rewrite ls to print softlink, permissions, etc..

	INODE *pt = (INODE *) ffs_fatdir;

	while (pt->pointer != 0) {

		kprintf("%s\t\t%d\t", pt->name, pt->size);
		kprintf("     slink? = %d\n", isSoftLink(pt));
		pt++;
	}
}

/*
*	Function to list directory permissions, and other info
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void ls_lsa(void) {
	INODE *pt = (INODE *) ffs_fatdir;
	USER *ptu = (USER *) ffs_userbuffer;
	GROUP *ptg = (GROUP *) ffs_groupbuffer;
	int contg = 0;
	int cont = 0;

	while (pt->pointer != 0)
	{
		//print permissions
		print_flag(pt->flag);

		//looking for username
		cont = 0;
		ptu = (USER *) ffs_userbuffer;
	while (ptu->iduser != 0 && cont < USER_LAST_ELEMENT && ptu->iduser != pt->idowner)
   	{   
        	cont++;
        	ptu++;
    	}
        kprintf(" %s ", ptu->name);

		//looking for group name
		contg = 0;
		ptg = (GROUP *) ffs_groupbuffer;
	while (ptg->idgroup != 0 && contg < GROUP_LAST_ELEMENT && ptg->idgroup != pt->idgroup)
   	{   
        	contg++;
        	ptg++;
    	}
        kprintf(" %s ", ptg->group);

		//printing file size
        kprintf(" %d ", pt->size);

		//printing file creation date
		printDate(pt->datac);

		//printing file name
		kprintf(" %s \n", pt->name);

		pt++;
	}
}

/* NOTE made a quick tool for path changing, (for proper absolute path
	handling). This is not very nice however, and I doubt its a good
	solution. But it will do for now.
*/
/*
	TODO I dont know if the ext2FS code should keep current directory state, as
	this should be handled inside processes.
*/

char * adjust_path(char * path) {
	char * last_bar = path-1;
	char * tmp = path;
	while (*tmp) {
		if (*tmp == '/')
			last_bar = tmp;
		tmp++;
	}
	if (last_bar >= path)
		*last_bar = 0;
	toPath(path);

	return last_bar+1;
}

/*
*	Retrives name index from the current directory
*	returns -1 if fails
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
int getEntry(char *name) {
	name = adjust_path(name);
	INODE *pt = (INODE *) ffs_fatdir;
	int position = 0;

	// if directory end has been reached
	while (pt->pointer != 0) {
		// if found, returns index
		if (kstrcmp(pt->name, name) == 0) {
			return position;
		}

		pt++;
		position++;
	}

	// if not found, then it doesnt exist, returns -1
	return -1;
}

/*
*	Prints the content of an entry in position 'position' in FAT
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void cat2(int position) {

	byte buffer[BLOCK_SIZE];
	INODE *p = (INODE *) ffs_fatdir;

	if (position == -1) {
		kprintf("\nFile does not exist...\n");
		return;
	}
	
	p+=position;
	
	if (isSoftLink(p))
	{
		FILE *f = fopen(p->name,"r");
		do
		{
			if (f->size < BLOCK_SIZE)
			{
				fread(buffer,f->size,1,f);
				kprintf("%s",buffer);	
				return;
			}
			else
			{				
				fread(buffer,BLOCK_SIZE-1,1,f);
				kprintf("%s",buffer);	
			}
			
		}while (!feof(f));
	}
	else
	{
		kprintf("Position = %d",position);
		
		kprintf("\nname: %s\n", p->name);
		kprintf("pointer: %d\n", p->pointer);
		kprintf("size: %d\n", p->size);
		unsigned int *paux = &p->pointer;
		unsigned int saux = p->size;
		unsigned short i;
		
		do {
			read_block(*paux, buffer, SECTORS_TO_READ);
			for (i = 0; i < (BLOCK_SIZE-POINTER_SIZE) && i < saux; i++)
				kprintf("%c", buffer[i]);
			saux -= i;
	
			//buffer[510]  least sig
			//buffer[511]  most sig
			paux = (unsigned int *) &buffer[(BLOCK_SIZE-POINTER_SIZE)];
			
		} while (*paux != 0);
		kprintf("\n");
	}
}


/*
*	Copies content of p to f
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void cpInodeToFile(INODE *p, FILE *f)
{
	kstrcpy(f->name,p->name);
	f->first_block = p->pointer;
        f->size = p->size;
        f->pslink = p->pslink;    
	kstrcpy((char *) f->datac, (char *) p->datac);
        f->flag = p->flag;
        f->idgroup = p->idgroup; // stores file group ID
        f->idowner = p->idowner; // stores file user ID
}
