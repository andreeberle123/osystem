#include "defines.h"
#include "stdio.h"
#include "floppy.h"
#include "string.h"
#include "ffs.h"

#include "group.h"

#include "mem.h"
#include "keyboard.h"
#include "conio.h"


byte *ffs_userbuffer;		// global variable, buffers for user and groups
byte *ffs_groupbuffer;


/*
*	Function to allocate user and groups buffers
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void allocUserAndGroup()
{
	int i;
	
	// creates user and groups buffers
	ffs_userbuffer = (byte *)kalloc(NUMBER_OF_USER*USER_SIZE);
	ffs_groupbuffer = (byte *)kalloc(NUMBER_OF_GROUP*GROUP_SIZE);
	
	// zero them
	for (i=0; i < NUMBER_OF_USER*USER_SIZE; i++)
		ffs_userbuffer[i] = 0;
		
	for (i=0; i < NUMBER_OF_GROUP*GROUP_SIZE; i++)
		ffs_groupbuffer[i] = 0;
}

/*
*	Function to change owner for file 'name', to user 'newowner'
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool chown(char *name, char *newowner)
{
	USER *pt = (USER *) ffs_userbuffer;
	int cont = 0;
	
	// looks the user up
	while (pt->iduser != 0 && cont < USER_LAST_ELEMENT && kstrcmp(pt->name,newowner)!=0)
    	{   
        	cont++;
        	pt++;		
   	}
	
	if (cont > USER_LAST_ELEMENT - 1 || pt->iduser == 0)
   	{
        	return false; // user not found
    	}
		
	// user has been found in user blocks
	int iduser = pt->iduser;	// retrieves newoner id
	
	
	int position = getEntry(name); //look for file fat entry
	INODE *p = (INODE *) ffs_fatdir; // pointer to INODE
	
	if (position == -1)
		return false;
	
	// position in INODE
	p+=position;
	
	if (kstrcmp(current_user.name,"root")!=0)
	{
		if (p->idowner != current_user.iduser)	// if current user owns the file
			return false;
	}
	
	p->idowner = iduser;	// puts the new owner id
	
	// rewrites current directory fat
	p = (INODE *) ffs_fatdir;
	write_block(p->pointer, ffs_fatdir, FAT_NUM_SECTORS); // rewrites the fat (block = value of .)
	
	return true;
}

/*
*	Function to change file group to newgroup
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool chgroup(char *name, char *newgroup)
{
	GROUP *ptg = (GROUP *) ffs_groupbuffer;	
	int cont = 0;
	
	// looks for the group
	while (ptg->idgroup != 0 && cont < USER_LAST_ELEMENT && kstrcmp(ptg->group,newgroup)!=0)
    	{   
        	cont++;
        	ptg++;		
   	}
	
	if (cont > USER_LAST_ELEMENT - 1 || ptg->idgroup == 0)
   	{
        	return false; // group not found
    	}
	
	// group found in group block
	int idgroup = ptg->idgroup;	// retrieves newgroup ID
	
	
	int position = getEntry(name); //looks for the entry file fat
	INODE *p = (INODE *) ffs_fatdir; //pointer to INODE
	
	if (position == -1)
		return false;
	
	// Position in INODE
	p+=position;
	
	if (kstrcmp(current_user.name,"root")!=0)
	{
		if (p->idowner != current_user.iduser)	// checks if current user owns the file
			return false;
	}
	
	p->idgroup = idgroup;	// puts the new group id
	
	// rewrites the fat for the current dir
	p = (INODE *) ffs_fatdir;
	write_block(p->pointer, ffs_fatdir, FAT_NUM_SECTORS); // rewrites fat (block = value of .)
	
	return true;
}

/*
*	Function to change file 'name' permissions, setting permissions
*	for the owner user as 'owner_p', for the group as 'group_p' and other
*	users as 'any_p'.
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool chmod(char * name, unsigned char owner_p, unsigned char group_p, unsigned char any_p) {

	int position = getEntry(name); //look for fat entry
	INODE *p = (INODE *) ffs_fatdir; //pointer to INODE
    	unsigned short aux, aux2;

	if (position == -1)
		return false;
		
	p+=position; //Look for file INODE position in the FAT

	if (kstrcmp(current_user.name,"root")!=0)
	{
		if (p->idowner != current_user.iduser)	// checks if current user owns the file
			return false;
	}
		
	aux = p->flag/512;

	aux2 = any_p * 64;
	aux2 += group_p *8;
	aux2 += owner_p;		// least significative bit for the owners (ffs.h)

	p->flag = aux * 512 + aux2;

	p = (INODE *) ffs_fatdir;

	write_block(p->pointer, ffs_fatdir, FAT_NUM_SECTORS); // TODO check size
	
	return true;
}

/*
*	Function to print the permissions for file 'name'
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void print_permition (char * name){
	int position = getEntry(name); //look for the entry in the FAT
	INODE *p = (INODE *) ffs_fatdir; //pointer to INODE
    	unsigned short p_any, p_grp, p_usr;

	p+=position; //look for the INODE file position in the FAT

	p_any = p->flag * 128;
	p_any = p_any / 8192;
	p_grp = p->flag * 1024;
	p_grp = p_grp / 8192;
	p_usr = p->flag * 8192;
	p_usr = p_usr / 8192;

	kprintf("\n\n%s  %s %s %s\n", name, perm(p_any), perm(p_grp), perm(p_usr));	
}

/*
*	Function that prints a flag
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void print_flag(unsigned short flag){

    unsigned short p_any, p_grp, p_usr, eh_dir, eh_slink;
    
    	eh_slink = flag & 1024;

	eh_dir = flag * 16;
	eh_dir = eh_dir / 8192;
	p_any = flag * 128;
	p_any = p_any / 8192;
	p_grp = flag * 1024;
	p_grp = p_grp / 8192;
	p_usr = flag * 8192;
	p_usr = p_usr / 8192;

	if(eh_dir == 1)
		kprintf("d");
	else if (eh_slink != 0)
		kprintf("l");
	else kprintf("-");

	kprintf(" %s %s %s ", perm(p_usr), perm(p_grp), perm(p_any));	
}

/*
*	Function to define types of permissions.
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
char * perm(unsigned short p){
	if (p == 0) return "---";
	if (p == 1) return "--x";
	if (p == 2) return "-w-";
	if (p == 3) return "-wx";
	if (p == 4) return "r--";
	if (p == 5) return "r-x";
	if (p == 6) return "rw-";
	if (p == 7) return "rwx";
	
	return "INVL";	// caso n�o seja um n�mero de 0 a 7
}

/*
*	Function to create a new user, in a group
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool create_user (char *name, char *password, char *group) {

	USER *pt = (USER *) ffs_userbuffer;
	GROUP *ptg = (GROUP *) ffs_groupbuffer;	
	int cont = 0;
	int contg = 0;

	while (pt->iduser != 0 && cont < USER_LAST_ELEMENT) // 
    	{   
        if (kstrcmp(pt->name,name)==0) // username already exists
		{
			kprintf("\nUsername already exists");
           		 return false;
		}
        cont++;
        pt++;		
   	}
	
	if (cont > USER_LAST_ELEMENT - 1)
   	{
        	return false; // It's not possible to add users, max limit reached
    	}
	
	// now put relevant info on this user
    kstrcpy(pt->name,name);
    kstrcpy(pt->password,password);
    pt->iduser = cont + 1;

	while (ptg->idgroup != 0 && contg < GROUP_LAST_ELEMENT) 
    {   
        if (kstrcmp(ptg->group,group)==0) // group found
            pt->idgroup = contg + 1;
        contg++;
        ptg++;
    }
	if (cont > GROUP_LAST_ELEMENT - 1)
    {
        pt->idgroup = 256;
    }

	pt++;
    pt->iduser = 0;
    write_block(BLOCK_USER, ffs_userbuffer,SECTORS_TO_READ);
    return true;
}

/*
*	Function to create a new group
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool create_group (char *name) {

	GROUP *pt = (GROUP *) ffs_groupbuffer;	
	int cont = 0;
	

	while (pt->idgroup != 0 && cont < GROUP_LAST_ELEMENT) 
    {   
        if (kstrcmp(pt->group,name)==0) // group already exists
		{
            kprintf("\nGroup already exists");
			return false;
        }cont++;
        pt++;		
    }
	
	if (cont > GROUP_LAST_ELEMENT - 1)
    {
        return false;
    }
	
	// now put relevant info for this group
    kstrcpy(pt->group,name);
    pt->idgroup = cont + 1;

	pt++;
    pt->idgroup = 0;
    write_block(BLOCK_GROUP, ffs_groupbuffer,SECTORS_TO_READ);
    return true;
}

/*
*	Function to print available groups
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void print_groups()
{
	GROUP *pt = (GROUP *) ffs_groupbuffer;	
	int cont = 0;

	kprintf("\\Groups");
	while (pt->idgroup != 0 && cont < GROUP_LAST_ELEMENT)  
    {
        kprintf("\n%s\n", pt->group);
		cont++;
        pt++;
    }
}

/*
*	Function to print available users
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void print_users()
{
	USER *pt = (USER *) ffs_userbuffer;
	GROUP *ptg = (GROUP *) ffs_groupbuffer;
	int cont = 0;
	int contg = 0;

	kprintf("\nUsers");
	kprintf("\nName - Password - Group");
	while (pt->iduser != 0 && cont < USER_LAST_ELEMENT) 
    {   
		contg = 0;
		ptg = (GROUP *) ffs_groupbuffer;
		while (ptg->idgroup != 0 && contg < GROUP_LAST_ELEMENT && ptg->idgroup != pt->idgroup)
   		{   
        	contg++;
        	ptg++;
    	}
	
        kprintf("\n%s - %s - %s\n", pt->name, pt->password, ptg->group);
        cont++;
        pt++;		
    }
}

/*
*	Function to create user root, with a password
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void createRootUser(char *defaultPassword)
{
	create_group("root");
	create_user("root",defaultPassword,"root");
}

/*
*	Function to perform the login of an user in the system
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
bool login(char *user, char *password)
{
	USER *pt = (USER *) ffs_userbuffer;
	int cont = 0;
	
	
	while (pt->iduser != 0 && cont < USER_LAST_ELEMENT) 
	{   
		if (kstrcmp(pt->name,user)==0) // user found
			{
				if (kstrcmp(pt->password, password)==0)	// password is correct
				{
					kstrcpy(current_user.name,pt->name);	// sets current info
					kstrcpy(current_user.password,pt->password);
					current_user.iduser = pt->iduser;
					current_user.idgroup = pt->idgroup;
					return true;				
				}
				else
					return false;	// invalid password
				
			}
		cont++;
		pt++;		
	}
	
	return false;

}

/*
*	Function to print info for the current user
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void whoami()
{
	kprintf("Current username: %s\n", current_user.name);
	kprintf("ID: %d\n", current_user.iduser);
	kprintf("Group ID: %d\n", current_user.idgroup);
	
}

/*
*	Function to load user into buffer
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void loadUser()
{
	read_block(BLOCK_USER, ffs_userbuffer,SECTORS_TO_READ);
}

/*
*	Function to load group into buffer
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void loadGroup()
{
	read_block(BLOCK_GROUP, ffs_groupbuffer,SECTORS_TO_READ);
}

/*
*	Function to check read permission
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
int canRead(FILE *f){
	unsigned short r_any, r_grp, r_usr;

	r_any = f->flag * 128;
	r_any = r_any / 32768; // retrive bit 8: r for any

	r_grp = f->flag * 1024;
	r_grp = r_grp / 32768; //retrive bit 5: r for grp

	r_usr = f->flag * 8192;
	r_usr = r_usr / 32768; //retrive bit 2: r for usr

	if(r_any == true)
		return 1;

	if(current_user.idgroup == f->idgroup)
	{	
		if(r_grp == true)
			return 1;

		if(current_user.iduser == f->idowner)
			if(r_usr == true)
				return 1;
	}

	return 0;
}

/*
*	Function to check write permission
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
int canWrite(FILE *f){

	unsigned short w_any, w_grp, w_usr;

	w_any = f->flag * 256;
	w_any = w_any / 32768; //retrieve bit 7: w for any

	w_grp = f->flag * 2048;
	w_grp = w_grp / 32768; //retrieve o bit 4: w for grp

	w_usr = f->flag * 16384;
   	w_usr = w_usr / 32768; //retrieve o bit 1: w for usr

	if(w_any == true)
		return 1;

	if(current_user.idgroup == f->idgroup)
	{	
		if(w_grp == true)
			return 1;

		if(current_user.iduser == f->idowner)
			if(w_usr == true)
				return 1;
	}

	return 0;
}

/*
*	Checks executing permission
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
int canExecute(FILE *f){

	unsigned short x_any, x_grp, x_usr;
	
	x_any = f->flag * 256;
	x_any = x_any / 32768; //retrieve bit 6: x for any

	x_grp = f->flag * 2048;
	x_grp = x_grp / 32768; //retrieve bit 3: x for grp

	x_usr = f->flag * 16384;
	x_usr = x_usr / 32768; //retrieve bit 0: x for usr

	if(x_any == true)
		return 1;

	if(current_user.idgroup == f->idgroup)
	{	
		if(x_grp == true)
			return 1;

		if(current_user.iduser == f->idowner)
			if(x_usr == true)
				return 1;
	}

	return 0;
}

/*
*	Checks if an INODE can be read
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
int canReadInode(INODE *f)
{
	unsigned short r_any, r_grp, r_usr;

	r_any = f->flag * 128;
	r_any = r_any / 32768; //retrieve bit 8: r for any

	r_grp = f->flag * 1024;
	r_grp = r_grp / 32768; //retrieve bit 5: r for grp

	r_usr = f->flag * 8192;
	r_usr = r_usr / 32768; //retrieve bit 2: r for usr

	if(r_any == true)
		return 1;

	if(current_user.idgroup == f->idgroup)
	{	
		if(r_grp == true)
			return 1;

		if(current_user.iduser == f->idowner)
			if(r_usr == true)
				return 1;
	}

	return 0;
}

/*
*	Checks if an INODE can be written
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
int canWriteInode(INODE *f){

	unsigned short w_any, w_grp, w_usr;

	w_any = f->flag * 256;
	w_any = w_any / 32768; //retrieve bit 7: w for any

	w_grp = f->flag * 2048;
	w_grp = w_grp / 32768; //retrieve bit 4: w for grp

	w_usr = f->flag * 16384;
   	w_usr = w_usr / 32768; //retrieve bit 1: w for usr

	if(w_any == true)
		return 1;

	if(current_user.idgroup == f->idgroup)
	{	
		if(w_grp == true)
			return 1;

		if(current_user.iduser == f->idowner)
			if(w_usr == true)
				return 1;
	}

	return 0;
}

/*
*	Function to check if an INODE may be executed
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
int canExecuteInode(INODE *f){

	unsigned short x_any, x_grp, x_usr;

	x_any = f->flag * 256;
	x_any = x_any / 32768; //retrieveo bit 6: x for any

	x_grp = f->flag * 2048;
	x_grp = x_grp / 32768; //retrieve bit 3: x for grp

	x_usr = f->flag * 16384;
	x_usr = x_usr / 32768; //retrieve bit 0: x for usr

	if(x_any == true)
		return 1;

	if(current_user.idgroup == f->idgroup)
	{	
		if(x_grp == true)
			return 1;

		if(current_user.iduser == f->idowner)
			if(x_usr == true)
				return 1;
	}

	return 0;
}




