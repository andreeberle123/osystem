/*
*/

#include "defines.h"
#include "stdio.h"
#include "floppy.h"
#include "string.h"
#include "ffs.h"

#include "group.h"

#include "mem.h"
#include "keyboard.h"
#include "conio.h"

byte *ffs_bitmap;	// stores the bitmap

/*
*	Looks in the bitmap for 'numberBits' consecutive zeroes
*	Procura na bitmap 'numberBits' zeros consecutivos
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
unsigned int searchFat(int numberBits)
{
	unsigned char val; 
	int i;

	int numbits = numberBits; // for the filesystem must be FAT_NUM_SECTORS/SECTORS_TO_READ;
	int numbitscount = numbits;	// counts how many consecutive 0 bits were found
	int pos = -1;

	// moves through the bitmap searching for the zeroes
	for (i=0; i < (BITMAP_NUM_SECTORS*SECTOR_SIZE); i++)
	{
		
	    val = ffs_bitmap[i];	// retrieves a byte from the bitmap

	    if ( (val & 128) == 0)  // do some bitwise to check
		{	if (numbitscount == numbits) pos = i*8 + 0;	// checks if zero has been found
			
			numbitscount--;		// found a zero, decrement
						
			if (numbitscount == 0)	// if all bits necessary have been found, returns
			{
				return pos;
			}	
		}
		else	// if it wasnt zero, there was some 1, and we failed to find
		{	// restore numbit count
			numbitscount = numbits;
		}

		// same logic for remaining read bytes (bits)
		if ( (val & 64) == 0)  
		{	if (numbitscount == numbits) pos = i*8 + 1;
			numbitscount--;
			
			if (numbitscount == 0)
			{
				return pos;
			}	
		}
		else
		{
			numbitscount = numbits;
		}
		
		if ( (val & 32) == 0)
		{	if (numbitscount == numbits) pos = i*8 + 2;
			numbitscount--;
			
			if (numbitscount == 0)
			{
				return pos;
			}	
		}
		else
		{
			numbitscount = numbits;
		}

		if ( (val & 16) == 0)
		{	if (numbitscount == numbits) pos = i*8 + 3;
			numbitscount--;
			
			if (numbitscount == 0)
			{
				return pos;
			}	
		}
		else
		{
			numbitscount = numbits;
		}

		if ( (val & 8) == 0)
		{	if (numbitscount == numbits) pos = i*8 + 4;
			numbitscount--;
			
			if (numbitscount == 0)
			{
				return pos;
			}	
		}
		else
		{
			numbitscount = numbits;
		}
		
		if ( (val & 4) == 0)
		{	if (numbitscount == numbits) pos = i*8 + 5;
			numbitscount--;
			
			if (numbitscount == 0)
			{
				return pos;
			}	
		}
		else
		{
			numbitscount = numbits;
		}
		
		if ((val & 2) == 0)
		{	if (numbitscount == numbits) pos = i*8 + 6;
			numbitscount--;
			
			if (numbitscount == 0)
			{
				return pos;
			}	
		}
		else
		{
			numbitscount = numbits;
		}
				
		if ((val & 1) == 0)
		{	if (numbitscount == numbits) pos = i*8 + 7;
			numbitscount--;
			
			if (numbitscount == 0)
			{
				return pos;
			}	
		}
		else
		{
			numbitscount = numbits;
		}

	}

	return -1;
}

/*
*	Changes n bits from bitInitial in the bitmap
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void alterBits(int bitInitial, int n)
{
    int i;
	for (i=0; i < n; i++)
		alterBit(bitInitial+i);
//	for(i = 0; i < (n/8);i++)
//		ffs_bitmap[i] ^= 0xFF;

}


/*
*	Changes a given bit in the bit map
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void alterBit(unsigned int bitVal)
{
	unsigned int offset;
	unsigned int posVet;
	unsigned int temp;
	if (bitVal >= NUM_BITS_BITMAP) // sanity check
		return;
	
	offset = bitVal % 8;
	posVet = bitVal / 8;

	temp = 1 << (7 - offset);	// temp to store selected bit

	ffs_bitmap[posVet] = ffs_bitmap[posVet] ^ temp;	// changes bit
}


/*
*	Function to search for the value of the first empty block
*	in the bitmap. Returns its index
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
unsigned int firstEmptyBlock()
{
	unsigned char val;
	int i;

	for (i=0; i < (BITMAP_NUM_SECTORS*SECTOR_SIZE); i++)
	{
		val = ffs_bitmap[i];	// do some some consecutive logical ands
		/* TODO check if theres a better way to do this */
       	if ( (val & 128) == 0)
			return i*8 + 0;

		if ( (val & 64) == 0)
			return i*8 + 1;
		
		if ( (val & 32) == 0)
			return i*8 + 2;

		if ( (val & 16) == 0)
			return i*8 + 3;

		if ( (val & 8) == 0)
			return i*8 + 4;
		
		if ( (val & 4) == 0)
			return i*8 + 5;
		
		if ((val & 2) == 0)
			return i*8 + 6;
				
		if ((val & 1) == 0)
			return i*8 + 7;

	}	
	return 0xFFFF;

}

/*
*	Prints the bitmap (debug helper)
*       @author GRUPO 16 - Sistemas Operacionais II - Turma A
*/
void printBitMap()
{
	unsigned char val;
	int i;

	// Prints first X bytes (bits?) of the bitmap
	int X = 20;
	
	for (i=0;i<=X; i++)
	{
		val = ffs_bitmap[i];
	       	if ( (val & 128) == 128)
			putch('1');
		else
			putch('0');

		if ( (val & 64) == 64)
			putch('1');
		else
			putch('0');

		if ( (val & 32) == 32)
			putch('1');
		else
			putch('0');

		if ( (val & 16) == 16)
			putch('1');
		else
			putch('0');

		if ( (val & 8) == 8)
			putch('1');
		else
			putch('0');

		if ( (val & 4) == 4)
			putch('1');
		else
			putch('0');

		if ((val & 2) == 2)
			putch('1');
		else
			putch('0');

		if ((val & 1) == 1)
			putch('1');
		else
			putch('0');	

		putch('\n');
	}	
}

