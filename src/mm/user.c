/* Copyright (c) Gregor Mueckl 2000
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * This file contains the code of the user space MM part
 */

#ifndef _DEBUG_MM
#define _DEBUG
#endif

#include "stdio.h"

/*
 * void mem_map(current, start_addr, size, type, from)
 *
 * This function twists the user space memory to the task's liking.
 * This may mean allocating new data regions, mapping other module's
 * memory (indirectly, this region is marked, but non-present in the
 * page tables). "from" is a module ID that is only used when mapping 
 * another module's memory. Current is the Id of the module to change 
 * memory layout for.
 */

/* the different types of requested memory (excluding each other) */
#define MMT_DATA 1 /* data region */
#define MMT_CODE 2 /* code region */
#define MMT_MAP  3 /* mapped memory region */

void mem_map(int current,
	     unsigned long start_addr, 
	     unsigned long size, 
	     int type,
	     int from)
{
}

/*
 * void mem_free(current, start_addr, size)
 *
 * This function unmaps and frees the memory in the given range regardless of the
 * type if it isn't mapped anywhere.
 */
void mem_free(int current, unsigned long start_addr, unsigned long size)
{
}
