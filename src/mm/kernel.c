/* Copyright (c) Gregor Mueckl 2000
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* 
 * This file contains the code for the kernel space MM
 */

/*
 * There are a lot of bugs in this code. And there is no way to debug
 * it - Gregor, 2000-11-29
 */

/*
 * This is to enable/disable debugging at module level
 */
#ifdef _DEBUG_MM
#define _DEBUG
#endif

#include "pages.h"
#include "mem.h"
#include "stdio.h"

/* HOW KERNEL MEMORY ALLOCATION WORKS
 *
 * Static allocation of kernel memory is performed in the region of 0-2MB in
 * the kernel segment. For dynamic allocation we keep a list of blocks (either 
 * free or allocated) in the region above 2MB. When we allocate memory we first
 * of all search for a free block which is large enough. When this happens we 
 * split this block up (depends on its size) and mark a part of it (or the 
 * whole of it) as used. If there is no such block in existance, we request a 
 * new page and allocate the memory there.  Finally we need to return the 
 * block's address.
 *
 * For deallocating the block we first mark the block as unused. Then we try
 * to join the block with any surrounding free blocks if there are any. Then
 * we try to find an entirely empty page we may free again. The latter is a bit
 * more difficult than it seems since blocks may stretch over page breaks.
 */

/*
 * No longer true, because large (>4088 Bytes) and small blocks are now 
 * distinguished. Only large blocks can stretch over page boundaries.
 * Small blocks don't do this since they are kept in page-sized large blocks.
 *
 * Gregor, 2000-10-12
 */

/*
 * Small block support still isn't up, so every block size is handled by the
 * large block code, which works, but is wasting a lot of memory for small
 * structures - Gregor, 2001-05-05
 */

page_entry_type *kernel_pd; /* the kernel's page directory */

int num_page_tables;        /* number of allocated page tables */

extern int free_phys_pages;

struct kmem_block *blocklist;

struct small_block_container *containerlist;

void *small_alloc(unsigned long size);
void *large_alloc(unsigned long size);

/* TODO: Make proper implementation of small_alloc */
void *kalloc(unsigned long size)
{
  /*  if(size<=4088) {
      return small_alloc(size);
      } else {
      return large_alloc(size);
      }*/
  
  return large_alloc(size);
}

struct small_block_container *alloc_container()
{
  struct small_block_container *container;
  struct kmem_block *block;

  container=large_alloc(4088);
  if(container==0) {
    //kprintf("alloc_container: unable to allocate large block");
    return 0;
  }

  block=(struct kmem_block*)container-8;
  block->used=2;

  container->num_blocks=1;

  return container;
}

void *small_alloc(unsigned long size)
{
  struct small_block_container *container;


  //kprintf("small_alloc() currently not implemented\n");

  return 0;

  if(containerlist==0) {
    /* There is currently no container, so we allocate one */
    container=alloc_container();
    if(container==0) {
      return 0;
    }
  }
}

void *large_alloc(unsigned long size)
{
  void *addr;
  struct kmem_block *block;

  if(blocklist==0) {
    //kprintf("empty blocklist\n");

    addr=get_kernel_page_sequence((size>>12)+1);

    if(addr==0) {
      //kprintf("kalloc: page allocation failed\n");
      return 0;
    }

    //kprintf("addr %d\n",(int)addr);

    blocklist=addr;
    blocklist->used=1;
    blocklist->size=size-(size%4096)+1; // The block sizes are multiples of 4096
    blocklist->next=0;
    /* The next line looks weird, but isn't. We just want to get
     * the actual number of pages we allocated, that's all. Think
     * about how you would do that yourself.
     */
    //kprintf("first block allocated\n");
    return addr+8;
  }

  //kprintf("blocklist is not empty\n");

  addr=get_kernel_page_sequence((size>>12)+1);
  
  //debug_value("addr",addr);
  
  if(addr==0) {
    //kprintf("kalloc: page allocation failed\n");
    return 0;
  }

  /* Insert the new block into the sorted "blocklist"
   * (sorting is achieved by inserting blocks at the right
   * positions; the list's order is never changed)
   */
  
  if((unsigned long) addr < (unsigned long) blocklist) {
    ((struct kmem_block *)addr)->next=blocklist;
    blocklist=addr;
    block=addr;
  } else {
    block=blocklist;
    do {
      if((unsigned long) block->next > (unsigned long) addr) {
	((struct kmem_block*)addr)->next=block->next;
	block->next=addr;
	block=block->next;
	break; /* break resumes after the while loop */
      }

      block=block->next;
    } while(block!=0);
    
    if(block==0) {
      block=blocklist;
      while(block->next!=0) block=block->next;
      block->next=addr;
      block=block->next;
      block->next=0;
    }
  }

  /* Now we got "block" pointing to the area we allocated and the
   * kmem_block struct is correctly inserted into the sorted list.
   * So we can now initalise the rest of this struct.
   */
  block->size=size;
  block->used=1;
  
  return addr+8;
}

void large_free(void *data);
void small_free(void *data);

/* TODO: Make proper implementation of small_free() */
void kfree(void *data)
{
  //  unsigned long addr=(unsigned long)data;

  /* 
   * if block inclusive block description start at a page boundary it it a
   * large block (small blocks can't ever do that)
   */
  /*  if((addr - 8) % 4096 == 0) {
      large_free(data);
      } else {
      small_free(data);
      }
  */

  large_free(data);
}

void large_free(void *data)
{
	void *addr;
	struct kmem_block *block, *prev_block, *iterator;
	int block_size;

	addr=data-8;

	block=blocklist;
	do {
		if(block==addr) break;
		block=block->next;
	} while(block!=0);

	if(block==0) return;      /* this means we got a wrong address */

	//kprintf("found the block\n");

	block->used=0;

	iterator=blocklist;
	do {
		if(iterator->used==0) {
			// needed to keep a foot on the list if block is removed
			block=iterator;
			iterator=iterator->next;

			//kprintf("found large free block: size: ");
			//kprintf("%d", block->size);
			//kprintf(" at ");
			//kprintf("%d", (unsigned long)block);
			//kprintf("\n");

			/* 
			* The blocks start and end at a page boundary. This simplyfies things a
			* _lot_.
			*/

			if(blocklist==block) {
				//kprintf("first block in blocklist is free\n");
				prev_block=0;
			} else {
				prev_block=blocklist;
				while(prev_block->next!=block) prev_block=prev_block->next;
			}
		
			if(prev_block!=0) {
				/* this block of memory is not the first one in the list */
				prev_block->next=block->next;
			} else {
				/* we got the first block in the blocklist */
				blocklist=block->next;    
				//kprintf("blocklist updated\n");
			}

			/* free the pages contained within that block */
			block_size=(block->size+8)/4096;
			free_page_series((unsigned long *) kernel_pd, (void *) addr, (int) block_size);
			/*      addr=block;
				for(i=0;i<=block_size;i++) {
					debug_value("addr",addr);
				free_page(kernel_pd,addr);
				addr+=4096;
				} */
			//kprintf("page freed\n");
		} else {
			iterator=iterator->next;
		}
		
		//    debug_value("block",(int)block);
		//    block=block->next; // XXX crashes here! is block valid at this point?
		//kprintf("passed through loop\n");
	} while(iterator!=0);

  //kprintf("large_free done\n");
}

void small_free(void *data)
{
  //kprintf("small_free() currently not implemented\n");
}

/* create a new page table referenced from kernel_pd[pd_index] */
/* 
 * TODO: Remove this code and use add_page_table from pages.c.
 * Note that this will also remove the 632MB limit for the kernel
 * segment.
 */
void create_kernel_page_table(int *pd_index)
{
	int i;
	page_entry_type *page_table;
	struct page_entry pe;

	if(num_page_tables>=157) {  /* currently that's all we support (632MB) */
		//kprintf("create_kernel_page_table: all page tables allocated!\n");
		*pd_index=-1;
		return;
	}

	pe.present=0;
	pe.contents.unused.avail=0;
	page_table=(page_entry_type *) ((num_page_tables+2)*4096);
	for(i=0;i<1024;i++) {
		page_table[i]=convert_to_entry(&pe);
	}

	for(i=0;i<1024;i++) {
		convert_from_entry(kernel_pd[i],&pe);
		if(pe.present==0) break;
	}

	if(pe.present==1) {        /* hey, what the hack went wrong? */
		//kprintf("create_kernel_page_table: no free entry in page diretory found!\n");
		*pd_index=-1;
		return;
	}

	/* Would you call that a hack? */
	//  create_pt_entry(kernel_pd,(num_page_tables+2)<<12,i,0);

	add_page_table((unsigned long*)kernel_pd,i,0);

	*pd_index=i;
}

int kernel_insert_free_pages(int pd_index, int pt_index, int num_pages)
{
	unsigned long linear_addr;

	linear_addr=(pd_index<<22)+(pt_index<<12);
	//alloc_pages((unsigned long *) kernel_pd, (unsigned long) linear_addr, (unsigned int) num_pages,0);
	int i = -1;
	while (++i < num_pages) {
		add_page((unsigned long *)kernel_pd,linear_addr+(i*4096),linear_addr+(i*4096),0);
	}

	return 1;
}

/* started refactoring this mess. Removed most of the code and moved it to pages.c.
	There are still things that need being moved, but it will be done futurely.
	For now, I replaced the 'brute-force'-like page searching mechanism with heap/
	avl tree search algorithm, which sould considerably faster in larger memory
	systems.
	TODO finish porting this code to pages.c 
*/

unsigned int _kernel_stack_entry(void (*function)(void), unsigned int args_size,... ) {
	/* kernel memory page allocation is now a bit dangerous. Since we must disable
		paging to write in the PD, the allocation must happen in kernel stack
		area (i.e. first 64kbytes of memory. Also we cant be interrupted here, so
		disable interrupts and change the stack ! */
	/* also theres no point (and its also pottentialy harmful) changing the stack if
		its alread at kernel (i.e inside the first 64kb), so check this beforehand
        NOTE now its the caller responsibility */
	unsigned int ret;

	/* put this as volatile, tell the compiler to stay the heck away from it */
	asm volatile ("cli");
	asm volatile ( 	"movl %[arg_count],%%ecx \n"
					"movl %[function_name],%%edx \n"
					"pushl %%ebp \n" 
					"movl %%esp,%%eax \n" 
					"movl $0xFFFF,%%esp \n" 
					"pushl %%eax \n"
					"subl %%ecx,%%esp \n"
					"movl %%esp,%%edi \n"
					"movl %%ebp,%%esi \n"
					"addl $0x10,%%esi \n" /* ignore parent ebp and 2 first args, and ret val */
					"cld \n"
					"rep movsb \n" /* copy arguments to new stack */
					"call *%%edx \n"
// 					//"addl $0x4,%%esp \n" /* we wont clear the stack, we cant be sure how much was pushed */
					"movl (0xFFFB),%%esp \n"
					"popl %%ebp \n"
					"movl %%eax,%[ret_val] \n" /* the ret val from the function */
					:[ret_val] "=r"(ret) :[arg_count] "r"(args_size),[function_name] "r"((unsigned int)function) : "eax","ecx","edx");

	asm volatile ("sti");
	kprintf("asm returning: %X\n",ret);
	return ret;

}


void *get_kernel_page_sequence(int num_pages)
{

	unsigned long * laddr;
	//kprintf("looking for page seq : %i\n",num_pages);
	if (!in_kernel_stack()) {
		laddr = (unsigned long *)_kernel_stack_entry((void (*)(void))get_free_kernel_pages_region,sizeof(unsigned int),num_pages);
	}
	else {
		laddr = get_free_kernel_pages_region(num_pages);
	}
	kprintf("found page seq : %i, at %X\n",num_pages,laddr);
	return laddr;
// 	int num_pt, num_page, page_count;
// 	int base_pt, base_page;
// 	unsigned long pt_addr;
// 	struct page_entry pe;
// 
// 	num_pt=0;
// 	num_page=512;
// 	page_count=0;
// 	base_pt=-1;
// 	base_page=-1;
// 
// 	/* In this loop a series of free pages is searched for.
// 	* It's a little bit tricky to read, but actually looks
// 	* through each page in each page table in the page directory
// 	* to find a first series of free pages that is long enough.
// 	* It just does "first match".
// 	*/
// 	while((page_count<num_pages) && ((num_pt<=1023) && (num_page<=1023))) {
// 		//kprintf("num_pt %d\n",num_pt);
// 		//kprintf("num_page %d\n",num_page);
// 		convert_from_entry(kernel_pd[num_pt],&pe);
// 		/* There are two possible cases: 
// 		* 1. The page table is there
// 		* 2. The page table does not exist
// 		*/
// 		if(pe.present==1) {
// 			/* In an existing page table every page is tested
// 			* whether it is free
// 			*/
// 			pt_addr=pe.contents.used.address<<12;
// 
// 			convert_from_entry(((unsigned long*)pt_addr)[num_page],&pe);
// 			if(pe.present==0) {
// 				/* Another free page found */
// 				if(base_pt!=-1) {
// 					page_count++;
// 				} else {
// 					base_pt=num_pt;
// 					base_page=num_page;
// 					page_count=1;
// 				}
// 			} else {
// 				/* The series of page tables isn't long enough,
// 				* so we have to reset the counters
// 				*/
// 				base_pt=base_page=-1;
// 				page_count=0;
// 			}
// 
// 			num_page++;
// 			if(num_page>=1024) {
// 				num_page=0;
// 				num_pt++;
// 			}
// 
// 		} else {
// 			/* When a page table does not exist the pages that would be held
// 			* by it are assumed free.
// 			*/
// 			if(base_pt!=-1) {
// 				if(page_count+1024>num_pages) {
// 					page_count=num_pages;
// 				} else {
// 					page_count+=1024;
// 				}
// 			} else {
// 				base_pt=num_pt;
// 				base_page=num_page;
// 				if(num_pages>1024) {
// 					page_count=1024;
// 				} else {
// 					page_count=num_pages;
// 				}
// 			}
// 			num_page=0;
// 			num_pt++;
// 		}
// 	}
// 
	/* Here two things may have happened: either a block of memory was found or
	* the loop ran through to its end not having found what was wanted.
	* We check for the latter case first
	*/
	//if (page_count<num_pages) {
// 	if (!laddr) {
// 		//kprintf("get_kernel_page_sequence: unable to get a series of pages as long as requested\n");
// 		return 0;
// 	}
// 
// 	unsigned int base_pt = PT_INDEX(laddr);
// 	unsigned int base_page = PAGE_OFFSET(laddr);
// 
// 	if(kernel_insert_free_pages(base_pt,base_page,num_pages)!=1) {
// 		return 0;
// 	}

	//return (void*)indices_to_linear_addr(base_pt,base_page);
	
}
