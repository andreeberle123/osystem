/* Copyright (c) Gregor Mueckl 2000
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*  WARNING when using functions from this file, care must be taken with
	the stack. Most functions here must run inside kernel-area stack. The
	pages.h file provides some inline functions, wrapped by safe_, to prevent
	such problems */

#ifdef _DEBUG_MM
#define _DEBUG
#endif

#include "mem.h"
#include "pages.h"
#include "stdio.h"
#include "heap.h"
#include "avl.h"

#include "process.h"
#include "sched.h"
#include "io.h"
#include "8259a.h"

/*
 * Here we might warp the memory layout a bit (actually, we won't...).  We
 * will set up a kernel page directory with linear addressing on the first
 * 2MB of physical memory (I don't have a good feeling about this, though).
 * The rest of the kernel segments (1GB) are voided and may be allocated
 * dynamically at need. The kernel page directory is located at 0x1000 since
 * I don't want to overwrite the real mode IDT. The page table is following
 * immediately thereafter (we need only one for the beginning).
 *
 * NOTE: this causes the current executable loader to crash!
 *
 * A user space page directory is _not_ set up since it will specific to
 * each individual process (and we don't have any yet) and this should be
 * done while getting the process up anyway.
 *
 * NOTE the kernel space is fixed for now, at 512 (first 2MB) memory.
 * Surpassing this limit will generate page fault. 2MB is too few, extend
 * it in the future.
 */

/*
 * This code is undergoing major changes. I am changing the memory layout
 * as follows: Kernel area is restricted to a fixed memory area, namely
 * 4MB for now, but this can be changed at any moment. First 2 MB is divided
 * as follows:
 * First 1MB is reserved, and used for basic kernel paging data, and other
 * memory allocation data. From 1MB to 2MB is reserved for boot code, where
 * boot data is loaded (code and static memory). Following space from 2MB to
 * 4MB is free and used for kernel dynamic pages.
 * NOTE changed this a bit. Actually from page 16 (to 0) is currently the kernel
 * stack. So, memory data is from 16 ahead.
 */

/* The first 1MB will contain basic data for memory allocation management
 * and the layout is as follows:
 * TODO
 */

extern page_entry_type *kernel_pd; /* the kernel's page directory */

int free_phys_pages;        /* number of free physical pages */

extern int num_page_tables;        /* number of allocated page tables */

extern struct kmem_block *blocklist;

extern struct small_block_container *containerlist;

unsigned long *first_free_non_kernel_page; /* the physical address of the first unused page, at non-kernel space */
unsigned long *first_free_kernel_page; /* the physical address of the first unused page, at kernel space */
unsigned long heap_size = 0;

int paging_enabled = 0;

/*********************************************************************/
/* some functions to optimize pages locating */

free_region_entry * free_region_heap_root = 0;

/* TODO I think a good hash map would be a better replacement for these avl trees */
free_region * free_region_avl_tree_root = 0;
free_region * free_region_avl_reverse_tree_root = 0;
unsigned long * first_free_region_avl_tree_entry = 0;


/* TODO the following functions are kernel-related paging search. Many of these functions
	can be generalized and used with process pd heap and avl search */

free_region_entry * add_free_kernel_region_entry(unsigned int num_pages, unsigned long *region_start) {
	free_region_entry f_entry;
	f_entry.num_pages=num_pages;
	if (!free_region_heap_root) {
		/* if no entry exist we set the heap root to it, the
		kernel free regions start at page 3, and can be expanded
		as necessary. As such the array */
		/* NOTE the heap requires a free continuous region. Since we only have 512
			free pages at kernel, a single page is enough for the heap (actually
			the heap needs half a page currently). As kernel mem increases tough,
			more pages will need to be freed to the heap. Currently, we will reserve
			8 pages for heap, which is a lot, but may need expansion in the future 
			(as a side note 8 pages heap can store up 16mb (in worst case) memory
			as pages).
			*/
		free_region_heap_root = (free_region_entry *) (FIRST_FREE_REGION << 12) ;
	}
	/* TODO move this code to a init() like code */
	free_region * f_region;
	if (!free_region_avl_tree_root) {
		f_region = (free_region *) (AVL_REGION << 12);
		dword i;
		first_free_region_avl_tree_entry = ((unsigned long*)f_region)+sizeof(free_region);
		unsigned long *ptr = first_free_region_avl_tree_entry;
		for(i=sizeof(free_region)+(unsigned long)f_region;i<(dword)(f_region+4096/sizeof(free_region));i+=sizeof(free_region)) {
			*ptr = i;
			ptr = (unsigned long *)*ptr;
		}
	}
	else {
		if (first_free_region_avl_tree_entry) {
			f_region = (free_region *)first_free_region_avl_tree_entry;
			first_free_region_avl_tree_entry = (unsigned long*)*first_free_region_avl_tree_entry;

		}
		else {
			kprintf("AVL Needs more pages!!\n");
			/* TODO allocate another page */
		}
	}
	f_region->linear_addr_strt = region_start;
	/* we supply a temporary region for the heap to perform swap ops. at this point
		this kind of allocation internally is not viable */
	free_region_entry f_temp;
	f_entry.page_region_start=f_region;
	/* insert the region at the heap */
	unsigned long * heap_entry = insert_heap((unsigned long *)free_region_heap_root, &f_entry,sizeof(free_region_entry), &heap_size, &f_temp);
	f_region->heap_addr_strt = heap_entry;

	insert_avl_tree((avl_tree_node **)&free_region_avl_tree_root, (avl_tree_node *)f_region);

	/* build also a reversed avl tree, containing the final address of each region
		*/
	if (!free_region_avl_reverse_tree_root) {
		f_region = (free_region *) ((AVL_REGION+1) << 12);
	}
	else {
		if (first_free_region_avl_tree_entry) {
			f_region = (free_region *)first_free_region_avl_tree_entry;
			first_free_region_avl_tree_entry = (unsigned long*)*first_free_region_avl_tree_entry;
		}
		else {
			kprintf("AVL Needs more pages!!\n");
			/* TODO allocate another page */
		}
	}

	f_region->linear_addr_strt = region_start+(num_pages << 12);
	f_region->heap_addr_strt = heap_entry;
	insert_avl_tree((avl_tree_node **)&free_region_avl_reverse_tree_root, (avl_tree_node *)f_region);

	return (free_region_entry *)heap_entry;
}

/*
 NOTE this function doesnt check if the region is already flagged */
void flag_free_kernel_region(unsigned long * linear_addr, unsigned int size) {
	//free_region_entry * f_entry = add_free_kernel_region_entry(size,linear_addr);
	add_free_kernel_region_entry(size,linear_addr);	
}



void free_avl_tree_region_entry(unsigned long * addr) {
	*addr = (unsigned long)first_free_region_avl_tree_entry;
	first_free_region_avl_tree_entry = addr;
}

void remove_h_entry(free_region_entry * h_entry) {

	free_region * p_data = h_entry->page_region_start;

	unsigned long * addr = (unsigned long *)remove_avl_tree((avl_tree_node **)&free_region_avl_reverse_tree_root, (unsigned long)p_data->linear_addr_strt+(h_entry->num_pages << 12));
	free_avl_tree_region_entry(addr);
	addr = (unsigned long *)remove_avl_tree((avl_tree_node **)&free_region_avl_tree_root, (unsigned long)p_data->linear_addr_strt);
	free_avl_tree_region_entry(addr);

	free_region_entry f_tmp;
	remove_heap((unsigned long *)free_region_heap_root,(unsigned long *)h_entry,&f_tmp,sizeof(free_region_entry),&heap_size);
}

void update_h_entry(free_region_entry * h_entry, int num_pages, unsigned long * new_addr) {
	if (num_pages) {
		unsigned long * end_addr = (unsigned long *)h_entry->page_region_start->linear_addr_strt+(h_entry->num_pages << 12);
		dword old_pages = h_entry->num_pages;
		h_entry->num_pages = num_pages;
		free_region_entry f_tmp;
		//kprintf("%i, %i, %i\n",h_entry,h_entry->page_region_start,num_pages);
		if (old_pages > num_pages)
			h_entry = (free_region_entry *)heap_down((unsigned long *)free_region_heap_root,(unsigned long *)h_entry,&f_tmp,sizeof(free_region_entry),heap_size);
		else 
			h_entry = (free_region_entry *)heap_up((unsigned long *)free_region_heap_root,(unsigned long *)h_entry,&f_tmp,sizeof(free_region_entry));
		//kprintf("%i, %i\n",h_entry,h_entry->page_region_start);
		h_entry->page_region_start->heap_addr_strt = (unsigned long *)h_entry;
		if (h_entry->page_region_start->linear_addr_strt != new_addr) {
			free_region * av_entry = remove_avl_tree((avl_tree_node **)&free_region_avl_tree_root,(unsigned long)h_entry->page_region_start->linear_addr_strt);
			av_entry->linear_addr_strt = new_addr;
			insert_avl_tree((avl_tree_node **)&free_region_avl_tree_root, (avl_tree_node *)av_entry);
		}
		inv_free_region *i_f_r = (inv_free_region *) avl_tree_bsearch_non_empty((avl_tree_node *)free_region_avl_reverse_tree_root,(unsigned long)end_addr);
		i_f_r->heap_addr_strt = (unsigned long *)h_entry;
	}
	else {
		remove_h_entry(h_entry);
	}
} 

/* This function always alloc the pages at heap top (first and largest fit) */
unsigned long * alloc_kernel_region(unsigned int size) {
	free_region_entry * f_entry = (free_region_entry *)free_region_heap_root;
	dword available = f_entry->num_pages;
	free_region * f_r = f_entry->page_region_start;
	unsigned long * addr = f_r->linear_addr_strt;
	if (available < size) {
		kprintf("Warning: Heap overflow!!");
		/* no region this large is available TODO check for relloc/defrag possibility */
		/* TODO panic? */
		return 0;
	}
//kprintf("updating %i, %i, %i, %i\n",free_region_heap_root,available-size,addr+(size<<10),addr);
	update_h_entry(free_region_heap_root,available-size,addr+(size<<10)); /* size <<10 since we are 4byte aligned (u long) */
//kprintf("found at = %i, heap root is now at = %i\n",addr,f_entry->page_region_start->linear_addr_strt);
//kprintf("looking, available: %i, looking for: %i\n",available,size);
	return addr;
}

void free_kernel_region(unsigned long * linear_addr, unsigned int size) {
	unsigned int freed_size = size;
	unsigned long * freed_region_start = linear_addr;
	/* concatenate (if necessary) region to its right region */
	free_region * f_rg = (free_region *) avl_tree_bsearch_non_empty((avl_tree_node *)free_region_avl_reverse_tree_root,(unsigned long)(linear_addr+(size << 12)));
	if (f_rg->linear_addr_strt == (linear_addr+(size << 12))) {
		freed_size += ((free_region_entry*)f_rg->heap_addr_strt)->num_pages;
		/* remove the right side region, since its is either being merged in the
			current region, or in the left one */
		remove_h_entry((free_region_entry *)f_rg->heap_addr_strt);
	}
	/* concatenate (if necessary) region to its left region */
	inv_free_region * f_r = (inv_free_region *) avl_tree_bsearch_non_empty((avl_tree_node *)free_region_avl_reverse_tree_root,(unsigned long)linear_addr);
	if (f_r->linear_addr_end == linear_addr) {
		freed_size += ((free_region_entry*)f_r->heap_addr_strt)->num_pages;
		freed_region_start = (unsigned long *)((free_region_entry*)f_r->heap_addr_strt)->page_region_start;
		/* just change the left region size to the current free size (and adjust the heap) */
		update_h_entry((free_region_entry *)f_r->heap_addr_strt,freed_size,freed_region_start);
	}
	else {
		/* if no concatenation to the left is performed, insert a new region in the heap */
		flag_free_kernel_region(linear_addr,size);
	}
}

unsigned long * get_free_kernel_pages_region(unsigned int size) {
	unsigned long * addr = alloc_kernel_region(size);
	int i = 0;
	while (i < size) {
		add_page((unsigned long *)kernel_pd,(unsigned long)(addr+(i<<10)),(unsigned long)(addr+(i<<10)),0);
		i++;
	}
	kprintf("returning %X\n",addr);
	return addr;
}


/*********************************************************************/

void InitPages()
{
  struct page_entry pe;
  page_entry_type *pagetable;
  int i, increment;

  /* setup the page directory */
  kernel_pd= (page_entry_type *) 0x1000;   /* base address of kernel page directory */
  pe.present=0;
	/* put empty entries in the entire directory (suppose some memset(0) call would be better )*/
  for(i=0;i<1024;i++) {
    kernel_pd[i]=convert_to_entry(&pe);
  }

  pe.present=1;
  pe.contents.used.address=2;        /* The first pages of memory should stay untouched, here the second kernel page is reserved */
  pe.contents.used.dirty=0;
  pe.contents.used.accessed=0;
  pe.contents.used.is_user=0;
  pe.contents.used.writeable=1;
  kernel_pd[0]=convert_to_entry(&pe);

  /* setup the first page table */
  pagetable= (page_entry_type *) 0x2000;

  pe.present=1;
  pe.contents.used.dirty=0;
  pe.contents.used.accessed=0;
  pe.contents.used.is_user=0;
  pe.contents.used.writeable=1;

  for(i=1;i<KERNEL_MAX_BOOT_AREA;i++) {
    pe.contents.used.address=i;
    pagetable[i]=convert_to_entry(&pe);
  }

  /* address 0x00000000 is used for the NULL pointer, so make it nonexistent */
  pe.present=0;
  pe.contents.unused.avail=0;
  pagetable[0]=convert_to_entry(&pe);

  /* make the rest of the page table invalid -- (why???) */
  for(i=KERNEL_MAX_BOOT_AREA;i<KERNEL_MAX_PAGES;i++) {
    pagetable[i]=convert_to_entry(&pe);
  }

  /* OK, now we set up the linked list of free pages */

  //first_free_page=(unsigned long *) (0x400<<12); /* the first free page is currently at 2MB */
	/* the first free (non-kernel) page is currently at 4MB */
	first_free_non_kernel_page=(unsigned long *) (KERNEL_MAX_PAGES << 12); 
	/* the first free kernel page is currently at 2MB */
	first_free_kernel_page = (unsigned long *) (KERNEL_MAX_BOOT_AREA << 12); 

	increment = (int) ((mem_end/4096) - 512) / 10;

	for(i=KERNEL_MAX_PAGES;i<mem_end/4096;i++) {
		unsigned long *ptr;
		ptr=(unsigned long *) (i<<12);
		if(i<((mem_end/4096)-1)) {
			*ptr=(i+1)<<12;
		} else {
			*ptr=0;
		}

		if ((i - 512) % increment == 0) {
			kprintf(".");
		}
	}
	/* some hack here, just make the last kernel page point to nowhere, spliting
		the lists */
	unsigned long *kernel_end = (unsigned long *)((KERNEL_MAX_PAGES-1) << 12);
	*kernel_end = 0;

	kprintf("\n");

	/* set up the heap/avltree for paging search. Currently mark the entire kernel
		paging area (2mb-4mb region) as free */
	flag_free_kernel_region((unsigned long *)(KERNEL_MAX_BOOT_AREA << 12),KERNEL_MAX_PAGES-KERNEL_MAX_BOOT_AREA);
	

	/* having done that lot we can now activate the kernel page directory */
	use_pd((unsigned long)kernel_pd);
	toggle_paging();
	paging_enabled = 1;

	/* Do some additional initalisation... */
	/* TODO revisit this free_phys_pages counter */
	free_phys_pages=(mem_end/4096)-512;    /* 2MB is 512 pages */
	num_page_tables=1;
	blocklist=0;
	containerlist=0;
}

/* Some weird macro magic would certainly be faster than this, but also more
 * complicated. That's why I leave this as it is for now. TODO change this
 * into macro, this is quite slow !!
 */
void convert_from_entry(page_entry_type entry, struct page_entry *pe)
{
  pe->present=entry&0x01;
  if(pe->present==0) {
    pe->contents.unused.avail=entry>>1;
  } else {
    pe->contents.used.writeable=(entry&0x02)>>1;
    pe->contents.used.is_user=(entry&0x04)>>2;
    pe->contents.used.accessed=(entry&0x10)>>5;
    pe->contents.used.dirty=(entry&0x20)>>6;
    pe->contents.used.avail=(entry&0x0100)>>9;
    pe->contents.used.address=(entry&0xFFFFF000)>>12;
  }
}

page_entry_type convert_to_entry(struct page_entry *pe)
{
  page_entry_type entry=0;

  entry=pe->present;
  if(pe->present==0) {
    entry+=pe->contents.unused.avail<<1;
  } else {
    entry+=pe->contents.used.writeable<<1;
    entry+=pe->contents.used.is_user<<2;
    entry+=pe->contents.used.accessed<<5;
    entry+=pe->contents.used.dirty<<6;
    entry+=pe->contents.used.avail<<9;
    entry+=pe->contents.used.address<<12;
  }

  return entry;
}

/*
 * These functions get tough because we need to find a free entry to the
 * page tables (not a big problem) and a free physical page at the same time
 * (the real problem).
 *
 * Finding a free space in the virtual address room isn't a problem: just
 * search the page tables for a free entry or create a new, empty page table.
 * Freeing a page primarily means marking the page entry in the appropriate
 * table as free and making the page itself reuseable (see below for how this
 * is done).
 *
 * Finding a free physical page is easy as well: In InitPages() all available
 * pages of memory are inserted into a linked list (a stack ,actually). To
 * get free page, we just pop the uppermost entry. To make a free page
 * available, we push it on top of the stack again.
 *
 */


void use_kernel_pd() {
	use_pd((unsigned long)kernel_pd);
}


/* try to find a free physical page of memory - easier than it sounds */
unsigned long get_free_page()
{
	unsigned long address;
	if(free_phys_pages==0) {   /* there's no free page left */
		//kprintf("free_phys_pages==0\n");
		return 0;
	}

  //  debug_value("free_phys_pages",free_phys_pages);
	toggle_paging();

	address=(unsigned long)first_free_non_kernel_page;
	first_free_non_kernel_page = (unsigned long *) *first_free_non_kernel_page;

	free_phys_pages--;

	toggle_paging();

	return address;
}

/*
 * TODO this is wrong. No kernel pages are stored here for now, so this code is
 * currently useless. We need to merge cetain kernel.c functions and this for proper
 * memory management.
 */
unsigned long get_free_kernel_page()
{
	/* TODO create a pool of single pages to alloc directly, outside the heap */
	return (unsigned long)get_free_kernel_pages_region(1);
// 	unsigned long address;
// 
// 	if(free_phys_pages==0) {   /* there's no free page left */
// 		//kprintf("free_phys_pages==0\n");
// 		return 0;
// 	}
// 
//   //  debug_value("free_phys_pages",free_phys_pages);
// 
// 	toggle_paging();
// 	
// 	address=(unsigned long)first_free_kernel_page;
// 	first_free_kernel_page = (unsigned long *) *first_free_kernel_page;
// 
// 	free_phys_pages--;
// 
// 	toggle_paging();
// 
// 	return address;
}

/* add the given page to the pool of free pages */
void free_physical_page(unsigned long addr)
{
	toggle_paging();
	*(unsigned long*)addr=(unsigned long)first_free_non_kernel_page;
	first_free_non_kernel_page= (unsigned long *) addr;
	toggle_paging();
}

/*
 * This set of functions does all the direct manipulations to the page tables
 * and page directories. They are used by both parts of the memory manager
 * (kernel and user space).
 * They return 0 on failure, 1 on success unless specified otherwise
 */

/*
 * Tries to retrieve the linear address for the given physical address
 * relative to the given page directory
 * Returns the address on success, 0 on failure
 * TODO set up a hashmap to avoid this mess
 */
unsigned long get_linear_address(page_entry_type *pd, unsigned long physical_addr)
{
	unsigned int pd_index, pt_index;
	unsigned long *page_table;
	struct page_entry pe;

	for(pd_index=0;pd_index<1024;pd_index++) {
		convert_from_entry(pd[pd_index],&pe);
		if(pe.present==1) {
			page_table=(unsigned long *) (pe.contents.used.address<<12);
			for(pt_index=0;pt_index<1024;pt_index++) {
				convert_from_entry(page_table[pt_index],&pe);
				if(pe.present==1) {
					if(pe.contents.used.address==(physical_addr>>12)) {
						//kprintf("getlin = %i %i %i\n",indices_to_linear_addr(pd_index,pt_index),physical_addr,indices_to_linear_addr(pd_index,pt_index)+(physical_addr & ((1<<13)-1)));
						/*
						* This looks like magic but does nothing else than
						* calculating the linear address from the indices we found
						*/
						/* NOTE changed the lshift value to 12, so we get the first 4091 bytes, 
							the <<13 was producing a wrong offset*/
						return indices_to_linear_addr(pd_index,pt_index)+(physical_addr & ((1<<12)-1));
					}
				}
			}
		}
	}
	/* When we leave the loop we failed */
	return 0;
}

/*
 * Tries to lookup the physical address of the given linear address using the
 * page table
 */
unsigned long get_physical_address(page_entry_type *pd, unsigned long linear_addr)
{
	unsigned long phys_addr;
	int pd_index, pt_index;
	unsigned long *page_table;
	struct page_entry pe;

	pd_index=PD_INDEX(linear_addr);
	pt_index=PT_INDEX(linear_addr);

	convert_from_entry(pd[pd_index],&pe);
	page_table=(unsigned long *) (pe.contents.used.address<<12);

	convert_from_entry(page_table[pt_index],&pe);
	phys_addr=pe.contents.used.address<<12;

	return phys_addr;
}
int userloc = 0;
/* shortcut to add a new page to a page table */
void create_pt_entry(page_entry_type *pt, unsigned long address, int pt_index, int isuser)
{

  struct page_entry pe;

  pe.present=1;
  pe.contents.used.address=address>>12;
	//kprintf("putting pt, addr = %i, pt_index = %i, pt = %i\n",address/4096,pt_index,((unsigned long)pt)/4096);
  if(!isuser) {
    pe.contents.used.is_user=0;
  } else {
    pe.contents.used.is_user=1;
  }
  pe.contents.used.dirty=0;
  pe.contents.used.accessed=0;
  pe.contents.used.writeable=1;

  pt[pt_index]=convert_to_entry(&pe);

}

/* Add an empty page table to a page directory at the given position */
int add_page_table(unsigned long *pd, unsigned int numentry, int isuser)
{
	struct page_entry pe;
	unsigned long phys_addr;
	int i;
	
	toggle_paging();
	convert_from_entry(pd[numentry],&pe);
	toggle_paging();
	
	if(pe.present==1) return 1;
	if (isuser) {
		phys_addr=get_free_page();
	}
	else {
		phys_addr=get_free_kernel_page();
	}
	
	toggle_paging();
	//phys_addr=(page_entry_type *) get_kernel_page_sequence(1);
	create_pt_entry((unsigned int*)pd,phys_addr,numentry,isuser);
	
	//kprintf("pt at %X, %i, %i\n",phys_addr,phys_addr>>22,numentry);
	
	/* TODO If the page table is for the kernel directory, it MUST be in kernel space,
		therefore it must be mapped directly to its physical page */
	// make all the pages in the table nonexistent
	pe.present=0;
	for(i=0;i<1024;i++) {
		((unsigned long *)phys_addr)[i]=convert_to_entry(&pe);
	}
	
	//kprintf("%X, %X, %i\n",phys_addr, phys_addr,numentry);
	/* NOTE patched it here, if the page where the page table is, is referenced by the
		page table itself, then we must write this reference here already, to avoid page
		fault.
		TODO still, this is done physically, with paging turned off. This will only work
		for some time, we must dump the current physical page, if another page is present
		(if swapping is supported)
	*/
	int alloc_ed = 0;
	if (PD_INDEX(phys_addr) == numentry) {
		alloc_ed = 1;
		//kprintf("Creating self reference to %X\n",(phys_addr>>12)%1024);
		create_pt_entry((unsigned int*)phys_addr,phys_addr,(phys_addr>>12)%1024,isuser);
//		kprintf("%i, idx = %i\n",phys_addr,(phys_addr>>12)%1024);
	}
	toggle_paging();

	if (!alloc_ed) {
	//	kprintf("Allocating pages for %i\n",phys_addr);
		/* the page table linear address must map to the same physical address
			so MMU can find it,
			TODO */
		add_page(pd,phys_addr,phys_addr,isuser);
		//kprintf("Allocated pages for %i\n",phys_addr);
	}

	// according to Intel docs, the cache has to be flushed now
	flush_paging_cache();

	return 1;
}

/* Check, whether given page table is empty - returns 0 when true, 1 when false*/
int page_table_empty(unsigned long *pd, unsigned int numentry)
{
  struct page_entry pe;
  unsigned long *pt;
  int i;

  convert_from_entry(pd[numentry],&pe);

  /* Assume a page table is empty when it is not present */
  if(pe.present==0) return 0;

  pt=(unsigned long *) get_linear_address(kernel_pd,pe.contents.used.address<<12);

  /* If we cannot access the page table (strange!) we must assume it is not empty */
  if(pt==0) {
    //kprintf("page_table_entry: unable to access page specified!\n");
    return 1;
  }

  for(i=0;i<1024;i++) {
    convert_from_entry(pt[i],&pe);
    /* there must no page be present */
    if(pe.present==1) return 1;
  }

  return 0;
}

/* Remove a page table from the given position in a page directory if empty */
int remove_page_table(unsigned long *pd, unsigned int numentry)
{
  if(page_table_empty(pd,numentry)) return 1;

  /* BUG: This function is not yet implemented */
  //kprintf("remove_page_table: code to actually remove a page table still missing!\n");  

  return 0;
}

/*
 * Add the pages at the given physical address to the page directory/page table
 * at the given linear address
 */
int add_page(unsigned long *pd, unsigned long linear_addr, unsigned long phys_addr, int isuser)
{
	
	//kprintf("Adding page, linear_addr is %X, phys is %X\n",linear_addr,phys_addr);
	unsigned long *page_table;
	struct page_entry pe;
	int pd_index, pt_index;
	//kprintf("Adding at %X, to %X\n",linear_addr,phys_addr);
	pt_index=PT_INDEX(linear_addr);
	
	pd_index=PD_INDEX(linear_addr);
	//kprintf("pti = %i, ptd = %d\n",pt_index,pd_index);
	/* get the address of the page tables we got to alter */
	toggle_paging();
	convert_from_entry(pd[pd_index],&pe);
	toggle_paging();

	if(pe.present!=1) {
		if(!add_page_table(pd, pd_index, isuser)) {
			return 1;
		}
		/* pd is no longer assumed to be safe (i.e. referenced by a page directory */
		toggle_paging();
		convert_from_entry(pd[pd_index],&pe);
		toggle_paging();
	}

	toggle_paging();
	/* get the linear address of the page table so we can access it */
	page_table=(unsigned long *) get_linear_address((unsigned int*)pd,pe.contents.used.address<<12);
	toggle_paging();

	if(page_table==0) {
		/* 
		* If this happens there's something wrong because the kernel ought to see every page table.
		* In fact we should PANIC in those cases (and thereby halt the system).
		* NOTE this is no longer true. kernel (as in kernel pd) doesnt see every page table,
		* we need to disable paging to seek them.
		*/
		//kprintf("add_pages: error: no linear address got for page table\n");
	}
	toggle_paging();
	/* finally insert the page */
	create_pt_entry((unsigned int*)page_table,phys_addr,pt_index,isuser);
	//kprintf("Finishing for %X, to %X\n",linear_addr,phys_addr);
	toggle_paging();
	return 0;
}

/*
 * Allocate pages and add them to the page directory/page table at the
 * specified address. Uses add_pages.
 */
/*
 * NOTE this allocates anywhere, specially the (kernel) page tables. Page tables should
 * be allocated at kernel space
 */
int alloc_pages(unsigned long *pd, unsigned long start_addr, unsigned int num_pages, int isuser)
{
	//kprintf("allocing: %X, %i, %i\n",start_addr,num_pages, isuser);
	unsigned long phys_addr;
	int i;
	for(i=0;i<num_pages;i++) {
		if (isuser)
			phys_addr=get_free_page();
		else
			phys_addr=get_free_kernel_page();
		if (!phys_addr)
			return 0;
		add_page(pd,start_addr+(i*4096),phys_addr,isuser);
	}
// 	//kprintf("alloc complete\n");
	// BUG: We don't check for failure of get_free_page or add_page yet
	return 1;
}

/* Try to find an array of free pages in the given region in the page directory */
int find_free_region(unsigned long *pd, unsigned int num_pages, unsigned long start_addr, unsigned long end_addr)
{
	// how to do this efficiently?
	//kprintf("find_free_region() unimplemented (will only be done when needed)\n");
	return 1;
}

/* Completely remove a page directory and all allocated memory within */
int free_page_directory(unsigned long *pd)
{
	//kprintf("free_page_directory() unimplemented (will only be done when needed)\n");
	return 1;
}

/* Free the page in the page table with the given linear address */
/* BUG: This function depends on a perfectly valid page.
 * No checks are made.
 */
void free_page(unsigned long *pd, void *addr)
{
	struct page_entry pe;
	unsigned long phys_addr;
	int pd_index, pt_index;
	unsigned long *pt;

	phys_addr=(unsigned long) get_physical_address((page_entry_type *)pd,(unsigned long)addr);

	//kprintf("phys_addr %d\n",(int)phys_addr);

	pd_index=PD_INDEX(addr);
  	pt_index=PT_INDEX(addr);

  //kprintf("pd_index %d\n",pd_index);
  //kprintf("pt_index %d\n",pt_index);

  convert_from_entry(pd[pd_index],&pe);
  pt=(unsigned long *) (pe.contents.used.address<<12);
  convert_from_entry(pt[pt_index],&pe);
  pe.present=0;
  pt[pt_index]=convert_to_entry(&pe);

  //debug("page present flage set to 0\n");

  flush_paging_cache();
  
  free_physical_page(phys_addr);

  if(page_table_empty(pd,pd_index)==0) {
    remove_page_table(pd,pd_index);
  }

  flush_paging_cache();
}

void free_page_series(unsigned long *pd, void *addr, int num_pages)
{
  int i;

  /* clear the lower 12 bits of the address */
  addr-=((unsigned long)addr)%4096;

  for(i=0;i<num_pages;i++) {
    free_page(pd,addr);
    addr+=4096;
  }

	if (num_pages && pd == (unsigned long *)kernel_pd) {
		free_kernel_region(addr,num_pages);
	}
}

/********************************************************************/

page_entry_type *valid_directories;

process_pd_entry * current_pd_entry;

//process_pd_entry * pd_entries;

#define page2addr(data) data/4096;

/*
 * NOTE I will tread lightly here, since I am not sure this is the
 * best way to handle process paging.
 */

/*
 * NOTE All following methods must be performed with paging off
 * (not necessary)
 */

/* This function sets up a directory for paging, given a certain process
 * TODO
 * I am not sure how this is actually supposed to work. For now allocate
 * continuously pages for new processes, reusing dead process spaces.
 * This however creates an (virtually) endlessly growing array of page directories, 
 * established in kernel area (physical) in arbitrary memory positions. I am
 * pretty sure this is unsafe right now, but I am counting this memory area will (at least
 * in future) remain untouched, as all kernel memory will reside in non-reserved
 * pages, where the directories will remain.
  */

page_entry_type *create_process_page_directory() {
	
	page_entry_type * process_pd;
	// BUG get_free_page() calls toggle_paging() having the opposite effect, fix this
	

	/* TODO this is messed up, kernel free pages arent stored here, so we will use kernel.c
		code for now, but the paging system must be cleaned up */
	//process_pd = (page_entry_type *) get_free_kernel_page();
	//process_pd = (page_entry_type *) get_free_kernel_pages_region(1);  
	/* put the directory in the next free page */
	/* the directory must exist in a non-referenced physical page (at least non
		referenced from a paging enabled point of view */
	process_pd = (page_entry_type *) get_free_page();
	toggle_paging();	
	/* map the page to the kernel page directory */
	/* TODO this is wrong. The new directory MUST be in kernel area (i.e. the 2MB or whatever we will
	be using as kernel space), otherwise kernel code in other directories may not find it. 
	Even though we can alloc_pages anywhere, the get_free_page() doesnt differentiate
	pages area */
	/* TODO rethink this. The directory building is performed with paging off, which means
	we are writing at a physical page. Instead writing at a kernel page ought to
	suffice, as long as it remains inside kernel reserved mem space. */

	/* So we store all process directories in kernel area? This cant work forever
		I think, and where are the page tables?? */
	struct page_entry pe;
	pe.present = 0;
	int i;

	for (i = 0; i < 1024 ; i++) {
		process_pd[i]=convert_to_entry(&pe);
	}

	/* build a page table now */
	//ptable = (page_entry_type *) get_free_page(); 
	//alloc_pages((unsigned long *)kernel_pd,(unsigned int)ptable,1,false);

	/* NOTE here we once again use get_kernel_page_sequence(), for the same reasons above */
	/* TODO check return val */
// 	ptable = (page_entry_type *) get_kernel_page_sequence(1);
// 	pe.present=1;
// 	pe.contents.used.address=page2addr((unsigned int)ptable); /*  */
// 	pe.contents.used.dirty=0;
// 	pe.contents.used.accessed=0;
// 	pe.contents.used.is_user=0;
// 	pe.contents.used.writeable=1;
	/* insert it at position zero of the directory */
	/* point it to the actual kernel page table */
	process_pd[0]=kernel_pd[0];	

	/* first 512 pages, (i.e. 2MB) is kernel code, map it directly to physical memory */
	/* TODO 2MB will be too small for kernel eventually, check this later */
	/* NOTE I extended this to 4MB, this is still not ok tough */
// 	for (i = 1; i < 1024; i++) {
// 		pe.present=1;
// 		pe.contents.used.address=i;
// 		pe.contents.used.dirty=0;
// 		pe.contents.used.accessed=0;
// 		pe.contents.used.is_user=0;
// 		pe.contents.used.writeable=1;
// 		ptable[i]=convert_to_entry(&pe);
// 	}

	/* lets follow kernel paging and leave address 0x0 empty */
// 	pe.present=0;
// 	pe.contents.unused.avail=0;
// 	ptable[0]=convert_to_entry(&pe);
// 
// 	pe.contents.unused.avail=1;
// 	for(i=1;i<1024;i++) {
// 		process_pd[i]=convert_to_entry(&pe);
// 	}

	toggle_paging();
	/* WARNING whomever uses the returned pd must NOT access its contents
		(unless they disable paging first, which shouldnt be done unless 
		strictly necessary)	under pain of causing a page fault (#PF) */

	return process_pd;
}

/* duplicates a page directory from a process, used mainly for forking, 
	the memory inside the pages is also copied. */

page_entry_type * duplicate_process_pd(page_entry_type * orig) {

	int i,j;
	struct page_entry pe;
	page_entry_type *pt;
	page_entry_type * dest = create_process_page_directory();
	/* TODO this code is very inefficient, fix this by finding a better
		way of finding the existent pages (some backing data structure or
		something. */
	/* skip kernel pt, it is supposed to be there already */
	toggle_paging();
	for (i = 1; i < 1024; i++) {
		if (orig[i] & 0x01) {
			convert_from_entry(orig[i],&pe);
			pt = (page_entry_type *)(pe.contents.used.address<<12);
			for (j = 0; j < 1024; j++) {	
				if (pt[j] & 0x1) {
					convert_from_entry(pt[j],&pe);
					toggle_paging();
					dword d_phys = get_free_page();
					toggle_paging();
					kmemcpy((byte*)(d_phys),(byte*)(pe.contents.used.address<<12),4096);
					/* TODO all this turning paging on/off is crappy. Find a better way
						to handle this, in all pages.c functions */
					toggle_paging();
					add_page((unsigned long *)dest,to_address(i,j,0),d_phys,1);
					toggle_paging();
				}
			}
		}
	}
	int pd = PD_INDEX(0xFFFFF920);
int pti = PT_INDEX(0xFFFFF920);
int off = PAGE_OFFSET(0xFFFFF920);

	convert_from_entry(orig[pd],&pe);
	pt = pe.contents.used.address<<12;
	convert_from_entry(pt[pti],&pe);
	kprintf("t=%X\n",*(unsigned int*)((pe.contents.used.address<<12)+off));
convert_from_entry(dest[pd],&pe);
	pt = pe.contents.used.address<<12;
	convert_from_entry(pt[pti],&pe);
	kprintf("t=%X\n",*(unsigned int*)((pe.contents.used.address<<12)+off));
	toggle_paging();
	return dest;
}

void destroy_process_page_directory(page_entry_type * pd_e) {

}

/*
 * Write data in a page, converting from a virtual (linear) address.
 * This is done with paging turned off (to ensure proper physical handling).
 * This however is dangerous. The current page(s) on the selected area must be
 * swapped (to disk probably), if in use, or backuped in some way. For now the
 * (physical) pages are being allocated in order, to avoid overlap, so this care
 * may be unnecessary (i.e. no page faults).
 * TODO this process memory allocation with pages is a mess, an actual policy must be
 * established.
 */
// void physical_page_put_data(unsigned char *data, unsigned long vaddr, unsigned long *pd) {
// 	toggle_paging();
// 	unsigned long *addr = (unsigned long *) get_physical_address(pd,vaddr);
// 	/* paging is off, so this should (hopefuly) write in the correct page */
// 	toggle_paging();
// }

void use_process_pd(process_pd_entry * pd_e) {
	current_pd_entry = pd_e;
	use_pd((unsigned long)pd_e->pd);
}

void handle_page_fault(dword address, page_fault_error e_code) {
	/* we need to allocate pages here dont let scheduler interrupt us */
	asm("cli");

	struct task_struct * ts = get_current_process();

	kprintf("Page Fault: %X\n",address);
	
	memory_descriptor * mem_d = ts->mem_area;
//	kprintf("mem area = %i\n",ts->mem_area);
//kprintf("mem area = %i, %i, %i, %i, %i\n",ts->mem_area,ts,mem_d->stack_base,mem_d->p_heap_start,mem_d->p_heap_break);
//	kprintf("my stack: %X\n",&mem_d);
//	kprintf("Process data: %X, %X, %X\n",mem_d->stack_base,mem_d->p_heap_start,mem_d->p_heap_break);
	if (address < mem_d->stack_base && (mem_d->stack_base-address) <= STACK_MAX_SIZE) {
		alloc_pages(ts->pd,address & 0xFFFFF000,1,1);
	}
	else if (address > mem_d->p_heap_start && address <= mem_d->p_heap_break) {
		alloc_pages(ts->pd,address & 0xFFFFF000,1,1);		
	}
	else {
		kprintf("We couldnt handle Page Fault: %X\n",address);
		asm("hlt"); /* halt for now */
	}
	kprintf("Page Fault handled\n");

	/* finish exception handling */
	outportb (MASTER, EOI);

}

int in_kernel_stack() {
	unsigned int esp;
	asm volatile ("movl %%esp, %0" : "=r" (esp));
	kprintf("%X\n",esp);
	return (esp <= (64*1024));
}