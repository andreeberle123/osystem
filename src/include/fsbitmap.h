#ifndef _FSBITMAP_H_
#define _FSBITMAP_H_


extern byte *ffs_bitmap;
//andre fez - imprime a bit map soh para ver como estah
void printBitMap();

// retorna um numero que eh o primeiro bloco vazio. Vai de zero ateh X (retorna
// zero para o bloco 303) 
unsigned int firstEmptyBlock();


// pega o mapa de bits e altera o bit de zero pra um (zero para o bloco 303)
void alterBit(unsigned int bitVal);

unsigned int searchFat(int numeroBits);
void alterBits(int bitInicial, int n);


#endif

