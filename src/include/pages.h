/*
ChallengeOS Operating System
Copyright (C) 2000 Gregor Mueckl <GregorMueckl@gmx.de>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef KERNEL_PAGES_H
#define KERNEL_PAGES_H

/* TODO IMPORTANT!! not all code use this define for limiting, fix this!!!! */

/* Kernel maximum paging space. This space is kernel-reserved and is fixed
 (i.e. all these pages map directly to the physical ones, and process mem
 address will include them as default). Furthermore, *all* kernel pages/data
 will be located here, so it's not expandable. 
 1024 pages = 4MB memory 
*/
#define KERNEL_MAX_PAGES 1024
/* The maximum boot area for the kernel is the maximum memory reserved for
	boot code loading, currently limited at 2MB 
	TODO improve this
*/
#define KERNEL_MAX_BOOT_AREA 512

/* macros for index calculations */
#define PD_INDEX(addr) (unsigned long)addr >> 22
#define PT_INDEX(addr) (((unsigned long)addr>>12)%1024)
#define PAGE_OFFSET(addr) (unsigned long)addr%4096 // we have 4kb page size

#define to_address(pd,pt,off) (unsigned long)((pd << 22)+ (pt << 12) + off)

#define FIRST_FREE_REGION 16 /* pages heap is at 16th page (after stack) */
#define AVL_REGION 24

struct _free_region;

#include "avl.h"

typedef struct _free_region {
	avl_tree_node node;
	unsigned long * linear_addr_strt;
	unsigned long * heap_addr_strt;
} free_region;

typedef struct _inv_free_region {
	avl_tree_node node;
	unsigned long * linear_addr_end;
	unsigned long * heap_addr_strt;
} inv_free_region;

typedef struct _free_region_entry {
	unsigned int num_pages;
	free_region * page_region_start;
} free_region_entry;

/*extern unsigned long * first_free_region_avl_tree_entry;

static inline void free_avl_tree_region_entry(unsigned long * addr) {
	*addr = (unsigned long)first_free_region_avl_tree_entry;
	first_free_region_avl_tree_entry = addr;
}*/

/***********************************************************************/

typedef unsigned int page_entry_type; /* for the actual representation */

/* a entry to either a page directory or a page table */
struct page_entry {
  union {
    struct {
      unsigned int address:20;
      unsigned int avail:3;
      unsigned int reserved1:2;
      unsigned int dirty:1;
      unsigned int accessed:1;
      unsigned int reserved2:2;
      unsigned int is_user:1;
      unsigned int writeable:1;
    } used;
    struct {
      unsigned int avail:31;
    } unused;
  } contents;
  unsigned int present:1;
} __attribute__((packed));


/* From mm/pages.c */
extern page_entry_type convert_to_entry(struct page_entry *pe);

extern void convert_from_entry(page_entry_type entry, struct page_entry *pe);

extern void InitPages();

void use_kernel_pd();

/*
 * Function for the management of free pages
 */

extern unsigned long get_free_page();

/*
 * We will want to allocate pages both specifically in kernel space and
 * outside it, this will allocate outside.
 */
extern unsigned long get_free_non_kernel_page();

extern void free_physical_page(unsigned long addr);

/*
 * Helpers for the manipulation of page tables
 */

extern unsigned long get_linear_address(page_entry_type *pd, unsigned long physical_addr);

extern void create_pt_entry(page_entry_type *pt, unsigned long address, int pt_index, int is_user);

extern int add_page_table(unsigned long *pd, unsigned int numentry, int isuser);

extern int add_page(unsigned long *pd, unsigned long linear_addr, unsigned long phys_addr, int isuser);

extern int alloc_pages(unsigned long *pd, unsigned long start_addr, unsigned int num_pages, int isuser);

//extern int alloc_pages_non_kernel(unsigned long *pd, unsigned long start_addr, unsigned int num_pages, int isuser);

/* simple converter to get linear addresses out of the indices to the tables */
static inline unsigned long indices_to_linear_addr(int pd_index, int pt_index)
{
  return (pd_index<<22)+(pt_index<<12);
}

extern void free_page(unsigned long *pd, void *addr); /* addr points to the beginning of the page */

extern void free_page_series(unsigned long *pd, void *addr, int num_pages);

static inline void flush_paging_cache()
{
  asm("pushl %eax       \n" \
      "movl  %cr3, %eax \n" \
      "movl  %eax, %cr3 \n" \
      "popl  %eax       \n" \
     );
}

/* forces the uses of the given page table - USE WITH CARE */
//extern void use_pd(unsigned long pd);

extern int paging_enabled;
/* enable or disable the use of page directories/page tables - USE WITH CARE */
static inline void toggle_paging()
{
  asm ("   pushl %eax               \n"  /* tell the cpu where the page dir is */
       "   movl  %cr0, %eax         \n"  /* cr0 cannot be manipulated directly */
       "   xorl   $0x80000000, %eax \n"
       "   movl  %eax, %cr0         \n"
       "   jmp   1f                 \n"  /* got to jump after setting cr0 */
       "1:                          \n"
       "   popl  %eax               \n"
      );
}

/* TODO replace (where it applies) the toggle_paging() calls by the following
	macros */

#define enable_paging() \
	if (!paging_enabled) {\
		toggle_paging(); \
		paging_enabled = 1; \
	}

#define disable_paging() \
	if (paging_enabled) {\
		toggle_paging(); \
		paging_enabled = 0; \
	}

/* From mm/kernel.c - should go away*/
//extern void *get_kernel_page(); /* returns a pointer to the new page */
extern void *get_kernel_page_sequence(int num_pages);

page_entry_type *create_process_page_directory();
page_entry_type * duplicate_process_pd(page_entry_type * orig);
void physical_page_put_data();

unsigned long * get_free_kernel_pages_region(unsigned int size);

int in_kernel_stack();
unsigned int _kernel_stack_entry(void (*function)(void), unsigned int args_size,... );

static inline int safe_alloc_pages(unsigned long *pd, unsigned long start_addr, unsigned int num_pages, int isuser) { \
	if (!in_kernel_stack()) {
		return (int) _kernel_stack_entry((void (*)(void))safe_alloc_pages,16,pd,start_addr,num_pages,isuser);
	}
	else {
		return alloc_pages(pd,start_addr, num_pages, isuser);
	}
}

static inline page_entry_type * safe_create_process_page_directory() {
	if (!in_kernel_stack()) {
		return (page_entry_type*) _kernel_stack_entry((void (*)(void))create_process_page_directory,0);
	}
	else {
		return create_process_page_directory();
	}
}

static inline page_entry_type * safe_duplicate_process_pd(page_entry_type * orig) {
	if (!in_kernel_stack()) {
		return (page_entry_type*) _kernel_stack_entry((void (*)(void))duplicate_process_pd,sizeof(page_entry_type *),orig);
	}
	else {
		return duplicate_process_pd(orig);
	}
}

/* make the given page directory the active one */
/* NOTE changed to a macro */
#define use_pd(pd) \
	\
  asm("pushl %%eax         \n" \
      "movl  %0, %%eax     \n" \
      "movl  %%eax, %%cr3  \n" \
      "popl  %%eax         \n" \
      ::"r" (pd));


/**************************************************************************/
/* Process Space PD data */

typedef struct __process_pd_entry {
	page_entry_type * pd;
	free_region * paging_heap_root;
	free_region_entry * paging_avl_root;
} process_pd_entry;

typedef struct _page_fault_error {
	unsigned int present:1;
	unsigned int writing:1;
	unsigned int user_mode:1;
	unsigned int reserved_bit_violation:1;
	unsigned int instruction_fetch:1;
}__attribute__((packed)) page_fault_error ;

#endif
