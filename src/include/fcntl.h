#ifndef _FCNTL_H_
#define _FCNTL_H_

#define O_ACCMODE	   000x3
#define O_RDONLY	     0x0
#define O_WRONLY	     0x1
#define O_RDWR		     0x2
#define O_CREAT		   0x100
#define O_EXCL		   0x200
#define O_NOCTTY	   0x400
#define O_TRUNC		  0x1000
#define O_APPEND	  0x2000
#define O_NONBLOCK	  0x4000
#define O_SYNC		 0x10000
#define FASYNC		 0x20000
#define O_DIRECT	 0x40000
#define O_LARGEFILE	0x100000
#define O_DIRECTORY	0x200000
#define O_NOFOLLOW	0x400000

#endif