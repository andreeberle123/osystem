#ifndef _KEYBOARD_H_
#define _KEYBOARD_H

#include "defines.h"

#define LED_NUM_LOCK		2
#define LED_SCROLL_LOCK		1
#define LED_CAPS_LOCK		4

#define CK_SHIFT			1
#define CK_ALT				2
#define CK_CTRL				4
#define CK_ALTERNATIVE		8

void initKeyboard ();
void setleds (byte ls);
unsigned char getch ();
bool kbhit ();

#endif
