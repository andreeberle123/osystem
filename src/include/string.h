#ifndef _STRING_H_
#define _STRING_H_

int kstrcmp (const char *a, const char *b);
int kstrncmp (const char *a, const char *b, int n);
char *kstrchtok (char *s, const char delim);
void kstrcpy (char *dest, const char *src);
void kstrncpy (char *a, const char *b, int n);
int kstrlen (const char *s);
char *kstrcat(char *first, const char *second);

#endif
