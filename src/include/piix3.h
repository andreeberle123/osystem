#ifndef _PIIX3_H_
#define _PIIX3_H_

#define PIIX3_PIRQ0_REG 	0x60 
#define PIIX3_PIRQ1_REG 	0x61 
#define PIIX3_PIRQ2_REG 	0x62 
#define PIIX3_PIRQ3_REG 	0x63 

#endif