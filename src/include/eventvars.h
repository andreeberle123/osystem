#ifndef _EVENTVARS_H_
#define _EVENTVARS_H_

/* defining the pointer to the event queue */

extern volatile EVENT *_evt;
extern unsigned int volatile _number_of_evt;
extern unsigned int volatile fifo_number;
extern unsigned int volatile semaphore_number;

#endif
