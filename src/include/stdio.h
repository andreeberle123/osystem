#ifndef _STDIO_H
#define _STDIO_H

#include "stdarg.h"

void kprints (char *s);
void kprintl (unsigned long i, unsigned int digits);
void kprinth (unsigned long i, unsigned int digits);
void kprintf (char *format, ...);
void ksprintf (char *ptr, const char *str, ...);
void kvprintf (char *format, va_list arglist);

void gets (char *s, unsigned int max);

#endif
