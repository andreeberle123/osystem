#ifndef _SCHED_H_
#define _SCHED_H_

#include "defines.h"
#include "process.h"

void sheduler (void);
void init_scheduler (void);

void set_current_process(struct task_struct *);
struct task_struct * get_current_process();

#endif
