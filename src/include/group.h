#ifndef _GROUP_H_
#define _GROUP_H_

#include "defines.h"
#include "ffs.h"

#define NUMBER_OF_USER 128
#define USER_SIZE 32
#define NUMBER_OF_GROUP 256
#define GROUP_SIZE 16
#define USER_LAST_ELEMENT NUMBER_OF_USER-1
#define GROUP_LAST_ELEMENT NUMBER_OF_GROUP-1

 
// User structure
typedef struct {
	char name[12];				// username
	char password[12];				// password (non-encrypted)
	unsigned int iduser;			// user id
	unsigned int idgroup;			// group id
}USER;

// Group structure
typedef struct {
	char group[12];				// group name
	unsigned int idgroup;			// group id
}GROUP;

extern USER current_user;


void allocUserAndGroup();

bool chown(char *name, char *newowner);

bool chgroup(char *name, char *newgroup);

bool chmod(char *, unsigned char, unsigned char, unsigned char/*,
	unsigned int, unsigned int*/);

void print_permition (char *);

char * perm(unsigned short);

void print_flag(unsigned short flag);

bool create_user (char *name, char *password, char *group);

bool create_group (char *name);

void print_groups();

void print_users();

void createRootUser(char *defaultPassword);

bool login(char *user, char *password);

void whoami();

void loadUser();

void loadGroup();

int canRead(FILE *f);

int canWrite(FILE *f);

int canExecute(FILE *f);

int canReadInode(INODE *f);

int canWriteInode(INODE *f);

int canExecuteInode(INODE *f);

//int canRead(char *);

#endif

