#ifndef _NIC_H_
#define _NIC_H_


#include "list.h"
#include "defines.h"

#define alloc_nic(n) 	nic * n = (nic*) kalloc(sizeof(nic));\
			n->pci_irq = 0;

typedef struct _nic {
	struct list_head head;
	byte * MAC;
	void (*interrupt)(void);
	void (*send)(void *packet, word length);
	dword pci_irq; /* irq number in pci, if used */
	const char * name;
	void (*receive)(void * frame);
} nic;

void add_nic(nic*);
void list_adapters();
void init_nic();

#endif
