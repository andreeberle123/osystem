#ifndef _TIMER_H_
#define _TIMER_H_

#include "defines.h"

#define MILISEC 	 10
#define FREQ 		(1000/MILISEC)
#define PRECISION	  8
#define MAXTRIGGERS   	 100

void loadTimer (void);
unsigned long calibrate_delay (void);
void delay (unsigned long mili);
//void addTrigger (void (*handler)(), dword timeout);
int addTrigger (void (*handler)(void *), dword timeout, void *args);

//extern unsigned long delay_count;

void removeTrigger (word n);

struct trigger {
	void (*handler)(void *);
	dword timeout;
	void *args;
};

struct trigger trigger[MAXTRIGGERS];

#endif
