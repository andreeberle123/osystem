#ifndef _PROCESS_H_
#define _PROCESS_H_

#include "defines.h"
#include "mem.h"

#define STACK_SIZE 4096
#define STACK_MAX_SIZE 65535

/* 4GB of virtual address */
#define VIRTUAL_MEMORY_SIZE 0xFFFFFFFF
/* The process heap starts at 512mb, the virtual space between 4mb - 512mb is reserved */
#define PROCESS_HEAP_START 0x800000

enum FLAGS {
	CODE = 0,
	DATA
};

enum STATUS {
	RUNNING=0,
	READY_TO_RUN,
	WAITING,
	FINISHED
};

#define TS_TSS(ts) ts+36

struct task_struct {

	unsigned int pid;
	unsigned int ppid;
	enum STATUS status;

	unsigned int bin_size;

	unsigned char name[16];

	unsigned char *stack;

	unsigned int entry_point;
	unsigned char *exe;

	unsigned int start; /* data de inicio ? */

	/* TSS bit */
	unsigned long io_map;
	unsigned long ldt;

	unsigned long gs;
	unsigned long fs;
	unsigned long es;
	unsigned long ds;
	unsigned long edi;
	unsigned long esi;
	unsigned long ebp;
	unsigned long esp;
	unsigned long ebx;
	unsigned long edx;
	unsigned long ecx;
	unsigned long eax;
	unsigned long flags;
	unsigned long eip;
	/* page directory */
	unsigned long *pd;
	unsigned long ss2;
	unsigned long esp2;
	unsigned long ss1;
	unsigned long esp1;
	unsigned long ss0;
	unsigned long esp0;
	unsigned long back_link;

	unsigned long cs;

	

	memory_descriptor * mem_area;

	struct task_struct *next; /* list helper */
};

struct registers {
	unsigned long gs;
	unsigned long fs;
	unsigned long es;
	unsigned long ds;
	unsigned long edi;
	unsigned long esi;
	unsigned long ebp;
	unsigned long esp;
	unsigned long ebx;
	unsigned long edx;
	unsigned long ecx;
	unsigned long eax;
	unsigned long flags;
	unsigned long eip;
	unsigned long cs;
};

struct list {
	struct task_struct *proc;
	struct list *next;
};

//?extern volatile struct list *plist;

struct task_struct* create_process(unsigned char *mem, int create_pd);
void build_ghost_process(/*void * retpoint,*/ unsigned char * entrypoint, void * args);
void add_process(struct task_struct* p);
void remove_process(struct task_struct* p);
struct task_struct *get_next_ready_to_run(void);
void change_process_status(unsigned int pid, enum STATUS st);
int fork(struct task_struct * ts);
float count_process();
void change_current_process_status( enum STATUS st);
void ktop();
struct task_struct * getPlist();
void pstree();
int fork_syscall_handler(void * regs);
void kill_current_process(int ret);
int execve_syscall_handler(char * filename, char ** argv, int edx);
void exit_syscall_handler(int status);


#endif
