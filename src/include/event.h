#ifndef _EVENT_H_
#define _EVENT_H_

#include "defines.h"

/* discover the code */
typedef struct {
	unsigned int code;
} evt_discoverer;

/* timer */
typedef struct {
	unsigned int code;
	unsigned int sleep;
	unsigned int pid;
} evt_timer;

/* semaphore */
typedef struct {
	unsigned int code;
	unsigned int semaphore_number;
	byte lock;
} evt_semaphore;

/* fifo */
typedef struct {
	unsigned int code;
	unsigned int fifo_number;
	unsigned char *memaddr;
} evt_fifo;

/* the union used */
union event_type {
	evt_discoverer discoverer;
	evt_timer timer;
	evt_semaphore semaphore;
	evt_fifo fifo;
};

typedef struct event {
	union event_type *event;
	struct event *next;
} EVENT;

enum CODE {
	TIME,
	SEMA,
	FIFO
};

void init_event_queue(void);
void add_event_queue(union event_type *event);
void del_event_queue(EVENT *evt);

#endif
