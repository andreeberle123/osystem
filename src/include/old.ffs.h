#ifndef _FFS_H_
#define _FFS_H_

#include "defines.h"

#define SECTOR_SIZE 512
#define FFS_FAT	301
#define FFS_BITMAP 302

typedef struct {
	char name[12];
	unsigned short pointer;
	unsigned short size;
} INODE;

typedef struct {
	//unsigned short id;
	char name[12];
	unsigned short mode;
	unsigned short first_block;
	unsigned short fulloffset;
	unsigned short size;
} FILE;

extern byte ffs_fatbuffer[512];
extern byte ffs_bitmap[512];


int ffilesize(FILE *stream);
int fread(void *ptr, int size, int nmemb, FILE *stream);
FILE * fopen(const char *path, const char *mode);

bool create_file(char *name, unsigned short pointer, unsigned short size);
bool unlink(char *name);
char load_fat(void);
int getEntry(char *);
void ffstry();
char load_fat(void);
void ls(void);
void cat(int);
int feof(FILE *stream);
int is_valid_file(char * name);

#endif
