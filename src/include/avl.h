#ifndef _AVL_H_
#define _AVL_H_

typedef struct _avl_tree_node {
	struct _avl_tree_node * left;
	struct _avl_tree_node * right;
	//struct _free_region * parent;
} avl_tree_node;

#define AVL_TREE_NODE_SIZE sizeof(avl_tree_node)

#include "avl.h" 
#include "stdio.h"
#include "defines.h"
#include "mem.h"

unsigned int count_depth(avl_tree_node * node, unsigned int * factor);

void avl_bsearch_and_insert(avl_tree_node ** root, avl_tree_node * current_node, avl_tree_node * target_node, 
							unsigned int * rotated, unsigned int * arm_depth, int * arm_factor, avl_tree_node * parent);
void insert_avl_tree(avl_tree_node ** root, avl_tree_node * data);
void * remove_avl_tree(avl_tree_node ** root, unsigned long index);
void * avl_tree_bsearch_non_empty(avl_tree_node * root, unsigned long data);

#endif