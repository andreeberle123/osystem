#ifndef _DDDS_H
#define _DDFS_H

#include "defines.h"
#include "net/dnp.h"
//#include ""

char *ddfs_ls(char *path);
bool ddfs_mkdir(char *path);
bool ddfs_rmdir(char *path);
bool ddfs_chdir(char *path);
char *ddfs_pwd(void);
bool ddfs_existdir(char *path);

/* STUB functions & defines */
#define DDFS_SPORT	222
#define DDFS_CPORT	223
/* functions */
#define DDFS_LS		0x01
#define DDFS_CHDIR	0x02
#define DDFS_RMKDIR	0x03
#define DDFS_CMKDIR	0x04
#define DDFS_RRMDIR	0x05
#define DDFS_CRMDIR	0x06
/* types */
#define DDFS_STRING	0x01

#define DDFS_DELAY	1000

/* funcoes de marshalling & unmarshalling */

void ddfs_stub_start ();
void ddfs_handle_request (dnp_packet *);
void ddfs_make_request (byte request, char *path);
void ddfs_stub_ls    (dnp_packet *p, char *path);
void ddfs_stub_chdir (dnp_packet *p, char *path);
void ddfs_stub_request_mkdir (dnp_packet *p, char *path);
void ddfs_stub_commit_mkdir (dnp_packet *p, char *path);
void ddfs_stub_request_rmdir (dnp_packet *p, char *path);
void ddfs_stub_commit_rmdir (dnp_packet *p, char *path);

#endif
