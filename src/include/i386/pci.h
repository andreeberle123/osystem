#ifndef _I386_PCI_H_
#define _I386_PCI_H_

#include "defines.h"
#include "pci.h"

#define PCI_BIOS_LOWER_BOUND (byte*)0x0E0000
#define PCI_BIOS_UPPER_BOUND (byte*)0x0FFFF0

// TODO improve this to avoid endianess problems
#define PCI_BIOS_MAGIC_NUMBER (('_' << 0) + ('3' << 8) + ('2' << 16) + ('_' << 24))
//0x5F33325F

#define PCI_IRQ_TABLE_MAGIC_NUMBER (('$' << 0) + ('P' << 8) + ('I' << 16) + ('R' << 24))

#define PCI_BIOS_SUCCESSFULY_FOUND 		0x00
#define PCI_BIOS_NOT_FOUND 			0xFF
#define PCI_IRQ_TABLE_NOT_FOUND 		0xFF

#define PCI_BIOS_SERVICE_ID 			0x049435024
#define PCI_BIOS_SIGNATURE 			0x20494350


#define PCI_FUNCTION_ID 			0xB1
	
#define PCI_BIOS_PRESENT_FN			(PCI_FUNCTION_ID << 8) + 0x01
#define PCI_FIND_DEVICE_FN			(PCI_FUNCTION_ID << 8) + 0x02

#define PCI_BIOS_READ_CONFIG_B_FN		(PCI_FUNCTION_ID << 8) + 0x08
#define PCI_BIOS_READ_CONFIG_W_FN		(PCI_FUNCTION_ID << 8) + 0x09
#define PCI_BIOS_READ_CONFIG_DW_FN		(PCI_FUNCTION_ID << 8) + 0x0A

#define PCI_BIOS_WRITE_CONFIG_B_FN		(PCI_FUNCTION_ID << 8) + 0x0B
#define PCI_BIOS_WRITE_CONFIG_W_FN		(PCI_FUNCTION_ID << 8) + 0x0C
#define PCI_BIOS_WRITE_CONFIG_DW_FN		(PCI_FUNCTION_ID << 8) + 0x0D

#define GET_IRQ_ROUTING_OPTIONS_FN		(PCI_FUNCTION_ID << 8) + 0x0E
#define SET_PCI_HW_INT_FN			(PCI_FUNCTION_ID << 8) + 0x0F

#define PCI_SUCCESS 				0x00
#define PCI_DEVICE_NOT_FOUND 			0x086
#define PCI_BAD_VENDOR_ID 			0x083

#define PCI_BUFFER_TOO_SMALL			0x089

#define PCI_DEVICE_MASK				0xF8 /* 5 upper bits for general devfn */

#define IRQ_ENTRY_NUM(p,t) p = (t->table_size-32)/16;

#define PIN_INTA				0
#define PIN_INTB				1
#define PIN_INTC				2
#define PIN_INTD				3

/*
 * PCI BIOS32 entry point structure
 */
typedef struct {
	dword signature		__attribute__ ((packed));
	dword *entry_point	__attribute__ ((packed));
	byte revision_id	;
	/* PCI specification defines length as 16 bytes, however
	   this value must always be 0x1h for some reason...*/
	byte length		;	
	byte checksum		;
	/* to achieve 16 bits size the remaining 5 bits must be zero
	   filled. This data is only relevant to check and will be
	   ignored in this structure */

} _pci_bios_entry;

typedef struct {
	byte state;
	dword address;
	dword length;
	dword entry_offset;
} pci_bios_service;

typedef struct {
	word	buffer_size;
	byte * data_buffer;
} IRQ_routing_options_buffer;



/* get the pin from the entry, just some pointer magic */
#define ENTRY_PIN(e,p) ((byte*)e)+2+(p*3)

typedef struct {
	byte bus;
	byte dev;
	byte LNKA;
	word INTA_bitmap	__attribute__ ((packed));
	byte LNKB;
	word INTB_bitmap	__attribute__ ((packed));
	byte LNKC;
	word INTC_bitmap	__attribute__ ((packed));
	byte LNKD;
	word INTD_bitmap	__attribute__ ((packed));
	byte slot_num;
	byte reserved;
} _irq_table_entry;

typedef struct {
	dword signature;
	word version;
	word table_size;
	byte interrupt_bus;
	byte interrupt_devfn;
	word pci_excl_irqs;
	dword pci_interrupt_router;
	dword miniport_data;
	byte reserved[11]; /* 11 bytes */
	byte checksum;
	_irq_table_entry entries[]; /* N entries, size dependent */
	
} _pci_irq_routing_table;

struct _pci_irq_driver;

int scan_bios32();
void search_pci_bios();
void pci_bios_init();
byte pci_get_irq(int devfn, byte pin);
void set_hw_interrupt(byte pin, byte irq, dword devfn, dword bus);
int scan_irq_routing_table();
void pci_irq_init();
void register_irq_driver(struct _pci_irq_driver *);

void pci_read_routing_table();

int pci_bios_write_configuration_byte(unsigned int devfn, unsigned int bus, unsigned int reg
			, unsigned int * val);
int pci_bios_write_configuration_word(unsigned int devfn, unsigned int bus, unsigned int reg
			, unsigned int * val);
int pci_bios_write_configuration_dword(unsigned int devfn, unsigned int bus, unsigned int reg
			, unsigned int * val);
int pci_bios_read_configuration_byte(unsigned int devfn, unsigned int bus, unsigned int reg
			, unsigned int * val);
int pci_bios_read_configuration_word(unsigned int devfn, unsigned int bus, unsigned int reg
			, unsigned int * val);
int pci_bios_read_configuration_dword(unsigned int devfn, unsigned int bus, unsigned int reg
			, unsigned int * val);
#endif
