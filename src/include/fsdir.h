#ifndef _FSDIR_H_
#define _FSDIR_H_

#include "ffs.h"

#include "sys/fs.h"

extern unsigned char* current_path;
extern FILE * fslink;	

#ifndef IN_USR
void createFileSLink();
int toPath(const char *path);
bool chdir(const char *dir);
bool slink(const char *file, const char *nome);
bool hlink(const char *file, const char *nome);
bool rmdir(const char *dir);
bool mkdir(char *name);

void createRoot();

int auxslink(INODE *pt);
int flush_directory_entries(struct _linux_dirent * buf, unsigned int size, FILE * stream);

#endif



#endif

