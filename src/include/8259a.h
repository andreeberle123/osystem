#ifndef _8259a_H
#define _8259a_H

#include "defines.h"

/* IRQs */
#define ALL 		0xFF
#define TIMER		0x00
#define KEYBOARD	0x01
#define CASCADE		0x02
#define COM2_4		0x03
#define COM1_3		0x04
#define LPT			0x05
#define FLOPPY		0x06
#define	FREE7		0x07
#define CLOCK		0x08
#define FREE9		0x09
#define FREE10		0x0A
#define FREE11		0x0B
#define PS2MOUSE	0x0C
#define COPROC		0x0D
#define IDE_1		0x0E
#define IDE_2		0x0F

#define MASTER		0x20
#define MASTERDATA	0x21
#define SLAVE		0xA0
#define SLAVEDATA	0xA1
#define EOI			0x20

/* We only worry about ICW1 and ICW4 */
#define ICW1_INIT	0x10            // required for PIC initialisation
#define ICW1_EDGE 	0x08            // edge triggered IRQs
#define ICW1_SINGLE	0x02            // only MASTER (not cascaded)
#define ICW1_ICW4	0x01            // there IS an ICW4 control word
                                                                       
#define ICW4_SFNM	0x10            // Special Fully Nested Mode
#define ICW4_BUFFER	0x08            // Buffered Mode
#define ICW4_MASTER	0x04            // this is the Master PIC
#define ICW4_AEOI	0x02            // Auto EOI
#define ICW4_8086	0x01            // 80/86 Mode

/* for timers */
#define TMR_CTRL	0x43            // I/O for control         
#define TMR_CNT0	0x40            // I/O for counter 0       
#define TMR_CNT1	0x41            // I/O for counter 1       
#define TMR_CNT2	0x42            // I/O for counter 2       
                                                                                   
#define TMR_SC0		0x00            // Select channel 0        
#define TMR_SC1		0x40            // Select channel 1        
#define TMR_SC2		0x80            // Select channel 2        
                                                                                   
#define TMR_LOW		0x10            // RW low byte only        
#define TMR_HIGH	0x20            // RW high byte only       
#define TMR_BOTH	0x30            // RW both bytes           
                                                                                   
#define TMR_MD0		0x00            // Mode 0                  
#define TMR_MD1		0x02            // Mode 1                  
#define TMR_MD2		0x04            // Mode 2                  
#define TMR_MD3		0x06            // Mode 3                  
#define TMR_MD4		0x08            // Mode 4                  
#define TMR_MD5		0x0A            // Mode 5                  
                                                                                   
#define TMR_BCD		0x01            // BCD mode                
                                                                                   
#define TMR_LATCH	0x00            // Latch command           
                                                                                   
#define TMR_READ	0xF0            // Read command              
#define TMR_CNT		0x20            // CNT bit  (Active low, subtract it) 
#define TMR_STAT	0x10            // Status bit  (Active low, subtract it) 
#define TMR_CH2		0x08            // Channel 2 bit             
#define TMR_CH1		0x04            // Channel 1 bit             
#define TMR_CH0		0x02            // Channel 0 bit             

void INTS (bool on);
void maskIRQ (byte irq);
void unmaskIRQ (byte irq);
void remapPIC (int pic1, int pic2);
void init_pit (float freq, unsigned char channel);
void set_highest_priority (byte high_master, byte high_slave);
void finish_interrupt();

#endif
