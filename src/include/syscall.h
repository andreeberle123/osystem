#ifndef _SYSCALL_H_
#define _SYSCALL_H_

#include "defines.h"

#define SYSCALL_NULL		0x00
#define SYSCALL_EXIT		0x01
#define SYSCALL_FORK		0x02
#define SYSCALL_READ		0x03
#define SYSCALL_WRITE		0x04
#define SYSCALL_OPEN		0x05

#define SYSCALL_EXECVE		0x0B
#define SYSCALL_CHDIR		0x0C

#define SYSCALL_ACCESS		0x21

#define SYSCALL_BRK			0x2D

#define SYSCALL_BRK			0x2D

#define SYSCALL_GETDENTS	0x8D

/* i declared this struct as a helper to pass args to tcp send stuff,
not sure if its necessary tough */
struct tcp_send_args {
	word id;
	byte flags;
	byte * buf;
	dword size;
};

void init_syscall (void);
void int_syscall ();

unsigned int __syscall(int sys_num, dword size, ...);

#define __syscall0(sys_num,ret)						\
	asm volatile ("pushl %eax");					\
	asm volatile ("movl %0, %%eax" : : "r" (sys_num));	\
	asm volatile ("int $0x80" : "=a" (ret));	\
	asm volatile ("popl %eax");

#define __syscall1(sys_num,arg1,ret)				\
	asm volatile ("pushl %eax");					\
	asm volatile ("movl %0, %%ebx" : : "r" (arg1));		\
	asm volatile ("movl %0, %%eax" : : "r" (sys_num));	\
	asm volatile ("int $0x80" : "=a" (ret));	\
	asm volatile ("popl %eax");

#define __syscall2(sys_num,arg1,arg2,ret)			\
	asm volatile ("pushl %eax");					\
	asm volatile ("movl %0, %%ebx" : : "r" (arg1));		\
	asm volatile ("movl %0, %%ecx" : : "r" (arg2));		\
	asm volatile ("movl %0, %%eax" : : "r" (sys_num));	\
	asm volatile ("int $0x80" : "=a" (ret));	\
	asm volatile ("popl %eax");

#define __syscall3(sys_num,arg1,arg2,arg3,ret)		\
	asm volatile ("pushl %eax");					\
	asm volatile ("movl %0, %%ebx" : : "r" (arg1));		\
	asm volatile ("movl %0, %%ecx" : : "r" (arg2));		\
	asm volatile ("movl %0, %%edx" : : "r" (arg3));		\
	asm volatile ("movl %0, %%eax" : : "r" (sys_num));	\
	asm volatile ("int $0x80" : "=a" (ret));	\
	asm volatile ("popl %eax");

#define __syscall4(sys_num,arg1,arg2,arg3,arg4,ret)	\
	asm volatile ("pushl %eax");					\
	asm volatile ("movl %0, %%ebx" : : "r" (arg1));		\
	asm volatile ("movl %0, %%ecx" : : "r" (arg2));		\
	asm volatile ("movl %0, %%edx" : : "r" (arg3));		\
	asm volatile ("movl %0, %%edi" : : "r" (arg4));		\
	asm volatile ("movl %0, %%eax" : : "r" (sys_num));	\
	asm volatile ("int $0x80" : "=a" (ret));	\
	asm volatile ("popl %eax");

#define __syscallA0(sys_num)						\
	asm volatile ("int $0x80");

#define __syscallA1(sys_num)				\
	asm volatile("movl 0x10(%ebp),%ebx"); \
	asm volatile ("int $0x80");

#define __syscallA2(sys_num)			\
	asm volatile("movl 0x10(%ebp),%ebx"); \
	asm volatile("movl 0x14(%ebp),%ecx"); \
	asm volatile ("int $0x80");

#define __syscallA3(sys_num)		\
	asm volatile("movl 0x10(%ebp),%ebx"); \
	asm volatile("movl 0x14(%ebp),%ecx"); \
	asm volatile("movl 0x18(%ebp),%edx"); \
	asm volatile ("int $0x80");

#define __syscallA4(sys_num)	\
	asm volatile("movl 0x10(%ebp),%ebx"); \
	asm volatile("movl 0x14(%ebp),%ecx"); \
	asm volatile("movl 0x18(%ebp),%edx"); \
	asm volatile("movl 0x1B(%ebp),%edi"); \
	asm volatile ("int $0x80");


#endif
