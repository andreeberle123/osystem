#ifndef _PSHELL_H_
#define _PSHELL_H_

#define ARROW_UP 0x48
#define ARROW_LEFT 0x4B
#define ARROW_RIGHT 0x4D
#define ARROW_DOWN 0x50

#define is_arrow(c) (c == ARROW_UP || c == ARROW_LEFT || c == ARROW_RIGHT || c == ARROW_DOWN)

#endif