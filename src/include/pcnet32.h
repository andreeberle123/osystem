#ifndef _PCNET32_H_
#define _PCNET32_H_

#define AMD_PCI_VENDOR_ID		0x1022
#define PCNET_LANCE_DEVICE_ID		0x2000

/* Offsets from base I/O address. */
#define PCNET32_WIO_RDP		0x10
#define PCNET32_WIO_RAP		0x12
#define PCNET32_WIO_RESET	0x14
#define PCNET32_WIO_BDP		0x16

#define PCNET32_DWIO_RDP	0x10
#define PCNET32_DWIO_RAP	0x14
#define PCNET32_DWIO_RESET	0x18
#define PCNET32_DWIO_BDP	0x1C

#define PCNET32_CSR0_TINT   1 << 9
#define PCNET32_CSR0_RINT   1 << 10 

#define PCNET32_TOTAL_SIZE	0x20

#define PCNET32_PORT_AUI      0x00
#define PCNET32_PORT_10BT     0x01
#define PCNET32_PORT_GPSI     0x02
#define PCNET32_PORT_MII      0x03

#define PCNET32_PORT_PORTSEL  0x03
#define PCNET32_PORT_ASEL     0x04
#define PCNET32_PORT_100      0x40
#define PCNET32_PORT_FD	      0x80

#define PCNET32_DMA_MASK 0xffffffff

/*
 * Set the number of Tx and Rx buffers, using Log_2(# buffers).
 * Reasonable default values are 4 Tx buffers, and 16 Rx buffers.
 * That translates to 2 (4 == 2^^2) and 4 (16 == 2^^4).
 */
#ifndef PCNET32_LOG_TX_BUFFERS
#define PCNET32_LOG_TX_BUFFERS 1
#define PCNET32_LOG_RX_BUFFERS 2
#endif

#define TX_RING_SIZE		(1 << (PCNET32_LOG_TX_BUFFERS))
#define TX_RING_MOD_MASK	(TX_RING_SIZE - 1)
/* FIXME: Fix this to allow multiple tx_ring descriptors */
#define TX_RING_LEN_BITS	0x0000	/*PCNET32_LOG_TX_BUFFERS) << 12) */

#define RX_RING_SIZE		(1 << (PCNET32_LOG_RX_BUFFERS))
#define RX_RING_MOD_MASK	(RX_RING_SIZE - 1)
#define RX_RING_LEN_BITS	((PCNET32_LOG_RX_BUFFERS) << 4)

/* I dont know if this magic buffer size is necessary */
#define PKT_BUF_SZ		1544

/* The PCNET32 Rx and Tx ring descriptors. */
typedef struct _pcnet32_rx_head {
	dword base;
	short buf_length;
	short status;
	dword msg_length;
	dword reserved;
} pcnet32_rx_head;

typedef struct _pcnet32_tx_head {
	dword base;
	short length;
	short status;
	dword misc;
	dword reserved;
} pcnet32_tx_head;

/* The PCNET32 32-Bit initialization block, described in databook. */
typedef struct _pcnet32_init_block {
	word mode;
	word tlen_rlen;
	byte phys_addr[6];
	word reserved;
	dword filter[2];
	/* Receive and transmit ring base, along with extra bits. */
	dword rx_ring;
	dword tx_ring;
} pcnet32_init_block;

#endif
