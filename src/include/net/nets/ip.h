/**************************************************************
	
	TODO
	Put UDP_TYPE and TCP_TYPE in hexadecimal form
	
***************************************************************/

#ifndef _IP_H
#define _IP_H

#include "defines.h"
#include "list.h"

#define UDP_TYPE 0x11
#define TCP_TYPE 0x06

struct ip_header {
	byte protocol;
	byte src_ip[4];
	byte dst_ip[4];
	dword size;
};

typedef struct ip_packet {
	struct ip_header header;
} ip_packet;


ip_packet *ip_build_message (byte protocol, byte *src_ip, byte *dst_ip, byte *data, dword size);

void ip_send (byte protocol, byte *src_ip, byte *dst_ip, byte *data, dword size);

void ip_send_eth_message (void *p_void);
//void ip_send_eth_message (ip_packet *p);

void ip_handle_packet (ip_packet *packet);

void ip_register_IP (byte *address);

byte *ip_get_IP ();

#endif
