#ifndef _ARP_H
#define _ARP_H

#include "defines.h"
#include "net/dlink/eth.h"
#include "list.h"

#define ARP_REQUEST  0x0001
#define ARP_REPLY    0x0002

#define ARP_ETHERNET 0x0001

#define ARP		 0x0800
#define ARP_IP      IP_TYPE

#define ARP_TRIES    10

struct arp_header {
	word hardware;
	word protocol;
	byte hwaddrlen;
	byte ptaddrlen;
	word opcode;
};

struct arp_address {
	struct list_head head;
	word protocol;
	byte ptaddrlen;
	byte *address;
};

struct arp_entry {
	byte *hwaddr;
	byte *ptaddr;
};

struct arp_hash {
	struct list_head head;
	word protocol;
	byte ptaddrlen;
	byte maxsize;
	byte *broadcast;
	struct arp_entry *hash;
};

typedef struct arp_packet {
	struct arp_header header;
} arp_packet;

arp_packet *arp_build_message  (word hardware, word protocol, 
								byte hwaddrlen, byte ptaddrlen,
								byte *srchwaddr, byte *srcptaddr,
								byte *dsthwaddr, byte *dstptaddr,
								word opcode);
void arp_send_eth_message (arp_packet *p);
void arp_handle_packet (arp_packet *packet);
//void arp_test (void);
void arp_register_address (word protocol, byte ptaddrlen, byte *address);
bool arp_lookup_eth_address (word protocol, byte ptaddrlen, byte *ptaddr, byte *hwaddr);
void arp_create_eth_lookup (word protocol, byte ptaddrlen, byte *broadcast, byte maxsize);

#endif 
