#ifndef _TCP_IDLIST_H
#define _TCP_IDLIST_H

#include "defines.h"

#define WINDOW_SIZE 5
struct tcp_idlist_message_header {
	dword size; // lenght dos dados
	dword timeout; // tempo de timeout
	byte repeat; // coisa do bodo
	byte ack_counter; // conta quantos acks foram recebidos, para verificar se foram recebidos 3 acks para a msg
};

struct tcp_idlist_message {
	struct tcp_idlist_message_header header;
	//byte *p;  se n�o tem mensagem, p � nulo -> for control reasons
};

struct app_msg_header {
	dword size;
	struct _app_msg *next;
	struct _app_msg *prev;
};

struct _app_msg {
	struct app_msg_header header;
};

typedef struct _app_msg app_msg;

struct tcp_idlist_entry {
	app_msg *app_msg_alfa;
	app_msg *app_msg_omega;
	word id;
	word dport;
	word sport;
	byte dst_ip[4];
	dword seqnum; // n�mero do �ltimo pacote enviado
	dword last_ack;  // n�mero de sequ�ncia do �ltimo pacote recebido sem pacotes perdidos antes
	byte status;
	struct tcp_idlist_entry *prev;
	struct tcp_idlist_entry *next;
	struct tcp_idlist_message *recebidas[WINDOW_SIZE];
	struct tcp_idlist_message *enviadas[WINDOW_SIZE];
};

inline void tcp_idlist_start ();

struct tcp_idlist_entry *tcp_idlist_add_id (word sport, byte *ip, word dport);

struct tcp_idlist_entry *tcp_idlist_check_id (word id);

struct tcp_idlist_message *tcp_idlist_build_message (byte *parameter_data, dword parameter_size);

bool tcp_idlist_add_message (word parameter_id, struct tcp_idlist_message *parameter_msg, byte posicao);

struct tcp_idlist_message *tcp_idlist_get_message (word parameter_id);

word tcp_idlist_get_id_from_port (word parameter_port);

word tcp_net_hash (word sport, byte *ip, word dport);

void tcp_idlist_print (void);

bool tcp_idlist_remove_id (word id);

bool tcp_idlist_check_numseq (word id, dword numseq);

void tcp_idlist_dispose_received (word id, byte pos);

void tcp_idlist_dispose_sent (word id, byte pos);

app_msg *tcp_idlist_build_app_msg (byte *parameter_data, dword parameter_size);

//bool tcp_idlist_add_app_msg (app_msg *msg, app_msg *app_msg_alfa, app_msg *app_msg_omega);
bool tcp_idlist_add_app_msg (word id, app_msg *msg);

app_msg *tcp_idlist_get_app_msg (word id);

#endif
