/*******************************************************

    dorothy/include/net/transp/mailbox/mailbox.h

    Copyright (C) 2006 D-OZ Team

*******************************************************/
#ifndef _MAILBOX_H
#define _MAILBOX_H
#include "defines.h"
#include "net/transp/udp.h"

struct mailbox {
	struct list_head head;
	struct list_head messages;
	word id;
};

struct mailbox_message {
	byte protocol;
	byte *data;
};

/* Creates a mailbox registered with the ID, should return the address of the new mailbox */
struct mailbox *mailbox_register (byte protocol, word id);

/* Add the message to the appropriate mailbox*/
void mailbox_add (struct mailbox *m, struct mailbox_message *mmsg); 

/* Checks if the id�s mailbox is registered*/
struct mailbox *mailbox_check (byte protocol, word id);

/* Gets the first message on id mailbox*/
byte *mailbox_get (byte protocol, word id);

/* Delete the mailbox related to the ID */
/*void mailbox_destroy (byte protocol, word id);*/

#endif
