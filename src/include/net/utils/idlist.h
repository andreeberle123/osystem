#ifndef _IDLIST_H
#define _IDLIST_H

#include "defines.h"
/*
 * Devemos criar uma lista no udp.c e uma no tcp.c e ae qdo formos 
 * usar o idlist n�s passamos por parametro a idlist_alfa_udp ou a 
 * idlist_alfa_tcp / idlist_omega_udp ou a idlist_omega_tcp para 
 * sabermos em qual lista devemos adicionar. General things rox
 * */

struct idlist_message_header {
    dword size;
    struct idlist_message *prev;
    struct idlist_message *next;
 
};

struct idlist_message {
    struct idlist_message_header header;   
};
 
struct idlist_entry {
    word id;
    word dport;
    word sport;
    byte dst_ip[4];   
    struct idlist_entry *next;
    struct idlist_message *msg;    
};
 
inline void idlist_start ();

struct idlist_entry *idlist_add_id (word sport, byte *ip, word dport);

struct idlist_entry *idlist_check_id (word id);

struct idlist_message *idlist_build_message (byte *parameter_data, dword parameter_size);

bool idlist_add_message (word parameter_id, struct idlist_message *parameter_msg);

struct idlist_message *idlist_get_message (word parameter_id);

word idlist_get_id_from_port (word parameter_port);

word net_hash (word sport, byte *ip, word dport);

void idlist_print (void);    
#endif
