#ifndef _ETH_H
#define _ETH_H

#include "defines.h"
#include "nic.h"

#define ETH_MIN_SIZE   60
#define ETH_MAX_SIZE 1518
#define ETH_ADDR_SIZE 6
#define ETH_HLEN 14 /* size of eth header, (hopefully) */

#define ARP_TYPE   0x0806
#define IP_TYPE   0x6667

/* ethernet header structures */
struct ethernet_header {
	byte dst_address[ETH_ADDR_SIZE];
	byte src_address[ETH_ADDR_SIZE];
	word type;
};

typedef struct ethernet_frame {
	struct ethernet_header header;
} ethernet_frame;

void eth_register_send (void (*function)(void *packet, word length));
void eth_register_MAC (byte *address);
void eth_register_nic(nic *);

void eth_send (void *packet, word length);

void eth_handle_frame (void *frame);

byte *eth_get_MAC (void);


#endif
