/*******************************************************
* dorothy/net/transp/tcp.h
*
* Copyright (C) 2006 D-OZ Team
*
* TCP - Transmission Control Protocol
*
*******************************************************/
#ifndef _TCP_H
#define _TCP_H
#include "defines.h"
#include "net/utils/tcp_idlist.h"

/* works only for litle endian */
struct tcp_header {
	word sport;
	word dport;
	dword seqnum;
	dword acknum;
	word offset:4, reserved:6, flags:6;
	word window;
	word checksum;
	word urg;
};

typedef struct _tcp_packet {
	struct tcp_header header;
} tcp_packet;

struct tcp_timeout_args {
	word id;
	dword seqnum;
	dword timeout;
	byte repeat;
};

#define FLG_URG (1 << 5) /* 6th bit */
#define FLG_ACK (1 << 4)
#define FLG_PSH (1 << 3)
#define FLG_RST (1 << 2)
#define FLG_SYN (1 << 1)
#define FLG_FIN (1 << 0) /* 1st bit */

#define CON_CLOSED 0
#define CON_LISTEN 1
#define CON_ESTABLISHED 2
#define CON_CLOSING 3
#define CON_SYNSENT 4
#define CON_SYNRECVD 5
#define CON_TIMEDWAIT 6
#define CON_CLOSEWAIT 7
#define CON_LASTACK 8
#define CON_FINWAIT 9
//#define CON_FINWAIT1 9
//#define CON_FINWAIT2 10
//#define WINDOW_SIZE 5

/* Retorna o ID da conexão */
word tcp_register (word sport, byte *dst_ip, word dport);

bool tcp_send (word id, byte flags, byte *data, dword size);

bool tcp_send_message (word id, byte flags, byte *data, dword size);

tcp_packet *tcp_build_packet (word sport, word dport, byte flags, dword seqnum, dword acknum, byte *data, dword size);

word tcp_do_cksum (byte *p, dword len);

void tcp_handle_packet(byte *dst_ip, tcp_packet *p, dword packet_size);

bool tcp_connect(word id);

word tcp_listen (word sport);

bool tcp_close (word id);

//void tcp_check_ack(word id, dword acknum);

void tcp_insert_rec(word id, struct tcp_idlist_message *msg, dword numseq);

inline int findTimer(word id, dword seqnum);

void resetTimer(word id, dword seqnum);

void removeTimer(word id, dword seqnum);

void tcp_timeout(void *args);

byte *tcp_get_message (word parameter_port);

bool tcp_test(word id);

bool tcp_insert_sent (word id, struct tcp_idlist_message *msg, dword numseq);

bool tcp_remove_sent (word id, byte pos);
#endif
