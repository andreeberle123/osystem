/********************************************************

    dorothy/include/net/transp/udp.h

    Copyright (C) 2006 D-OZ Team

    UDP - User Datagram Protocol
	
********************************************************/

/********************************************************

	TODO
	Make the checksum works
********************************************************/
#ifndef _UDP_H
#define _UDP_H
#include "defines.h"
#include "net/utils/idlist.h"

struct udp_header {
    word sport;
    word dport;
    dword size;
    dword checksum;
};


typedef struct _udp_packet {
    struct udp_header header;
} udp_packet;

/* Sends the message */
bool udp_send (word id, byte *data, dword size);

/* Mounts the package */
udp_packet *udp_build_packet (word sport, word dport, 
                        dword size, byte *data);

void udp_handle_packet (byte *ip, udp_packet *p);

/* Gets the first message on the correspondent mailbox */
struct idlist_message *udp_get_message (word parameter_port);

/* Calculates the packet's checksum */   
word udp_checksum_do (byte *p, dword len);

/* Checks the packet's checksum */
bool udp_checksum_check (udp_packet *p);

word udp_register (word sport, byte *dst_ip, word dport);

#endif
