#ifndef _DEFINES_H
#define _DEFINES_H

#define byte  unsigned char
#define word  unsigned short
#define dword unsigned int

#define uint8_t byte
#define uint16_t word
#define uint32_t dword

#define size_t unsigned int

#define bool  byte

#define false 0
#define true  !false

#define NULL  0x00
#define null 0x00

/* These are the hton (host to network) macros. 
 * it is needed for 2-byte wide variables in 
 * network operations. Tricky, uhn? XD
 */
#define htons(n) (((n & 0xFF00) >> 8) | ((n & 0x00FF) << 8))
#define htonl(n) (((n & 0xFF000000) >> 24) | ((n & 0x00FF0000) >> 8) | ((n & 0x0000FF00) << 8) | ((n & 0x000000FF) << 24))
/* ntoh is hton backwards */
#define ntohs(n) (((n & 0xFF00) >> 8) | ((n & 0x00FF) << 8))
#define ntohl(n) (((n & 0xFF000000) >> 24) | ((n & 0x00FF0000) >> 8) | ((n & 0x0000FF00) << 8) | ((n & 0x000000FF) << 24))

#endif
