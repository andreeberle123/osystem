#ifndef _TFS_H
#define _TFS_H

#include "defines.h"

#define TFS_SEC_ERROR 1000000
#define PATH_ROOT 1000000
/* Attribute Types - Validos para o bitmap de arquivos */ 
#define T_DIR 	 1	//Atributo para diretorio
#define T_ARQ 	 2	//Atributo para arquivo
#define T_LNK 	 3	//Atributo para link
#define T_EMPTY  4	//Atributo para espaço vazio

/* Open File Types */
#define F_READ 	 1	//Tipo Leitura
#define F_WRITE  2 	//Tipo Escrita
#define F_RW	 3	//Tipo L/E
#define F_NONE	 4

/* Descritores */
#define TAMFAT 	 192	//Tamanho da FAT em setores
#define NUMDESC	 2048	//Numero maximo de arquivos na FAT (Numero de Descritores)
#define TAMBM	 512	//Tamanho do Bitmap de arquivos
#define FATSTART 301	//Setor inicial do disquete para a FAT
#define POSBM	 493	//Setor inicial do disquete para o Bitmap
			//494 � o primeiro setor de dados

/* Sectors */
#define NOSECTOR -1
#define SECTOR_SIZE 512
#define SECTOR_PAYLOAD 504

// cada setor do disco ter�
#define SECTOR_NEXT 505
#define SECTOR_PREV 509

/* fseek */
#define SEEK_SET 0
#define SEEK_END 1
#define SEEK_CUR 2

/* TFS FAT descriptor */
typedef struct tfsfat {
	char name[43];                          /* Descriptor Name */
	byte attrib;				/* Link, Directory, File*/
	byte ptr[4];				/* Pointer for next sector */
	//byte size[4]; -- 32 bits
} tfsfat;

/* File descriptor */
typedef struct FILE {
	unsigned long FID;
	unsigned long offset;
	byte type; 				/* Read, Write, R&W */
	unsigned long secnumber; 
	byte secbuffer[SECTOR_SIZE]; //setor atual
	
	unsigned long psector;
	unsigned long nsector;
} FILE;

typedef struct tfsfile {
	char name[43];
	byte attrib;
	byte ptr[4];
	byte data[464];
} tfsfile;

FILE *tfs_fopen(char *filename, byte type);	//falta
void tfs_fclose(FILE);			//falta

long tfs_read(FILE *fd, byte *buffer, int size /* in bytes */);
long tfs_write(FILE Arq, byte Data);			//falta

char *pwdtfs(void);		/* Retorna o diretorio atual */
bool tfschdir(char *Name);	/* Chama diretorio */
bool tfsmkdir(char *Name);	/* Cria diretorio */
bool tfsrmdir(char *Name);	/* Apaga diretorio */
bool tfsmkfile(char *Name);	//falta
bool tfsrmfile(char *Name);	//falta
bool tfsmklink(char *Name);	//falta
bool tfsrmlink(char *Name);	//falta
void init_dir(unsigned long sector); /* Inicializa diretorio */
bool chk_empty_dir(unsigned long sector, bool remote); /* Verifica se o diretorio esta vazio */
bool load_tfs(void); /* Carrega o TFS na memoria */
bool write_tfat(void); /* Escreve TFAT e BM no disco */
bool read_tfat(void);	/* Le TFAT e BM do disco para a memoria */	
bool fdformat_tfs(void); /* Formata disquete para TFS */
bool init_tfat(void);	/* Inicializa TFAT */
bool init_tfs(void);	/* Inicializa o TFS */

unsigned long getemptysec(void); /* Procura setor vazio em BM */
void setsec(unsigned long sec);  /* Seta setor como ocupado */
void rsetsec(unsigned long sec); /* Seta setor como desocupado */

char *tfs_ls (char *); /* Lista o diretorio atual */

void write_buffer(void); 
tfsfat *get_tfsfat(void);

unsigned long tfs_pos(char *path);
#endif
