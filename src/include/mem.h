/*
GazOS Operating System
Copyright (C) 1999  Gareth Owen <gaz@athene.co.uk>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#ifndef MEM_INCLUDE
#define MEM_INCLUDE

#include "defines.h"

extern unsigned long mem_end, bse_end;

void count_memory();

/* this structure precedes all large memory areas allocated by the kernel */
struct kmem_block {
  unsigned char used;         /* 2 if small block container, 1 if block is used, 0 if it is a free block */
  unsigned long size:24;
  struct kmem_block *next;
};

/* this structure follws a kmem_block in a small block container */
struct small_block_container {
  struct small_block_container *next; /* points to the next container in the list */
  struct small_block_header *first; /* pointer to the first block */
  unsigned short num_blocks;  /* the number of blocks in this container */
};

/* this structure precedes all small blocks of memory */
struct small_block_header {
  unsigned char used;         /* 1 if block is used, 0 if block is free */
  unsigned short size;
  struct small_block_container *next;
};

extern void *kalloc(unsigned long size);
extern void kfree(void *data);
extern void *kmemcpy(byte *dest, const byte *src, unsigned long n);
extern void testMemory(void);

/* syscall handler */
dword brk_syscall_handler(void * end_address);

typedef struct _memory_descriptor {
	dword stack_base; /* this is memory allocated to stack start, not the actual ebp */
	dword stack_end; /* this is memory allocated to stack end, not the actual esp */
	dword p_heap_start;
	dword p_heap_break;
} memory_descriptor;

#endif
