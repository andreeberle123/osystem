#ifndef _IO_H
#define _IO_H

#include "defines.h"

inline void outportb  (dword port, byte value);
inline void outportw  (dword port, word value);
inline void outportdw (dword port, dword value);
inline byte inportb   (dword port);
inline word inportw   (dword port);
inline dword inportdw (dword port);

#endif
