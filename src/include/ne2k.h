#ifndef _NE2K_H_
#define _NE2K_H_

#include "defines.h"

/* NIC registers */
#define COMMAND 				0x00
#define PAGESTART				0x01
#define PAGESTOP				0x02
#define BOUNDARY				0x03
#define TRANSMITSTATUS			0x04
#define TRANSMITPAGE			0x04
#define TRANSMITBYTECOUNT0		0x05
#define NCR						0x05
#define TRANSMITBYTECOUNT1		0x06
#define INTERRUPTSTATUS			0x07
#define CURRENT					0x07	// in page 1
#define REMOTESTARTADDRESS0		0x08
#define CRDMA0					0x08
#define REMOTESTARTADDRESS1		0x09
#define CRDMA1					0x09
#define REMOTEBYTECOUNT0		0x0a
#define REMOTEBYTECOUNT1		0x0b
#define RECEIVESTATUS			0x0c
#define RECEIVECONFIGURATION	0x0c
#define TRANSMITCONFIGURATION	0x0d
#define FAE_TALLY				0x0d
#define DATACONFIGURATION		0x0e
#define CRC_TALLY				0x0e
#define INTERRUPTMASK			0x0f
#define MISS_PKT_TALLY			0x0f
#define IOPORT					0x10

#define PSTART					0x46
#define PSTOP					0x80
#define TRANSMITBUFFER			0x40
#define rcr						0x04
#define tcr						0x00
#define dcr						0x58
#define imr						0x0b

/* Device structures */
typedef struct ne2k_isa {
	word iobase;
	byte irq;
	byte next_pkt;
	byte mac_addr[6];
} ne2k_isa;

/* NIC primitives */
void ne2k_init (word iobase, byte irq);
void ne2k_send (void *packet, word length);
void ne2k_irq  ();

/* External wrappers */
extern void ne2kirq (void);

#endif
