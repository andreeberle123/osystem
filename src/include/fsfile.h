#ifndef _FSFILE_H_
#define _FSFILE_H_

#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

#include "ffs.h"

// creates an entry in the fat and points it to a valid block, sets in 
// the bitmap, looks for a valid block and mark it, and store the block
// in the disk.
bool create_file(const char *name);

// sets all bitmap file blocks to zero, remove entry from fat ->
// retrieves the last one, move it to the local where the removed one was
// and sets the last one pointer to zero.
bool unlink(char *name);

// returns file size.
unsigned int ffilesize(FILE *stream);

FILE* fopen(const char*, const char*);

bool feof(FILE *);

int fread(void *ptr, int size, int nmemb, FILE *stream);

int fwrite(void *ptr, int size, int nmemb, FILE *stream);

int fseek(FILE *stream, long offset, int whence);

// sets flag to zero
int fclose(FILE *stream);

int aux_fseek(FILE *stream, long offset);

FILE * open_directory_fd(char * path);

#endif
