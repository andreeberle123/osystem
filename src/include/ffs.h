#ifndef _FFS_H_
#define _FFS_H_

#include "defines.h"

// TODO recalculate some of the following values (i.e. FAT_LAST_ELEMENT)
#define DISK_SIZE 2880		// number of sectors in the disk

#define SECTOR_SIZE 512		// number of bytes in a sector
#define SECTORS_TO_READ 8	// 
#define BLOCK_SIZE (SECTOR_SIZE*SECTORS_TO_READ)	// number of bytes in a block
#define POINTER_SIZE 4		// number of bytes used by a pointer
#define BITMAP_NUM_SECTORS 8 // 256(for 4gb) - number of sectors in the bitmap
#define NUMBER_OF_FILES  256 //2048 - number of files in a directory
#define FAT_LAST_ELEMENT NUMBER_OF_FILES-1	// index of the last element in the fat
#define INODE_SIZE 64			// INODE size
#define NUM_BITS_BITMAP (BITMAP_NUM_SECTORS*SECTOR_SIZE*8) // Number of bits in the bitmap


#define BLOCK_USER 300			
#define BLOCK_GROUP BLOCK_USER+8		// VALUE 308
#define FFS_BITMAP BLOCK_GROUP+8		// VALUE 316
#define FFS_FAT_ROOT FFS_BITMAP+BITMAP_NUM_SECTORS	// VALUE 324
#define FAT_NUM_SECTORS ((NUMBER_OF_FILES*INODE_SIZE)/SECTOR_SIZE)  // VALUE 32
#define FIRST_BLOCK FFS_FAT_ROOT+FAT_NUM_SECTORS    	// VALUE 356 first sector of the file system

#define FIRST_FILESYSTEM_BLOCK FFS_BITMAP
#define LAST_FILESYSTEM_BLOCK 2880-FIRST_FILESYSTEM_BLOCK

#define TAM_MAX_PATH 512

#define MAX_FILENAME 37

// sector FIRST_BLOCK is the first sector of the first block to be used in the file system 
typedef struct{

        char name[MAX_FILENAME];          // Filename - increased from 12 to 38 characters.
        unsigned int pointer;   // Pointer to block where the file is stored in the
                                // system, increased from short to int to address
                                // 4 GBytes.
        unsigned int size;      // File size. int represent files up to 4 GBytes.
     
        unsigned int pslink;     //pointer to softlink

		unsigned char datac[5];
        unsigned short flag;    // Bits with file info such as:
                                // permissions
                                // -> bit 2,1,0: file owner rwx
                                // -> bit 5,4,3: file group rwx
                                // -> bit 8,7,6: everyone rwx
                                // directory, softlink, regular file
                                // -> bit 9: flag directory
                                // -> bit 10: flag softlink
                                // -> bit 11: regular file.
        unsigned short idgroup; // stores the file group ID
        unsigned short idowner; // stores file owner user ID

} INODE;



typedef struct {
 	char name[MAX_FILENAME];           // File name
	char namelink[38];
	
	
	char mode; // read/write mode. 0 is readonly, 1 is writeonly, 2 is read/write
	char open_close; // states whether the file is open or closed.
	unsigned int first_block;

	unsigned int offset; // in bytes from first block

	unsigned int size;      // file size. up to 4GBytes.
	unsigned int pslink;    // pointer to softlink.
	unsigned short flag;    // Bits with file info such as:
							// permissions
							// -> bit 2,1,0: file owner rwx
							// -> bit 5,4,3: file group rwx
							// -> bit 8,7,6: everyone rwx
							// diretorio, softlink, arquivo ordinario
							// -> bit 9: flag directory
							// -> bit 10: flag softlink
							// -> bit 11: regular file.
							// -> bit 12: hidden file
	unsigned char datac[5];
	unsigned short idgroup; // Stores file group ID
	unsigned short idowner; // stores file owner user ID.
} FILE;


extern byte *ffs_fatdir;		
extern byte *ffs_bitmap;

extern unsigned char* current_path;

extern FILE * fslink;		// Store soflinks paths

extern byte *ffs_userbuffer;
extern byte *ffs_groupbuffer;

// includes
#include "fsbitmap.h"
#include "fsfile.h"
#include "fsdir.h"
#include "fsutil.h"

#endif
