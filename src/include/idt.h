#ifndef _IDT_H_
#define _IDT_H_

#include "defines.h"

/* Interrupt types */
#define INT_0		0x8E00		// 1000111000000000 = present, ring0, int_gate
#define INT_3		0xEE00		// 1110111000000000 = present, ring3, int_gate

/* An interrupt */
typedef struct {
	word low_offset;						// low nibble of offset to handler
	word selector;							// GDT selector
	word settings;							// settings for interrupt
	word high_offset;						// high nibble to handler
} __attribute__ ((packed)) x86_interrupt;

typedef struct {
	word limit;								// limit or size of IDT
	x86_interrupt *base;					// base of the IDT
} __attribute__ ((packed)) idtr;

void loadIDTR (void);
void addInt (int number, void (*handler)(), dword dpl);

#endif
