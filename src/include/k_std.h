/*
k_std.h que tem os prototipos das funcoes e as definiçoes necessárias
comentários das funções feitos no k_std.c
*/


#ifndef __k_std_H_

#define __k_std_H_

#define WHITE_TEXT 0x07
#define VIDEO_MEMORY 0xB8000
#define SCREEN_WIDTH 80
#define SCREEN_HEIGHT 25


void k_itoa(int, char *);
int k_atoi(char *);

int k_strlen(char *);


/* Convert ascii string to integer... */
int k_atoi(char *);



void k_clear_screen();		     // do mello
int k_pow(int,int);		     // auxiliar - mas conta como mais uma funcao implementada :-)

void k_strcpy(char *dest, char *src);


int k_strstr(char *, char *);
#endif
