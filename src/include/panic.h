#ifndef _PANIC_H
#define _PANIC_H_

#include "defines.h"

void loadExceptions (void);
void panic (char *message, char *mnemonic, bool halt); //exception panic

#endif
