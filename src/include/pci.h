#ifndef _PCI_H_
#define _PCI_H_

#include "defines.h"
#include "list.h"
#include "i386/pci.h"


/*
 * PCI device configuration space (256 bytes) is divided in static positions,
 * each one represented in the following addresses, according to 2.2 revision
 * specification
 */
#define PCI_VENDOR_ID			0x00
#define PCI_DEVICE_ID			0x02
#define PCI_COMMAND			0x04
#define PCI_STATUS			0x04

#define PCI_CLASS_REVISION		0x08
#define PCI_REVISION_ID			0x08
#define PCI_CLASS_PROG			0x09
#define PCI_CLASS_DEVICE		0x0A

#define PCI_CACHE_LINE_SIZE		0x0C
#define PCI_LATENCY_TIMER		0x0D
#define PCI_HEADER_TYPE			0x0E
#define PCI_BIST			0x0F

#define PCI_BAR_0			0x10
#define PCI_BAR_1			0x14
#define PCI_BAR_2			0x18
#define PCI_BAR_3			0x1c
#define PCI_BAR_4			0x20
#define PCI_BAR_5			0x24

#define PCI_CARDBUS_CIS_POINTER		0x28
#define PCI_SUBSYSTEM_ID		0x2C
#define PCI_SUBSYSTEM_VENDOR_ID		0x2E

#define PCI_EXPANSION_ROOM_ADDR		0x30
#define PCI_COMPATIBILITIES_POINTER	0x34

/* 0x35 to 0x3C are reserved */

#define PCI_INTERRUPT_LINE		0x3C
#define PCI_INTERRUPT_PIN		0x3D
#define PCI_MIN_GNT			0x3E
#define PCI_MAX_LAT			0x3F

#define PCI_COMMAND_IO			0x01
#define PCI_COMMAND_MEM			0x02
#define PCI_COMMAND_MASTER		0x04

/* Base Address Registers Masks */

#define PCI_ADDRESS_TYPE_MASK		0x01
#define PCI_ADDRESS_MEM_BREADTH_MASK	0x06
#define PCI_ADDRESS_MEM_PREFETCH_MASK	0x08

#define PCI_ADDRESS_IO_BASE_MASK	~(0x03)
#define PCI_ADDRESS_MEM_BASE_MASK	~(0x0F)

#define PCI_COMMAND_MASTER		0x04

#define PCI_INTEL_VENDOR		0x8086
#define PCI_INTEL_DEVICE_PIIX3		0x7000

#define PIRQ_IRQ_N 		0xB

struct _pci_device;

typedef struct _pci_driver {
	/* allow us to build a list out of it */
	struct list_head head;
	dword vendor_id;
	dword device_id;
	int (*probe) (struct _pci_device *dev);
	const char * name;
} pci_driver;



typedef struct _pci_device {
	/* allow us to build a list out of it */
	struct list_head head;
	byte bus;
	byte device_number;
	byte function_number;
	byte devfn; /* composita of device_number and function_number */
	word device_id;
	word vendor_id;
	dword io_address;
	dword mem_space;
	dword class;
	dword irq;
	pci_driver *driver;
} pci_device;

typedef struct _pci_irq_driver{
	/* allow us to build a list out of it */
	struct list_head head;
	pci_driver * driver;
	int (*map) (int irq);
} pci_irq_driver;

void pci_init();
#endif
