#ifndef _DMA_H
#define _DMA_H

void dma_transfer (byte channel, dword address, dword length, byte read);

#endif
