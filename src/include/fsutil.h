#ifndef _FSUTIL_H_
#define _FSUTIL_H_

void load_fat(void);

int getEntry(char *);

void ls(void);
void ls_lsa(void);

void cat2(int);
void cat(FILE *f);

void toLog();

void pwd(void);
void initializeFileSystem();
void allocPath();
bool isDirectory(INODE *p);
bool isSoftLink(INODE *p);
bool isOrdinaryFile(INODE *p);
bool isSoftLinkFile(FILE *p);
int fileSystemAlreadyInUse();
void allocFatAndBitmap();

void cpInodeToFile(INODE *p, FILE *f);
void printDate(unsigned char date[5]);
void getDate(unsigned char data[5]);

#endif
