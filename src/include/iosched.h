#include "ffs.h"


struct io_queue {
	struct task_struct * process;
	int exec_ready;
	void (*handler)();
	void * buffer;
	int block_num;
	int size;
	int num;
	struct io_queue * next;
};

void init_io_scheduler(void); 
void add_ionode(struct io_queue * ionode);
struct io_queue * make_ionode();
