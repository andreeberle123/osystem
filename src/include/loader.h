#ifndef _LOADER_H_
#define _LOADER_H_

#include "defines.h"

/* it loads the program, create the task_struct,
 * checks if it is a static ELF executable, prepares
 * the sections of ELF and put the process in the
 * READY_TO_RUN status.
 *
 * After that, the scheduler will call the process
 * */
void prepare_binary_to_run(char *filename);
void testLoader(void);
void executable_loader(char * file_name, char ** argv, int keep_process);
struct task_struct * make_ts(unsigned char * buffer);

#endif
