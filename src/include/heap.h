#ifndef _HEAP_H_
#define _HEAP_H_

#include "defines.h"

void swap_heap(unsigned long * p1,unsigned long * p2, void * temp, dword size);
void * heap_up(unsigned long * heap_root, unsigned long * curr,void * temp, unsigned int size);
void *heap_down(unsigned long * heap_root, unsigned long * curr,void * temp, unsigned int size,
				dword h_size);
unsigned long * insert_heap(unsigned long * heap_root, void * data, unsigned int size, 
				unsigned long * current, void * temp);
void remove_heap(unsigned long * heap_root, unsigned long * h_data ,void * temp, unsigned int size,
				unsigned long * h_size);

#endif