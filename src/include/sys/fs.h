#ifndef _SYS_FS_H_
#define _SYS_FS_H_

#include "ffs.h"

/* created this struct to provide compatibility with linux syscalls */
typedef struct _linux_dirent {
	unsigned long d_ino;
	unsigned long d_off;
	unsigned short d_reclen;
	char d_name[MAX_FILENAME+1]; /* 1 byte for string termination \0*/
	char d_type;
	
} linux_dirent;

int open_syscall_handler(const char * filename, unsigned int flags, int mode);
size_t write_syscall_handler(unsigned int fd, const char * buf, size_t count);
size_t read_syscall_handler(unsigned int fd, char * buf, size_t count);
long getdents_syscall_handler(unsigned int fd, struct _linux_dirent * buf, unsigned int size);
void init_fs_syscalls();
long chdir_syscall_handler(char * path);
int access_syscall_handler(char * name, int mode);

#endif