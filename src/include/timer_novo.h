#ifndef _TIMER_H_
#define _TIMER_H_

#include "defines.h"

#define MILISEC 	 10
#define FREQ 		(1000/MILISEC)
#define PRECISION	  8
#define MAXTRIGGERS   	 100

void loadTimer (void);
unsigned long calibrate_delay (void);
void delay (unsigned long mili);
void addTrigger (void (*handler)(void *, void *, void *, void *), dword timeout, void *arg1, void *arg2, void *arg3, void *arg4);

extern unsigned long delay_count;

extern struct trigger {
	    void (*handler)(void *, void *, void *, void *);
	    dword timeout;
	    void *arg1;
		void *arg2;
		void *arg3;
		void *arg4;
};

extern struct trigger trigger[MAXTRIGGERS];
extern byte ntriggers;

#endif
