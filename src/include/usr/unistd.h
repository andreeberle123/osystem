#ifndef _UNISTD_H_
#define _UNISTD_H_

#define IN_USR
#include "fsdir.h"

long chdir(const char *path);
long getdents(dword fd, linux_dirent *buf, dword size);
int fork();
int execve(char * filename, char ** argv, char ** envp);

#define F_OK		0
#define X_OK		1
#define W_OK		2
#define R_OK		4

#endif