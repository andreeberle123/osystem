#ifndef _USR_STDIO_H
#define _USR_STDIO_H

#include "stdarg.h"

int sprintf (char *ptr, const char *str, ...);
int vprintf (char *format, va_list arglist);
int printf ( const char * format, ... );

#endif