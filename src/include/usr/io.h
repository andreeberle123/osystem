#ifndef _IO_H_
#define _IO_H_

int open(char * path, int flags, ...);
void close(int fd);

#endif