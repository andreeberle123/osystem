#ifndef _USR_MALLOC_H_
#define _USR_MALLOC_H_

void * malloc(dword size);
void free (void*);

#endif