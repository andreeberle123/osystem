#include "event.h"
#include "eventvars.h"
#include "mem.h"

unsigned int create_fifo(unsigned int size, unsigned char **memaddr) {
	union event_type *event_fifo = (union event_type *) kalloc(sizeof(union event_type));
	
	((evt_fifo *) event_fifo)->code = FIFO;
	((evt_fifo *) event_fifo)->fifo_number = fifo_number++;
	((evt_fifo *) event_fifo)->memaddr = (unsigned char *) kalloc(sizeof(char) * size);

	//to return the memory address of fifo
	*memaddr = ((evt_fifo *) event_fifo)->memaddr;

	/* adding the event */
	add_event_queue(event_fifo);

	return ((evt_fifo *) event_fifo)->fifo_number;
}

void remove_fifo(unsigned int fifo_number) {
	EVENT *node = (EVENT *) _evt;
	EVENT *prev = NULL;

	while (node != NULL && ((evt_fifo *) node->event)->code == FIFO &&
			((evt_fifo *) node->event)->fifo_number == fifo_number) {
		prev = node;
		node = node->next;
	}

	if (node != NULL && ((evt_fifo *) node->event)->fifo_number == fifo_number) {
		if (prev == NULL) {
			_evt = node->next;
			kfree(((evt_fifo *)node)->memaddr);
			kfree(node);
		} else {
			prev->next = node->next;
			kfree(((evt_fifo *)node)->memaddr);
			kfree(node);
		}
	}

	_number_of_evt--;
}
