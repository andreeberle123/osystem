#include "event.h"
#include "eventvars.h"
#include "mem.h"
#include "process.h"

void create_timer(unsigned int sleep, unsigned int pid) {
	union event_type *event_timer = (union event_type *) kalloc(sizeof(union event_type));
	
	((evt_timer *) event_timer)->code = TIME;
	((evt_timer *) event_timer)->sleep = sleep;
	((evt_timer *) event_timer)->pid = pid;

	/* changing the process status */
	change_process_status(pid, WAITING);

	/* adding the event */
	add_event_queue(event_timer);
}

// dec timers and remove them from the queue
void dec_timer(void) {
	EVENT *n = (EVENT *) _evt;
	EVENT *prev = NULL;

	while (n != NULL) {
		if (((evt_discoverer *) n->event)->code == TIME) {
			/* if its a timer dec */ 
			((evt_timer *) n->event)->sleep--;

			if (((evt_timer *) n->event)->sleep == 0) {
				EVENT *remove = (EVENT *) n;
				if (remove == _evt) {
					prev = (EVENT *) _evt;
					_evt = _evt->next;
					//remove from the queue
					kfree(prev);
				} else {
					remove = n;
					prev->next = n->next;
					//remove from the queue
					kfree(remove);
				}
			}
		}
		prev = n;
		n = n->next;
	}
}
