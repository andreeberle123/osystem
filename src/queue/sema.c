#include "event.h"
#include "eventvars.h"
#include "mem.h"

unsigned int create_semaphore(void) {

	union event_type *event_semap = (union event_type *) kalloc(sizeof(union event_type));
	
	((evt_semaphore *) event_semap)->code = SEMA;
	((evt_semaphore *) event_semap)->semaphore_number = semaphore_number++;
	((evt_semaphore *) event_semap)->lock = 0;

	/* adding the event */
	add_event_queue(event_semap);

	return ((evt_semaphore *) event_semap)->semaphore_number;
}

void remove_semaphore(unsigned int semaphore_number) {
	EVENT *node = (EVENT *) _evt;
	EVENT *prev = NULL;

	while (node != NULL && ((evt_semaphore *) node->event)->semaphore_number != semaphore_number) {
		prev = node;
		node = node->next;
	}

	if (node != NULL && ((evt_semaphore *) node->event)->semaphore_number == semaphore_number) {
		if (prev == NULL) {
			_evt = node->next;
			kfree(node);
		} else {
			prev->next = node->next;
			kfree(node);
		}
	}

	_number_of_evt--;
}

/*
 * If you want to wait until the sema is locked you have
 * to make a loop testing the bool from this function.
 *
 * while (!test_and_lock_semaphore(semaid));
 *
 * Waits until the sema is locked!
 */
bool test_and_lock_semaphore(unsigned int semaphore_number) {
	EVENT *node = (EVENT *) _evt;

	while (node != NULL && ((evt_semaphore *) node->event)->semaphore_number != semaphore_number) {
		node = node->next;
	}

	if (node != NULL && ((evt_semaphore *) node->event)->semaphore_number == semaphore_number) {
		if (((evt_semaphore *) node->event)->lock == 0) {
			((evt_semaphore *) node->event)->lock = 1;
		}	
	}

	return true;
}

void unlock_semaphore(unsigned int semaphore_number) {
	EVENT *node = (EVENT *) _evt;

	while (node != NULL && ((evt_semaphore *) node->event)->semaphore_number != semaphore_number) {
		node = node->next;
	}

	if (node != NULL && ((evt_semaphore *) node->event)->semaphore_number == semaphore_number) {
		((evt_semaphore *) node->event)->lock = 0;
	}
}
