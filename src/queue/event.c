#include "event.h"
#include "eventvars.h"
#include "mem.h"

/* The main functions to manipulate events */

//initializing event queue
void init_event_queue(void) {
	_evt = NULL;
	_number_of_evt = 0;
}

// adding events to the queue
void add_event_queue(union event_type *event) {

	EVENT *node = (EVENT *) kalloc(sizeof(EVENT));
	
	/* receiving the params */
	node->event = event;
	node->next = NULL;

	if (_evt == NULL) {
		_evt = node;
	} else {
		EVENT *n = (EVENT *) _evt;
		while (n->next != NULL) {
			n = n->next;
		}

		n->next = node;
	}

	_number_of_evt++;
}

// removing events of the queue
void del_event_queue(EVENT *evt) {
	EVENT *node = (EVENT *) _evt;
	EVENT *prev = NULL;

	while (node != evt && node != NULL) {
		prev = node;
		node = node->next;
	}

	if (node == evt) {
		if (prev == NULL) {
			_evt = node->next;
			kfree(node);
		} else {
			prev->next = node->next;
			kfree(node);
		}
	}

	_number_of_evt--;
}
