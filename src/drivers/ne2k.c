/*******************************************************************************

  dorothy/drivers/ne2k.c
 
  Copyright (C) 2005 D-OZ Team

  NE2K ISA driver

  OBS: Seems the infinite loop bug is fixed. Just put a next_pkt instead of
  boundary for the loop condition in receive. Hope it works forever. :)
 
*******************************************************************************/
#include "ne2k.h"
#include "io.h"
#include "idt.h"
#include "stdio.h"
#include "8259a.h"
#include "net/dlink/eth.h"
#include "defines.h"
#include "stdio.h"
#include "nic.h"
#include "mem.h"

ne2k_isa ne;

void ne2k_prepare_MAC (void) {
	int i;

	/* reads MAC address from PROM */
    	outportb (ne.iobase + REMOTEBYTECOUNT0, 0x20);
    	outportb (ne.iobase + REMOTEBYTECOUNT1, 0x00);

	outportb (ne.iobase + REMOTESTARTADDRESS0, 0x00);
    	outportb (ne.iobase + REMOTESTARTADDRESS1, 0x00);

    	outportb (ne.iobase + COMMAND, 0x0A);

	for (i = 0; i < 6; i++) {
		ne.mac_addr[i] = inportb (ne.iobase + IOPORT);
		ne.mac_addr[i] = inportb (ne.iobase + IOPORT);
		//kprintf("%i ",ne.mac_addr[i]);
	}
	
	/* writes MAC address to registers */
	outportb (ne.iobase + COMMAND, 0x42);				//page 1, start mode

	for (i = 0; i < 6; i++) 
		outportb (ne.iobase + 0x01 + i, ne.mac_addr[i]);
	
	outportb (ne.iobase + COMMAND, 0x22);				//page 0, start mode
	
}

void ne2k_init (word iobase, byte irq) {
	/* TODO verify if the adapter is present! */
	ne.iobase = iobase;
	ne.irq = irq;
	outportb (ne.iobase + COMMAND, 0x21);               //stop mode
	outportb (ne.iobase + DATACONFIGURATION, dcr);
	outportb (ne.iobase + REMOTEBYTECOUNT0, 0x00);
	outportb (ne.iobase + REMOTEBYTECOUNT1, 0x00);
	outportb (ne.iobase + RECEIVECONFIGURATION, rcr);
	outportb (ne.iobase + TRANSMITPAGE, 0x20);
	outportb (ne.iobase + TRANSMITCONFIGURATION, 0x02); //temporarily go into loopback mode
	outportb (ne.iobase + PAGESTART, PSTART);
	outportb (ne.iobase + BOUNDARY, PSTART);
	outportb (ne.iobase + PAGESTOP, PSTOP);
	outportb (ne.iobase + COMMAND, 0x61);               //page 1 registers
	outportb (ne.iobase + CURRENT, PSTART + 1);
	outportb (ne.iobase + COMMAND, 0x22);               //page 0, start mode
	outportb (ne.iobase + INTERRUPTSTATUS, 0xff);
	outportb (ne.iobase + INTERRUPTMASK, imr);
	outportb (ne.iobase + TRANSMITCONFIGURATION, tcr); //normal mode, ready for operation
	ne.next_pkt = PSTART + 1;

	ne2k_prepare_MAC ();
	
	//INTS (false);
	/* the NE2K interrupt must be interrupted because of the floppy */
//	addInt (0x20 + ne.irq, ne2kirq, 2);                //interruption handler
	//addInt (0x20 + ne.irq, ne2kirq, 0);                //interruption handler
	//unmaskIRQ (ne.irq);
	//INTS (true);
	//nic nic = kalloc(sizeof(nic));
	//nic->send = ne2k_send;
	//nic->MAC = ne.mac_addr;
	//eth_register_send (ne2k_send);					   //ethernet handler
	//eth_register_MAC (ne.mac_addr);
	//eth_register_nic(nic);

	alloc_nic(_nic);
	_nic->MAC = ne.mac_addr;
	_nic->send = (void (*) (void *,word)) ne2k_send;
	_nic->interrupt = ne2kirq;
	_nic->pci_irq = 0;
	_nic->name = "NE2000 Adapter";
	
	add_nic(_nic);
}

void ne2k_PCtoNIC (void *frame, word length, word addr) {
	byte *data;
	byte is;

	/* prepare NIC for data receiving */
	outportb (ne.iobase + REMOTEBYTECOUNT0, length & 0x00ff);
	outportb (ne.iobase + REMOTEBYTECOUNT1, (length >> 8) & 0x00ff);
	outportb (ne.iobase + REMOTESTARTADDRESS0, addr & 0x00ff);
	outportb (ne.iobase + REMOTESTARTADDRESS1, (addr >> 8) & 0x00ff);
	outportb (ne.iobase + COMMAND, 0x12);               //write and start
	/* writes frame data to NIC */
	data = (byte *) frame;
	while (length > 0) {
		outportb (ne.iobase + IOPORT, *data);
		length--;
		data++;
	}
	/* waits for DMA completion */
	do {
		is = inportb (ne.iobase + INTERRUPTSTATUS);
	} while ((is & 0x40) == 0);
	outportb (ne.iobase + INTERRUPTSTATUS, 0x40);		//clear DMA isr bit
}

void ne2k_NICtoPC (void *frame, word addr) {
	byte *data;
	byte is;
	word length;
	

	/* prepare NIC for transmitting data to PC */
	outportb (ne.iobase + REMOTEBYTECOUNT0, length & 0x00ff);
	outportb (ne.iobase + REMOTEBYTECOUNT1, (length >> 8) & 0x00ff);
	outportb (ne.iobase + REMOTESTARTADDRESS0, addr & 0x00ff);
	outportb (ne.iobase + REMOTESTARTADDRESS1, (addr >> 8) & 0x00ff);
	outportb (ne.iobase + COMMAND, 0x0a);              //read and start

	inportb (ne.iobase + IOPORT);
	ne.next_pkt = inportb (ne.iobase + IOPORT);
	length = inportb (ne.iobase + IOPORT);
	length = length | (inportb (ne.iobase + IOPORT) << 8);
	outportb (ne.iobase + BOUNDARY, ne.next_pkt - 1);

	/* reads frame from NIC */
	data = (byte *) frame;
	while (length > 0) {
		*data = inportb (ne.iobase + IOPORT);
		length--;
		data++;
	}
	/* waits for DMA completion */
	do {
		is = inportb (ne.iobase + INTERRUPTSTATUS);
	} while ((is & 0x40) == 0);
	outportb (ne.iobase + INTERRUPTSTATUS, 0x40);      //clear DMA isr bit
}

void ne2k_send (void *frame, word length) {
	byte cr;

	INTS (false);
	cr = inportb (ne.iobase + COMMAND);

	if (cr == 0x26) {//transmitting already?
		kprintf ("ne2k: queueing frame...\n"); //ok, in the future we will queue
	} else {
		ne2k_PCtoNIC (frame, length, TRANSMITBUFFER << 8);
		outportb (ne.iobase + TRANSMITPAGE, TRANSMITBUFFER);
		outportb (ne.iobase + TRANSMITBYTECOUNT0, length & 0x00ff);
		outportb (ne.iobase + TRANSMITBYTECOUNT1, (length >> 8) & 0x00ff);
		outportb (ne.iobase + COMMAND, 0x26);               //transmit
	}
	INTS (true);
}

void ne2k_irq () {
	byte isr;
	for (;;) {
		isr = inportb (ne.iobase + INTERRUPTSTATUS);

		if (isr & 0x01) {           //frame received
//			byte boundary,
			byte current;
			byte frame [1518];
//			struct ethernet_header *header = (struct ethernet_header *) frame;
			
			int i;

			do {
///				kprintf ("ne2k: frame received.\n");
				isr = inportb (ne.iobase + INTERRUPTSTATUS);
				if (isr & 0x10) {         //ring overflow
					kprintf ("ne2k: overflow occurred.\n");
					outportb (ne.iobase + COMMAND, 0x21);          //stop mode
					outportb (ne.iobase + REMOTEBYTECOUNT0, 0x00);
					outportb (ne.iobase + REMOTEBYTECOUNT1, 0x00);
					for (i = 0; i < 0x7fff; i++)
						if (inportb (ne.iobase + INTERRUPTSTATUS) & 0x80)
							break;

					outportb (ne.iobase + TRANSMITCONFIGURATION, 0x02);
					outportb (ne.iobase + COMMAND, 0x22);          //start mode
					ne2k_NICtoPC (frame, ne.next_pkt << 8);
					outportb (ne.iobase + INTERRUPTSTATUS, 0x10);  //clear overflow bit
					outportb (ne.iobase + TRANSMITCONFIGURATION, tcr);
	
				} else {                  //normal frame
					outportb (ne.iobase + INTERRUPTSTATUS, 0x01);
					ne2k_NICtoPC (frame, ne.next_pkt << 8);  
//					kprintf ("frame: ");
//					for (i = 0; i < 12; i++)
//						kprintf ("%X ", *((byte *) frame + i));
//					kprintf ("\n");
//					kprintf ("length: %d\n", header->type);
					eth_handle_frame (frame);
				}	

//				boundary = inportb (ne.iobase + BOUNDARY);
				outportb (ne.iobase + COMMAND, 0x62);       //page 1
				current = inportb (ne.iobase + CURRENT);
				outportb (ne.iobase + COMMAND, 0x22);       //page 0
//				current = current < boundary? PSTOP - PSTART - 2 + current : current - 2;

//				if ((header->type == 22359) || (header->type == 65535))	//Workaround for BOCHS
//				if ((header->type == 0))	//Workaround for weird bug
//					break;
				
			} while (ne.next_pkt != current);
		} else if (isr & 0x0a) {	//frame transmitted
//			kprintf ("ne2k: frame transmitted.\n");
			outportb (ne.iobase + INTERRUPTSTATUS, 0x0a);
		} else
			break;
	}

	outportb (ne.iobase + INTERRUPTSTATUS, 0xff);           //clear interrupts
	outportb (ne.iobase + INTERRUPTMASK, 0);
}

void ne2k_imr () {
	outportb (ne.iobase + INTERRUPTMASK, imr);
}
