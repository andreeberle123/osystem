/*******************************************************************************

  dorothy/drivers/8259a.c
 
  Copyright (C) 2005 D-OZ Team

  8259a PIC (Programmable Interrupt Controller) driver
 
*******************************************************************************/

#include "8259a.h"
#include "io.h"

void INTS (bool on) {
	if (on)
		asm ("sti");
	else
		asm ("cli");
}

void maskIRQ (byte irq) {
	if (irq == ALL) {
		outportb (MASTERDATA, 0xFF);
		outportb (SLAVEDATA, 0xFF);
	} else {
		irq = irq | (1 << irq);
		if (irq < 8)
			outportb (MASTERDATA, irq & 0xFF);
		else
			outportb (SLAVEDATA, irq >> 8);
	}
}

void unmaskIRQ (byte irq) {
	if (irq == ALL) {
		outportb (MASTERDATA, 0x00);
		outportb (SLAVEDATA, 0x00);
	} else {
		irq = irq & (1 << irq);
		if (irq < 8)
			outportb (MASTERDATA, irq & 0xFF);
		else
			outportb (SLAVEDATA, irq >> 8);
	}
}

void remapPIC (int master, int slave) {
	byte md,
		 sd;

	md = inportb (MASTERDATA);
	sd = inportb (SLAVEDATA);

	outportb (MASTER, EOI);			 			// EOI == resets the chip
	outportb (MASTER, ICW1_INIT + ICW1_ICW4);
	outportb (SLAVE, ICW1_INIT + ICW1_ICW4);

	outportb (MASTERDATA, master);				
	outportb (SLAVEDATA, slave);

	outportb (MASTERDATA, 0x04);
	outportb (SLAVEDATA, 0x02);

	outportb (MASTERDATA, ICW4_8086);
	outportb (SLAVEDATA, ICW4_8086);

	outportb (MASTERDATA, md);
	outportb (SLAVEDATA, sd);
}

void init_pit (float freq, unsigned char channel) {
	unsigned int temp = 1193180 / freq;

	outportb (TMR_CTRL, TMR_BOTH + TMR_MD3);
	outportb (TMR_CNT0 + channel, (byte) temp);
	outportb (TMR_CNT0 + channel, (byte) (temp >> 8));
}

void set_highest_priority (byte high_master, byte high_slave) {
	high_master = (high_master - 1) & 7;
	high_slave = (high_slave - 8 - 1) & 7;
	outportb (MASTER, 0xc0 | high_master);
	outportb (SLAVE, 0xc0 | high_slave);
}

/*
	Tell PIC it is free to handle interrupts again
*/
void finish_interrupt() {
	outportb (SLAVE, EOI);
	/* TODO check if other iterrupts are pending in the slave
	   bank before signaling the master EOI */
	outportb (MASTER, EOI);
}