#include "i386/_pci.h"
#include "io.h"

/*
 * Functions for accessing PCI configuration space with type 1 accesses
 * Architecture specific for i386 platforms. 
 * Most of this taken from Etherboot (TODO por creditos) 
 */

#define  PCIBIOS_SUCCESSFUL                0x00

#define CONFIG_CMD(bus, device_fn, where)   (0x80000000 | (bus << 16) | (device_fn << 8) | (where & ~3))

int pcibios_read_config_byte(unsigned int bus, unsigned int device_fn,
			       unsigned int where, uint8_t *value)
{
    outportdw(CONFIG_CMD(bus,device_fn,where), 0xCF8);
    *value = inportb(0xCFC + (where&3));
    return PCIBIOS_SUCCESSFUL;
}

int pcibios_read_config_word (unsigned int bus,
    unsigned int device_fn, unsigned int where, uint16_t *value)
{
    outportdw(CONFIG_CMD(bus,device_fn,where), 0xCF8);
    *value = inportw(0xCFC + (where&2));
    return PCIBIOS_SUCCESSFUL;
}

int pcibios_read_config_dword (unsigned int bus, unsigned int device_fn,
				 unsigned int where, uint32_t *value)
{
    outportdw(CONFIG_CMD(bus,device_fn,where), 0xCF8);
    *value = inportdw(0xCFC);
    return PCIBIOS_SUCCESSFUL;
}

int pcibios_write_config_byte (unsigned int bus, unsigned int device_fn,
				 unsigned int where, uint8_t value)
{
    outportdw(CONFIG_CMD(bus,device_fn,where), 0xCF8);
    outportb(value, 0xCFC + (where&3));
    return PCIBIOS_SUCCESSFUL;
}

int pcibios_write_config_word (unsigned int bus, unsigned int device_fn,
				 unsigned int where, uint16_t value)
{
    outportdw(CONFIG_CMD(bus,device_fn,where), 0xCF8);
    outportw(value, 0xCFC + (where&2));
    return PCIBIOS_SUCCESSFUL;
}

int pcibios_write_config_dword (unsigned int bus, unsigned int device_fn, unsigned int where, uint32_t value)
{
    outportdw(CONFIG_CMD(bus,device_fn,where), 0xCF8);
    outportdw(value, 0xCFC);
    return PCIBIOS_SUCCESSFUL;
} 
