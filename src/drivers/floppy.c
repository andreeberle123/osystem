/*******************************************************************************

  dorothy/drivers/floppy.c

  Copyright (C) 2005 D-OZ Team

  Based on the GazOS floppy driver by Gareth Owen (1999)

*******************************************************************************/

#include "floppy.h"
#include "defines.h"
#include "timer.h"
#include "idt.h"
#include "irqwrapper.h"
#include "io.h"
#include "8259a.h"
#include "stdio.h"
#include "dma.h"

/* globals */
static volatile bool done = false;
static bool dchange = false;
static bool motor = false;
static bool killmotor = false;
static volatile int tmout = 0;
static byte status[7] = { 0 };
static byte statsz = 0;
static byte sr0 = 0;
static byte fdc_track = 0xff;
static drvgeom geometry = { DG144_HEADS, DG144_TRACKS, DG144_SPT };

unsigned long tbaddr = 0x80000L;    /* physical address of track buffer located below 1M */

void init_floppy (void) {
	INTS (false);
	addInt (0x26, floppyirq, 1);
	unmaskIRQ (6);
	INTS (true);

	reset ();
}

/* this gets the FDC to a known state */
void reset (void) {
	/* stop the motor and disable IRQ/DMA */
	outportb (FDC_DOR,0);

   	killmotor = false;
	motor = false;

	/* program data rate (500K/s) */
	outportb (FDC_DRS, 0);

	/* re-enable interrupts */
	outportb (FDC_DOR, 0x0C);

	/* resetting triggered an interrupt - handle it */
	done = true;
	wait_floppy (true);

	/* specify drive timings (got these off the BIOS) */
	sendbyte (CMD_SPECIFY);
	sendbyte (0xDF);  /* SRT = 3ms, HUT = 240ms */
	sendbyte (0x02);  /* HLT = 16ms, ND = 0 */

	/* clear "disk change" status */
	seek (1);
	recalibrate_floppy ();

	dchange = false;
}

void floppy_irq (void) {

	done = true;				/* signal operation finished */
//D	kprints ("debug: floppy done.\n");
	outportb (MASTER, EOI);
}

/*
 * converts linear block address to head/track/sector
 *
 * blocks are numbered 0..heads*tracks*spt-1
 * blocks 0..spt-1 are serviced by head #0
 * blocks spt..spt*2-1 are serviced by head 1
 *
 * WARNING: you give me garbage, I throw you garbage!
 */
void block2hts (int block, int *head, int *track, int *sector) {
	*head   = (block % (geometry.spt * geometry.heads)) / (geometry.spt);
	*track  = block / (geometry.spt * geometry.heads);
	*sector = block % geometry.spt + 1;
}

/* turns the motor on */
void motoron (void) {
	if (!motor) {
		killmotor = false;
		outportb (FDC_DOR, 0x1C);
		delay (500);
		motor = true;
	}
}

/* prepares the trigger to turn the motor off */
void motoroff (void) {
	if (motor) {
		killmotor = true;
//		addTrigger (motorkiller, 13500);
		/* Do we really need this as a trigger? I am spotting multiple calls to this motoroff
			function, therefore there are several motor killing triggers being called 
			at the same time */
		addTrigger ((void (*) (void *))motorkiller, 350, NULL);
	}
}

/* turns off the motor */
void motorkiller (void) {
	if (killmotor && motor) {
//D		kprints ("debug: motor killed.\n");
		outportb (FDC_DOR, 0x0C);
		motor = false;
	}
}

/* sendbyte() routine from intel manual */
void sendbyte (int b) {
	volatile int msr;
	int tmo;

	for (tmo = 0;tmo < 128;tmo++) {
		msr = inportb (FDC_MSR);
   		if ((msr & 0xc0) == 0x80) {
	 		outportb (FDC_DATA, b);
	 		return;
      	}
      	inportb(0x80);   /* Delay */
	}
}

/* getbyte() routine from intel manual */
int getbyte (void) {
	volatile int msr;
	int tmo;

	for (tmo = 0;tmo < 128;tmo++) {
		msr = inportb (FDC_MSR);
		if ((msr & 0xd0) == 0xd0)
			return inportb(FDC_DATA);
      	inportb(0x80);   /* Delay */
	}
	return -1;   /* read timeout */
}

/* recalibrate the drive */
void recalibrate_floppy (void) {
	/* turn the motor on */
	motoron();

	/* send actual command bytes */
	sendbyte(CMD_RECAL);
	sendbyte(0);

	/* wait until seek finished */
	wait_floppy (true);

	/* turn the motor off */
	motoroff();
}

/* floppy timeout trigger */
void floppy_timeout (void) {
	tmout = true;
}

/* this waits for FDC command to complete */
bool wait_floppy (bool sense_is) {
	tmout = false;

	/* NOTE Patched it here, this function was generating tons of triggers and not clearing
		them, so I removed the triggers. I suppose we can fix this later in the I/O scheduler */
	//int trg = addTrigger ((void (*) (void *))floppy_timeout, 50000, NULL); /* set timeout */
	//D	addTrigger (floppy_timeout, 500); /* set timeout */

	/* wait for IRQ6 handler to signal command finished */

	/* added some sort of message to the scheduler here,
	i don't want to overload the processor while we wait for the irq6 */
  	while (!done && !tmout)
		;

	/* read in command result bytes */
	statsz = 0;
	while ((statsz < 7) && (inportb (FDC_MSR) & (1<<4)))
		status[statsz++] = getbyte();

	if (sense_is) {
    	/* send a "sense interrupt status" command */
    	sendbyte(CMD_SENSEI);
    	sr0 = getbyte();
    	fdc_track = getbyte();
	}

	done = false;

	if (tmout) {  	/* timed out! */
    	if (inportb(FDC_DIR) & 0x80)  /* check for diskchange */
			dchange = true;
      	return false;
   	} else {
		//removeTrigger(trg);
    	return true;
	}
}

/* seek to track */
bool seek (int track){
	if (fdc_track == track)  /* already there? */
		return true;

//? motoron();
	/* send actual command bytes */
	sendbyte(CMD_SEEK);
	sendbyte(0);
	sendbyte(track);
	/* wait until seek finished */
	if (!wait_floppy (true))
		return false;     /* timeout! */

	/* now let head settle for 15ms */
	delay(15);

//?motoroff();

	/* check that seek worked */
	if ((sr0 != 0x20) || (fdc_track != track))
		return false;
	else
		return true;
}

/* common part between read_block() and write_block() */
bool floppy_rw (int block, byte *buffer, bool read, unsigned long nosectors) {
	int head,
		track,
		sector,
		tries,
		stries,
		copycount = 0;
	byte *p_tbaddr = (byte *) 0x80000;
	byte *p_buffer = buffer;

	block2hts (block, &head, &track, &sector);

	/* spin it, spin it, dorothy! */
	motoron ();
//D	kprints ("debug: motoron ok\n");

	if (!read && buffer) {
		/* it is a write, copy data from data buffer into track buffer */
		for (copycount = 0; copycount < (nosectors * 512); copycount++) {
			*p_tbaddr = *p_buffer;
			p_buffer++;
			p_tbaddr++;
		}
	}

	for (tries = 0; tries < 3; tries++) {
		if (inportb (FDC_DIR) & 0x80) {	/* disk change */
			dchange = true;
			seek (1);					/* clear disk change status */
			recalibrate_floppy ();
			motoroff ();
			kprints ("Floppy: Disk change detected. Trying again...\n");
			/* disk change: trying again */
			return floppy_rw (block, buffer, read, nosectors);
		}

		/* here we go for the track! move, head, move! */
		for (stries = 0; stries < 3; stries++)
			if (seek (track))
				break;
//D		kprints ("debug: seek ok\n");

		/* could not seek track, abort! */
		if (stries >= 3) {
			motoroff ();
			kprints ("Floppy: Error seeking track.\n");
			return false;
		}

		/* program data rate (500K/s) */
		outportb (FDC_CCR, 0);

		/* sends command */
		if (read) {
			dma_transfer (2, tbaddr, nosectors * 512, false);
			sendbyte (CMD_READ);
		} else {
			dma_transfer (2, tbaddr, nosectors * 512, true);
			sendbyte (CMD_WRITE);
		}
//D		kprints ("debug: dma start ok\n");

		sendbyte (head << 2);
		sendbyte (track);
		sendbyte (head);
		sendbyte (sector);
		sendbyte (2);			/* 512 bytes/sector */
		sendbyte (geometry.spt);

		if (geometry.spt == DG144_SPT)
			sendbyte (DG144_GAP3RW);
		else
			sendbyte (DG168_GAP3RW);
		sendbyte(0xff);

		/* wait for completion */
//?		GAZOS says read/write don't need sense_is, but passes 'true'. which is right?
		if (!wait_floppy (true)) {
			kprints ("Floppy: timed out. Trying again after reset.\n");
			reset ();
			return floppy_rw (block, buffer, read, nosectors);
		}
//D		kprints ("debug: dma finish ok\n");

		if ((status[0] & 0xC0) == 0)	/* works! back to Kansas! */
			break;

		recalibrate_floppy ();					/* still have to kill a wicked witch */
	}

	motoroff ();

	/* we are reading, updates the outside buffer */
	if (read && buffer) {
		p_buffer = buffer;
		for (copycount = 0; copycount < (nosectors * 512); copycount++) {
			*p_buffer = *p_tbaddr;
			p_buffer++;
			p_tbaddr++;
		}
	}

	return (tries < 3);
}

/* reads a block */
bool read_block(int block, byte *buffer, unsigned long nosectors) {
	int track  = 0,
		sector = 0,
		head   = 0,
		ftrack = 0,
		i;
	bool result = false;

	/* the FDC can read multiple sides at once but not multiple tracks */
	block2hts(block, &head, &track, &sector);
	block2hts(block+nosectors, &head, &ftrack, &sector);

	/* final track != initial track => reads one track at a time */
	if (track != ftrack) {
		for(i = 0; i < nosectors; i++) {
			result = floppy_rw(block + i, buffer + (i * 512), true, 1);
		}
		return result;
	}
	return floppy_rw(block, buffer, true, nosectors);
}

/* writes a block */
bool write_block(int block, byte *buffer, unsigned long nosectors) {
//? Does writing have the same problem with tracks as reading?
	return floppy_rw(block, buffer, false, nosectors);
}
