/*******************************************************************************

  dorothy/drivers/piix3.c
 
  Copyright (C) 2005 D-OZ Team

  PIIX3 driver. This code follows the intel spec on the PIR PIIX3 chipset.

  This driver is essentially driven to PIRQ to IRQ mapping.

*******************************************************************************/

#include "pci.h"
#include "io.h"
#include "stdio.h"
#include "mem.h"
#include "piix3.h"

/* base address for accessing the device through the PCI bus, passed by the PCI probe functions */
dword px_io_address;

dword px_devfn;
dword px_bus;

/*
	This function maps all 4 pirq pins to a given IRQ
*/
void pirq_map(byte irq) {
	
	unsigned reg = PIIX3_PIRQ0_REG;

	while (reg <= PIIX3_PIRQ3_REG) {
		//outportb(io_address + reg, irq);
		//kprintf("piix data = %d %d\n",px_devfn,px_bus);
		unsigned int val;
		irq = 0xB;
		pci_bios_write_configuration_byte(px_devfn,px_bus,reg,(dword*)&irq);
		pci_bios_read_configuration_byte(px_devfn, px_bus, reg, &val);
		
		//pci_bios_write_configuration_byte(px_devfn,px_bus,0x70,(dword*)&irq);
		//pci_bios_read_configuration_byte(px_devfn, px_bus, 0x70, &val);
//		pci_bios_read_configuration_byte(px_devfn, px_bus, val, &val2);

		//kprintf("piix data = %X\n",val);
		//int r = pci_bios_write_configuration_byte(pci_irq_table->interrupt_devfn,pci_irq_table->interrupt_bus,reg, (dword*)&irq);
		/*if (!r)
			kprintf("	PIRQ succesfuly mapped to %X!\n",PIRQ_IRQ_N);
		else
			kprintf("	PIRQ mapping failed with code %X!\n",r);*/
		
		reg++;
	}
	kprintf("	PIRQ mapped to %X!\n",irq);
}

int probe_piix3_device(pci_device * pci_dev) {
	px_io_address = pci_dev->io_address;
	px_devfn = pci_dev->devfn;
	px_bus = pci_dev->bus;
	// assume that if vendor and device id matches piix3 specs, we are ok
	return 0;
}

pci_driver piix3_driver = {
	{
	NULL,NULL
	},	
	/* TODO change this to accept any pcnet32 vendor and device IDs */
	PCI_INTEL_VENDOR,
	PCI_INTEL_DEVICE_PIIX3,
	probe_piix3_device,
	"PIIX3 Chipset Driver"
};

pci_irq_driver piix3_irq_driver = {
	{
	NULL,NULL
	},	
	&piix3_driver,
	pirq_map
};