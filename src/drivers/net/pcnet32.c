/*******************************************************************************

  dorothy/drivers/pcnet32.c
 
  Copyright (C) 2005 D-OZ Team

  PCNET32 Driver. Parts of this code were taken from Etherboot pcnet32 driver
  implementation

  TODO  - prevent errors if multiple PCNET devices exist
	- better support for chips other than PCnet/PCI II 79C970A

*******************************************************************************/


#include "pci.h"
#include "pcnet32.h"
#include "io.h"
#include "stdio.h"
#include "nic.h"
#include "mem.h"
#include "net/dlink/eth.h"

/* NOTE: this include is here for testing purposes, remove it asap */
#include "8259a.h"

/* the associated device MAC address */
byte mac_addr[6];

/* pcnet initialization structure */
pcnet32_init_block init_block;

/* access function (differ in word/dword) */
struct pcnet32_access * access;

/* base address for accessing the device through the PCI bus, passed by the PCI probe functions */
dword io_address;

nic * attached_nic;

dword cur_tx = 0;
dword cur_rx = 0;

/* PCnet32 access functions */
struct pcnet32_access {
	word(*read_csr) (unsigned long, int);
	void (*write_csr) (unsigned long, int, word);
	 word(*read_bcr) (unsigned long, int);
	void (*write_bcr) (unsigned long, int, word);
	 word(*read_rap) (unsigned long);
	void (*write_rap) (unsigned long, word);
	void (*reset) (unsigned long);
};

/* Buffers for the tx and Rx */

/* Create a static buffer of size PKT_BUF_SZ for each
TX Descriptor.  All descriptors point to a
part of this buffer */
static unsigned char txb[PKT_BUF_SZ * TX_RING_SIZE];
//    __attribute__ ((aligned(16)));

/* Create a static buffer of size PKT_BUF_SZ for each
RX Descriptor   All descriptors point to a
part of this buffer */
static unsigned char rxb[RX_RING_SIZE * PKT_BUF_SZ];

/* Define the TX Descriptor */
static pcnet32_tx_head tx_ring[TX_RING_SIZE] __attribute__ ((aligned(16)));


/* Define the RX Descriptor */
static pcnet32_rx_head rx_ring[RX_RING_SIZE] __attribute__ ((aligned(16)));

static word pcnet32_wio_read_csr(unsigned long addr, int index)
{
	outportw(addr + PCNET32_WIO_RAP, index);
	return inportw(addr + PCNET32_WIO_RDP);
}

static void pcnet32_wio_write_csr(unsigned long addr, int index, word val)
{
	outportw(addr + PCNET32_WIO_RAP, index);
	outportw(addr + PCNET32_WIO_RDP, val);
}

static word pcnet32_wio_read_bcr(unsigned long addr, int index)
{
	outportw(addr + PCNET32_WIO_RAP, index);
	return inportw(addr + PCNET32_WIO_BDP);
}

static void pcnet32_wio_write_bcr(unsigned long addr, int index, word val)
{
	outportw(addr + PCNET32_WIO_RAP, index);
	outportw(addr + PCNET32_WIO_BDP, val);
}

static word pcnet32_wio_read_rap(unsigned long addr)
{
	return inportw(addr + PCNET32_WIO_RAP);
}

static void pcnet32_wio_write_rap(unsigned long addr, word val)
{
	outportw(addr + PCNET32_WIO_RAP, val);
}

static void pcnet32_wio_reset(unsigned long addr)
{
	inportw(addr + PCNET32_WIO_RESET);
}

static int pcnet32_wio_check(unsigned long addr)
{
	outportw(addr + PCNET32_WIO_RAP, 88);
	return (inportw(addr + PCNET32_WIO_RAP) == 88);
}

static struct pcnet32_access pcnet32_wio = {
      read_csr:pcnet32_wio_read_csr,
      write_csr:pcnet32_wio_write_csr,
      read_bcr:pcnet32_wio_read_bcr,
      write_bcr:pcnet32_wio_write_bcr,
      read_rap:pcnet32_wio_read_rap,
      write_rap:pcnet32_wio_write_rap,
      reset:pcnet32_wio_reset
};

static word pcnet32_dwio_read_csr(unsigned long addr, int index)
{
	outportdw(addr + PCNET32_DWIO_RAP, index);
	return (inportdw(addr + PCNET32_DWIO_RDP) & 0xffff);
}

static void pcnet32_dwio_write_csr(unsigned long addr, int index, word val)
{
	outportdw(addr + PCNET32_DWIO_RAP, index);
	outportdw(addr + PCNET32_DWIO_RDP, val);
}

static word pcnet32_dwio_read_bcr(unsigned long addr, int index)
{
	outportdw(addr + PCNET32_DWIO_RAP, index);
	return (inportdw(addr + PCNET32_DWIO_BDP) & 0xffff);
}

static void pcnet32_dwio_write_bcr(unsigned long addr, int index, word val)
{
	outportdw(addr + PCNET32_DWIO_RAP, index);
	outportdw(addr + PCNET32_DWIO_BDP, val);
}

static word pcnet32_dwio_read_rap(unsigned long addr)
{
	return (inportdw(addr + PCNET32_DWIO_RAP) & 0xffff);
}

static void pcnet32_dwio_write_rap(unsigned long addr, word val)
{
	outportdw(addr + PCNET32_DWIO_RAP, val);
}

static void pcnet32_dwio_reset(unsigned long addr)
{
	inportdw(addr + PCNET32_DWIO_RESET);
}

static int pcnet32_dwio_check(unsigned long addr)
{
	outportdw(addr + PCNET32_DWIO_RAP, 88);
	return ((inportdw(addr + PCNET32_DWIO_RAP) & 0xffff) == 88);
}

static struct pcnet32_access pcnet32_dwio = {
      read_csr:pcnet32_dwio_read_csr,
      write_csr:pcnet32_dwio_write_csr,
      read_bcr:pcnet32_dwio_read_bcr,
      write_bcr:pcnet32_dwio_write_bcr,
      read_rap:pcnet32_dwio_read_rap,
      write_rap:pcnet32_dwio_write_rap,
      reset:pcnet32_dwio_reset
};

/**************************************************************************
	Interrupt controls
**************************************************************************/

unsigned int transmit_wait_irq = 0;

void pcnet32_interrupt() {

	//kprintf("Interrupted\n");
	dword control = access->read_csr(io_address, 0);
	if (!(control & (1 << 7))) {
		/* shouldnt happen, interruption without interrupt flag? */
		kprintf("ERROR in PCNET 32 driver: false interruption");
		return;
	}
	//kprintf("PCNET 32 interrupt 0x%X\n",control);
	//access->write_csr(io_address, 0, control);
//kprintf("status=0x%X\n",access->read_csr(io_address,0));
	access->write_csr(io_address, 0, control);
	//kprintf("status=0x%X\n",access->read_csr(io_address,0));
	if (control & PCNET32_CSR0_TINT) {
		//transmit_wait_irq = 1;
		/* Stop pointing at the current txb
		 * otherwise the card continues to send the packet */
//		tx_ring[0].base = 0;
		//access->write_csr(io_address, 0, control);
		access->write_csr(io_address, 0, control);
		//kprintf("status=0x%X\n",access->read_csr(io_address,0));
	}
	if (control & PCNET32_CSR0_RINT) {
		kprintf("Frame received with size %d\n",rx_ring[0].msg_length);
		/* We are using only one rx ring, 0 */
		int entry = cur_rx & RX_RING_MOD_MASK;
		byte * frame = kalloc(rx_ring[entry].msg_length*sizeof(byte));
		kmemcpy(frame,(byte*)(rx_ring[entry].base), rx_ring[entry].msg_length);
		attached_nic->receive(frame);
		/* clear the ring status to receive next frame */
		rx_ring[entry].status |= 0x8000;

		/*access->write_csr(io_address, 7, access->read_csr(io_address, 7));
        access->write_csr(io_address, 5, access->read_csr(io_address, 5));
        access->write_csr(io_address, 4, access->read_csr(io_address, 4));*/

		/* cur_rx will grow forever (well at least until it reaches the max
		   int size, then it will zero again), a mask is used to assert only
		   the valid range 
			NOTE this wont work well if max int % ring size != 0*/
		cur_rx++;

	}
	/*
		NOTE this call belongs to the PCI interrupt handler function.
		It remains here while all PCI IRQs are being handled here.
	*/
	finish_interrupt();
	 
}

/**************************************************************************
TRANSMIT - Transmit a frame
***************************************************************************/
static void pcnet32_transmit(const byte *frame, word s) {
	//kprintf("On transmit, csr0 = 0x%X\n",access->read_csr(io_address,0));
	//kprintf("On transmit, csr3 = 0x%X\n",access->read_csr(io_address,3));
	/* send the packet to destination */
//	unsigned long time;
	byte *ptxb;
//	word nstype;
	word status;
	int entry = 0;		/*lp->cur_tx & TX_RING_MOD_MASK; */

	status = 0x8300;
	/* using single ring */
	ptxb = txb;
	//kmemcpy(((struct ethernet_header*)frame)->dst_address,mac_addr,6);
	/* copy the packet to ring buffer */
	kmemcpy(ptxb, frame, s);

	tx_ring[entry].length = -s; /* negative? */
	tx_ring[entry].misc = 0x00000000;
	tx_ring[entry].base = (dword) ptxb;

	/* we set the top byte as the very last thing */
	tx_ring[entry].status = status;
//kprintf("before %X %X \n",access->read_csr(io_address, 3),(short) tx_ring[entry].status);
	
	/* Trigger an immediate send poll */
	access->write_csr(io_address, 0, 0x0048);

//kprintf("sending...!\n");
	/* wait for transmit complete */
	//lp->cur_tx = 0;		/* (lp->cur_tx + 1); */

	//time = currticks() + TICKS_PER_SEC;	/* wait one second */
	while (//currticks() < time &&
	       (((short) tx_ring[entry].status) < 0)); /* weird wait, i suppose some irq alternative is possible, also time wait is disabled here, for now lets just hope the card respond timely (under pain of lockup) */
//kprintf("sent! %d\n",(access->read_csr(io_address,0)));
	
	//kprintf("%X %X %X \n",access->read_csr(io_address, 3),access->read_csr(io_address, 5), (short) tx_ring[entry].status);

	if ((short) tx_ring[entry].status < 0) /* shouldnt happen unless using timer */
		kprintf("PCNET32 timed out on transmit\n");
	//kprintf("csr3=%X\n",access->read_csr(io_address,3));
		/* Stop pointing at the current txb
		 * otherwise the card continues to send the packet */
		tx_ring[0].base = 0;

}

/* Initialize the PCNET32 Rx and Tx rings. */
static int pcnet32_init_ring() {
	int i;

	//lp->tx_full = 0;
	//lp->cur_rx = lp->cur_tx = 0;

	for (i = 0; i < RX_RING_SIZE; i++) {
		rx_ring[i].base = (dword) &rxb[i * PKT_BUF_SZ];
		rx_ring[i].buf_length = -PKT_BUF_SZ;
		rx_ring[i].status = 0x8000;
	}

	/* The Tx buffer address is filled in as needed, but we do need to clear
	   the upper ownership bit. */
	for (i = 0; i < TX_RING_SIZE; i++) {
		tx_ring[i].base = 0;
		tx_ring[i].status = 0;
	}


	init_block.tlen_rlen = TX_RING_LEN_BITS | RX_RING_LEN_BITS;
	for (i = 0; i < 6; i++)
		init_block.phys_addr[i] = mac_addr[i];
	init_block.rx_ring = (dword) &rx_ring[0];
	init_block.tx_ring = (dword) &tx_ring[0];
	return 0;
}

/**************************************************************************
RESET - Reset adapter
***************************************************************************/
static void pcnet32_reset() {
	/* put the card in its initial state */
	word val;
	int i;

	/* Reset the PCNET32 */
	access->reset(io_address);

	/* switch pcnet32 to 32bit mode */
	access->write_bcr(io_address, 20, 2);

	/* set/reset autoselect bit */
	val = access->read_bcr(io_address, 2) & ~2;
	access->write_bcr(io_address, 2, val);

	/* set/reset GPSI bit in test register */
	val = access->read_csr(io_address, 124) & ~0x10;
	access->write_csr(io_address, 124, val);
	init_block.mode = PCNET32_PORT_ASEL & PCNET32_PORT_PORTSEL << 7;
	init_block.filter[0] = 0xffffffff;
	init_block.filter[1] = 0xffffffff;
	pcnet32_init_ring();

	/* Re-initialize the PCNET32, and start it when done. */
	access->write_csr(io_address, 1, ((dword)&init_block) & 0xffff);
	access->write_csr(io_address, 2, ((dword)&init_block) >> 16);
	access->write_csr(io_address, 4, 0x0915);
	
	access->write_csr(io_address, 0, 0x0041);

	i = 0;
	while (i++ < 100)
		if (access->read_csr(io_address, 0) & 0x0100)
			break;
	//kprintf("csr0 %X, csr15 %X.\n", access->read_csr(io_address, 0), access->read_csr(io_address, 15));
	/* 
	 * We used to clear the InitDone bit, 0x0100, here but Mark Stockton
	 * reports that doing so triggers a bug in the '974.
	 */
	/* NOTE well, guess what, we set IDONE again now */

	//dword c15 = access->read_csr(io_address, 15 );
	//access->write_csr(io_address, 15, c15 & ~(0x02));
	access->write_csr(io_address, 0, 0x0142);

	kprintf("pcnet32 open, csr0 %X, csr15 %X.\n", access->read_csr(io_address, 0), access->read_csr(io_address, 15));

}

/**
 * Verify if a pci_device is a pcnet32 card and which version.
 * Parts of this code were taken from Etherboot source
 */

int probe_pcnet32_device(pci_device * pci_dev) {

	dword chip_version;
	char * chipname;
	int i, media;
	int fdx, mii, fset, dxsuflo, ltint;
	int shared = 1;

	io_address = pci_dev->io_address;
	
	if (io_address == 0)
		return -1;

	/* reset the chip */
	pcnet32_wio_reset(io_address);

	/* NOTE: 16-bit check is first, otherwise some older PCnet chips fail */
	//kprintf("bit = %d\n",pcnet32_wio_read_csr(io_address, 0));
	//kprintf("bit = %d\n",pcnet32_wio_check(io_address));
	if (pcnet32_wio_read_csr(io_address, 0) == 4
	    && pcnet32_wio_check(io_address)) {
		access = &pcnet32_wio;
	} else {
		pcnet32_dwio_reset(io_address);
		if (pcnet32_dwio_read_csr(io_address, 0) == 4
		    && pcnet32_dwio_check(io_address)) {
			access = &pcnet32_dwio;
		} else
			return 0;
	}

	chip_version = access->read_csr(io_address, 88) | (access->read_csr(io_address, 89) << 16);

	kprintf("PCnet chip version is 0x%X\n", chip_version);
	if ((chip_version & 0xfff) != 0x003)
		return 0;

	/* initialize variables */
	fdx = mii = fset = dxsuflo = ltint = 0;
	chip_version = (chip_version >> 12) & 0xffff;

	switch (chip_version) {
	case 0x2420:
		chipname = "PCnet/PCI 79C970";	/* PCI */
		break;
	case 0x2430:
		if (shared)
			chipname = "PCnet/PCI 79C970";	/* 970 gives the wrong chip id back */
		else
			chipname = "PCnet/32 79C965";	/* 486/VL bus */
		break;
	case 0x2621:
		chipname = "PCnet/PCI II 79C970A";	/* PCI */
		fdx = 1;
		break;
	case 0x2623:
		chipname = "PCnet/FAST 79C971";	/* PCI */
		fdx = 1;
		mii = 1;
		fset = 1;
		ltint = 1;
		break;
	case 0x2624:
		chipname = "PCnet/FAST+ 79C972";	/* PCI */
		fdx = 1;
		mii = 1;
		fset = 1;
		break;
	case 0x2625:
		chipname = "PCnet/FAST III 79C973";	/* PCI */
		fdx = 1;
		mii = 1;
		break;
	case 0x2626:
		chipname = "PCnet/Home 79C978";	/* PCI */
		fdx = 1;
		/* 
		 * This is based on specs published at www.amd.com.  This section
		 * assumes that a card with a 79C978 wants to go into 1Mb HomePNA
		 * mode.  The 79C978 can also go into standard ethernet, and there
		 * probably should be some sort of module option to select the
		 * mode by which the card should operate
		 */
		/* switch to home wiring mode */
		media = access->read_bcr(io_address, 49);

		kprintf("media reset to %#x.\n", media);
		access->write_bcr(io_address, 49, media);
		break;
	case 0x2627:
		chipname = "PCnet/FAST III 79C975";	/* PCI */
		fdx = 1;
		mii = 1;
		break;
	default:
		chipname = "UNKNOWN";
		kprintf("PCnet version %#x, no PCnet32 chip.\n",
		       chip_version);
		return -1;
	}

	kprintf("chipname = %s, devfn = %d\n",chipname,pci_dev->devfn);

	kprintf("on-board MAC address is: ");
	/* read MAC address */
	for (i = 0; i < 6; i++) {
		mac_addr[i] = inportb(io_address + i);
		kprintf("0x%X ",mac_addr[i]);
	}
	kprintf("\n");

	init_block.mode = 0x0003;	/* Disable Rx and Tx. */
	init_block.tlen_rlen = TX_RING_LEN_BITS | RX_RING_LEN_BITS;
	for (i = 0; i < 6; i++) {
		init_block.phys_addr[i] = mac_addr[i];
	}
	init_block.filter[0] = 0xFFFFFFFF;
	init_block.filter[1] = 0xFFFFFFFF;
	init_block.rx_ring = (dword) &rx_ring;
	init_block.tx_ring = (dword) &tx_ring;

	/* switch pcnet32 to 32bit mode */
	access->write_bcr(io_address, 20, 2);

	access->write_csr(io_address, 1, ((dword)(&init_block)) & 0xFFFF);
	access->write_csr(io_address, 2, ((dword)(&init_block)) >> 16);
//dword b;
//pci_bios_read_configuration_byte(24,0,0x3c,&b);
//kprintf("the IRQ = %X\n",b & 0xFF);
	pcnet32_reset();

	alloc_nic(_nic);
	_nic->MAC = mac_addr;
	_nic->send = (void (*) (void *,word)) pcnet32_transmit;
	_nic->interrupt = pcnet32_interrupt;
	//_nic->interrupt = pcnetirq;
	_nic->pci_irq = pci_dev->irq;
	_nic->name = chipname;
	attached_nic = _nic;
	kprintf("Adding NIC\n" );
	add_nic(_nic);
	return 0;
}



pci_driver pcnet32_driver = {
	{
	NULL,NULL
	},	
	/* TODO change this to accept any pcnet32 vendor and device IDs */
	AMD_PCI_VENDOR_ID,
	PCNET_LANCE_DEVICE_ID,
	probe_pcnet32_device,
	"PCNET 32 Driver"
};
