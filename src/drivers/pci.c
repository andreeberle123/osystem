#include "pci.h"
#include "i386/pci.h"
#include "stdio.h"
#include "mem.h"

#include "pci_drivers.h"

LIST_HEAD(pci_drivers);

/**
 * Register all pre-existant drivers in the pci_drivers list.
 * Currently uses pci_drivers.h to get the driver structures
 */
void pci_init_drivers() {
	/* Add PCNET 32 driver */
	list_add((struct list_head *)&pcnet32_driver,&pci_drivers);
	/* Add PIIX3 driver */
	list_add((struct list_head *)&piix3_driver,&pci_drivers);
}

extern dword pci_last_bus;

#define PCI_BAR(i) i = PCI_BAR_0 ; i <= PCI_BAR_5 ; i += 0x04

void pci_map_base_addresses(pci_device * dev) {
	dword reg, val;
	for (PCI_BAR(reg)) {
		pci_bios_read_configuration_dword( dev->devfn, dev->bus, reg, &val );
		if (val & PCI_ADDRESS_TYPE_MASK) {
			/* IO addressing space */
			/* IO spaces are always 32-bits wide (with flags) */
			dev->io_address = (val & PCI_ADDRESS_IO_BASE_MASK);
		}
		else {
			/* Memory addressing space */
			if (!(val & PCI_ADDRESS_MEM_BREADTH_MASK)) {
				/* we are ignoring the prefetch bit for now */
				dev->mem_space = (val & PCI_ADDRESS_MEM_PREFETCH_MASK);
			}
			else {
				if ((val & PCI_ADDRESS_MEM_BREADTH_MASK) == 0x02) {
					/* 64-bit wide, ignore and consume next 32 bits */
					reg += 0xFF;
				}
				/* Address is reserved, ignore*/
			}
		}
	}
}

void scan_drivers(pci_device *dev);

LIST_HEAD(pci_devices);

void scan_devices() {
	pci_device * pci_dev;
	dword bus, devfn, val;

	for (bus = 0 ; bus <= pci_last_bus ; bus++) {
		for (devfn = 0 ; devfn <= 0xFF ; devfn++) {
			
			/* This is an optimization over pci registers, since formal definition
			   declares two bytes for vendor id and two for device. We read 4 and
			   do some bitwise to split them up */
			if (pci_bios_read_configuration_dword( devfn, bus, PCI_VENDOR_ID, &val ))
				// TODO add err msg
				continue;
			/* It seems that some devices return 0xFFFF0000 or 0xFFFF when not
			   present */

			if ( ( val == 0xFFFFFFFF ) || ( val == 0 ) ||
				(val == 0xFFFF0000) || (val == 0xFFFF) )
				continue;
			
			//kprintf("val = %X\n",val);
			pci_dev = (pci_device *) kalloc(sizeof(pci_device));
			pci_dev->vendor_id = val & 0xFFFF;
			pci_dev->device_id = (val >> 16) & 0xFFFF;
			pci_dev->bus = (byte)bus;
			pci_dev->devfn = (byte)devfn;
			pci_bios_read_configuration_dword( devfn, bus, PCI_CLASS_REVISION, &val );
			pci_dev->class = val;
			pci_dev->driver = NULL;
			
			pci_bios_read_configuration_byte( devfn, bus, PCI_INTERRUPT_PIN, &val );
			//kprintf("val = %X ",val&0xFF);
			pci_bios_read_configuration_byte( devfn, bus, PCI_INTERRUPT_LINE, &val );
			//kprintf("val = %X ",val&0xFF);
			//pci_dev->irq = val & 0xFF;
			/* not sure how to associate interrupt lines with int pins, so lets use bios
			   probe to determine pins */
			/* INTA is a pin for single function devices, no support for multi function is enable yet */
			pci_dev->irq = pci_get_irq(devfn, PIN_INTA);

			pci_map_base_addresses(pci_dev);

			//unsigned int	new_command, pci_command;
		
			/*pci_bios_read_configuration_word(devfn, bus, PCI_COMMAND, &pci_command);
			new_command = pci_command | PCI_COMMAND_MASTER|PCI_COMMAND_IO;
			if (pci_command != new_command) {
				pci_bios_write_configuration_word(devfn, bus, PCI_COMMAND, &new_command);
			}*/
			
			//pci_bios_read_configuration_word( devfn, bus, PCI_COMMAND, &val );
			//word exp_cmd = (val | PCI_COMMAND_IO | PCI_COMMAND_MEM | PCI_COMMAND_MASTER);
			//kprintf("cmd = %d %d\n",val & 0xFFFF,exp_cmd);
			scan_drivers(pci_dev);
			
			kprintf("\tPCI Device found, vendor ID = %X, device ID = %X %s \n",pci_dev->vendor_id,pci_dev->device_id, (pci_dev->driver == NULL ? "-driver not found-" : pci_dev->driver->name));

			list_add((struct list_head *)pci_dev,&pci_devices);
		}
	}
}


void scan_drivers(pci_device *dev) {
	pci_driver * driver;
	struct list_head * iterator;
	list_for_each(iterator,&pci_drivers) {
		driver = (pci_driver *) iterator;
		/*
			TODO add support to PCI_ANY_ID and multiple driver IDs (...)
		*/
		if ((driver->vendor_id == dev->vendor_id) 
			&& (driver->device_id == dev->device_id)) {
			dev->driver = driver;
		}
	}
}

void pci_drivers_probe() {
	/* I am not sure if probing here is the best way to proceed. This function will
	   start all attached drivers probe functions and check if the driver accepts
	   the device. Future work will tell if this is an appropiate manner of doing
	   this */
	struct list_head * iterator;
	pci_device * dev;
	list_for_each(iterator,&pci_devices) {
		dev = (pci_device *) iterator;
		if (dev->driver != NULL && dev->driver->probe(dev)) {
			//fail
		}
	}

}

void pci_init() {
	pci_init_drivers();
	scan_devices();
	/* This call may be not adequate */
	pci_drivers_probe();

	// assume piix3 as the irq driver for now
	// TODO check if piix3 PIR is available, by sweeping devices list
	register_irq_driver(&piix3_irq_driver);
 
}
