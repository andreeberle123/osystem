/********************************************************
 * dorothy/drivers/nic.c
 *
 * Copyright (C) 2006 D-OZ Team
 *
 * NIC Network interface connector
 *
 * Replaces current network register mechanism by a 
 * full proxy nic structure
 *
 * TODO - currently uses only 1 nic at a time, allow choice
 *	for pograms
 * *****************************************************/

#include "nic.h"
#include "stdio.h"
#include "net/dlink/eth.h"

LIST_HEAD(available_nics);

nic * current_nic;

//void (*int_network)(void );

void int_network() {
	current_nic->interrupt();
}

void add_nic(nic * nic) {
	list_add((struct list_head *)nic,&available_nics);
//	int_network = nic->interrupt;
}

void list_adapters() {
	struct list_head * _nic;

	kprintf("Available Network Adapters:\n");
	int i = 0;
	list_for_each(_nic,&available_nics) {
		kprintf("\t%d - %s\n",i++,((nic *) _nic)->name);
	}
	kprintf("\n");
}

void init_nic() {
	/* set one adapter as the default, currently sets the first in the list */
	if ((&available_nics)->next != (&available_nics)) {
	    current_nic = (nic*) available_nics.next;
		// register the (currently only) ethernet interface with this NIC
	    eth_register_nic(current_nic);
	    kprintf("	NICs initialized. Using %s\n",current_nic->name);
	}

}
