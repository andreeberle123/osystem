/*******************************************************************************
 
  dorothy/fs/ddfs/ddfs.c

  Copyriht (C) 2005 D-OZ Team

  Dorothy Distributed File System

  OBS: Under test

*******************************************************************************/
/*	TODO
*/
#include "tfs.h"
#include "string.h"
#include "stdio.h"
#include "mem.h"
#include "floppy.h"
#include "timer.h"
#include "ddfs.h"
#include "net/dnp.h"

char *ddfs_ls(char *path)
{
	char *local;
	dnp_packet *p;
	
	ddfs_make_request (DDFS_LS, path);
	delay (DDFS_DELAY);
	do {
		p = dnp_get_message (DDFS_CPORT);
		if (p) {
			byte *content = (byte *) p + sizeof (struct dnp_header);
			kprintf ("%s\n", content);
			kfree (p);
		}
	} while (p);

	
	local = tfs_ls (path);
	if (local) kprintf ("%s\n\n", local);

	return NULL;
}

bool ddfs_chdir (char *path) {
	bool exists;
	dnp_packet *p;
	char *local;
	char nome[200];

	kstrcpy (nome, pwdtfs());
	//modifiquei aqui ************************************
	if (kstrlen(nome) != 1)
		kstrcpy (nome + kstrlen(nome), "/");
	kstrcpy (nome + kstrlen(nome), path);
	//kprintf ("chdir to %s\n", nome);
	
	local = tfs_ls (nome);
	if (local)
		exists = true;
	else
		exists = false;
	
	if (!exists) {
		ddfs_make_request (DDFS_CHDIR, nome);
		delay (DDFS_DELAY);
		exists = false;
		do {
			p = dnp_get_message (DDFS_CPORT);
			if (p) {
				byte *content = (byte *) p + sizeof (struct dnp_header);
				if (kstrcmp (content, "OK") == 0)
					exists = true;
				kfree (p);
			}
		} while (p);
	}

	if (exists)
		kstrcpy (pwdtfs(), nome);

	return exists;
}

bool ddfs_rmdir (char *path) {
	bool ok;
	dnp_packet *p;
	unsigned long sector;
	char nome[200];

	kstrcpy (nome, pwdtfs());
	kstrcpy (nome + kstrlen(nome), "/");
	kstrcpy (nome + kstrlen(nome), path);
	//kprintf ("rmdir %s\n", nome);

	/* we have this directory and it is not empty: can't erase */
	sector = tfs_pos (nome);
	if (sector != TFS_SEC_ERROR)
		if (!chk_empty_dir (sector, true))
			return false;

	/* ask if any machine is against erasing this directory */
	ddfs_make_request (DDFS_RRMDIR, nome);
				ok = false;
	delay (DDFS_DELAY);
	ok = true;
	do {
		p = dnp_get_message (DDFS_CPORT);
		if (p) {
			byte *content = (byte *) p + sizeof (struct dnp_header);
			if (kstrcmp (content, "NO") == 0)
				ok = false;
			kfree (p);
		}
	} while (p);

	/* somebody doesn't want it. can't erase */
	if (!ok) return false;

	/* everything is ok. erase, people, erase! */
	ddfs_make_request (DDFS_CRMDIR, nome);

	/* erase our copy, if any */
	if (sector != TFS_SEC_ERROR)
		tfsrmdir(path);

	return true;
}

bool ddfs_mkdir (char *path) {
	bool ok;
	dnp_packet *p;
	unsigned long sector;
	char nome[200];
	byte Addr[2], *content;

	kstrcpy (nome, pwdtfs());
	/* we have this directory and it is not empty: can't erase */
	sector = tfs_pos (nome);
	if (sector != TFS_SEC_ERROR)
	{
		tfsmkdir(path);
		//kprintf ("mkdir %s\n", nome);
		return true;
	}
	
	kstrcpy (nome + kstrlen(nome), "/");
	kstrcpy (nome + kstrlen(nome), path);
	//kprintf ("mkdir %s\n", nome);

	/* ask if any machine is against erasing this directory */
	ddfs_make_request (DDFS_RMKDIR, nome);
	delay (DDFS_DELAY);
	ok = false;
	do {
		p = dnp_get_message (DDFS_CPORT);
		if (p) {
			byte *content = (byte *) p + sizeof (struct dnp_header);
			if (kstrcmp (content, "OK") == 0) {
				Addr[0] = p->header.sdna[0];
				Addr[1] = p->header.sdna[1];
				ok = true;
				break;
			}
			kfree (p);
		}
	} while (p);

	/* nobody wants to create it. aborting... :( */
	if (!ok) return false;

	/* everything is ok. create now! */
	//ddfs_make_request (DDFS_CMKDIR, nome);
	content = (byte *) kalloc (sizeof(byte) * (4+kstrlen(nome)));
	content[0] = DDFS_CMKDIR;
	content[1] = DDFS_STRING;
	content[2] = '\0';
	kstrcpy (&content[3], nome);
	content[3 + kstrlen(&content[3])] = '\0';

	p = dnp_build_message (dnp_get_DNA(), DDFS_CPORT,
			       Addr, DDFS_SPORT,
			       content, 4 + kstrlen(nome));
	dnp_send_eth_message (p);
	//kprintf("Maquina %d.%d pode criar!\n", Addr[0], Addr[1]);
	kfree(p);
	kfree(content);
	
	return true;
}
