/*******************************************************************************
 
  dorothy/fs/ddfs/stub.c

  Copyriht (C) 2005 D-OZ Team

  Dorothy Distributed File System Server STUB

*******************************************************************************/
#include "tfs.h"
#include "string.h"
#include "stdio.h"
#include "mem.h"
#include "floppy.h"
#include "timer.h"
#include "ddfs.h"
#include "net/dnp.h"
#include "defines.h"
#include "8259a.h"

extern byte _DNA_bcast[2];

void ddfs_stub_start () {
	dnp_register_mailbox (DDFS_SPORT, ddfs_handle_request);
	dnp_register_mailbox (DDFS_CPORT, NULL);
}

void ddfs_handle_request (dnp_packet * p) {
	byte *content = (byte *) p + sizeof (struct dnp_header);
	/* in this stage just functions with ONE string parameter
	 * may be used. they all start in content[4] */
	
	switch (content[0]) {
		/* ls starting at 4 is just a neat hack. fix it XD~ */
		case DDFS_LS: ddfs_stub_ls (p, &content[3]);
			      break;
		case DDFS_CHDIR: ddfs_stub_chdir (p, &content[3]);
			      	 break;
		case DDFS_RMKDIR: ddfs_stub_request_mkdir (p, &content[3]);
			      	 break;
		case DDFS_CMKDIR: ddfs_stub_commit_mkdir (p, &content[3]);
			      	 break;
		case DDFS_RRMDIR: ddfs_stub_request_rmdir (p, &content[3]);
			      	 break;
		case DDFS_CRMDIR: ddfs_stub_commit_rmdir (p, &content[3]);
			      	 break;
	}
}

void ddfs_make_request (byte request, char *path) {
	byte *content = kalloc (sizeof (byte) * (4 + kstrlen(path)));
	dnp_packet *p;

	content[0] = request;
	content[1] = DDFS_STRING;
	content[2] = '\0';
	kstrcpy (&content[3], path);
	content[3 + kstrlen(&content[3])] = '\0';

	//kprintf ("%d.%d\n", _DNA_bcast[0], _DNA_bcast[1]);
	p = dnp_build_message (dnp_get_DNA(), DDFS_CPORT,
			       _DNA_bcast, DDFS_SPORT,
			       content, 4 + kstrlen(path));
	dnp_send_eth_message (p);
	kfree (p);
	kfree (content);
}

void ddfs_stub_ls (dnp_packet *p, char *path) {
	char *list;
	dnp_packet *r;
	
	//kprintf ("stub ls: %s\n", path);
	list = tfs_ls (path);
	/* we have this directory in our disk */
	if (list != NULL) {
		//kprintf ("directory exists: answering.\n");
		r = dnp_build_message (dnp_get_DNA(), DDFS_SPORT,
				       p->header.sdna, p->header.sport,
				       list, 1+kstrlen (list));
		dnp_send_eth_message (r);
		kfree (r);
		kfree (list);
	}
}

void ddfs_stub_chdir (dnp_packet *p, char *path) {
	dnp_packet *r;

	//kprintf ("stub chdir: %s\n", path);
	/* we have this directory in our disk */
	if (tfs_ls(path) != NULL) {
		//kprintf ("directory exists: answering.\n");
		r = dnp_build_message (dnp_get_DNA(), DDFS_SPORT,
				       p->header.sdna, p->header.sport,
				       "OK\0", 3);
		dnp_send_eth_message (r);
		kfree (r);
	}
}

void ddfs_stub_request_mkdir (dnp_packet *p, char *path) {
	dnp_packet *r;
	int i;
	unsigned long sector;
	char tmpPath[500];
	//kprintf ("stub request mkdir: %s\n", path);

	for(i=kstrlen(path); ((i >= 0) && (path[i]!='/')); i--);
//	kstrcpy(dir, &path[i + 1]);
	kstrncpy (tmpPath, path, i);	

	sector = tfs_pos(tmpPath);
	if (sector == TFS_SEC_ERROR){
		return;
	}
	r = dnp_build_message (dnp_get_DNA(), DDFS_SPORT,
			       p->header.sdna, p->header.sport,
			       "OK\0", 3);
	dnp_send_eth_message (r);
	//kprints("\nEnviando ok!\n");
	kfree (r);
}

void ddfs_stub_commit_mkdir (dnp_packet *p, char *path) {
	char dir[43], tmpPath[500];
	int i, maxdesc;
	unsigned long sector, empty;
	tfsfat *tmpFAT;
	
	//kprintf ("stub commit mkdir: %s\n", path);
	
	for(i=kstrlen(path); ((i >= 0) && (path[i]!='/')); i--);
	kstrcpy(dir, &path[i + 1]);
	kstrncpy (tmpPath, path, i);	

	sector = tfs_pos(tmpPath);
	if (sector == TFS_SEC_ERROR){
		/* Not found! Maybe wrong guy*/
		//kprints("\nErro! nao posso criar!\n");
		return;
	}
	if (sector == FATSTART) 
	{
		tmpFAT = get_tfsfat();
		maxdesc = NUMDESC;
	} else {
		tmpFAT = (tfsfat *) kalloc (512);
		read_block(sector, (byte *) tmpFAT, 1);
		maxdesc = 10;
	}
	for (i=2; i<maxdesc; i++)
	{
		if (tmpFAT[i].attrib == T_EMPTY)
		{
			empty = getemptysec();
			if (empty == 100000)
				/* Not enough FAT space!*/
				return;
			else {
				kstrcpy((char *) tmpFAT[i].name, dir);
				tmpFAT[i].attrib = T_DIR;
				tmpFAT[i].ptr[3] = (empty & 0x000000FF);
				tmpFAT[i].ptr[2] = (empty & 0x0000FF00) >> 8;
				tmpFAT[i].ptr[1] = (empty & 0x00FF0000) >> 16;
				tmpFAT[i].ptr[0] = (empty & 0xFF000000) >> 24;
			}
			break;
		}
	}
	setsec(empty);
	if (sector != FATSTART)
	{
		write_block(sector, (byte *) tmpFAT, 1);
		kfree(tmpFAT);
	}
}

void ddfs_stub_request_rmdir (dnp_packet *p, char *path) {
	dnp_packet *r;
	unsigned long sector;

	//kprintf ("stub request rmdir: %s\n", path);

	sector = tfs_pos (path);
	if (sector == TFS_SEC_ERROR) return;
	if (chk_empty_dir (sector, true)) {
		//kprintf ("directory exists and is empty: answering. sector = %d\n", sector);
		r = dnp_build_message (dnp_get_DNA(), DDFS_SPORT,
				       p->header.sdna, p->header.sport,
				       "OK\0", 3);
		dnp_send_eth_message (r);
		kfree (r);
	} else {
		//kprintf ("directory exists and is not empty: answering. sector = %d\n", sector);
		r = dnp_build_message (dnp_get_DNA(), DDFS_SPORT,
				       p->header.sdna, p->header.sport,
				       "NO\0", 3);
		dnp_send_eth_message (r);
		kfree (r);
	}
}

void ddfs_stub_commit_rmdir (dnp_packet *p, char *path) {
	char dir[43], tmpPath[500];
	int i, maxdesc;
	unsigned long sector, sec, sec0, sec1, sec2, sec3;
	tfsfat *tmpFAT;
	
	//kprintf ("stub commit rmdir: %s\n", path);
	
	for(i=kstrlen(path); ((i >= 0) && (path[i]!='/')); i--);
	kstrcpy(dir, &path[i + 1]);
	kstrncpy (tmpPath, path, i);	

	sector = tfs_pos(path);
	if (sector == TFS_SEC_ERROR){
		//kprints("nao tenho!\n");
		return;
	}
	
	sector = tfs_pos(tmpPath);

	if (sector == TFS_SEC_ERROR){
		//kprints("nao tenho!\n");
		return;
	}

	if (sector == FATSTART) 
	{
		tmpFAT = get_tfsfat();
		maxdesc = NUMDESC;
	} else {
		tmpFAT = (tfsfat *) kalloc (512);
		read_block(sector, (byte *) tmpFAT, 1);
		maxdesc = 10;
	}
	
	for (i=2; i<maxdesc; i++)
	{
		if ((kstrcmp((char *) dir, (char *) tmpFAT[i].name) == 0) && (tmpFAT[i].attrib != T_EMPTY))
		{
			sec3 = (tmpFAT[i].ptr[3]);
			sec2 = (tmpFAT[i].ptr[2] << 8);
			sec1 = (tmpFAT[i].ptr[1] << 16);
			sec0 = (tmpFAT[i].ptr[0] << 24);
			sec = sec1+sec2+sec3+sec0;
			break;
		}
	}
	if (i != maxdesc)
	{
		rsetsec(sec);
	//	kprintf("Removido! - setor = %d\n", sec);
		tmpFAT[i].attrib = T_EMPTY;
		if (sector != FATSTART)
		{
			write_block(sector, (byte *) tmpFAT, 1);
			kfree(tmpFAT);
		}
	}
}
