/*******************************************************************************
 
  dorothy/fs/tfs/tfs.c

  Copyriht (C) 2005 D-OZ Team

  Test File System

  OBS: Under test

*******************************************************************************/
/*	TODO
 *	Criacao de arquivos
 *	Edicao de arquivos
 *	Fechamento de arquivos
 *	Copia de arquivos
 *	Renomeacao de arquivos
 *	Remocao de arquivos
*/
#include "tfs.h"
#include "string.h"
#include "stdio.h"
#include "mem.h"
#include "floppy.h"
#include "timer.h"

static tfsfat volatile *tfatbuffer = NULL; 	// FAT do disquete
static FILE volatile *FIDList = NULL;		// Lista de arquivos abertos
static byte volatile *tfsBM = NULL;		// Bitmap da FAT
static unsigned long volatile Path = PATH_ROOT;	// Caminho
static char volatile *strPath = NULL;

bool init_tfs(void)
{
	int i;
	if (load_tfs() == false)
		return false;
	if (read_tfat() == false)
		return false;
	strPath = (char *) kalloc (sizeof(char) * 5000);
	strPath[0]='/';
	for(i=1;i<5000;i++)
		strPath[0] = '\0';
	return true;
}

bool load_tfs(void)
{
	tfatbuffer = (tfsfat *) kalloc(sizeof(byte) * TAMFAT * 512);
	if (tfatbuffer == NULL)
		return false;
	tfsBM = (byte *) kalloc (sizeof(byte) * TAMBM);
	if (tfsBM == NULL)
		return false;
	FIDList = (FILE *) kalloc (sizeof(FILE) * 100);
	if (FIDList == NULL)
		return false;
	return true;
}

bool read_tfat()
{
	bool ret = false;
	
	ret = read_block(FATSTART, (byte *) tfatbuffer, TAMFAT);
	if (ret == false)
		return false;
	ret = read_block(POSBM, (byte *) tfsBM, 1);
	if (ret == false)
		return false;

	return true;
	
}

bool fdformat_tfs()
{
	int i;
	
	// Inicializa os diretorios '.' e '..'
	// e limpa o espaço restante
	
	kstrcpy((char *)tfatbuffer[0].name, ".");
	tfatbuffer[0].attrib = T_DIR;
	tfatbuffer[0].ptr[3] = (PATH_ROOT & 0x000000FF);
	tfatbuffer[0].ptr[2] = (PATH_ROOT & 0x0000FF00) >> 8;
	tfatbuffer[0].ptr[1] = (PATH_ROOT & 0x00FF0000) >> 16;
	tfatbuffer[0].ptr[0] = (PATH_ROOT & 0xFF000000) >> 24;
	
	kstrcpy((char *)tfatbuffer[1].name, "..");
	tfatbuffer[1].attrib = T_DIR;
	tfatbuffer[1].ptr[3] = (PATH_ROOT & 0x000000FF);
	tfatbuffer[1].ptr[2] = (PATH_ROOT & 0x0000FF00) >> 8;
	tfatbuffer[1].ptr[1] = (PATH_ROOT & 0x00FF0000) >> 16;
	tfatbuffer[1].ptr[0] = (PATH_ROOT & 0xFF000000) >> 24;
		
	for(i=2;i<NUMDESC;i++)
	{
		tfatbuffer[i].name[0]='\0';
		tfatbuffer[i].attrib = T_EMPTY;
		tfatbuffer[i].ptr[0] = 0;
		tfatbuffer[i].ptr[1] = 0;
		tfatbuffer[i].ptr[2] = 0;
		tfatbuffer[i].ptr[3] = 0;
	}

	// Inicializa o 'Path' e zera a tabela de bits
	// de alocacao
	
	for(i=0;i<TAMBM; i++)
		tfsBM[i] = 0;
	Path = PATH_ROOT;
	kstrcpy((char *)&strPath, "/\0");
	return (write_tfat());
}

bool write_tfat()
{
	bool ret = false;
	
	ret = write_block(FATSTART, (byte *) tfatbuffer, TAMFAT);
	if (ret == false) 
		return false;
	ret = write_block(POSBM, (byte *) tfsBM, 1);
	if (ret == false)
		return false;

	return true;
}


unsigned long tfs_pos(char *path)
{
	char tmpPath[43];
	int start = 1, i = 1, j, maxdesc = NUMDESC, desc=0, compara;
	unsigned long sec, sec0, sec1, sec2, sec3;
	tfsfat *tmpFAT = NULL;

	sec = FATSTART;
	while (i < kstrlen(path)) {

		for(i=start, j=0; (path[i] != '/') && (i < kstrlen(path)); i++, j++)
			tmpPath[j] = path[i];

		tmpPath[j] = '\0';
		start += kstrlen(tmpPath)+1;
		
		/* Check disk for directory */	
		if (tmpFAT == NULL)
			tmpFAT = get_tfsfat();
		else {
			tmpFAT = (tfsfat *) kalloc (512);
			read_block(sec, (byte *) tmpFAT, 1);
			maxdesc = 10;
		}

		for (desc=0; desc < maxdesc; desc++)
		{
			compara = kstrcmp((char *) tmpPath, (char *) tmpFAT[desc].name);
			if ((compara == 0) && (tmpFAT[desc].attrib == T_DIR))
			{
				sec3 = (tmpFAT[desc].ptr[3]);
				sec2 = (tmpFAT[desc].ptr[2] << 8);
				sec1 = (tmpFAT[desc].ptr[1] << 16);
				sec0 = (tmpFAT[desc].ptr[0] << 24);
				sec = sec1+sec2+sec3+sec0;
				sec += POSBM +1; 
				desc = maxdesc+1;
			} 
		}
//		if (tmpFAT != FATSTART) //Pode conter erros!
//			kfree(tmpFAT);
		if (desc == maxdesc)
			return TFS_SEC_ERROR;
		
	}
	return sec;
}

bool tfsmkdir(char *Name)
{
	int desc, maxdesc;
	unsigned long empty;
	tfsfat *tmpFAT;
	
	if (Path == PATH_ROOT)
	{
		tmpFAT = (tfsfat *)tfatbuffer;
		maxdesc = NUMDESC;
	}
	else
	{
		tmpFAT = (tfsfat *) kalloc(sizeof(byte) * 512);
		read_block(Path, (byte *) tmpFAT, 1);
		maxdesc = 10;
	}
	for (desc=0; desc<maxdesc; desc++)
		if (tmpFAT[desc].attrib == T_EMPTY)
		{
			empty = getemptysec();
			if (empty == TFS_SEC_ERROR)
				return false;
			else
			{
				kstrcpy((char *)tmpFAT[desc].name, Name);
				tmpFAT[desc].attrib = T_DIR;
				tmpFAT[desc].ptr[3] = (empty & 0x000000FF);
				tmpFAT[desc].ptr[2] = (empty & 0x0000FF00) >> 8;
				tmpFAT[desc].ptr[1] = (empty & 0x00FF0000) >> 16;
				tmpFAT[desc].ptr[0] = (empty & 0xFF000000) >> 24;
			}	
			desc=maxdesc+1;
		}
	setsec(empty);
	if (maxdesc == 10) 
	{
		write_block(Path, (byte *)tmpFAT, 1);
		kfree(tmpFAT);
	}
	if (desc == maxdesc) 
	{
		kprints("\nError! Disk Full!\n");
		return false;
	}
	init_dir(empty);
	return true;
}

bool tfsrmdir(char *Name)
{
	int desc, compara = 0, maxdesc, apaga;
	unsigned long sec3 = 0, sec2 = 0, sec1 = 0, sec0 = 0, sec = 0;
	tfsfat *tmpFAT;
	
	if (Path == PATH_ROOT)
	{
		tmpFAT = (tfsfat *)tfatbuffer;
		maxdesc = NUMDESC;
	}
	else
	{
		tmpFAT = (tfsfat *) kalloc(sizeof(byte) * 512);
		read_block(Path, (byte *) tmpFAT, 1);
		maxdesc = 10;
	}
	
	for (desc=0; desc<maxdesc; desc++)
	{
		compara = kstrcmp((char *) Name, (char *) tmpFAT[desc].name);
		if (compara == 0)
		{
			sec3 = (tmpFAT[desc].ptr[3]);
			sec2 = (tmpFAT[desc].ptr[2] << 8);
			sec1 = (tmpFAT[desc].ptr[1] << 16);
			sec0 = (tmpFAT[desc].ptr[0] << 24);
			sec = sec1+sec2+sec3+sec0;
			apaga = desc;
			desc = maxdesc+1;
		}
	}
	if (desc == maxdesc)
	{
		kprints("\nError! Directory not Found!\n");
		return false;
	}
	
	if (chk_empty_dir(sec, false) == true)
	{
		rsetsec(sec);
		tmpFAT[apaga].attrib = T_EMPTY;
		if (Path != PATH_ROOT)
		{
			write_block(Path, (byte *) tmpFAT, 1);
			kfree(tmpFAT);
		}
	}
	else
		kprints("\nDirectory not empty!\n\n");
	return true;
}

bool tfschdir(char *Name)
  {
    	int desc, maxdesc, compara, i, pathlen = 0;
	unsigned long sec3 = 0, sec2 = 0, sec1 = 0, sec0 = 0, sec = 0;
	tfsfat *tmpFAT;
	
	if (Path == PATH_ROOT)
	{
		tmpFAT = (tfsfat *)tfatbuffer;
		maxdesc = NUMDESC;
	}
	else
	{
		tmpFAT = (tfsfat *) kalloc(sizeof(byte) * 512);
		read_block(Path, (byte *) tmpFAT, 1);
		maxdesc = 10;
	}
	
	for (desc=0; desc<maxdesc; desc++)
	{
		if (kstrcmp((char *) Name, "/") == 0) {
			Path = PATH_ROOT;
			strPath[0]='/';
			strPath[1]='\0';
			return true;
		}	
		compara = kstrcmp((char *) Name, (char *) tmpFAT[desc].name);
		if (compara == 0 && tmpFAT[desc].attrib == T_DIR)
		{
			sec3 = (tmpFAT[desc].ptr[3]);
			sec2 = (tmpFAT[desc].ptr[2] << 8);
			sec1 = (tmpFAT[desc].ptr[1] << 16);
			sec0 = (tmpFAT[desc].ptr[0] << 24);
			sec = sec1+sec2+sec3+sec0;
			desc = maxdesc+1;
			if ((kstrcmp((char *)Name, "..") != 0) && (kstrcmp((char *) Name, ".") != 0)) {
				pathlen = kstrlen((char *)strPath);
				if (kstrlen((char *)strPath) != 1){
					strPath[pathlen] = '/';
					strPath[pathlen+1] = '\0';
				}
				pathlen = kstrlen((char *)strPath);
				kstrcpy((char *) &strPath[pathlen], Name);
			}
		}
	}
	if (Path != PATH_ROOT)
		kfree(tmpFAT);
	if (desc == maxdesc)
	{
		kprints("\nError! Directory not Found!\n");
		return false;
	}
	if (kstrcmp((char *)Name, "..") == 0) {
		Path = sec;
		for(i=kstrlen((char *)strPath); (strPath[i] != '/') & (i != 1); i--)
			strPath[i]='\0';
		strPath[i]='\0';
	}
	else if (kstrcmp((char *)Name, ".") == 0)
		Path = Path;
	else		
	{
		if (sec != TFS_SEC_ERROR)
			Path = sec + POSBM + 1;
		else
			Path = PATH_ROOT;
	}
	return true;
}

char *pwdtfs(void)
{
	return (char *)strPath;
}

unsigned long getemptysec(void)
{
	int i;
	unsigned long atual = 0;
		
	for (i=0; i<TAMBM; i++, atual += 8)
	{
		if ((tfsBM[i] & 1) == 0)
			return (atual);
		else if ((tfsBM[i] & 2) == 0)
			return (atual + 1);
		else if ((tfsBM[i] & 4) == 0)
			return (atual + 2);
		else if ((tfsBM[i] & 8) == 0)
			return (atual + 3);
		else if ((tfsBM[i] & 16) == 0)
			return (atual + 4);
		else if ((tfsBM[i] & 32) == 0)
			return (atual + 5);
		else if ((tfsBM[i] & 64) == 0)
			return (atual + 6);
		else if ((tfsBM[i] & 128) == 0)
			return (atual + 7);
	}
	
	return TFS_SEC_ERROR;
}

void setsec(unsigned long sec)
{
	int i;

	i = sec / 8;
	if ((sec % 8) == 0)
		tfsBM[i] = tfsBM[i] | 1;
	else if ((sec % 8) == 1)
		tfsBM[i] = tfsBM[i] | 2;
	else if ((sec % 8) == 2)
		tfsBM[i] = tfsBM[i] | 4;
	else if ((sec % 8) == 3)
		tfsBM[i] = tfsBM[i] | 8;
	else if ((sec % 8) == 4)
		tfsBM[i] = tfsBM[i] | 16;
	else if ((sec % 8) == 5)
		tfsBM[i] = tfsBM[i] | 32;
	else if ((sec % 8) == 6)
		tfsBM[i] = tfsBM[i] | 64;
	else if ((sec % 8) == 7)
		tfsBM[i] = tfsBM[i] | 128;
}

void rsetsec(unsigned long sec)
{
	int i;

	i = sec / 8;
	if ((sec % 8) == 0)
		tfsBM[i] = tfsBM[i] & 254;
	else if ((sec % 8) == 1)
		tfsBM[i] = tfsBM[i] & 253;
	else if ((sec % 8) == 2)
		tfsBM[i] = tfsBM[i] & 251;
	else if ((sec % 8) == 3)
		tfsBM[i] = tfsBM[i] & 247;
	else if ((sec % 8) == 4)
		tfsBM[i] = tfsBM[i] & 239;
	else if ((sec % 8) == 5)
		tfsBM[i] = tfsBM[i] & 223;
	else if ((sec % 8) == 6)
		tfsBM[i] = tfsBM[i] & 191;
	else if ((sec % 8) == 7)
		tfsBM[i] = tfsBM[i] & 127;
}

char *tfs_ls(char *path)
{
	unsigned long sector;
	int i, j=0, maxdesc;
	tfsfat *tmpFAT = NULL;
	char *ret;
	
	sector = tfs_pos(path);
	if (sector == TFS_SEC_ERROR)
		//kprintf("ERRO: Sector = %d", sector);
		return NULL;
	else if (sector == FATSTART) {
		tmpFAT = get_tfsfat();
		maxdesc = NUMDESC;
	} else {
		tmpFAT = (tfsfat *) kalloc (512);
		read_block(sector, (byte *) tmpFAT, 1);	
		maxdesc = 10;
	}

	ret = (char *) kalloc (maxdesc * 48 + 1);
	j=0;
	for (i=0; i < maxdesc; i++)
	{
		if (tmpFAT[i].attrib == T_DIR)
		{
			kstrcpy(&ret[j], "<DIR> ");
			j+=6;
			kstrcpy(&ret[j], tmpFAT[i].name);
			j+=kstrlen(tmpFAT[i].name);
			ret[j++] ='\n';
		}
		else if (tmpFAT[i].attrib == T_ARQ)
		{
			kstrcpy(&ret[j], "<FLE> ");
			j+=6;
			kstrcpy(&ret[j], tmpFAT[i].name);
			j+=kstrlen(tmpFAT[i].name);
			ret[j++] ='\n';
		}
		else if (tmpFAT[i].attrib == T_LNK)
		{
			kstrcpy(&ret[j], "<LNK> ");
			j+=6;
			kstrcpy(&ret[j], tmpFAT[i].name);
			j+=kstrlen(tmpFAT[i].name);
			ret[j++] ='\n';
		}
	}

	ret[j]='\0';
	return ret;	
}

void init_dir (unsigned long sector)
{
	tfsfat sec[11];
	int i;
	sector += POSBM + 1;
	
	kstrcpy((char *)sec[0].name, ".");
	sec[0].attrib = T_DIR;
	sec[0].ptr[3] = (sector & 0x000000FF);
	sec[0].ptr[2] = (sector & 0x0000FF00) >> 8;
	sec[0].ptr[1] = (sector & 0x00FF0000) >> 16;
	sec[0].ptr[0] = (sector & 0xFF000000) >> 24;
	
	kstrcpy((char *)sec[1].name, "..");
	sec[1].attrib = T_DIR;
	sec[1].ptr[3] = (Path & 0x000000FF);
	sec[1].ptr[2] = (Path & 0x0000FF00) >> 8;
	sec[1].ptr[1] = (Path & 0x00FF0000) >> 16;
	sec[1].ptr[0] = (Path & 0xFF000000) >> 24;

	for (i=2; i<10; i++)
	{
		sec[i].name[0] = '\0';
		sec[i].attrib = T_EMPTY;
		sec[i].ptr[3] = 0;
		sec[i].ptr[2] = 0;
		sec[i].ptr[1] = 0;
		sec[i].ptr[0] = 0;
	}
	write_block(sector, (byte *) sec, 1);

}

bool chk_empty_dir(unsigned long sector, bool remote)
{
	tfsfat sec[11];
	int vazio = 0, i;
	
	if (!remote){
		if (sector != TFS_SEC_ERROR && Path != sector)
			sector += POSBM + 1;
		else 
			return false;
	}
	
	read_block(sector, (byte*)sec, 1);
	
	for (i=2; i<10; i++)
		if (sec[i].attrib == T_EMPTY) 
			vazio++;

	if (vazio == 8) 
		return true;
	return false;
}

/*void write_buffer(void)
{
	//write_tfat();
	kprints("Trigger");
	addTrigger(write_buffer, 100);
}
*/
tfsfat *get_tfsfat(void)
{

	if (tfatbuffer == NULL) {

        	if (load_tfs() == false)
		        return NULL;
	        if (read_tfat() == false)
			return NULL;
	}
	
	return (tfsfat *)tfatbuffer;
}

/* Jaziel */
unsigned long tfs_get_first_sector(char *name, unsigned long *fid)
{
	int desc=0, compara;
	unsigned long sec, sec0, sec1, sec2, sec3;
	tfsfat *tmpFAT = NULL;

	/* Check disk for directory */	
	tmpFAT = get_tfsfat();

	sec = FATSTART;

	for (desc=0; desc < NUMDESC; desc++)
	{
		compara = kstrcmp((char *) name, (char *) tmpFAT[desc].name);
		if ((compara == 0) && (tmpFAT[desc].attrib == T_ARQ))
		{
			sec3 = (tmpFAT[desc].ptr[3]);		// quarto byte mais significativo que aponta para o setor
			sec2 = (tmpFAT[desc].ptr[2] << 8);	// terceiro byte mais significativo que aponta para o setor
			sec1 = (tmpFAT[desc].ptr[1] << 16);	// segundo byte mais significativo que aponta para o setor
			sec0 = (tmpFAT[desc].ptr[0] << 24); 	// primeiro byte mais significativo que aponta para o setor

			// remontando o ponteiro para setores
			sec = sec0+sec1+sec2+sec3;

                        // soma o offset do sec em rela��o ao in�cio da regi�o de dados
			// + o header
			sec += POSBM +1; 
			desc = NUMDESC+1;
		} 
	}

	if (desc == NUMDESC)
		return TFS_SEC_ERROR;

	*fid = desc;

	return sec;
}

FILE *tfs_fopen(char *filename, byte attr) {
	FILE *fp = NULL;
	
	fp = (FILE *) kalloc(sizeof(FILE));

	fp->secnumber = tfs_get_first_sector(filename, &fp->FID);
	fp->nsector = NOSECTOR;
	fp->psector = NOSECTOR;
	fp->type = attr;
	fp->offset = 0;

	return fp;
}

int tfs_fseek(FILE *fp, long offset, int whence) {
	unsigned long sector,sec0,sec1,sec2,sec3; /*descobir e acertar o erro do passagem de parametros para a fun��o de chamada do read_block*/
	tfsfat *tmpFAT = NULL;
	
		
	switch (whence) {
		case SEEK_SET: {
		        tmpFAT = get_tfsfat();
				    
		        sec3 = (tmpFAT[fp->FID].ptr[3]);	// quarto byte mais significativo que aponta para o setor
			sec2 = (tmpFAT[fp->FID].ptr[2] << 8);	// terceiro byte mais significativo que aponta para o setor
			sec1 = (tmpFAT[fp->FID].ptr[1] << 16);	// segundo byte mais significativo que aponta para o setor
			sec0 = (tmpFAT[fp->FID].ptr[0] << 24); 	// primeiro byte mais significativo que aponta para o setor

			// remontando o ponteiro para setores
			sector = sec1+sec2+sec3+sec0;

  	        	read_block(sector, fp->secbuffer, 1);


	                           while (offset  > SECTOR_PAYLOAD ) {
						// carregar pr�ximo setor e atualizar offset
						
						sec0 = fp->secbuffer[SECTOR_NEXT] << 24; 	// byte mais significativo 
						sec1 = fp->secbuffer[SECTOR_NEXT+1] << 16;
						sec2 = fp->secbuffer[SECTOR_NEXT+2] << 8;
						sec3 = fp->secbuffer[SECTOR_NEXT+3];  	// byte menos significativo

						sector = sec0+sec1+sec2+sec3;
	
						read_block(sector, fp->secbuffer, 1);

						offset -= SECTOR_PAYLOAD;
					}
				   
		        fp->offset = offset;
				      
					      break;
			       }
		case SEEK_END: {
			           while (offset > SECTOR_PAYLOAD ) {
				   //carregar setor anterior e atualizar o offset
				           sec0 = fp->secbuffer[SECTOR_PREV] << 24;
				 	   sec1 = fp->secbuffer[SECTOR_PREV+1] << 16;
					   sec2 = fp->secbuffer[SECTOR_PREV+2] << 8;
					   sec3 = fp->secbuffer[SECTOR_PREV+3];

					   sector = sec0+sec1+sec2+sec3;

					   read_block(sector, fp->secbuffer, 1 );

					   offset -= SECTOR_PAYLOAD;
				   
	                        }
			           fp->offset = (SECTOR_PAYLOAD - offset);
		               	   
				       break;
			       }
		case SEEK_CUR: {
					if  ( SECTOR_PAYLOAD - fp->offset < offset) {
						// carregar pr�ximo setor e atualizar offset
						
						sec0 = fp->secbuffer[SECTOR_NEXT] << 24; 	// byte mais significativo 
						sec1 = fp->secbuffer[SECTOR_NEXT+1] << 16;
						sec2 = fp->secbuffer[SECTOR_NEXT+2] << 8;
						sec3 = fp->secbuffer[SECTOR_NEXT+3];  	// byte menos significativo

						sector = sec0+sec1+sec2+sec3;
	
						read_block(sector, fp->secbuffer, 1);

						offset -= ( SECTOR_PAYLOAD -fp->offset);
					}
					
					

				
	                           while (offset  > SECTOR_PAYLOAD ) {
						// carregar pr�ximo setor e atualizar offset


						sec0 = fp->secbuffer[SECTOR_NEXT] << 24; 	// byte mais significativo 
						sec1 = fp->secbuffer[SECTOR_NEXT+1] << 16;
       						sec2 = fp->secbuffer[SECTOR_NEXT+2] << 8;
						sec3 = fp->secbuffer[SECTOR_NEXT+3];  	// byte menos significativo
				  
	                       
						sector = sec0+sec1+sec2+sec3;
	
	                                        read_block(sector, fp->secbuffer, 1); 
				       
	                                        offset -= SECTOR_PAYLOAD;  
	                      
		   		         }
				   
				   fp->offset = offset;
				  
			   	   break;
			       }
	}

	// apenas para testar
	return 0;
}

long tfs_read(FILE *fd, byte *buffer, int size /* in bytes */) {

	read_block(fd->secnumber, buffer, 1);
	return 0;
}


// TODO
// - criar todas as fun��es
// - explicar como chamar as fun��es
// - shell para fazer essas chamadas
// - compilar e testar
