#include "defines.h"
#include "mem.h"
#include "stdio.h"
#include "fsdir.h"
//#include "unistd.h"

#include "fcntl.h"

#include "keyboard.h"

#include "sys/fs.h"

typedef struct _fd_hash_entry {
	unsigned int key;
	void * file_data;
	struct _fd_hash_entry * bucket_next;
} fd_hash_entry;

fd_hash_entry * fd_hash_start;

/* we are maxing the number of open file descriptor by 257 for now */
#define MAX_FD_HASH 257

void create_fd_hash() {
	fd_hash_start=(fd_hash_entry *)kalloc(MAX_FD_HASH*sizeof(fd_hash_entry));
	fd_hash_entry * tmp = fd_hash_start;
	fd_hash_entry * end = fd_hash_start+MAX_FD_HASH;
	while (tmp < end) {
		tmp->key = -1;
		tmp->bucket_next = 0;
		tmp++;
	}
}

void init_fs_syscalls() {
	create_fd_hash();
}

#define fd_hash_function(key) (key*key)%MAX_FD_HASH

int put_fd_hash(unsigned int key, void * data) {
	unsigned int hash = fd_hash_function(key);
	if (fd_hash_start[hash].key != -1) {
		fd_hash_entry * tmp = fd_hash_start+hash;
		while(tmp->bucket_next != 0) tmp = tmp->bucket_next;
		fd_hash_entry * dt = (fd_hash_entry*)kalloc(sizeof(fd_hash_entry));
		dt->key = key;
		dt->file_data = data;
		dt->bucket_next = 0;
		tmp->bucket_next = dt;
	}
	else {
		fd_hash_start[hash].key = key;
		fd_hash_start[hash].file_data = data;
	}
	return 0;		
}

void * retrieve_fd_hash(unsigned int key) {
	unsigned int hash = fd_hash_function(key);
	if (fd_hash_start[hash].key == key) {
		return fd_hash_start[hash].file_data;
	}
	fd_hash_entry * tmp = fd_hash_start+hash;
	while(tmp->bucket_next != 0) {
		tmp = tmp->bucket_next;
		if (tmp->key == key)
			return tmp->file_data;
	}
	return 0;
}

/* NOTE we dont return the entry here, so any data inside the entry
	should be freed before calling this function, or stored previously 
	if necessary */
void remove_fd_hash(unsigned int key) {
	unsigned int hash = fd_hash_function(key);
	if (fd_hash_start[hash].key == key) {
		if (fd_hash_start[hash].bucket_next) {
			fd_hash_entry * tmp = fd_hash_start[hash].bucket_next;
			kmemcpy((byte*)(fd_hash_start+hash),(byte*)(tmp),sizeof(fd_hash_entry));
			kfree(tmp);
		}
		else {
			fd_hash_start[hash].key = -1;
		}
	}
	else {
		fd_hash_entry * tmp = (fd_hash_start+hash);
		fd_hash_entry * tmp2 = 0;
		while(tmp->bucket_next != 0) {
			tmp2 = tmp;
			tmp = tmp->bucket_next;
			if (tmp->key == key) {
				tmp2->bucket_next = tmp->bucket_next;
				kfree(tmp);
			}			
		}
	}
}

int fd_count = 3;

int open_syscall_handler(const char * filename, unsigned int flags, int mode) {
	FILE * fp;
	char * f_flags;
 	//kprintf("open called, flags: %X, %s\n",flags,filename);
	if (flags & O_DIRECTORY) {
		fp = open_directory_fd((char *)filename);
		put_fd_hash(fd_count++,fp);
		//kprintf("%i\n",((FILE *)retrieve_fd_hash(fd_count-1))->size);
		return fd_count-1;
	}

	if (flags & O_EXCL) {
		
	}

	if ((flags & O_APPEND) && (flags & O_RDONLY))  {
		flags ^= O_APPEND; /* append and read only are nonsensical, ignore the append */
	}

	if (flags & O_APPEND) {
		if (flags & O_WRONLY) {
			f_flags = "a";
		}
		else if (flags & O_RDWR) {
			f_flags = "a+";
		}
		
	}
	else if (flags & O_RDWR) {
		/* TODO check if file exists for O_CREAT */
		if (flags & O_TRUNC) {
			f_flags = "w+";
		}
		else {
			f_flags = "r+";
		}
	}
	else if (flags & O_WRONLY) {
		/* TODO check if file exists for O_CREAT */
		f_flags = "w";
	}
	else if (flags & O_RDONLY) {
		f_flags = "r";
	}
	fp = fopen(filename,f_flags);
	put_fd_hash(fd_count++,fp);
	return fd_count-1;
}

int access_syscall_handler(char * name, int mode) {
	//kprintf("Access called on %s, mode is %i\n",name,mode);
	asm("sti");
	FILE * fp = fopen(name,"r");
	if (!fp)
		return 0;
	/* TODO actually check permissions */
	return 1;
}

#define stdin 0
#define stdout 1
#define stderr 2

size_t write_syscall_handler(unsigned int fd, const char * buf, size_t count) {
	/* for now just write out and err to screen (i.e. kprints) (fd 1 and 2) */
	if (fd == stdout || fd == stderr) {
		kprints((char *)buf);
		return count;
	}
	return 0;
}

size_t read_syscall_handler(unsigned int fd, char * buf, size_t count) {
	/* for now use gets() for stdin (fd 0) */
	if (fd == stdin) {
		/* since we are inside an interrupt, tell the processor it can receive interrupts for now */
		asm("sti");
		if (count > 1) {
			gets(buf,count);
			
		}
		else {			
			*buf = getch();
		}
		asm("cli");
		return 1;
	}


	return 0;
}

long chdir_syscall_handler(char * path) {
	//kprintf("Changing dir to %s\n",path);
	/* allow interrupts so disk can work */
	asm("sti");
	return !toPath(path);
}

long getdents_syscall_handler(unsigned int fd, struct _linux_dirent * buf, unsigned int size) {
	FILE * fp = (FILE *)retrieve_fd_hash(fd);
	//kprintf("getdents over %s, %i, %i, %i.\n",fp->name,fp->flag & 0x200,fp->size,fp->flag);
	if (!(fp->flag & 0x200)) {
		return -1;
	}
	/* allow interrupts so disk can work */
	asm("sti");
	return flush_directory_entries(buf, size, fp);
}
