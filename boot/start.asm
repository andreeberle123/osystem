;*******************************************************************************
;
; dorothy/boot/start.asm
;
; Copyright (C) 2005 D-OZ Team
;
;*******************************************************************************
[bits 32]
[global _start]
[extern kmain]

_start:
	call kmain
	hlt
