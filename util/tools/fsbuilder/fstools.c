#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
//void d_access(char*,char*);

int fd;

// NOTE max initial fat is currently at 10mb, which is not even
// available yet, since its a floppy with roughly 1.44mb
#define MAX_BUFFER 5*1024*1024
unsigned char fat_buffer[MAX_BUFFER];
unsigned char * buf_end = fat_buffer;

#define block2addr(block) (block*512)-(300*512)
#define block2size(block) (block*512)

int main(int argc, char * argv[]) {

	if (argc == 1)
		return 0;
	char * filename = argv[1];

	fd = open(filename,O_EXCL | O_CREAT | O_RDWR, 0x777);
	if (fd < 0) {
		// ok
		printf("Using existing fs - %s\n",filename);
		fd = open(filename,O_RDWR);
	}
	else {
		printf("Creating new fs - %s\n",filename);
	}
	if (fd < 0 || !fd) {
		printf("Error\n");
		return 0;
	}

	buf_end = fat_buffer;
	//lseek(fd,block2addr(300),SEEK_SET);
	while (1) {
		int rd = read(fd,buf_end,1024);
		if (rd < 0) {
			printf("Error - %s\n",strerror(errno));
			return 0;
		}
		if (!rd)
			break;
 		buf_end += rd;
	}
	//printf("%i\n",(buf_end-fat_buffer));
	if (argc > 2) {

		initializeFileSystem();
		char * cmd = argv[2];
		d_access(cmd,argc,argv);
	}

	lseek(fd,0,SEEK_SET);	
	write(fd,fat_buffer,MAX_BUFFER);
	close(fd);

	return 0;
}

/* reads a block */
unsigned char read_block(int block, unsigned char *buffer, unsigned long nosectors) {
	memcpy(buffer,fat_buffer+block2addr(block),block2size(nosectors));
}

/* writes a block */
unsigned char write_block(int block,  unsigned char *buffer, unsigned long nosectors) {
	memcpy(fat_buffer+block2addr(block),buffer,block2size(nosectors));
}

unsigned char local_buf[16384];

int ffd;

int prepare_file(char * src) {
	ffd = open(src,O_RDWR);
	return (!(ffd > 0));
}

unsigned char * read_file(int * fsize) {
	*fsize = read(ffd,local_buf,16384);
	//if(*fsize > 0)printf("d %i\n",local_buf[0]);
	return local_buf;
}

int close_file() {
	close(ffd);
}

/* system bridge */

void *kalloc(unsigned long size) {
	return malloc(size);
}

void kmemcpy(unsigned char *a, unsigned char *b, int c) {
	memcpy(a,b,c);
}

int kstrcmp (const char *a, const char *b) {
	return strcmp(a,b);
}

void kstrcpy (char *dest, const char *src) {
	strcpy(dest,src);
}

int kstrlen (const char *s) {
	return strlen(s);
}

char *kstrcat(char *first, const char *second) {
	return strcat(first,second);
}

void kfree(void *data) {
	free(data);
}

void kprintf (char *format, ...) {
	va_list argList;
	va_start(argList, format);

	vprintf(format, argList);

	va_end(argList);
}

char *kstrchtok (char *s, const char delim) {
	return strtok(s,&delim);
}

void kstrncpy (char *a, const char *b, int n) {
	strncpy(a,b,n);
}

void putch (char c) {
	printf("%c",c);
}

void outportb  (unsigned int port, unsigned char value) {
	
}

unsigned char inportb   (unsigned int port) {
	return 0;
}

