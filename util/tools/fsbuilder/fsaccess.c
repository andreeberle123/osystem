#include "ffs.h"
#include "fsdir.h"

// TODO put this in a .h
int prepare_file(char * src);
unsigned char * read_file(int * fsize);
int close_file();

char * path_set(char * arg) {
	char * tmp = arg;
	while (1) {
		while(*tmp !='/' && *tmp)tmp++;
		if (*tmp) {
			*tmp++ = 0;
			chdir(arg);
			arg = tmp;
		}
		else 
			break;
	}
	return arg;
}

int d_access(char * cmd, int argc, char * argv[]) {
	if (!kstrcmp(cmd,"ls")) {
		char * arg;
		if (argc <= 3)
			return 0;
		arg = argv[3];
		arg = path_set(arg);
		ls();
	}
	else if (!kstrcmp(cmd,"put")) { /*  */
		char * arg;
		if (argc <= 4)
			return 0;
		arg = argv[3];
		char * src = argv[4];
		arg = path_set(arg);
		kprintf("%s\n",arg);
		FILE* fl = fopen(arg, "w");
		prepare_file(src);		
		int rd;
		while(1) {
			unsigned char * buf = read_file(&rd);
			//printf("%s\n",buf[0]);
			if (rd > 0)
			    fwrite(buf,rd,1,fl);
			else
				break;
		}	
		close_file();
		fclose(fl);		
	}
	else if(!kstrcmp(cmd,"mkdir")) {
		if (argc <= 3)
			return 0;
		mkdir(argv[3]);
	}
	else if(!kstrcmp(cmd,"rm")) {
		if (argc <= 3)
			return 0;
	}
}