#include <stdio.h>

int main(void) {
	char fname[43];
	FILE *fp;
	int i;

	fp = fopen("file.TAA", "wb+");

	/*
	TFS FAT descriptor 
	typedef struct tfsfat {
		char name[43];                        	// Descriptor Name 
		byte attrib;				// Link, Directory, File
		byte ptr[4];				// Pointer for next sector 
	} tfsfat;
	*/

	// adding a file to filesystem
	strcpy(fname, "bozo");
	fprintf(fp, "%s", fname);
	fprintf(fp, "%c", 0);

	for (i = 0; i < 42-strlen(fname); i++)
		fprintf(fp, " ");

	//#define T_DIR 	 1
	//#define T_ARQ 	 2
	//#define T_LNK 	 3
	//#define T_EMPTY  4
	fprintf(fp, "%c", 2);


	// using sector 700
	//
	// soh que o kernel faz 0x2BC + 493 + 1 = 1194 (posicao onde deve ser gravado o arquivo - veja no Makefile)
	// 0x2BC
	// 0x000002BC
	fprintf(fp, "%c", 0x00);
	fprintf(fp, "%c", 0x00);
	fprintf(fp, "%c", 0x02);
	fprintf(fp, "%c", 0xBC);

	fclose(fp);

	// 512 bytes = 64 * 8

	fp = fopen("file.BM", "wb+");
	// 10000000 -> 128 decimal -> 0x80
	fprintf(fp, "%c", 0x80);
	for (i = 0; i < 511; i++)
		fprintf(fp, "%c", 0x00);
	fclose(fp);

	return 0;
}
