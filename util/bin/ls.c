#include "fcntl.h"

#include "usr/unistd.h"
#include "usr/io.h"
#include "usr/malloc.h"
#include "usr/string.h"

#include "stdio.h"

int main(int argc, char * argv[]) {
	linux_dirent dir_buf[64];
	int pread = 0;
	int fd = open(".",O_DIRECTORY);
	do {
		pread = getdents(fd, dir_buf, 64);
		int i;
		for(i=0;i<pread;i++) {
			printf("%s\t%i\n",dir_buf[i].d_name,0);
		}
	} while(pread);
}