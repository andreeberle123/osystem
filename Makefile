INCLUDES= src/include/

AS = nasm
CC = gcc -Wall -fno-builtin -I$(INCLUDES)

ASM_FILES = src/boot/start.o src/kernel/irqwrapper.o
KERNEL_FILES = src/kernel/kernel.o src/kernel/io.o src/kernel/conio.o src/kernel/stdio.o src/kernel/idt.o src/kernel/panic.o src/kernel/timer.o src/kernel/string.o src/kernel/sched.o src/kernel/process.o src/kernel/loader.o src/kernel/syscall.o src/kernel/stdlib.o src/kernel/top.o src/kernel/ide.o src/kernel/iosched.o src/kernel/k_std.o src/fs/fs_syscall.o
EXT2FS_FILES = src/ext2fs/fsbitmap.o src/ext2fs/fsdir.o src/ext2fs/fsfile.o src/ext2fs/fsutil.o src/ext2fs/group.o
QUEUE_FILES = src/queue/event.o src/queue/fifo.o src/queue/sema.o src/queue/timer.o
DRIVER_FILES = src/drivers/8259a.o src/drivers/keyboard.o src/drivers/ne2k.o src/drivers/floppy.o src/drivers/dma.o src/devices/i386/pci_bios.o src/drivers/pci.o src/drivers/net/pcnet32.o src/drivers/nic.o src/devices/i386/pci_irq.o src/drivers/piix3.o
MM_FILES = src/mm/kernel.o src/mm/mem.o src/mm/pages.o src/mm/user.o
NET_FILES = src/net/dlink/eth.o src/net/addrs/arp.o src/net/nets/ip.o src/net/utils/idlist.o src/net/transp/udp.o src/net/utils/tcp_idlist.o src/net/transp/tcp.o
#FS_FILES = src/fs/tfs/tfs.o
#FS_FILES = src/fs/tfs/tfs.o src/fs/ddfs/ddfs.o src/fs/ddfs/stub.o
UTIL_FILES = src/util/heap.o src/util/avl.o
OTHER_FILES = src/kish/kish.o

all: output/kernel.img

output/kernel.img: src/boot/boot.img output/kernel.bin
	cat src/boot/boot.img output/kernel.bin > output/kernel.img

output/kernel.bin: $(ASM_FILES) $(KERNEL_FILES) $(QUEUE_FILES) $(DRIVER_FILES) $(MM_FILES) $(NET_FILES) $(OTHER_FILES) $(UTIL_FILES) $(EXT2FS_FILES)
#	ld -o output/kernel.bin $(ASM_FILES) $(KERNEL_FILES) $(QUEUE_FILES) $(DRIVER_FILES) $(MM_FILES) $(NET_FILES) $(OTHER_FILES) -T link.ld -M
	ld --oformat binary -o output/kernel.bin $(ASM_FILES) $(KERNEL_FILES) $(QUEUE_FILES) $(DRIVER_FILES) $(MM_FILES) $(NET_FILES) $(OTHER_FILES)  $(UTIL_FILES) $(EXT2FS_FILES) -Ttext 0x100000



src/boot/boot.img: src/boot/boot.asm
	$(AS) -f bin -o src/boot/boot.img src/boot/boot.asm

src/boot/start.o: src/boot/start.asm
	$(AS) -f aout -o src/boot/start.o src/boot/start.asm

src/kernel/irqwrapper.o: src/kernel/irqwrapper.asm
	$(AS) -f aout -o src/kernel/irqwrapper.o src/kernel/irqwrapper.asm

clean:
	find -name "*.o" | xargs rm -f
	find -name "*~" | xargs rm -f
	find -name "*.img" | xargs rm -f
	find -name "*.bin" | xargs rm -f

disk:
	
	cat output/kernel.img fat.op > output/floppy.img
	#dd if=fat.dat of=output/floppy.img seek=301 bs=512
	dd if=$(FAT) of=output/floppy.img seek=300 bs=512
	#cp output/kernel.img floppy.img
	#cp output/kernel.img floppy.img
	rm -rf /root/shared/floppy.img

vdisk:
	dd if=output/kernel.img of=bench/1/disk.flp
	dd if=bench/1/tree.flp of=bench/1/disk.flp seek=301
	dd if=output/kernel.img of=bench/2/disk.flp
	dd if=bench/2/tree.flp of=bench/2/disk.flp seek=301
	dd if=output/kernel.img of=bench/3/disk.flp
	dd if=bench/3/tree.flp of=bench/3/disk.flp seek=301
	dd if=output/kernel.img of=bench/4/disk.flp
	dd if=bench/4/tree.flp of=bench/4/disk.flp seek=301

