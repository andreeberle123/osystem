void _start(void) {

	/* testing OZ syscalls */
	asm ("movl $0x00, %ebx");
	asm ("movl $0x0A, %ecx");

	asm ("int $0x80");

	asm ("movl $0x00, %ebx");
	asm ("movl $0x41, %ecx");

	asm ("int $0x80");

	asm ("movl $0x00, %ebx");
	asm ("movl $0x42, %ecx");

	asm ("int $0x80");

	asm ("movl $0x00, %ebx");
	asm ("movl $0x43, %ecx");

	asm ("int $0x80");

	asm ("movl $0x00, %ebx");
	asm ("movl $0x44, %ecx");

	asm ("int $0x80");

	asm ("movl $0x00, %ebx");
	asm ("movl $0x45, %ecx");

	asm ("int $0x80");

	asm ("movl $0x00, %ebx");
	asm ("movl $0x46, %ecx");

	asm ("int $0x80");

	asm ("movl $0x00, %ebx");
	asm ("movl $0x47, %ecx");

	asm ("int $0x80");

	asm ("movl $0x00, %ebx");
	asm ("movl $0x0A, %ecx");

	asm ("int $0x80");

	asm ("movl $0x00, %ebx");
	asm ("movl $0x0A, %ecx");

	asm ("int $0x80");
}
