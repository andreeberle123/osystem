#include "mem.h"

void uprintf(char *str, int size) {
	int i;
	
	for (i = 0; i < size; i++) {
		/* testing OZ syscalls */
		asm ("movl $0x00, %ebx");
		asm ("movl $0x00, %ecx");
		asm ("movb %0, %%cl" : : "r" (str[i])); //((unsigned long) str[i]));

		asm ("int $0x80");
	}

}

void _start(void) {
	char str[] = "Testando o OZ...\n";
	unsigned char *stack = (unsigned char *) kalloc(4096);

	asm ("movl %0, %%ebp" : : "m" (stack) );
	asm ("movl %0, %%esp" : : "m" (stack) );

	uprintf(str, 17);
}
