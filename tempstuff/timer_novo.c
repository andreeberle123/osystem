/*******************************************************************************

  dorothy/kernel/timer.c
 
  Copyright (C) 2005 D-OZ Team

  Timer and delay functions 
 
*******************************************************************************/

#include "timer.h"
#include "defines.h"
#include "io.h"
#include "8259A.h"
#include "idt.h"
#include "irqwrapper.h"

struct trigger; /*{
	void (*handler)(void *, void *, void *, void *);
	dword timeout;
	void *arg1;
	void *arg2;
	void *arg3;
	void *arg4;
};*/

struct trigger trigger[MAXTRIGGERS];
byte ntriggers=0;

void addTrigger (void (*handler)(void *, void *, void *, void *), dword timeout, void *arg1, void *arg2, void *arg3, void *arg4) {
	int i;
	
	if (ntriggers >= MAXTRIGGERS) return;

	for (i = 0; trigger[i].handler != NULL; i++) 
		;

	trigger[i].handler = handler;
	trigger[i].timeout = timeout;
	trigger[i].arg1 = arg1;
	trigger[i].arg2 = arg2;
	trigger[i].arg3 = arg3;
	trigger[i].arg4 = arg4;

	ntriggers++;
}

unsigned long ticks = 1;
unsigned long delay_count = 1;

void _delay (unsigned long loops) {
	unsigned long i;
	for (i = 0; i < loops; i++)
		;
}

void int_timer (void) {
	int i;
	void (*handler)(void *, void *, void *, void *) = NULL;
	void *arg1 = NULL;
	void *arg2 = NULL;
	void *arg3 = NULL;
	void *arg4 = NULL;
	
	ticks++;
	/* handles the timeouts */
	for (i = 0; i < MAXTRIGGERS; i++) 
		if (trigger[i].handler != NULL) {
			trigger[i].timeout--;
			if (trigger[i].timeout == 0) {
				handler = trigger[i].handler;
				arg1 = trigger[i].arg1;
				arg2 = trigger[i].arg2;
				arg3 = trigger[i].arg3;
				arg4 = trigger[i].arg4;
				trigger[i].handler = NULL;
				ntriggers--;
				handler(arg1, arg2, arg3, arg4);
			}
		}
	outportb (MASTER, EOI);
}

void loadTimer (void) {
	unsigned long temp = 1193180 / 100;
	int i;
	
	for (i = 0; i < MAXTRIGGERS; i++)
		trigger[i].handler = NULL;
	
	init_pit (100, 0);
	outportb (0x43, 0x36);
	outportb (0x40, (byte) temp);
	outportb (0x40, (byte) (temp >> 8));
	addInt (0x20, inttimer, 0);
	unmaskIRQ (0);
}

unsigned long calibrate_delay (void) {
	unsigned int prevtick;
	unsigned int i;
	unsigned int calib_bit;
//?	unsigned int temp = 1193180 / 18.2;

	/* Stage 1: first calibration */
	do {
		delay_count <<= 1;

		prevtick = ticks;
		while (prevtick==ticks)
			;

		prevtick = ticks;
		_delay (delay_count);
	} while (prevtick == ticks);
	
	delay_count >>= 1;

	/* Stage 2: fine calibration */
	calib_bit = delay_count;

	for (i = 0; i < PRECISION; i++) {
		calib_bit >>= 1;
		if (calib_bit == 0)
			break;

		delay_count |= calib_bit;

		prevtick = ticks;
		while (prevtick == ticks)
			;

		prevtick = ticks;
		_delay (delay_count);

		if (prevtick != ticks)
			delay_count &= ~calib_bit;
	}

	/* Stage 3: final adjustment */
	delay_count /= MILISEC;

	return delay_count;
}		

void delay (unsigned long mili) {
	_delay (mili * delay_count);
}

