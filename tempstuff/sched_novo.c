/*******************************************************************************

  dorothy/kernel/sched.c
 
  Copyright (C) 2005 D-OZ Team

  Scheduler
 
*******************************************************************************/

#include "defines.h"
#include "timer.h"
#include "stdio.h"
#include "io.h"
#include "8259A.h"
#include "process.h"

static volatile struct task_struct *current = NULL;

void cswitch(void) {
	struct task_struct *next;
	
	asm ("cli");

	if (current != NULL) {
		asm ("movl (%%esp), %0" : "=r" (current->gs));
		asm ("movl 4(%%esp), %0" : "=r" (current->fs));
		asm ("movl 8(%%esp), %0" : "=r" (current->es));
		asm ("movl 12(%%esp), %0" : "=r" (current->ds));
		
		/* AX, CX, DX, BX, original SP, BP, SI, and DI -> 32 bytes 
		 * in the following order:
		 *
		 * push eax
		 * push ecx
		 * push edx
		 * push ebx
		 * push esp
		 * push ebp
		 * push esi
		 * push edi
		 * */
		asm ("movl 16(%%esp), %0" : "=r" (current->edi));
		asm ("movl 20(%%esp), %0" : "=r" (current->esi));
		asm ("movl 24(%%esp), %0" : "=r" (current->ebp));
		asm ("movl 28(%%esp), %0" : "=r" (current->esp));
		asm ("movl 32(%%esp), %0" : "=r" (current->ebx));
		asm ("movl 36(%%esp), %0" : "=r" (current->edx));
		asm ("movl 40(%%esp), %0" : "=r" (current->ecx));
		asm ("movl 44(%%esp), %0" : "=r" (current->eax));
		
		asm ("movw 46(%%esp), %0" : "=r" (current->flags));
		asm ("movl 50(%%esp), %0" : "=r" (current->eip));
		asm ("movl 54(%%esp), %0" : "=r" (current->cs));

		kprintf("\tRegisters:\neax: %d\tebx: %d\tecx: %d\tedx: %d\n\tesi: %d\tedi: %d\tebp: %d\tesp: %d\n\tflags: %d\teip: %d\tcs: %d\n", current->eax, current->ebx, current->ecx, current->edx, current->esi, current->edi, current->ebp, current->esp, current->flags, current->eip, current->cs);
	} /*else {
		kprintf("Current is NULL\n");
	}*/

	/* Switching -> changing from a current to other one 
	 *
	 * put current into the process queue (ready to run)
	 * get the next ready to run process
	 * */
	
	next = get_next_ready_to_run();
	remove_process(next);

	if (current != NULL) {
		add_process((struct task_struct*) current);
	}

	current = next;

	if (next != NULL) {
		
		/* changing the stack */
		asm ("movl %0, (%%esp)"   : : "r" (current->gs));
		asm ("movl %0, 4(%%esp)"  : : "r" (current->fs));
		asm ("movl %0, 8(%%esp)"  : : "r" (current->es));
		asm ("movl %0, 12(%%esp)" : : "r" (current->ds));
					     
		asm ("movl %0, 16(%%esp)" : : "r" (current->edi));
		asm ("movl %0, 20(%%esp)" : : "r" (current->esi));
		asm ("movl %0, 24(%%esp)" : : "r" (current->ebp));
		asm ("movl %0, 28(%%esp)" : : "r" (current->esp));
		asm ("movl %0, 32(%%esp)" : : "r" (current->ebx));
		asm ("movl %0, 36(%%esp)" : : "r" (current->edx));
		asm ("movl %0, 40(%%esp)" : : "r" (current->ecx));
		asm ("movl %0, 44(%%esp)" : : "r" (current->eax));
					     
		asm ("movw %0, 46(%%esp)" : : "r" (current->flags));
		asm ("movl %0, 50(%%esp)" : : "r" (current->eip));
		asm ("movl %0, 54(%%esp)" : : "r" (current->cs));
	} /*else {
		kprintf("Next is NULL\n");
	}*/

	asm ("sti");
}

void sheduler (void) {
	//kprintf("scheduler... %d\n", tick++);
	//tick++;

	/* checking up the event queue */

	/* checking up the process queue */
	cswitch();

	addTrigger (sheduler, 10, 0, 0, 0, 0);
}

void init_scheduler (void) {
	addTrigger (sheduler, 10, 0, 0, 0, 0);
}
